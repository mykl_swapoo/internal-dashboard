######################################
# Created by: King Matthew Ochoa
# Quantitative Analyst
# Swapoolabs
# Date: April 2022
#######################################
from __future__ import annotations

import boto3
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import datetime as dt
import time
import pandas as pd


current = dt.datetime.now()
current_td = dt.timedelta(
    hours=current.hour,
    minutes=current.minute,
    seconds=current.second,
    microseconds=current.microsecond,
)
today = current - current_td
today.strftime("%Y-%m-%d")
yesterday = today - dt.timedelta(days=1)

def run_report(monday):
    ses_client = boto3.client(
    "ses",
    region_name="ap-southeast-1",
    aws_access_key_id="AKIA3E3HTSYVCW6TSKBS",
    aws_secret_access_key="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",
    )
    msg = MIMEMultipart()
    #msg['To'] = 'matthew.ochoa@swapoolabs.com'
    msg['From'] = 'trading.monitor@swapoolabs.com'
    msg['To'] = 'matthew.ochoa@swapoolabs.com, anshul.dang@swapoolabs.com, mykhal.mangada@swapoolabs.com, lorenzo.ajoc@swapoolabs.com, glen.macadaeg@swapoolabs.com, emjc.santos@swapoolabs.com'
    msg['Subject'] = f'[LAETITUDE BOTS] Current Performance as of {yesterday.strftime("%Y-%m-%d")}'
    text = ''
    if not monday:
        text = '''
            <h1> TEST BOTS (BTC-Denominated) </h1>
            <p>Highlighted are the current balances of the BTC-denominated bots as of yesterday’s candle close:</p>
            <img src="cid:total_equity-table">
            <!------------- Notes ------------------------------->
            <p> Notes: Internal transfers done from OCC-FTX on May 26, 2022 </p>
            <p> Notes: Minor adjustment done on OCC-FTX: added withdrawl of 0.00068786 in the records of Feb 3, 2022 </p>
            <!------------- Notes ------------------------------->
            <br/>
            <p> Here’s the equity curve: </p>
            <img src="cid:total_equity">
            <!------------- Notes ------------------------------->
            <p> Notes: Discontinued bots with remaining funds are still included </p>
            <!------------- Notes ------------------------------->
            <br/>
            <hr/>
            <h1> TEST BOTS (USD-Denominated) </h1>
            <p>Highlighted are the current balances of the USD-denominated bots as of yesterday’s candle close:</p>
            <img src="cid:total_usd_equity-table">
            <br/>
            <p> Here’s the equity curve: </p>
            <img src="cid:total_usd_equity">
            <br/>
            <hr/>
            <h1> LAETITUDE BOTS </h1>
            <img src="cid:Laetitudebots-table"
            <p> *Live Bots PNL has been adjusted to reflect correct execution & the execution errors pnl has been attributed to Error PNL
            <br/>
            <p> Here’s the equity curve: </p>
            <img src="cid:Laetitudebots">
            <p> Notes: Discontinued bots removed. </p> <br/>
            <p> * Rebalancing of capital allocation on 02.03.2022 </p>
            <hr/>
            <p> Also, here's the performance of the bots on Swapoo.ai since launch: </p>
            <img src="cid:Swapoobots">
            <p> * Notes:  </p>
            <p> - FN3v2, MM3v2, TT1v2 and TT4v2 launched last January 1, 2022. Equity curves reset to 1.</p>
            <p> - removed other bots not yet on Swapoo.ai </p>
            <p> Current positions of Laetitude Bots can be viewed here: <a href="https://makatisoft-my.sharepoint.com/:p:/g/personal/glen_macadaeg_swapoolabs_com/EU-UbnmkbBBEnPn3PS-Q6eYBrsHpjN93OEA-a9AamwSg_g?e=wBwpds"> Current Positions.pptx </a>
            <p> The<a href="https://docs.google.com/spreadsheets/d/1wnjUrjZmOos5mtHq0ckEvQo4qASjHtBpyQe3HSoBcyM/edit?usp=sharing"><b> Google Sheet </b></a>for the actual bot balances. </p>
        '''
    else:
        text ='''
            <!------------- BTC bots ---------------------------->
            <h1> TEST BOTS (BTC-Denominated) </h1>
            <p>Highlighted are the current balances of the BTC-denominated bots as of yesterday’s candle close:</p>
            <img src="cid:total_equity-table">
            <!------------- Notes ------------------------------->
            <p> Notes: Internal transfers done from OCC-FTX on May 26, 2022 </p>
            <p> Notes: Minor adjustment done on OCC-FTX: added withdrawl of 0.00068786 in the records of Feb 3, 2022 </p>
            <!------------- Notes ------------------------------->
            <br/>
            <p> Here’s the equity curve: </p>
            <img src="cid:total_equity">
            <!------------- Notes ------------------------------->
            <p> Notes: Discontinued bots with remaining funds are still included </p>
            <!------------- Notes ------------------------------->
            <p> Here's the week on week performance of the BTC-denominated test bots:</p>
            <img src="cid:test_bots_weekly_performance">
            <br/>
            <hr/>
            <!------------ USD Bots ----------------------------->
            <h1> TEST BOTS (USD-Denominated) </h1>
            <p>Highlighted are the current balances of the USD-denominated bots as of yesterday’s candle close:</p>
            <img src="cid:total_usd_equity-table">
            <p>*additional capital on STv3 from OCC-FTX last 06.01.2022</p>
            <p> Here’s the equity curve: </p>
            <img src="cid:total_usd_equity">
            <br/>
            <hr/>
            <p> Here’s the week on week performance of the USD-denominated test bots: </p>
            <img src="cid:test_usd_bots_weekly_performance">
            <!----------- Laetitude Bots ------------------------>
            <h1> LAETITUDE BOTS </h1>
            <img src="cid:Laetitudebots-table"
            <p> *Live Bots PNL has been adjusted to reflect correct execution & the execution errors pnl has been attributed to Error PNL
            <br/>
            <p> Here’s the equity curve: </p>
            <img src="cid:Laetitudebots">
            <p> Notes: Discontinued bots removed. </p> <br/>
            <p> * Rebalancing of capital allocation on 02.03.2022 </p>
            <p> And the weekly performance: </p>
            <img src="cid:laetitude_bots_weekly_performance">
            <hr/>
            <p> Also, here's the performance of the bots on Swapoo.ai since launch: </p>
            <img src="cid:Swapoobots">
            <p> * Notes:  </p>
            <p> - FN3v2, MM3v2, TT1v2 and TT4v2 launched last January 1, 2022. Equity curves reset to 1.</p>
            <p> - removed other bots not yet on Swapoo.ai </p>
            <p> Current positions of Laetitude Bots can be viewed here: <a href="https://makatisoft-my.sharepoint.com/:p:/g/personal/glen_macadaeg_swapoolabs_com/EU-UbnmkbBBEnPn3PS-Q6eYBrsHpjN93OEA-a9AamwSg_g?e=wBwpds"> Current Positions.pptx </a>
            <p> The<a href="https://docs.google.com/spreadsheets/d/1wnjUrjZmOos5mtHq0ckEvQo4qASjHtBpyQe3HSoBcyM/edit?usp=sharing"> <b> Google Sheet </b> </a> for the actual bot balances. </p>
            <h2>SUMMARY </h2>
            <p> Weekly Performance of the Test Bots </p>
            <p>  ₿ - Denominated </p>
            <img src="cid:test_bots_weekly_performance-table">
            <p>  USD - Denominated </p>
            <img src="cid:test_usd_bots_weekly_performance-table">
            <p> Weekly Performance of the Laetitude Bots </p>
            <img src="cid:laetitude_bots_weekly_performance-table">
            <p> Total ₿ - Denominated </p>
            <img src="cid:total-table">
            <!------- Note --------------->
            <p> * Note: Transfer from OCC-FTX to TS2v3 and STv3 = 0.0176</p>
            <p> **Note: Transfer from Chaos-FTX(dev) and TTR-FTX(dev) to TTxv3 = 0.00992781 </p>
            <p> ***Note: Returned to Chaos-FTX(dev) from TT7v3 = 0.0074, from TS2v3-USD-FTX to MJ FTX parked account = 0.012105 </p>   
            </br>
            <p> Total USD - Denominated </p>
            <img src="cid:total-usd-table">
        '''
        ###############################################################
        # BTC Test Bots BAR
        logo = f'email_data/TestbotsPerformance-{yesterday.strftime("%Y-%m-%d")}.png'
        fp = open(logo, 'rb')
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', 'test_bots_weekly_performance')
        msg.attach(img)

        ###############################################################
        # BTC Laetitude BAR
        logo = f'email_data/LaetitudebotsPerformance-{yesterday.strftime("%Y-%m-%d")}.png'
        fp = open(logo, 'rb')
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', 'laetitude_bots_weekly_performance')
        msg.attach(img)

        # USD Laetitude BAR
        logo = f'email_data/TestUsdbotsPerformance-{yesterday.strftime("%Y-%m-%d")}.png'
        fp = open(logo, 'rb')
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', 'test_usd_bots_weekly_performance')
        msg.attach(img)
        
        ###############################################################
        # BTC Test Bots table
        logo = f'email_data/weekly_bots-table-{yesterday.strftime("%Y-%m-%d")}.png'
        fp = open(logo, 'rb')
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', 'test_bots_weekly_performance-table')
        msg.attach(img)

        # USD Test Bots table
        logo = f'email_data/weekly_usd_bots-table-{yesterday.strftime("%Y-%m-%d")}.png'
        fp = open(logo, 'rb')
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', 'test_usd_bots_weekly_performance-table')
        msg.attach(img)


        ###############################################################
        # BTC live Bots table
        logo = f'email_data/weekly_live_bots-table-{yesterday.strftime("%Y-%m-%d")}.png'
        fp = open(logo, 'rb')
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', 'laetitude_bots_weekly_performance-table')
        msg.attach(img)

        ###############################################################
        # overall BTC performance 
        logo = f'email_data/total-table-{yesterday.strftime("%Y-%m-%d")}.png'
        fp = open(logo, 'rb')
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', 'total-table')
        msg.attach(img)

        #overall USD performance
        logo = f'email_data/total-usd-table-{yesterday.strftime("%Y-%m-%d")}.png'
        fp = open(logo, 'rb')
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', 'total-usd-table')
        msg.attach(img)
        

    
    part = MIMEText(text, 'html')
    msg.attach(part)

    #####################################################################
    # Load Images
    #####################################################################

    # Test bots btc 
    logo = f'email_data/total_equity-{yesterday.strftime("%Y-%m-%d")}.png'
    fp = open(logo, 'rb')
    img = MIMEImage(fp.read())
    fp.close()
    img.add_header('Content-ID', 'total_equity')
    msg.attach(img)

    logo = f'email_data/total_equity-table-{yesterday.strftime("%Y-%m-%d")}.png'
    fp = open(logo, 'rb')
    img = MIMEImage(fp.read())
    fp.close()
    img.add_header('Content-ID', 'total_equity-table')
    msg.attach(img)

    # Test bots usd
    logo = f'email_data/total_usd_equity-{yesterday.strftime("%Y-%m-%d")}.png'
    fp = open(logo, 'rb')
    img = MIMEImage(fp.read())
    fp.close()
    img.add_header('Content-ID', 'total_usd_equity')
    msg.attach(img)

    logo = f'email_data/total_usd_equity-table-{yesterday.strftime("%Y-%m-%d")}.png'
    fp = open(logo, 'rb')
    img = MIMEImage(fp.read())
    fp.close()
    img.add_header('Content-ID', 'total_usd_equity-table')
    msg.attach(img)


    # live bots 
    logo = f'email_data/Laetitudebots-{yesterday.strftime("%Y-%m-%d")}.png'
    fp = open(logo, 'rb')
    img = MIMEImage(fp.read())
    fp.close()
    img.add_header('Content-ID', 'Laetitudebots')
    msg.attach(img)

    logo = f'email_data/Laetitudebots-table-{yesterday.strftime("%Y-%m-%d")}.png'
    fp = open(logo, 'rb')
    img = MIMEImage(fp.read())
    fp.close()
    img.add_header('Content-ID', 'Laetitudebots-table')
    msg.attach(img)

    # swapoobots
    logo = f'email_data/Swapoobots-{yesterday.strftime("%Y-%m-%d")}.png'
    fp = open(logo, 'rb')
    img = MIMEImage(fp.read())
    fp.close()
    img.add_header('Content-ID', 'Swapoobots')
    msg.attach(img)

    result = ses_client.send_raw_email(Source = msg['From'], 
        Destinations = msg['To'].split(', '),
        RawMessage = {'Data': msg.as_string(),})
    print(result)


if dt.date.today().weekday() == 0:
    run_report(True)
else:
    run_report(False)