mkdir -p /home/adang/dashboard/logs/

export TIMESTAMP=$(date +%F-%H-%M-%S)
export LOG_FILENAME="/home/adang/dashboard/logs/dashboard_$TIMESTAMP.log"

echo "Starting daily dashboard Bash script..." > $LOG_FILENAME

echo "Starting data collection..." >> $LOG_FILENAME
cd /home/adang/dashboard/data
echo "Running \"python3.7 -m botmonitoring\"..." >> $LOG_FILENAME
python3.7 -m botmonitoring
sleep 2m
echo "Running \"python3.7 -m session-change\"..." >> $LOG_FILENAME
python3.7 -m session-change
sleep 2m
echo "Running \"python3.7 -m bmv3\"..." >> $LOG_FILENAME
python3.7 -m bmv3
sleep 2m
echo "Running \"python3.7 -m botcorrection\"..." >> $LOG_FILENAME
python3.7 -m botcorrection
sleep 2m
echo "Running \"python3.7 -m ohlcv-gathering.py\"..." >> $LOG_FILENAME
python3.7 -m ohlcv-gathering
sleep 2m
echo "Starting data collection... done!" >> $LOG_FILENAME

echo "Killing previous Flask process..." >> $LOG_FILENAME
kill $(pgrep -f flask)
echo "Kill command status: $?" >> $LOG_FILENAME
echo "Killing previous Flask process... done!" >> $LOG_FILENAME

echo "Starting dashboard process..." >> $LOG_FILENAME
cd /home/adang/dashboard
python3.7 -m flask run --host=0.0.0.0