from os import remove
import flask
import datetime as dt
import plotly.express as px
import plotly.graph_objects as go
import dash_bootstrap_components as dbc
import matplotlib.pyplot as plt
from dash.dash_table import DataTable
import ccxt
import pandas as pd
import numpy as np
import math
import os
import dash
import time
import math
from dash import Dash, Input, Output, State, dcc, html, callback
from pyparsing import delimited_list
from dash_extensions.enrich import Dash, Trigger, Output, Input, ServersideOutput, FileSystemStore

fss = FileSystemStore(cache_dir="some_dir", default_timeout=10) 
fss1 = FileSystemStore(cache_dir="some_dir", default_timeout=10)  # timeout in seconds, i.e. yours should be 3600
#df = pd.read_csv("data/session-change.csv")
#df.columns = ['datetime'] + [col for col in df.columns[1:]]
df = pd.DataFrame()
dash.register_page(__name__)
app = dash.Dash(external_stylesheets = [dbc.themes.BOOTSTRAP])
current = dt.datetime.now()
current_td = dt.timedelta(
    hours=current.hour,
    minutes=current.minute,
    seconds=current.second,
    microseconds=current.microsecond,
)
today = current - current_td
today.strftime("%Y-%m-%d")
yesterday = today - dt.timedelta(days=1)

app.layout = dbc.Container([
    html.H1(id          = 'H1',
                children    = 'Market Session Dashboard',
                style       = {'textAlign'      :'center',
                                'marginTop'     : 40,
                                'marginBottom'  : 40}),
    html.H4("8h-Session Change (%)"),
    html.P('Last Updated on:',id="time"), 
    html.Hr(),
    DataTable(
    id              = "nice",
    data            = df.round(decimals =3).to_dict('records'),
    style_table= {
        'height': '300px',
        },
    style_data      = {
        'whiteSpace': 'normal',
        'height': 'auto',
        },
    style_header    = {
        'backgroundColor'   : 'pink',
        'fontWeight'        : 'bold',
        'color'             : 'black',
        },
    style_cell      = {
        'textAlign'         : 'center',
        'textAlign': 'center',
        'minWidth': 5,
        'maxWidth': 55,
        'width': 20
        },
    ),  # logging of data, a mock replacement of your graph
    dcc.Store(id="store"),  # store that holds the data reference
    dcc.Interval(id="trigger", interval = 10000),  # trigger to invoke data refresh attempt, defaults to once per second

    #############

])
layout = app.layout

@callback(ServersideOutput("store", "data", session_check=False, backend=fss),
              Trigger("trigger", "n_intervals"), memoize=True)
def update_data(self):
    zdf = pd.read_csv('data/session-change.csv')
    zdf.columns = ['datetime'] + [col for col in zdf.columns[1:]]
    data = zdf.round(decimals =3).to_dict('records')
    path = "data/session-change.csv"
    ti_m = os.path.getmtime(path)
    m_ti = time.ctime(ti_m)
    # Using the timestamp string to create a 
    # time object/structure
    t_obj = time.strptime(m_ti)
    # Transforming the time object to a timestamp 
    # of ISO 8601 format
    T_stamp = time.strftime("%Y-%m-%d %H:%M:%S", t_obj)

    print(f"The file located at the path {path} was last modified at {T_stamp}")
    data.append({'timestamp':pd.to_datetime(T_stamp)+pd.Timedelta(hours = 8)})
    return data  # put your update logic here


@callback([Output('time', "children"), Output('nice', "data")] , Input("store", "data"))
def show_data(data):
    dataframe = data.copy() 
    dataframe.pop()
    return f"Last Updated on : {data[-1]['timestamp']} PHT", dataframe

# @callback(Output('time', "children"), Input("store", "data"))
# def show_too_data(data):
#     return f"Last Updated on : {data[-1]['timestamp']} PHT"