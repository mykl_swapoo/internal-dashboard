from errno import ENOMSG
import flask
import datetime as dt
import plotly.express as px
import plotly.graph_objects as go
import dash_bootstrap_components as dbc
import matplotlib.pyplot as plt
from dash.dash_table import DataTable
from ccxt import binance, bitmex, ftx
import ccxt
import pandas as pd
import numpy as np
import dash
import platform


from dash import Dash, Input, Output, State, dcc, html


import plotly.io as pio
import kaleido
#pio.kaleido.scope.mathjax = None
scope = pio.kaleido.scope
print(scope._std_error.getvalue().decode())

dash.register_page(__name__)
# server      = flask.Flask(__name__) # define flask app.server
# app         = Dash(__name__, 
#                    server = server,
#                    url_base_pathname='/equity/',
#                    external_stylesheets = [dbc.themes.BOOTSTRAP])
#show = pd.to_datetime(df.index[-1]).dayofweek + 1
app = dash.Dash(external_stylesheets = [dbc.themes.BOOTSTRAP])

current = dt.datetime.now()
current_td = dt.timedelta(
    hours=current.hour,
    minutes=current.minute,
    seconds=current.second,
    microseconds=current.microsecond,
)
today = current - current_td
today.strftime("%Y-%m-%d")
yesterday = today - dt.timedelta(days=1)

# data processing
df = pd.read_csv('data/botmonitoring'+yesterday.strftime("%Y-%m-%d")+'.csv')
df = df.drop('Unnamed: 0', 1)
df.set_index("date", inplace=True)
# df.loc['2021-04-29':]['STM-BMEx'] = np.nan
# df.loc['2021-03-03':]['OCC-BMEx'] = np.nan
df.loc['2021-03-03':]['STM-BMEx'] = np.nan
df.loc['2021-03-03':]['OCC-BMEx'] = np.nan
df.loc['2021-05-27':]['TTR-BMEx'] = np.nan
df.loc['2021-06-11':]['ICH-BMEx'] = np.nan

df.loc['2021-06-10']['ICH-BMEx'] = 0.30746904
df.loc['2021-06-10']['ICH-BMEx-Live'] = 0.72656542
df.loc['2021-06-10']['FN3-BMEx-Live'] = np.nan
df.loc['2022-02-13']['DMIv2-BMEx'] = 0

df.loc['2022-05-26']['TT7v3-FTX'] = 0.001634
df.loc['2022-05-26']['TT8v3-FTX'] = 0.00041000 
df.loc['2022-05-26']['STv3-USD-FTX'] = 118.0984049
df.loc['2022-05-26']['TS2v3-USD-FTX'] = 40.2
df.loc['2022-05-26']['TT5v3-USD-FTX'] = 3.189589137
df.loc['2022-05-26']['TT6v3-USD-FTX'] = 13.19747944

ttr_ftx = pd.DataFrame(df['TTR-FTX']['2020-10-10':])
ttr_ftx['deposits'] = 0
ttr_ftx['deposits'].loc['2020-10-10'] = 0.005
ttr_ftx['deposits'].loc['2022-02-03'] = 0.669415979
# ttr_ftx['balance'] = ttr_ftx['TTR-FTX'] + ttr_ftx['deposits']
ttr_ftx['TTR-FTX_ch'] = ttr_ftx['TTR-FTX']/(ttr_ftx['deposits']+ttr_ftx['TTR-FTX'].shift(1)) - 1
ttr_ftx.fillna(0, inplace=True)
ttr_ftx['equity'] = (1 + ttr_ftx['TTR-FTX_ch']).cumprod() 
ttr_ftx.index = pd.to_datetime(ttr_ftx.index)
# ttr_ftx.to_csv('TTR-prod_perf-01.20.2022.csv')

ttr_bmx = pd.DataFrame(df['TTR-BMEx']['2020-10-10':])
ttr_bmx['deposits'] = 0
ttr_bmx['deposits'].loc['2020-10-10'] = 0.005
ttr_bmx['deposits'].loc['2021-05-27'] = -0.00871956
# ttr_bmx['balance'] = ttr_bmx['TTR-BMEx'] + ttr_bmx['deposits']
ttr_bmx['TTR-BMEx_ch'] = ttr_bmx['TTR-BMEx']/(ttr_bmx['deposits']+ttr_bmx['TTR-BMEx'].shift(1)) - 1
ttr_bmx.fillna(0, inplace=True)
ttr_bmx['equity'] = (1 + ttr_bmx['TTR-BMEx_ch']).cumprod() 
ttr_bmx.index = pd.to_datetime(ttr_bmx.index)
ttr_bmx = ttr_bmx[:'2021-05-26']

stm_bmx = pd.DataFrame(df['STM-BMEx']['2020-10-10':])
stm_bmx['deposits'] = 0
stm_bmx['deposits'].loc['2020-10-10'] = 0.005
stm_bmx['deposits'].loc['2021-03-03'] = -0.00630284
# stm_bmx['balance'] = stm_bmx['STM-BMEx'] + stm_bmx['deposits']
stm_bmx['STM-BMEx_ch'] = stm_bmx['STM-BMEx']/(stm_bmx['deposits']+stm_bmx['STM-BMEx'].shift(1)) - 1
stm_bmx.fillna(0, inplace=True)
stm_bmx['equity'] = (1 + stm_bmx['STM-BMEx_ch']).cumprod() 
stm_bmx.index = pd.to_datetime(stm_bmx.index)
stm_bmx = stm_bmx[:'2021-03-03']

nmm_bmx = pd.DataFrame(df['NMM-BMEx']['2020-10-10':])
nmm_bmx['deposits'] = 0
nmm_bmx['deposits'].loc['2020-10-10'] = 0.005
nmm_bmx['deposits'].loc['2021-10-12'] = 0.0185 
nmm_bmx['deposits'].loc['2022-02-03'] = 0.019386706
# nmm_bmx['balance'] = nmm_bmx['NMM-BMEx'] + nmm_bmx['deposits']
nmm_bmx['NMM-BMEx_ch'] = nmm_bmx['NMM-BMEx']/(nmm_bmx['deposits']+nmm_bmx['NMM-BMEx'].shift(1)) - 1
nmm_bmx.fillna(0, inplace=True)
nmm_bmx['equity'] = (1 + nmm_bmx['NMM-BMEx_ch']).cumprod() 
nmm_bmx.index = pd.to_datetime(nmm_bmx.index)

occ_bmx = pd.DataFrame(df['OCC-BMEx']['2020-10-10':])
occ_bmx['deposits'] = 0
occ_bmx['deposits'].loc['2020-10-10'] = 0.005
occ_bmx['deposits'].loc['2021-03-03'] = -0.00370938
# occ_bmx['balance'] = occ_bmx['OCC-BMEx'] + occ_bmx['deposits']
occ_bmx['OCC-BMEx_ch'] = occ_bmx['OCC-BMEx']/(occ_bmx['deposits']+occ_bmx['OCC-BMEx'].shift(1)) - 1
occ_bmx.fillna(0, inplace=True)
occ_bmx['equity'] = (1 + occ_bmx['OCC-BMEx_ch']).cumprod() 
occ_bmx.index = pd.to_datetime(occ_bmx.index)
occ_bmx = occ_bmx[:'2021-03-03']

dmi_bmx = pd.DataFrame(df['DMI-BMEx']['2020-10-10':])
dmi_bmx['deposits'] = 0
dmi_bmx['deposits'].loc['2020-10-10'] = 0.005
dmi_bmx['deposits'].loc['2021-06-10'] = 0.021
dmi_bmx['deposits'].loc['2022-02-03'] = 0.013108333
# dmi_bmx['balance'] = dmi_bmx['DMI-BMEx'] + dmi_bmx['deposits']
dmi_bmx['DMI-BMEx_ch'] = dmi_bmx['DMI-BMEx']/(dmi_bmx['deposits']+dmi_bmx['DMI-BMEx'].shift(1)) - 1
dmi_bmx.fillna(0, inplace=True)
dmi_bmx['equity'] = (1 + dmi_bmx['DMI-BMEx_ch']).cumprod() 
dmi_bmx.index = pd.to_datetime(dmi_bmx.index)
# dmi_bmx.to_csv("DMI-BMX-01.21.2022.csv")

occ_ftx = pd.DataFrame(df['OCC-FTX']['2020-10-26':])
occ_ftx['deposits'] = 0
occ_ftx['deposits'].loc['2020-10-26'] = 0.005
occ_ftx['deposits'].loc['2022-02-03'] = -0.00068786
occ_ftx['deposits'].loc['2022-03-30'] = 0.047795 #<==-- DOUBLE CHECK!!!
occ_ftx['deposits'].loc['2022-05-26'] = - 0.0041 - 0.0135 # transfer to TS2v3 and STv3
occ_ftx['deposits'].loc['2022-05-30'] = -0.00929 #transfer to STv3
occ_ftx['deposits'].loc['2022-06-01'] = -0.011 # transfer to STv3
occ_ftx['deposits'].loc['2022-06-15'] = -0.0052632 #transfer to payday bmx
# occ_ftx['balance'] = occ_ftx['OCC-FTX'] + occ_ftx['deposits']
occ_ftx['OCC-FTX_ch'] = occ_ftx['OCC-FTX']/(occ_ftx['deposits']+occ_ftx['OCC-FTX'].shift(1)) - 1
occ_ftx.fillna(0, inplace=True)
occ_ftx['equity'] = (1 + occ_ftx['OCC-FTX_ch']).cumprod() 
occ_ftx.index = pd.to_datetime(occ_ftx.index)
occ_ftx = occ_ftx[:'2021-10-07']

dmi_ftx = pd.DataFrame(df['DMI-FTX']['2020-10-26':])
dmi_ftx['deposits'] = 0
dmi_ftx['deposits'].loc['2020-10-26'] = 0.005
dmi_ftx['deposits'].loc['2022-02-03'] = 0.036202333
# dmi_ftx['balance'] = dmi_ftx['DMI-FTX'] + dmi_ftx['deposits']
dmi_ftx['DMI-FTX_ch'] = dmi_ftx['DMI-FTX']/(dmi_ftx['deposits']+dmi_ftx['DMI-FTX'].shift(1)) - 1
dmi_ftx.fillna(0, inplace=True)
dmi_ftx['equity'] = (1 + dmi_ftx['DMI-FTX_ch']).cumprod() 
dmi_ftx.index = pd.to_datetime(dmi_ftx.index)
# dmi_ftx.to_csv("DMI-FTX.01.21.2022.csv")

tdc_ftx = pd.DataFrame(df['TDC-FTX']['2020-10-26':])
tdc_ftx['deposits'] = 0
tdc_ftx['deposits'].loc['2020-10-26'] = 0.005
tdc_ftx['deposits'].loc['2022-02-03'] = 0.018485529
# tdc_ftx['balance'] = tdc_ftx['TDC-FTX'] + tdc_ftx['deposits']
tdc_ftx['TDC-FTX_ch'] = tdc_ftx['TDC-FTX']/(tdc_ftx['deposits']+tdc_ftx['TDC-FTX'].shift(1)) - 1
tdc_ftx.fillna(0, inplace=True)
tdc_ftx['equity'] = (1 + tdc_ftx['TDC-FTX_ch']).cumprod() 
tdc_ftx.index = pd.to_datetime(tdc_ftx.index)
# tdc_ftx.to_csv("TDC-FTX.01.21.2022.csv")

ich_bmx = pd.DataFrame(df['ICH-BMEx']['2020-11-02':])
ich_bmx['deposits'] = 0
ich_bmx['deposits'].loc['2020-11-02'] = 0.005
ich_bmx['deposits'].loc['2021-06-10'] = 0.298604
ich_bmx['ICH-BMEx'].loc['2021-06-10'] = 0.30746904
ich_bmx['deposits'].loc['2021-06-11'] = -0.30746904
# ich_bmx['balance'] = ich_bmx['ICH-BMEx'] + ich_bmx['deposits']
ich_bmx['ICH-BMEx_ch'] = ich_bmx['ICH-BMEx']/(ich_bmx['deposits']+ich_bmx['ICH-BMEx'].shift(1)) - 1
ich_bmx.fillna(0, inplace=True)
ich_bmx['equity'] = (1 + ich_bmx['ICH-BMEx_ch']).cumprod() 
ich_bmx.index = pd.to_datetime(ich_bmx.index)
ich_bmx = ich_bmx[:'2021-06-11']

cha_bmx = pd.DataFrame(df['CHA-BMEx']['2020-12-08':])
cha_bmx['deposits'] = 0
cha_bmx['deposits'].loc['2020-12-08'] = 0.005
cha_bmx['deposits'].loc['2021-06-10'] = 0.014294
cha_bmx['deposits'].loc['2021-10-12'] = -0.0185 
cha_bmx['deposits'].loc['2021-12-07'] = -0.000069
# cha_bmx['balance'] = cha_bmx['CHA-BMEx'] + cha_bmx['deposits']
cha_bmx['CHA-BMEx_ch'] = cha_bmx['CHA-BMEx']/(cha_bmx['deposits']+cha_bmx['CHA-BMEx'].shift(1)) - 1
cha_bmx.fillna(0, inplace=True)
cha_bmx['equity'] = (1 + cha_bmx['CHA-BMEx_ch']).cumprod() 
cha_bmx.index = pd.to_datetime(cha_bmx.index)
cha_bmx = cha_bmx[:'2021-10-07']

vor_bmx = pd.DataFrame(df['VOR-BMEx']['2020-12-08':])
vor_bmx['deposits'] = 0
vor_bmx['deposits'].loc['2020-12-08'] = 0.005
vor_bmx['deposits'].loc['2021-06-10'] = 0.014294
vor_bmx['deposits'].loc['2022-02-03'] = 0.000877
# vor_bmx['balance'] = vor_bmx['VOR-BMEx'] + vor_bmx['deposits']
vor_bmx['VOR-BMEx_ch'] = vor_bmx['VOR-BMEx']/(vor_bmx['deposits']+vor_bmx['VOR-BMEx'].shift(1)) - 1
vor_bmx.fillna(0, inplace=True)
vor_bmx['equity'] = (1 + vor_bmx['VOR-BMEx_ch']).cumprod() 
vor_bmx.index = pd.to_datetime(vor_bmx.index)

tdc_bmx = pd.DataFrame(df['TDC-BMEx']['2020-12-08':])
tdc_bmx['deposits'] = 0
tdc_bmx['deposits'].loc['2020-12-08'] = 0.005
tdc_bmx['deposits'].loc['2021-06-10'] = 0.142529
tdc_bmx['deposits'].loc['2022-02-03'] = 0.376203751
# tdc_bmx['balance'] = tdc_bmx['TDC-BMEx'] + tdc_bmx['deposits']
tdc_bmx['TDC-BMEx_ch'] = tdc_bmx['TDC-BMEx']/(tdc_bmx['deposits']+tdc_bmx['TDC-BMEx'].shift(1)) - 1
tdc_bmx.fillna(0, inplace=True)
tdc_bmx['equity'] = (1 + tdc_bmx['TDC-BMEx_ch']).cumprod() 
tdc_bmx.index = pd.to_datetime(tdc_bmx.index)

cha_ftx = pd.DataFrame(df['CHA-FTX']['2021-01-17':])
cha_ftx['deposits'] = 0
cha_ftx['deposits'].loc['2021-01-17'] = 0.005
cha_ftx['deposits'].loc['2022-06-14'] = -0.0001 - 0.00068 # transfer from Chaos-FTX to TT8v3
# cha_ftx['balance'] = cha_ftx['CHA-FTX'] + cha_ftx['deposits']
cha_ftx['CHA-FTX_ch'] = cha_ftx['CHA-FTX']/(cha_ftx['deposits']+cha_ftx['CHA-FTX'].shift(1)) - 1
cha_ftx.fillna(0, inplace=True)
cha_ftx['equity'] = (1 + cha_ftx['CHA-FTX_ch']).cumprod() 
cha_ftx.index = pd.to_datetime(cha_ftx.index)
cha_ftx = cha_ftx[:'2021-10-07']

vor2_bmx = pd.DataFrame(df['VOR-V2-BMEx']['2021-04-28':])
vor2_bmx['deposits'] = 0
vor2_bmx['deposits'].loc['2021-04-28'] = 0.00630284
vor2_bmx['deposits'].loc['2021-06-10'] = 0.000427
# vor2_bmx['balance'] = vor2_bmx['VOR-V2-BMEx'] + vor2_bmx['deposits']
vor2_bmx['VOR-V2-BMEx_ch'] = vor2_bmx['VOR-V2-BMEx']/(vor2_bmx['deposits']+vor2_bmx['VOR-V2-BMEx'].shift(1)) - 1
vor2_bmx.fillna(0, inplace=True)
vor2_bmx['equity'] = (1 + vor2_bmx['VOR-V2-BMEx_ch']).cumprod() 
vor2_bmx.index = pd.to_datetime(vor2_bmx.index)
vor2_bmx = vor2_bmx[:'2021-10-07']

dmi2_bmx = pd.DataFrame(df['DMI-V2-BMEx']['2021-05-02':])
dmi2_bmx['deposits'] = 0
dmi2_bmx['deposits'].loc['2021-05-02'] = 0.00507377
dmi2_bmx['deposits'].loc['2021-07-02'] = -0.005074
# dmi2_bmx['balance'] = dmi2_bmx['DMI-V2-BMEx'] + dmi2_bmx['deposits']
dmi2_bmx['DMI-V2-BMEx_ch'] = dmi2_bmx['DMI-V2-BMEx']/(dmi2_bmx['deposits']+dmi2_bmx['DMI-V2-BMEx'].shift(1)) - 1
dmi2_bmx.fillna(0, inplace=True)
dmi2_bmx['equity'] = (1 + dmi2_bmx['DMI-V2-BMEx_ch']).cumprod() 
dmi2_bmx.index = pd.to_datetime(dmi2_bmx.index)
dmi2_bmx = dmi2_bmx[:'2021-07-02']

tdc2_bmx = pd.DataFrame(df['TDC-V2-BMEx']['2021-05-04':])
tdc2_bmx['deposits'] = 0
tdc2_bmx['deposits'].loc['2021-05-04'] = 0.00370938
tdc2_bmx['deposits'].loc['2021-06-10'] = 0.004601
tdc2_bmx['deposits'].loc['2021-11-15'] = 0.572179
tdc2_bmx['deposits'].loc['2021-11-17'] = -0.00205605 #transferred to MM3v3
tdc2_bmx['deposits'].loc['2021-11-25'] = -0.0158 #transferred to TS2v2 on 11.25.2021 
tdc2_bmx['deposits'].loc['2021-12-01'] = -0.0167 #transferred to TS2v2 and TS2v3 on 12.01.2021
tdc2_bmx['deposits'].loc['2021-12-07'] = -0.03881 - 0.0045 - 0.01854 - 0.00788 #transferred to MM3v2, TT4v2, TT1v2 on 12.07.2021
tdc2_bmx['deposits'].loc['2022-01-31'] = 0.272029 #transferred for testing
tdc2_bmx['deposits'].loc['2022-02-03'] = -0.74562605
tdc2_bmx['deposits'].loc['2022-02-14'] = 0.071932 #transferred from Thomas
tdc2_bmx['deposits'].loc['2022-02-17'] = -0.0141373 #transferred to ts2v3
tdc2_bmx['deposits'].loc['2022-03-30'] = -0.01132456 - 0.047795 # transferred to PAYDAY STRATEGY and OCC-FTX
# tdc2_bmx['balance'] = tdc2_bmx['TDC-V2-BMEx'] + tdc2_bmx['deposits']
tdc2_bmx['TDC-V2-BMEx_ch'] = tdc2_bmx['TDC-V2-BMEx']/(tdc2_bmx['deposits']+tdc2_bmx['TDC-V2-BMEx'].shift(1)) - 1
tdc2_bmx.fillna(0, inplace=True)
tdc2_bmx['equity'] = (1 + tdc2_bmx['TDC-V2-BMEx_ch']).cumprod() 
tdc2_bmx.index = pd.to_datetime(tdc2_bmx.index)
tdc2_bmx = tdc2_bmx[:'2021-10-07']

ichlo_bmx = pd.DataFrame(df['ICH-LO-BMEx']['2021-05-16':])
ichlo_bmx['deposits'] = 0
ichlo_bmx['deposits'].loc['2021-05-16'] = 0.005
ichlo_bmx['deposits'].loc['2021-06-10'] = 0.015
ichlo_bmx['deposits'].loc['2021-11-18'] = -0.02080546
# ichlo_bmx['balance'] = ichlo_bmx['ICH-LO-BMEx'] + ichlo_bmx['deposits']
ichlo_bmx['ICH-LO-BMEx_ch'] = ichlo_bmx['ICH-LO-BMEx']/(ichlo_bmx['deposits']+ichlo_bmx['ICH-LO-BMEx'].shift(1)) - 1
ichlo_bmx.fillna(0, inplace=True)
ichlo_bmx['equity'] = (1 + ichlo_bmx['ICH-LO-BMEx_ch']).cumprod() 
ichlo_bmx.index = pd.to_datetime(ichlo_bmx.index)
ichlo_bmx = ichlo_bmx[:'2021-10-07']

dmilo_bmx = pd.DataFrame(df['DMI-LO-BMEx']['2021-05-19':])
dmilo_bmx['deposits'] = 0
dmilo_bmx['deposits'].loc['2021-05-16'] = 0.005
dmilo_bmx['deposits'].loc['2021-06-10'] = 0.015
dmilo_bmx['deposits'].loc['2021-12-07'] = -0.02331031
# dmilo_bmx['balance'] = dmilo_bmx['DMI-LO-BMEx'] + dmilo_bmx['deposits']
dmilo_bmx['DMI-LO-BMEx_ch'] = dmilo_bmx['DMI-LO-BMEx']/(dmilo_bmx['deposits']+dmilo_bmx['DMI-LO-BMEx'].shift(1)) - 1
dmilo_bmx.fillna(0, inplace=True)
dmilo_bmx['equity'] = (1 + dmilo_bmx['DMI-LO-BMEx_ch']).cumprod() 
dmilo_bmx.index = pd.to_datetime(dmilo_bmx.index)
dmilo_bmx = dmilo_bmx[:'2021-10-07']

tt3_bmx = pd.DataFrame(df['TT3-BMEx']['2021-05-26':])
tt3_bmx['deposits'] = 0
tt3_bmx['deposits'].loc['2021-05-26'] = 0.00882454
tt3_bmx['deposits'].loc['2021-06-10'] = 0.030236
tt3_bmx['deposits'].loc['2022-02-03'] = -0.01051679
# tt3_bmx['balance'] = tt3_bmx['TT3-BMEx'] + tt3_bmx['deposits']
tt3_bmx['TT3-BMEx_ch'] = tt3_bmx['TT3-BMEx']/(tt3_bmx['deposits']+tt3_bmx['TT3-BMEx'].shift(1)) - 1
tt3_bmx.fillna(0, inplace=True)
tt3_bmx['equity'] = (1 + tt3_bmx['TT3-BMEx_ch']).cumprod() 
tt3_bmx.index = pd.to_datetime(tt3_bmx.index)

tt4_bmx = pd.DataFrame(df['TT4-BMEx']['2021-05-26':])
tt4_bmx['deposits'] = 0
tt4_bmx['deposits'].loc['2021-05-26'] = 0.005
tt4_bmx['deposits'].loc['2021-06-10'] = 0.00611211
tt4_bmx['deposits'].loc['2022-02-03'] = 0.3966272
# tt4_bmx['balance'] = tt4_bmx['TT4-BMEx'] + tt4_bmx['deposits']
tt4_bmx['TT4-BMEx_ch'] = tt4_bmx['TT4-BMEx']/(tt4_bmx['deposits']+tt4_bmx['TT4-BMEx'].shift(1)) - 1
tt4_bmx.fillna(0, inplace=True)
tt4_bmx['equity'] = (1 + tt4_bmx['TT4-BMEx_ch']).cumprod() 
tt4_bmx.index = pd.to_datetime(tt4_bmx.index)

fn1_bmx = pd.DataFrame(df['FN1-BMEx']['2021-06-10':])
fn1_bmx['deposits'] = 0
fn1_bmx['deposits'].loc['2021-06-10'] = 0.30746904
# fn1_bmx['balance'] = fn1_bmx['FN1-BMEx'] + fn1_bmx['deposits']
fn1_bmx['deposits'].loc['2022-01-31'] = -0.272029 #transferred for testing
fn1_bmx['FN1-BMEx_ch'] = fn1_bmx['FN1-BMEx']/(fn1_bmx['deposits']+fn1_bmx['FN1-BMEx'].shift(1)) - 1
fn1_bmx.fillna(0, inplace=True)
fn1_bmx['equity'] = (1 + fn1_bmx['FN1-BMEx_ch']).cumprod() 
fn1_bmx.index = pd.to_datetime(fn1_bmx.index)
fn1_bmx = fn1_bmx[:'2021-10-07']

ts2_bmx = pd.DataFrame(df['TS2-BMEx']['2021-07-02':])
ts2_bmx['deposits'] = 0
ts2_bmx['deposits'].loc['2021-07-02'] = 0.005074
ts2_bmx['deposits'].loc['2022-02-03'] = 0.015923
# ts2_bmx['balance'] = ts2_bmx['TS2-BMEx'] + ts2_bmx['deposits']
ts2_bmx['TS2-BMEx_ch'] = ts2_bmx['TS2-BMEx']/(ts2_bmx['deposits']+ts2_bmx['TS2-BMEx'].shift(1)) - 1
ts2_bmx.fillna(0, inplace=True)
ts2_bmx['equity'] = (1 + ts2_bmx['TS2-BMEx_ch']).cumprod() 
ts2_bmx.index = pd.to_datetime(ts2_bmx.index)

ts2v3_bmx = pd.DataFrame(df['TS2v3-BMEx']['2021-11-11':])
ts2v3_bmx['deposits'] = 0
ts2v3_bmx['deposits'].loc['2021-11-11'] = 0.01410001
ts2v3_bmx['deposits'].loc['2021-12-01'] = 0.0097 
ts2v3_bmx['deposits'].loc['2022-02-03'] = -0.024686
ts2v3_bmx['deposits'].loc['2022-02-17'] = 0.0141373
# ts2v3_bmx['balance'] = ts2v3_bmx['TS2v3-BMEx'] + ts2v3_bmx['deposits']
ts2v3_bmx['TS2v3-BMEx_ch'] = ts2v3_bmx['TS2v3-BMEx']/(ts2v3_bmx['deposits']+ts2v3_bmx['TS2v3-BMEx'].shift(1)) - 1
ts2v3_bmx.fillna(0, inplace=True)
ts2v3_bmx['equity'] = (1 + ts2v3_bmx['TS2v3-BMEx_ch']).cumprod() 
ts2v3_bmx.index = pd.to_datetime(ts2v3_bmx.index)

mm3v3_bmx = pd.DataFrame(df['MM3v3-BMEx']['2021-11-17':])
mm3v3_bmx['deposits'] = 0
mm3v3_bmx['deposits'].loc['2022-02-03'] = 0.017025
mm3v3_bmx['MM3v3-BMEx_ch'] = mm3v3_bmx['MM3v3-BMEx']/(mm3v3_bmx['deposits']+mm3v3_bmx['MM3v3-BMEx'].shift(1)) - 1
mm3v3_bmx.fillna(0, inplace=True)
mm3v3_bmx['equity'] = (1 + mm3v3_bmx['MM3v3-BMEx_ch']).cumprod() 
mm3v3_bmx.index = pd.to_datetime(mm3v3_bmx.index)

fn3v2_bmx = pd.DataFrame(df['FN3v2-BMEx']['2021-11-18':])
fn3v2_bmx['deposits'] = 0
fn3v2_bmx['deposits'].loc['2021-11-18'] = 0.02080546
# fn3v2_bmx['balance'] = fn3v2_bmx['FN3v2-BMEx'] + fn3v2_bmx['deposits']
fn3v2_bmx['FN3v2-BMEx_ch'] = fn3v2_bmx['FN3v2-BMEx']/(fn3v2_bmx['deposits']+fn3v2_bmx['FN3v2-BMEx'].shift(1)) - 1
fn3v2_bmx.fillna(0, inplace=True)
fn3v2_bmx['equity'] = (1 + fn3v2_bmx['FN3v2-BMEx_ch']).cumprod() 
fn3v2_bmx.index = pd.to_datetime(fn3v2_bmx.index)
fn3v2_bmx = fn3v2_bmx[:'2021-12-07']

# TIME BASED

ts2v2_bmx = pd.DataFrame(df['TS2v2-BMEx']['2021-11-24':])
ts2v2_bmx['deposits'] = 0
ts2v2_bmx['deposits'].loc['2021-11-24'] = 0.001 
ts2v2_bmx['deposits'].loc['2021-11-25'] = 0.0158 
ts2v2_bmx['deposits'].loc['2021-12-01'] = 0.0070
ts2v2_bmx['deposits'].loc['2021-12-07'] = -0.0002 - 0.0236  + 0.001 #transfer fee to new account + parked balance on new account
ts2v2_bmx['deposits'].loc['2021-12-08'] = 0.0236
ts2v2_bmx['deposits'].loc['2022-02-03'] = 0.869685946
# ts2v2_bmx['balance'] = ts2v2_bmx['TS2v2-BMEx'] + ts2v2_bmx['deposits']
ts2v2_bmx['TS2v2-BMEx_ch'] = ts2v2_bmx['TS2v2-BMEx']/(ts2v2_bmx['deposits']+ts2v2_bmx['TS2v2-BMEx'].shift(1)) - 1
ts2v2_bmx.fillna(0, inplace=True)
ts2v2_bmx['equity'] = (1 + ts2v2_bmx['TS2v2-BMEx_ch']).cumprod() 
ts2v2_bmx.index = pd.to_datetime(ts2v2_bmx.index)

#TIME BASED

mm3v2_bmx = pd.DataFrame(df['MM3v2-BMEx']['2021-12-07':])
mm3v2_bmx['deposits'] = 0
mm3v2_bmx['deposits'].loc['2022-02-03'] = 0.934501
# mm3v2_bmx['deposits'].loc['2021-12-07'] = 0.000069 + 0.03881 # IF NO TRADES CHANGE DATE!!!
mm3v2_bmx['MM3v2-BMEx_ch'] = mm3v2_bmx['MM3v2-BMEx']/(mm3v2_bmx['deposits']+mm3v2_bmx['MM3v2-BMEx'].shift(1)) - 1
mm3v2_bmx.fillna(0, inplace=True)
mm3v2_bmx['equity'] = (1 + mm3v2_bmx['MM3v2-BMEx_ch']).cumprod() 
mm3v2_bmx.index = pd.to_datetime(mm3v2_bmx.index)

#TIME BASED

tt4v2_bmx = pd.DataFrame(df['TT4v2-BMEx']['2021-12-07':])
tt4v2_bmx['deposits'] = 0
# tt4v2_bmx['deposits'].loc['2022-02-03'] = 0.035045353 #recorded transfer
tt4v2_bmx['deposits'].loc['2022-02-03'] = 0.03504530 #actual transfer
# tt4v2_bmx['deposits'].loc['2021-12-07'] = 0.02331031 + 0.0045 #from DMI-LO and TDCv2 on 12.07.2021 IF NO TRADES CHANGE DATE!!!
tt4v2_bmx['TT4v2-BMEx_ch'] = tt4v2_bmx['TT4v2-BMEx']/(tt4v2_bmx['deposits']+tt4v2_bmx['TT4v2-BMEx'].shift(1)) - 1
tt4v2_bmx.fillna(0, inplace=True)
tt4v2_bmx['equity'] = (1 + tt4v2_bmx['TT4v2-BMEx_ch']).cumprod() 
tt4v2_bmx.index = pd.to_datetime(tt4v2_bmx.index)

#TIME BASED

tt1v2_bmx = pd.DataFrame(df['TT1v2-BMEx']['2021-12-07':])
tt1v2_bmx['deposits'] = 0
# tt1v2_bmx['deposits'].loc['2022-02-03'] = 0.888837065 #recorded transfer
tt1v2_bmx['deposits'].loc['2022-02-03'] = 0.88883700 #actual transfer
# tt1v2_bmx['deposits'].loc['2021-12-07'] = 0.01854 #from TDCv2 on 12.07.2021 IF NO TRADES CHANGE DATE!!!
tt1v2_bmx['TT1v2-BMEx_ch'] = tt1v2_bmx['TT1v2-BMEx']/(tt1v2_bmx['deposits']+tt1v2_bmx['TT1v2-BMEx'].shift(1)) - 1
tt1v2_bmx.fillna(0, inplace=True)
tt1v2_bmx['equity'] = (1 + tt1v2_bmx['TT1v2-BMEx_ch']).cumprod() 
tt1v2_bmx.index = pd.to_datetime(tt1v2_bmx.index)

# TIME BASED

fn3v2rev_bmx = pd.DataFrame(df['FN3v2rev-BMEx']['2021-12-07':])
fn3v2rev_bmx['deposits'] = 0
fn3v2rev_bmx['deposits'].loc['2022-02-03'] = 0.007685294
# fn3v2rev_bmx['deposits'].loc['2021-12-07'] = 0.00788 + 0.021441
# fn3v2_bmx['balance'] = fn3v2_bmx['FN3v2-BMEx'] + fn3v2_bmx['deposits']
fn3v2rev_bmx['FN3v2rev-BMEx_ch'] = fn3v2rev_bmx['FN3v2rev-BMEx']/(fn3v2rev_bmx['deposits']+fn3v2rev_bmx['FN3v2rev-BMEx'].shift(1)) - 1
fn3v2rev_bmx.fillna(0, inplace=True)
fn3v2rev_bmx['equity'] = (1 + fn3v2rev_bmx['FN3v2rev-BMEx_ch']).cumprod() 
fn3v2rev_bmx.index = pd.to_datetime(fn3v2rev_bmx.index)

#MIXED EXECUTION

dmiv2_bmx = pd.DataFrame(df['DMIv2-BMEx']['2022-02-14':])
dmiv2_bmx['deposits'] = 0
dmiv2_bmx['deposits'].loc['2022-02-14'] = 0.028068 #actual transfer
dmiv2_bmx['DMIv2-BMEx_ch'] = dmiv2_bmx['DMIv2-BMEx']/(dmiv2_bmx['deposits']+dmiv2_bmx['DMIv2-BMEx'].shift(1)) - 1
dmiv2_bmx.fillna(0, inplace=True)
dmiv2_bmx['equity'] = (1 + dmiv2_bmx['DMIv2-BMEx_ch']).cumprod() 
dmiv2_bmx.index = pd.to_datetime(dmiv2_bmx.index)

# payday
payday_bmx = pd.DataFrame(df['PAYDAY-BMEx']['2022-03-30':])
payday_bmx['deposits'] = 0
payday_bmx['deposits'].loc['2022-03-30'] = 0.01132456 #actual transfer from TDCv2
payday_bmx['deposits'].loc['2022-06-15'] = 0.0052632 #transfer from OCC-FTX
payday_bmx['PAYDAY-BMEx_ch'] = payday_bmx['PAYDAY-BMEx']/(payday_bmx['deposits']+payday_bmx['PAYDAY-BMEx'].shift(1)) - 1
payday_bmx.fillna(0, inplace=True)
payday_bmx['equity'] = (1 + payday_bmx['PAYDAY-BMEx_ch']).cumprod() 
payday_bmx.index = pd.to_datetime(payday_bmx.index)

# tt7v3_ftx
tt7v3_ftx = pd.DataFrame(df['TT7v3-FTX']['2022-05-26':])
tt7v3_ftx['deposits'] = 0
# tt7v3_ftx['deposits'].loc['2022-05-26'] = 0.001634 #transfer from ChaosStrategy (Dev)
# tt7v3_ftx['TT7v3-FTX'].loc['2022-05-26'] = 0.001634
tt7v3_ftx['TT7v3-FTX_ch'] = tt7v3_ftx['TT7v3-FTX']/(tt7v3_ftx['deposits']+tt7v3_ftx['TT7v3-FTX'].shift(1)) - 1
tt7v3_ftx.fillna(0, inplace=True)
tt7v3_ftx['equity'] = (1 + tt7v3_ftx['TT7v3-FTX_ch']).cumprod() 
tt7v3_ftx.index = pd.to_datetime(tt7v3_ftx.index)

# tt8v3_ftx
tt8v3_ftx = pd.DataFrame(df['TT8v3-FTX']['2022-05-26':])
tt8v3_ftx['deposits'] = 0
# tt8v3_ftx['deposits'].loc['2022-05-26'] = 0.00041000 #transfer from TTR-FTX (dev)
# tt8v3_ftx['TT8v3-FTX'].loc['2022-05-26'] = 0.00041000
tt8v3_ftx['deposits'].loc['2022-06-14'] = 0.0001 + 0.00068 # transfer from Chaos-FTX to TT8v3
tt8v3_ftx['TT8v3-FTX_ch'] = tt8v3_ftx['TT8v3-FTX']/(tt8v3_ftx['deposits']+tt8v3_ftx['TT8v3-FTX'].shift(1)) - 1
tt8v3_ftx.fillna(0, inplace=True)
tt8v3_ftx['equity'] = (1 + tt8v3_ftx['TT8v3-FTX_ch']).cumprod() 
tt8v3_ftx.index = pd.to_datetime(tt8v3_ftx.index)

initial = {
    "date" : ["2020-10-10"],
    "TTR-FTX" : [0.005], "TTR-BMEx": [0.005], "STM-BMEx" : [0.005], 
    "NMM-BMEx" : [0.005], "OCC-BMEx": [0.00477], "DMI-BMEx": [0.005]
    }
initial = pd.DataFrame(initial)
initial.set_index("date", inplace=True)

strats = [
    "TTR-FTX", "TTR-BMEx", "STM-BMEx", "NMM-BMEx", "OCC-BMEx",
    "DMI-BMEx", "OCC-FTX", "DMI-FTX", "TDC-FTX", "ICH-BMEx",
    "CHA-BMEx", "VOR-BMEx", "TDC-BMEx", "CHA-FTX", "VOR-V2-BMEx",
    "DMI-V2-BMEx", "TDC-V2-BMEx", "ICH-LO-BMEx", "DMI-LO-BMEx", 
    "TT3-BMEx", 'TT4-BMEx', 'FN1-BMEx', 'TS2-BMEx', 'TS2v3-BMEx',
    'MM3v3-BMEx', 'TS2v2-BMEx', 'FN3v2-BMEx', 'FN3v2rev-BMEx',
    'MM3v2-BMEx', 'TT1v2-BMEx', 'TT4v2-BMEx', 'DMIv2-BMEx', 'PAYDAY-BMEx',
    'TT7v3-FTX', 'TT8v3-FTX'
]
pd.set_option('display.max_columns', None)
prod = df[strats]
prod = pd.concat([initial, prod], ignore_index=False)

prod['deposits'] = 0
prod['deposits'].loc['2020-10-10'] = prod.loc['2020-10-10'][0:6].sum()  # initial capital
prod['deposits'].loc['2020-10-27'] = 0.015                              # funded OCC-FTX, DMI-FTX, TDC-FTX
prod['deposits'].loc['2020-11-03'] = 0.005                              # funded ICH-BMEx
prod['deposits'].loc['2020-12-09'] = 0.015                              # funded CHA-BMEx, VOR-BMEx, TDC-BMEx
prod['deposits'].loc['2021-01-17'] = 0.005                              # funded CHA-FTX
prod['deposits'].loc['2021-03-03'] = -0.00630284 + -0.00370938          # stopped STM-BMEx and OCC-BMEx
prod['deposits'].loc['2021-04-29'] = 0.00630284                         # funded VOR-V2-BMEx using STM-BMEx fund
prod['deposits'].loc['2021-05-03'] = 0.00507377                         # funded DMI-V2-BMEx
prod['deposits'].loc['2021-05-05'] = 0.00370938                         # funded TDC-V2-BMEx using OCC-BMEx
prod['deposits'].loc['2021-05-17'] = 0.005                              # funded ICH-LO-BMEx
prod['deposits'].loc['2021-05-20'] = 0.005                              # funded DMI-LO-BMEx
prod['deposits'].loc['2021-05-27'] = 0.005 - 0.00882454 + 0.00882454    # stopped TTR-BMEx. funded TT4-BMEx using capital and TT3-BMEx using TTR-BMEx fund
prod['deposits'].loc['2021-06-10'] = 0.55262                            # additional funds
prod['deposits'].loc['2021-06-11'] = 0                                  # stopped ICH-BMEx, funds transferred to FN1-BMEx
prod['deposits'].loc['2021-07-02'] = 0                                  # stopped DMIv2, funds transferred to TS2-BMEx
prod['deposits'].loc['2021-10-12'] = 0.0185 - 0.0185                    # transferred balance from Chaos v1 to NMM-BMEx
prod['deposits'].loc['2021-11-11'] = 0.01410001                         # funded TS2v3
prod['deposits'].loc['2021-11-15'] = 0.572179                           # trasnferred from FN3
prod['deposits'].loc['2021-11-17'] = 0.00205605 - 0.00205605            # transfer from TDCv2 to MM3v3 
prod['deposits'].loc['2021-11-18'] = 0.02080546 - 0.02080546            # transfer from ICH-LO to FN3v2
prod['deposits'].loc['2021-11-24'] = 0.001                              # remaining balance on TS2v2 account "dust"
prod['deposits'].loc['2021-11-25'] = 0.0158 - 0.0158                    # transfer from TDCv2 to TS2v2 (funded 11.25.2021)  
prod['deposits'].loc['2021-12-01'] = 0.0167 - 0.0167                    # transfer from TDCv2 to TS2v2 and TS2v3 
prod['deposits'].loc['2021-12-07'] = -0.0236 - 0.0002 + 0.001           # TS2v2 balance transferred FN3 for transfer to a new account with parked balance
prod['deposits'].loc['2021-12-08'] = 0.0236                             # transfer from FN3 (Thomas) to TS2v2
prod['deposits'].loc['2022-01-31'] = 0.272029 - 0.272029                #transferred for testing from FN1 to TDCv2
# prod['deposits'].loc['2022-02-03'] = (0.669415979 + 0.019386706 + 0.013108333 + 0.036202333 + 0.018485529 + 0.000877    #REBALANCING
#                                       + 0.376203751 + 0.01051679 + 0.3966272 + 0.015923 + 0.017025 + 0.869685946
#                                       + 0.007685294 + 0.934501 + 0.888837065 + 0.035045353 - 0.024686 + 0.74562605)
prod['deposits'].loc['2022-02-03'] = (0.669415979 + 0.019386706 + 0.013108333 + 0.036202333 + 0.018485529 + 0.000877 + 0.376203751 - 0.74562605 
                                      - 0.01051679 + 0.3966272 + 0.015923 - 0.024686 + 0.017025 + 0.869685946 + 0.934501 + 0.03504530 + 0.88883700
                                      + 0.007685294)
prod['deposits'].loc['2022-02-14'] = 0.028068 + 0.071932                # transfer from Thomas to DMIv2 and TDCv2 (parked balance)
prod['deposits'].loc['2022-02-17'] = 0.0141373 - 0.0141373              # transfer from TDCv2 to TS2v3
prod['deposits'].loc['2022-03-30'] = 0.05911956 - 0.047795 - 0.01132456 # transfer from TDCv2 to OCC-FTX and PAYDAY
prod['deposits'].loc['2022-05-26'] = -0.0041 - 0.0135 + 0.001634 + 0.00041000   # transfer from OCC-FTX to TS2v3 and STv3, from Chaosdev to TT7v3, from TTR-FTXdev to TT8v3
prod['deposits'].loc['2022-05-30'] = -0.00929                           # transfer from OCC-FTX to STv3
prod['deposits'].loc['2022-06-01'] = -0.011                             # transfer from OCC-FTX to STv3
prod['deposits'].loc['2022-06-14'] = 0.0001 + 0.00068 - 0.0001 - 0.00068 # transfer from Chaos-FTX to TT8v3
prod['deposits'].loc['2022-06-15'] = 0.0052632 - 0.0052632 + 0.00353382 # transfer from OCC-FTX to Payday, from Chaos-dev to TT7v3 

prod.loc['2021-03-03':]['STM-BMEx'] = np.nan
prod.loc['2021-03-03':]['OCC-BMEx'] = np.nan
prod.loc['2021-05-27':]['TTR-BMEx'] = np.nan

prod.loc['2021-06-10']['ICH-BMEx'] = 0.30746904

prod.fillna(0, inplace=True)
prod['balance'] = (
    prod['TTR-FTX'] + prod['TTR-BMEx'] + prod['STM-BMEx'] + prod['NMM-BMEx'] + prod['OCC-BMEx'] + prod['DMI-BMEx'] + prod['OCC-FTX'] + prod['DMI-FTX'] + prod['TDC-FTX'] + 
    prod['ICH-BMEx'] + prod['CHA-BMEx'] + prod['VOR-BMEx'] + prod['TDC-BMEx'] + prod['CHA-FTX'] + prod['VOR-V2-BMEx'] + prod['DMI-V2-BMEx'] + prod['TDC-V2-BMEx'] + prod['ICH-LO-BMEx'] +
    prod['DMI-LO-BMEx'] + prod['TT3-BMEx'] + prod['TT4-BMEx'] + prod['FN1-BMEx'] + prod['TS2-BMEx'] + prod['TS2v3-BMEx'] + prod['MM3v3-BMEx'] + prod['FN3v2-BMEx'] + prod['TS2v2-BMEx'] +
    prod['FN3v2rev-BMEx'] + prod['MM3v2-BMEx'] + prod['TT1v2-BMEx'] + prod['TT4v2-BMEx'] + prod['DMIv2-BMEx'] + prod['PAYDAY-BMEx'] + prod['TT7v3-FTX'] + prod['TT8v3-FTX']
)
prod['% ch'] = (prod['balance']/(prod['deposits']+prod['balance'].shift(1)) - 1) * 100
prod.fillna(0, inplace=True)
prod['equity'] = (1 + prod['% ch']/100).cumprod() 
prod['% ch'] = round(prod['% ch'], 2)
prod.index = pd.to_datetime(prod.index)

show = pd.to_datetime(df.index[-1]).dayofweek + 1
main_datatable = prod.drop(columns=['TTR-BMEx', 'STM-BMEx', 'OCC-BMEx', 'ICH-BMEx', 'DMI-V2-BMEx', 'VOR-V2-BMEx', 'ICH-LO-BMEx', 'CHA-BMEx', 'DMI-LO-BMEx', 'FN3v2-BMEx', 'FN1-BMEx','TDC-V2-BMEx'])
# prod.drop(columns=['TTR-BMEx', 'STM-BMEx', 'OCC-BMEx', 'ICH-BMEx', 'DMI-V2-BMEx']).tail(3)
main_datatable.reset_index(inplace=True)
main_datatable.date = pd.DatetimeIndex(main_datatable.date).strftime("%Y-%m-%d")

################################
#USD DENOMINATED BOTS

#stv3 ftx
stv3_usd_ftx = pd.DataFrame(df['STv3-USD-FTX']['2022-05-26':])
stv3_usd_ftx['deposits'] = 0
# stv3_usd_ftx['deposits'].loc['2022-05-26'] = 118.0984049 #transfer from OCC-FTX
# stv3_usd_ftx['STv3-USD-FTX'].loc['2022-05-26'] = 0.001634
stv3_usd_ftx['deposits'].loc['2022-05-30'] = 281.6151091
stv3_usd_ftx['deposits'].loc['2022-06-01'] = 344.97694
stv3_usd_ftx['STv3-USD-FTX_ch'] = stv3_usd_ftx['STv3-USD-FTX']/(stv3_usd_ftx['deposits']+stv3_usd_ftx['STv3-USD-FTX'].shift(1)) - 1
stv3_usd_ftx.fillna(0, inplace=True)
stv3_usd_ftx['equity'] = (1 + stv3_usd_ftx['STv3-USD-FTX_ch']).cumprod() 
stv3_usd_ftx.index = pd.to_datetime(stv3_usd_ftx.index)

#ts2v3 ftx
ts2v3_usd_ftx = pd.DataFrame(df['TS2v3-USD-FTX']['2022-05-26':])
ts2v3_usd_ftx['deposits'] = 0
# ts2v3_usd_ftx['deposits'].loc['2022-05-26'] = 40.2 #transfer from OCC-FTX
# ts2v3_usd_ftx['TS2v3-USD-FTX'].loc['2022-05-26'] = 0.001634
ts2v3_usd_ftx['TS2v3-USD-FTX_ch'] = ts2v3_usd_ftx['TS2v3-USD-FTX']/(ts2v3_usd_ftx['deposits']+ts2v3_usd_ftx['TS2v3-USD-FTX'].shift(1)) - 1
ts2v3_usd_ftx.fillna(0, inplace=True)
ts2v3_usd_ftx['equity'] = (1 + ts2v3_usd_ftx['TS2v3-USD-FTX_ch']).cumprod() 
ts2v3_usd_ftx.index = pd.to_datetime(ts2v3_usd_ftx.index)

#tt5v3 ftx
tt5v3_usd_ftx = pd.DataFrame(df['TT5v3-USD-FTX']['2022-05-26':])
tt5v3_usd_ftx['deposits'] = 0
# tt5v3_usd_ftx['deposits'].loc['2022-05-26'] = 40.2 #transfer from OCC-FTX
# tt5v3_usd_ftx['TT5v3-USD-FTX'].loc['2022-05-26'] = 3.18958914
tt5v3_usd_ftx['TT5v3-USD-FTX_ch'] = tt5v3_usd_ftx['TT5v3-USD-FTX']/(tt5v3_usd_ftx['deposits']+tt5v3_usd_ftx['TT5v3-USD-FTX'].shift(1)) - 1
tt5v3_usd_ftx.fillna(0, inplace=True)
tt5v3_usd_ftx['equity'] = (1 + tt5v3_usd_ftx['TT5v3-USD-FTX_ch']).cumprod() 
tt5v3_usd_ftx.index = pd.to_datetime(tt5v3_usd_ftx.index)

#ttv3
tt6v3_usd_ftx = pd.DataFrame(df['TT6v3-USD-FTX']['2022-05-26':])
tt6v3_usd_ftx['deposits'] = 0
# tt6v3_usd_ftx['deposits'].loc['2022-05-26'] = 40.2 #transfer from OCC-FTX
# tt6v3_usd_ftx['TT6v3-USD-FTX'].loc['2022-05-26'] = 13.19747944
tt6v3_usd_ftx['TT6v3-USD-FTX_ch'] = tt6v3_usd_ftx['TT6v3-USD-FTX']/(tt6v3_usd_ftx['deposits']+tt6v3_usd_ftx['TT6v3-USD-FTX'].shift(1)) - 1
tt6v3_usd_ftx.fillna(0, inplace=True)
tt6v3_usd_ftx['equity'] = (1 + tt6v3_usd_ftx['TT6v3-USD-FTX_ch']).cumprod() 
tt6v3_usd_ftx.index = pd.to_datetime(tt6v3_usd_ftx.index)

#flockls

flockls_usd_ftx = pd.DataFrame(df['FLOCKLS-USD-FTX']['2022-06-17':])
flockls_usd_ftx['deposits'] = 0
# flockls_usd_ftx['deposits'].loc['2022-05-26'] = 40.2 #transfer from OCC-FTX
flockls_usd_ftx['FLOCKLS-USD-FTX'].loc['2022-06-17'] = 100.18406843
flockls_usd_ftx['FLOCKLS-USD-FTX_ch'] = flockls_usd_ftx['FLOCKLS-USD-FTX']/(flockls_usd_ftx['deposits']+flockls_usd_ftx['FLOCKLS-USD-FTX'].shift(1)) - 1
flockls_usd_ftx.fillna(0, inplace=True)
flockls_usd_ftx['equity'] = (1 + flockls_usd_ftx['FLOCKLS-USD-FTX_ch']).cumprod() 
flockls_usd_ftx.index = pd.to_datetime(flockls_usd_ftx.index)

strats = [
    'STv3-USD-FTX', 'TS2v3-USD-FTX', 'TT5v3-USD-FTX', 'TT6v3-USD-FTX', 'FLOCKLS-USD-FTX'
]
pd.set_option('display.max_columns', None)
prod_usd = df[strats]

prod_usd['deposits'] = 0
# prod_usd = prod_usd['2022-05-26':]
prod_usd['deposits'].loc['2022-05-26'] = 118.0984049 + 40.2 + 3.189589137 + 13.19747944 # transfer from OCC-FTX to TS2v3 and STv3, from TTR-FTX to TT5v3 and TT6v3
prod_usd['deposits'].loc['2022-05-30'] = 281.6151091
prod_usd['deposits'].loc['2022-06-01'] = 344.97694
prod_usd['deposits'].loc['2022-06-17'] = 100.18406843 #transferred from xxx to FLOCKLS-USD-FTX

prod_usd.fillna(0, inplace=True)
prod_usd['balance'] = (
    prod_usd['STv3-USD-FTX'] + prod_usd['TS2v3-USD-FTX'] + prod_usd['TT5v3-USD-FTX'] + prod_usd['TT6v3-USD-FTX'] + prod_usd['FLOCKLS-USD-FTX']
)
prod_usd['% ch'] = (prod_usd['balance']/(prod_usd['deposits']+prod_usd['balance'].shift(1)) - 1) * 100
prod_usd.fillna(0, inplace=True)
prod_usd['equity'] = (1 + prod_usd['% ch']/100).cumprod() 
prod_usd['% ch'] = round(prod_usd['% ch'], 2)
prod_usd.index = pd.to_datetime(prod_usd.index)

prod_usd = prod_usd['2022-05-26':].copy()
prod_usd_datatable = prod_usd['2022-05-26':].copy()
# prod.drop(columns=['TTR-BMEx', 'STM-BMEx', 'OCC-BMEx', 'ICH-BMEx', 'DMI-V2-BMEx']).tail(3)
prod_usd_datatable.reset_index(inplace=True)
prod_usd_datatable.date = pd.DatetimeIndex(prod_usd_datatable.date).strftime("%Y-%m-%d")





#################################################################

live = df[["NMM-BMEx-Live", "STM-BMEx-Live", "TTR-BMEx-Live", "ICH-BMEx-Live", "MM3-BMEx-Live", "TT1-BMEx-Live", "FN3-BMEx-Live"]]
live = live['2021-01-27':]
live.index = pd.to_datetime(live.index)

# nmm
nmm = pd.DataFrame(live['NMM-BMEx-Live'])
nmm['deposits'] = 0
nmm['deposits'].loc['2021-01-27'] = nmm.loc['2021-01-27'][0]
nmm['deposits'].loc['2021-02-04'] = 0.5
nmm['deposits'].loc['2021-05-27'] = nmm['NMM-BMEx-Live'].loc['2021-05-26'] * -1
nmm['balance'] = nmm['NMM-BMEx-Live']
nmm['% ch'] = nmm['NMM-BMEx-Live']/(nmm['deposits']+nmm['NMM-BMEx-Live'].shift(1)) - 1
nmm = nmm.replace([np.inf, -np.inf], np.nan)
nmm.fillna(0, inplace=True)
nmm['equity'] = (1 + nmm['% ch']).cumprod() 
nmm = nmm[:"2021-05-27"] 
nmm.index = pd.to_datetime(nmm.index)

# stm
stm = pd.DataFrame(live['STM-BMEx-Live'])
stm['deposits'] = 0
stm['deposits'].loc['2021-01-27'] = stm.loc['2021-01-27'][0]
stm['deposits'].loc['2021-02-04'] = 0.5
stm['deposits'].loc['2021-02-14'] = stm['STM-BMEx-Live'].loc['2021-02-14'] * -1
stm['balance'] = stm['STM-BMEx-Live']
stm['% ch'] = (stm['STM-BMEx-Live']/(stm['deposits']+stm['STM-BMEx-Live'].shift(1)) - 1)
stm = stm.replace([np.inf, -np.inf], np.nan)
stm.fillna(0, inplace=True)
stm['equity'] = (1 + stm['% ch']).cumprod()
stm = stm[:"2021-02-14"] 
stm.index = pd.to_datetime(stm.index)

# ttr
ttr = pd.DataFrame(live['TTR-BMEx-Live'])
ttr['deposits'] = 0
ttr['deposits'].loc['2021-01-27'] = ttr.loc['2021-01-27'][0]
ttr['deposits'].loc['2021-02-04'] = 0.5
ttr['deposits'].loc['2021-05-27'] = ttr['TTR-BMEx-Live'].loc['2021-05-26'] * -1
ttr['balance'] = ttr['TTR-BMEx-Live']
ttr['% ch'] = ttr['TTR-BMEx-Live']/(ttr['deposits']+ttr['TTR-BMEx-Live'].shift(1)) - 1
ttr = ttr.replace([np.inf, -np.inf], np.nan)
ttr.fillna(0, inplace=True)
ttr['equity'] = (1 + ttr['% ch']).cumprod() 
ttr = ttr[:"2021-05-27"] 
ttr.index = pd.to_datetime(ttr.index)

#ich
ich = pd.DataFrame(live['ICH-BMEx-Live']['2021-02-14':])
ich['deposits'] = 0
# ich['deposits'].loc['2021-02-13'] = ich.loc['2021-02-13'][0]
ich['deposits'].loc['2021-02-14'] = 1.108138
ich['deposits'].loc['2021-06-10'] = -0.55873211
ich['deposits'].loc['2021-06-11'] = -0.726565
# ich['balance'] = ich['ICH-BMEx-Live'] + ich['deposits']
ich['% ch'] = ich['ICH-BMEx-Live']/(ich['deposits']+ich['ICH-BMEx-Live'].shift(1)) - 1
ich.fillna(0, inplace=True)
ich['equity'] = (1 + ich['% ch']).cumprod() 
ich.index = pd.to_datetime(ich.index)
ich = ich[:'2021-06-10']

#mm3
mm3 = pd.DataFrame(live['MM3-BMEx-Live']['2021-05-27':]) # DOUBLE CHECK IF THERE ARE TRADES!
# mm3['MM3-BMEx-Live'].loc['2021-05-27'] = 0
mm3['deposits'] = 0
mm3['deposits'].loc['2021-05-27'] = 1.358086
mm3['deposits'].loc['2022-02-03'] = -0.83985639
# mm3['balance'] = mm3['MM3-BMEx-Live'] + mm3['deposits']
mm3['% ch'] = mm3['MM3-BMEx-Live']/(mm3['deposits']+mm3['MM3-BMEx-Live'].shift(1)) - 1
mm3.fillna(0, inplace=True)
mm3['equity'] = (1 + mm3['% ch']).cumprod() 
mm3.index = pd.to_datetime(mm3.index)

#tt1
tt1 = pd.DataFrame(live['TT1-BMEx-Live']['2021-05-27':]) # DOUBLE CHECK IF THERE ARE TRADES!
# tt1['TT1-BMEx-Live'].loc['2021-05-27'] = 0
tt1['deposits'] = 0
tt1['deposits'].loc['2021-05-27'] = 1.383415
tt1['deposits'].loc['2022-02-03'] = -0.905879
# tt1['balance'] = tt1['TT1-BMEx-Live'] + tt1['deposits']
tt1['% ch'] = tt1['TT1-BMEx-Live']/(tt1['deposits']+tt1['TT1-BMEx-Live'].shift(1)) - 1
tt1.fillna(0, inplace=True)
tt1['equity'] = (1 + tt1['% ch']).cumprod() 
tt1.index = pd.to_datetime(tt1.index)

#fn3
fn3 = pd.DataFrame(live['FN3-BMEx-Live']['2021-06-10':])
fn3['deposits'] = 0
fn3['deposits'].loc['2021-06-10'] = 0.726565
fn3['deposits'].loc['2021-11-15'] = -0.572179 
fn3['deposits'].loc['2021-12-07'] = 0.0236 
fn3['deposits'].loc['2021-12-08'] = -0.0236
fn3['deposits'].loc['2022-02-03'] = 0.009632471
# fn3['balance'] = fn3['FN3-BMEx-Live'] + (fn3['deposits'])
fn3['% ch'] = fn3['FN3-BMEx-Live']/(fn3['deposits']+fn3['FN3-BMEx-Live'].shift(1))- 1
fn3.fillna(0, inplace=True)
fn3['equity'] = (1 + fn3['% ch']).cumprod() 
fn3.index = pd.to_datetime(fn3.index)

## live
live['deposits'] = 0
live['deposits'].loc['2021-01-27'] = live.loc['2021-01-27'][0:3].sum()  #initial capital
live['deposits'].loc['2021-02-04'] = 1.5                                #additional capital to NMM-BMEx-Live, STM-BMEx-Live and TTR-BMEx-Live
live['deposits'].loc['2021-02-15'] = 0                                  #stopped STM-BMEx-Live and transferred capital to ICH-BMEx-Live
live['deposits'].loc['2021-05-27'] = 0                                  #stopped NMM-BMEx-Live & TTR-BMEx-Live, and transferred capital to MM3 and TT1
live['deposits'].loc['2021-06-10'] = -0.55873211                        #rebalanced capital from ICH-BMEx-Live to Prod Bots, stopped ICH-BMEx-Live, transferred cap to FN3
live['deposits'].loc['2021-11-15'] = -0.572179                          #transferred to TDCv2
live['deposits'].loc['2021-12-07'] = 0.0236                             #transferred to Thomas for transfer to TS2v2
live['deposits'].loc['2021-12-08'] = -0.0236                            #transferred to TS2v2
live['deposits'].loc['2022-02-03'] = (-0.83985639 - 0.905879 + 0.009632471) #REBALANCING

live.fillna(0, inplace=True)
live['balance'] = (live['NMM-BMEx-Live'] + live['STM-BMEx-Live'] + live['TTR-BMEx-Live'] + live['ICH-BMEx-Live'] + live['TT1-BMEx-Live'] + live['MM3-BMEx-Live']
                    + live['FN3-BMEx-Live'])
live['% ch'] = (live['balance']/(live['deposits']+live['balance'].shift(1)) - 1) * 100
live.fillna(0, inplace=True)
live['equity'] = (1 + live['% ch']/100).cumprod() 
live['% ch'] = round(live['% ch'], 2)
live.index = pd.to_datetime(live.index)

live.drop(columns=['STM-BMEx-Live', 'TTR-BMEx-Live', 'NMM-BMEx-Live', 'ICH-BMEx-Live']).tail(8)

#################################################################

#END OF S1
#################################################################

#################################################################
# START OF S3
corrected_bot_balance = pd.read_csv('data/correct_bot_balance_'+yesterday.strftime("%Y-%m-%d")+'.csv')
error_pnl = pd.read_csv('data/error_pnl_'+yesterday.strftime("%Y-%m-%d")+'.csv')
# corrected_bot_balance['date'] = pd.to_datetime(corrected_bot_balance['date'])
# corrected_bot_balance['date'] = corrected_bot_balance['date'].dt.date 
# corrected_bot_balance.set_index("date", inplace=True)


corrected_bot_balance = corrected_bot_balance.drop('Unnamed: 0', 1)
corrected_bot_balance = pd.concat([corrected_bot_balance, error_pnl['error_pnl']], axis=1)


corrected_bot_balance.set_index("date", inplace=True)
# corrected_bot_balance.loc['2021-04-29':]['STM-BMEx'] = np.nan
# corrected_bot_balance.loc['2021-03-03':]['OCC-BMEx'] = np.nan
corrected_bot_balance.loc['2021-03-03':]['STM-BMEx'] = np.nan
corrected_bot_balance.loc['2021-03-03':]['OCC-BMEx'] = np.nan
corrected_bot_balance.loc['2021-05-27':]['TTR-BMEx'] = np.nan
corrected_bot_balance.loc['2021-06-11':]['ICH-BMEx'] = np.nan

corrected_bot_balance.loc['2021-06-10']['ICH-BMEx'] = 0.30746904
corrected_bot_balance.loc['2021-06-10']['ICH-BMEx-Live'] = 0.72656542
corrected_bot_balance.loc['2021-06-10']['FN3-BMEx-Live'] = np.nan
corrected_bot_balance.loc['2022-02-13']['DMIv2-BMEx'] = 0

# error_pnl.set_index('Unnamed: 0', inplace=True)


live_s3 = corrected_bot_balance[["NMM-BMEx-Live", "STM-BMEx-Live", "TTR-BMEx-Live", "ICH-BMEx-Live", "MM3-BMEx-Live", "TT1-BMEx-Live", "FN3-BMEx-Live", "error_pnl"]]
live_s3 = live_s3['2021-01-27':]
#live_s3.index = pd.to_datetime(live_s3.index).dt.date

## NMM s3
nmm_s3 = pd.DataFrame(live_s3['NMM-BMEx-Live'])
nmm_s3['deposits'] = 0
#####nmm_s3['deposits'].loc['2021-01-27'] = nmm_s3.loc['2021-01-27'][0]
nmm_s3['deposits'].loc['2021-02-04'] = 0.5
nmm_s3['deposits'].loc['2021-05-27'] = nmm_s3['NMM-BMEx-Live'].loc['2021-05-26'] * -1
nmm_s3['balance'] = nmm_s3['NMM-BMEx-Live']
nmm_s3['% ch'] = nmm_s3['NMM-BMEx-Live']/(nmm_s3['deposits']+nmm_s3['NMM-BMEx-Live'].shift(1)) - 1
nmm_s3 = nmm.replace([np.inf, -np.inf], np.nan)
nmm_s3.fillna(0, inplace=True)
nmm_s3['equity'] = (1 + nmm_s3['% ch']).cumprod() 
nmm_s3 = nmm_s3[:"2021-05-27"] 
nmm_s3.index = pd.to_datetime(nmm_s3.index)

## STM s3
stm_s3 = pd.DataFrame(live_s3['STM-BMEx-Live'])
stm_s3['deposits'] = 0
#####stm_s3['deposits'].loc['2021-01-27'] = stm_s3.loc['2021-01-27'][0]
stm_s3['deposits'].loc['2021-02-04'] = 0.5
stm_s3['deposits'].loc['2021-02-14'] = stm_s3['STM-BMEx-Live'].loc['2021-02-14'] * -1
stm_s3['balance'] = stm_s3['STM-BMEx-Live']
stm_s3['% ch'] = (stm_s3['STM-BMEx-Live']/(stm_s3['deposits']+stm_s3['STM-BMEx-Live'].shift(1)) - 1)
stm_s3 = stm.replace([np.inf, -np.inf], np.nan)
stm_s3.fillna(0, inplace=True)
stm_s3['equity'] = (1 + stm_s3['% ch']).cumprod()
stm_s3 = stm_s3[:"2021-02-14"] 
stm_s3.index = pd.to_datetime(stm_s3.index)

## TTR S3
ttr_s3 = pd.DataFrame(live_s3['TTR-BMEx-Live'])
ttr_s3['deposits'] = 0
#####ttr_s3['deposits'].loc['2021-01-27'] = ttr_s3.loc['2021-01-27'][0]
ttr_s3['deposits'].loc['2021-02-04'] = 0.5
ttr_s3['deposits'].loc['2021-05-27'] = ttr_s3['TTR-BMEx-Live'].loc['2021-05-26'] * -1
ttr_s3['balance'] = ttr_s3['TTR-BMEx-Live']
ttr_s3['% ch'] = ttr_s3['TTR-BMEx-Live']/(ttr_s3['deposits']+ttr_s3['TTR-BMEx-Live'].shift(1)) - 1
ttr_s3 = ttr.replace([np.inf, -np.inf], np.nan)
ttr_s3.fillna(0, inplace=True)
ttr_s3['equity'] = (1 + ttr['% ch']).cumprod() 
ttr_s3 = ttr_s3[:"2021-05-27"] 
ttr_s3.index = pd.to_datetime(ttr_s3.index)

##ICH BMEx Live S3
ich_s3 = pd.DataFrame(live_s3['ICH-BMEx-Live']['2021-02-14':])
ich_s3['deposits'] = 0
# ich['deposits'].loc['2021-02-13'] = ich.loc['2021-02-13'][0]
ich_s3['deposits'].loc['2021-02-14'] = 1.108138
ich_s3['deposits'].loc['2021-06-10'] = -0.55873211
ich_s3['deposits'].loc['2021-06-11'] = -0.726565
ich_s3['balance'] = ich_s3['ICH-BMEx-Live'] + ich_s3['deposits']
ich_s3['% ch'] = ich_s3['ICH-BMEx-Live']/(ich_s3['deposits']+ich_s3['ICH-BMEx-Live'].shift(1)) - 1
ich_s3.fillna(0, inplace=True)
ich_s3['equity'] = (1 + ich_s3['% ch']).cumprod() 
ich_s3.index = pd.to_datetime(ich_s3.index)
ich_s3 = ich_s3[:'2021-06-10']

## MM3 BMEX S3
mm3_s3 = pd.DataFrame(live_s3['MM3-BMEx-Live']['2021-05-27':]) # DOUBLE CHECK IF THERE ARE TRADES!
# mm3['MM3-BMEx-Live'].loc['2021-05-27'] = 0
mm3_s3['deposits'] = 0
mm3_s3['deposits'].loc['2021-05-27'] = 1.358086
mm3_s3['deposits'].loc['2022-02-03'] = -0.83985639
# mm3['balance'] = mm3['MM3-BMEx-Live'] + mm3['deposits']
mm3_s3['% ch'] = mm3_s3['MM3-BMEx-Live']/(mm3_s3['deposits']+mm3_s3['MM3-BMEx-Live'].shift(1)) - 1
mm3_s3.fillna(0, inplace=True)
mm3_s3['equity'] = (1 + mm3_s3['% ch']).cumprod() 
mm3_s3.index = pd.to_datetime(mm3_s3.index)

## TT1 BMEX LIVE S3
tt1_s3 = pd.DataFrame(live_s3['TT1-BMEx-Live']['2021-05-27':]) # DOUBLE CHECK IF THERE ARE TRADES!
# tt1['TT1-BMEx-Live'].loc['2021-05-27'] = 0
tt1_s3['deposits'] = 0
tt1_s3['deposits'].loc['2021-05-27'] = 1.383415
tt1_s3['deposits'].loc['2022-02-03'] = -0.905879
# tt1_s3['balance'] = tt1_s3['TT1-BMEx-Live'] + tt1_s3['deposits']
tt1_s3['% ch'] = tt1_s3['TT1-BMEx-Live']/(tt1_s3['deposits']+tt1_s3['TT1-BMEx-Live'].shift(1)) - 1
tt1_s3.fillna(0, inplace=True)
tt1_s3['equity'] = (1 + tt1_s3['% ch']).cumprod() 
tt1_s3.index = pd.to_datetime(tt1_s3.index)

## FN3 BMEX LIVE S3
fn3_s3 = pd.DataFrame(live_s3['FN3-BMEx-Live']['2021-06-10':])
fn3_s3['deposits'] = 0
fn3_s3['deposits'].loc['2021-06-10'] = 0.726565
fn3_s3['deposits'].loc['2021-11-15'] = -0.572179 
fn3_s3['deposits'].loc['2021-12-07'] = 0.0236 
fn3_s3['deposits'].loc['2021-12-08'] = -0.0236
fn3_s3['deposits'].loc['2022-02-03'] = 0.009632471
# fn3['balance'] = fn3['FN3-BMEx-Live'] + (fn3['deposits'])
fn3_s3['% ch'] = fn3_s3['FN3-BMEx-Live']/(fn3_s3['deposits']+fn3_s3['FN3-BMEx-Live'].shift(1))- 1
fn3_s3.fillna(0, inplace=True)
fn3_s3['equity'] = (1 + fn3_s3['% ch']).cumprod() 
fn3_s3.index = pd.to_datetime(fn3_s3.index)

## live s3
live_s3['deposits'] = 0
live_s3['deposits'].loc['2021-01-27'] = live_s3.loc['2021-01-27'][0:3].sum()  #initial capital
live_s3['deposits'].loc['2021-02-04'] = 1.5                                #additional capital to NMM-BMEx-Live, STM-BMEx-Live and TTR-BMEx-Live
live_s3['deposits'].loc['2021-02-15'] = 0                                  #stopped STM-BMEx-Live and transferred capital to ICH-BMEx-Live
live_s3['deposits'].loc['2021-05-27'] = 0                                  #stopped NMM-BMEx-Live & TTR-BMEx-Live, and transferred capital to MM3 and TT1
live_s3['deposits'].loc['2021-06-10'] = -0.55873211                        #rebalanced capital from ICH-BMEx-Live to Prod Bots, stopped ICH-BMEx-Live, transferred cap to FN3
live_s3['deposits'].loc['2021-11-15'] = -0.572179                          #transferred to TDCv2
live_s3['deposits'].loc['2021-12-07'] = 0.0236                             #transferred to Thomas for transfer to TS2v2
live_s3['deposits'].loc['2021-12-08'] = -0.0236                            #transferred to TS2v2
live_s3['deposits'].loc['2022-02-03'] = (-0.83985639 - 0.905879 + 0.009632471) #REBALANCING

live_s3.fillna(0, inplace=True)
live_s3['balance'] = (live_s3['NMM-BMEx-Live'] + live_s3['STM-BMEx-Live'] + live_s3['TTR-BMEx-Live'] + live_s3['ICH-BMEx-Live'] + live_s3['TT1-BMEx-Live'] + live_s3['MM3-BMEx-Live']
                    + live_s3['FN3-BMEx-Live'])
live_s3['% ch'] = (live_s3['balance']/(live_s3['deposits']+live_s3['balance'].shift(1)) - 1) * 100
live_s3.fillna(0, inplace=True)
live_s3['equity'] = (1 + live_s3['% ch']/100).cumprod() 
live_s3['% ch'] = round(live_s3['% ch'], 2)
live_s3.index = pd.to_datetime(live_s3.index)
# live
laetitude_bots = live_s3.drop(columns=['STM-BMEx-Live', 'TTR-BMEx-Live', 'NMM-BMEx-Live', 'ICH-BMEx-Live']).tail(7)
laetitude_bots.reset_index(inplace = True)
laetitude_bots.rename(columns = {'index':'date'}, inplace = True)
laetitude_bots.date = pd.DatetimeIndex(laetitude_bots.date).strftime("%Y-%m-%d")

#END OF S3
#################################################################

# graph creation for all test btc bots
def build_graph_test_btc_bots():
    layout = go.Layout(
        autosize=False,
        width=1200,
        height=800,
        xaxis= go.layout.XAxis(linecolor = 'black',
                            linewidth = 1,
                            mirror = True),

        yaxis= go.layout.YAxis(linecolor = 'black',
                            linewidth = 1,
                            mirror = True),

        margin=go.layout.Margin(
            l=50,
            r=50,
            b=100,
            t=100,
            pad = 4
        )
    )
    fig = go.Figure(layout = layout)
    fig.update_layout(
    title="Test Bots (BTC-denominated)Daily Equity Curves",
    legend_title="BOT",
    font=dict(
        family="Courier New, monospace",
        size=18,
        color="Black")
    )
    prod_go = go.Scatter(x = prod.index, y = prod['equity'], mode = 'lines',  name = 'Total Equity')
    prod_go['line'] = dict(width=5,color='#006400')

    ttr_ftx_go = go.Scatter(x = ttr_ftx.index, y = ttr_ftx['equity'], mode = 'lines',  name = 'TTR-FTX', opacity = 0.5)
    ttr_bmx_go = go.Scatter(x = ttr_bmx.index, y = ttr_bmx['equity'], mode = 'lines',  name = 'TTR-BMEx', opacity = 0.5)
    stm_bmx_go = go.Scatter(x = stm_bmx.index, y = stm_bmx['equity'], mode = 'lines',  name = 'STM-BMEx',  opacity = 0.5)
    nmm_bmx_go = go.Scatter(x = nmm_bmx.index, y = nmm_bmx['equity'], mode = 'lines',  name = 'NMM-BMEx',  opacity = 0.5)
    occ_bmx_go = go.Scatter(x =occ_bmx.index, y = occ_bmx['equity'], mode = 'lines',  name = 'OCC-BMEx',  opacity = 0.5)
    dmi_bmx_go = go.Scatter(x =dmi_bmx.index, y = dmi_bmx['equity'], mode = 'lines',  name = 'DMI-BMEx',  opacity = 0.5)
    occ_ftx_go = go.Scatter(x =occ_ftx.index, y = occ_ftx['equity'], mode = 'lines',  name = 'OCC-FTX',  opacity = 0.5)
    tdc_ftx_go = go.Scatter(x =tdc_ftx.index, y = tdc_ftx['equity'], mode = 'lines',  name = 'TDC-FTX',  opacity = 0.5)
    ich_bmx_go = go.Scatter(x =ich_bmx.index, y = ich_bmx['equity'], mode = 'lines',  name = 'ICH-BMEx',  opacity = 0.5)
    cha_bmx_go = go.Scatter(x =cha_bmx.index, y = ['equity'], mode = 'lines',  name = 'CHA-BMEx',  opacity = 0.5)
    vor_bmx_go = go.Scatter(x =vor_bmx.index, y = vor_bmx['equity'], mode = 'lines',  name = 'VOR-BMEx',  opacity = 0.5)
    tdc_bmx_go = go.Scatter(x =tdc_bmx.index, y = tdc_bmx['equity'], mode = 'lines',  name = 'TDC-BMEx',  opacity = 0.5)
    cha_ftx_go = go.Scatter(x =cha_ftx.index, y = cha_ftx['equity'], mode = 'lines',  name = 'CHA-FTX',  opacity = 0.5)
    vor2_bmx_go = go.Scatter(x =vor2_bmx.index, y = vor2_bmx['equity'], mode = 'lines',  name = 'VOR-V2-BMEx',  opacity = 0.5)
    dmi2_bmx_go = go.Scatter(x =dmi2_bmx.index, y = dmi2_bmx ['equity'], mode = 'lines',  name = 'DMI-V2-BMEx',  opacity = 0.5)
    tdc2_bmx_go = go.Scatter(x =tdc2_bmx.index, y = tdc2_bmx['equity'], mode = 'lines',  name = 'TDC-V2-BMEx',  opacity = 0.5)
    ichlo_bmx_go = go.Scatter(x =ichlo_bmx.index, y = ichlo_bmx['equity'], mode = 'lines',  name = 'ICH-LO-BMEx',  opacity = 0.5)
    dmilo_bmx_go = go.Scatter(x =dmilo_bmx.index, y = ['equity'], mode = 'lines',  name = 'DMI-LO-BMEx',  opacity = 0.5)
    tt3_bmx_go = go.Scatter(x =tt3_bmx.index, y = ['equity'], mode = 'lines',  name = 'TT3-BMEx',  opacity = 0.5)
    tt4_bmx_go = go.Scatter(x =tt4_bmx.index, y = tt4_bmx['equity'], mode = 'lines',  name = 'TT4-BMEx',  opacity = 0.5)
    fn1_bmx_go = go.Scatter(x =fn1_bmx.index, y = fn1_bmx['equity'], mode = 'lines',  name = 'FN1-BMEx',  opacity = 0.5)
    ts2_bmx_go = go.Scatter(x =ts2_bmx.index, y = ts2_bmx['equity'], mode = 'lines',  name = 'TS2-BMEx',  opacity = 0.5)
    ts2v2_bmx_go = go.Scatter(x =ts2v2_bmx.index, y = ts2v2_bmx['equity'], mode = 'lines',  name = 'TS2v2-BMEx',  opacity = 0.5)
    mm3v3_bmx_go = go.Scatter(x =mm3v3_bmx.index, y = mm3v3_bmx['equity'], mode = 'lines',  name = 'MM3v3-BMEx',  opacity = 0.5)
    fn3v2_bmx_go = go.Scatter(x =fn3v2_bmx.index, y = fn3v2_bmx['equity'], mode = 'lines',  name = 'FN3v2-BMEx',  opacity = 0.5)
    ts2v3_bmx_go = go.Scatter(x =ts2v3_bmx.index, y = ts2v3_bmx['equity'], mode = 'lines',  name = 'TS2v3-BMEx',  opacity = 0.5)
    mm3v2_bmx_go = go.Scatter(x =mm3v2_bmx.index, y = mm3v2_bmx['equity'], mode = 'lines',  name = 'MM3v2-BMEx',  opacity = 0.5)
    tt1v2_bmx_go = go.Scatter(x =tt1v2_bmx.index, y = tt1v2_bmx['equity'], mode = 'lines',  name = 'TT1v2-BMEx',  opacity = 0.5)
    tt4v2_bmx_go = go.Scatter(x =tt4v2_bmx.index, y = tt4v2_bmx['equity'], mode = 'lines',  name = 'TT4v2-BMEx',  opacity = 0.5)
    fn3v2rev_bmx_go = go.Scatter(x =fn3v2rev_bmx.index, y = fn3v2rev_bmx['equity'], mode = 'lines',  name = 'FN3v2rev-BMEx',  opacity = 0.5)
    dmiv2_bmx_go = go.Scatter(x =dmiv2_bmx.index, y = dmiv2_bmx['equity'], mode = 'lines',  name = 'DMIv2-BMEx',  opacity = 0.5)
    dmiv2_bmx_go = go.Scatter(x =dmiv2_bmx.index, y = dmiv2_bmx['equity'], mode = 'lines',  name = 'DMIv2-BMEx',  opacity = 0.5)
    payday_bmx_go = go.Scatter(x = payday_bmx.index, y =  payday_bmx['equity'], mode = 'lines',  name = 'PAYDAY-BMEx',  opacity = 0.5)
    tt7v3_ftx_go = go.Scatter(x = tt7v3_ftx.index, y =  tt7v3_ftx['equity'], mode = 'lines',  name = 'TT7v3_FTX',  opacity = 0.5)
    tt8v3_ftx_go = go.Scatter(x = tt8v3_ftx.index, y =  tt8v3_ftx['equity'], mode = 'lines',  name = 'TT8v3_FTX',  opacity = 0.5)

    

    fig.add_trace(ttr_ftx_go)
    fig.add_trace(ttr_bmx_go)
    fig.add_trace(stm_bmx_go)
    fig.add_trace(nmm_bmx_go)
    fig.add_trace(occ_bmx_go)
    fig.add_trace(dmi_bmx_go)
    fig.add_trace(occ_ftx_go)
    fig.add_trace(tdc_ftx_go)
    fig.add_trace(ich_bmx_go)
    fig.add_trace(cha_bmx_go)
    fig.add_trace(vor_bmx_go)
    fig.add_trace(tdc_bmx_go)
    fig.add_trace(cha_ftx_go)
    fig.add_trace(vor2_bmx_go)
    fig.add_trace(dmi2_bmx_go)
    fig.add_trace(tdc2_bmx_go)
    fig.add_trace(ichlo_bmx_go)
    fig.add_trace(dmilo_bmx_go)
    fig.add_trace(tt3_bmx_go)
    fig.add_trace(tt4_bmx_go)
    fig.add_trace(prod_go)
    fig.add_trace(fn1_bmx_go)
    fig.add_trace(ts2_bmx_go)
    fig.add_trace(ts2v2_bmx_go)
    fig.add_trace(mm3v3_bmx_go)
    fig.add_trace(fn3v2_bmx_go)
    fig.add_trace(ts2v3_bmx_go)
    fig.add_trace(mm3v2_bmx_go)
    fig.add_trace(tt1v2_bmx_go)
    fig.add_trace(tt4v2_bmx_go)
    fig.add_trace(fn3v2rev_bmx_go)
    fig.add_trace(dmiv2_bmx_go)
    fig.add_trace(payday_bmx_go)
    fig.add_trace(tt7v3_ftx_go)
    fig.add_trace(tt8v3_ftx_go)
  
    
    if platform.system() == 'Linux':
        fig.write_image(file=f'email_data/total_equity-{yesterday.strftime("%Y-%m-%d")}.png', engine ='kaleido')
    main_datatable_7 = main_datatable.tail(7).round(decimals = 6)
    figtable = go.Figure(data=[go.Table(
    columnwidth = [75] + [50 for i in range(len(main_datatable_7.columns)-1)],
    header=dict(values=list(main_datatable_7.columns),
                align='left'),
    cells=dict(values = [main_datatable_7[col] for col in main_datatable_7.columns],
               align='left',
               font_size=14,
               height=32))])
    figtable.update_layout(width=2300, height=500,
    margin=go.layout.Margin(
        l=0, #left margin
        r=0, #right margin
        b=0
    ))

    if platform.system() == 'Linux':
        figtable.write_image(file=f'email_data/total_equity-table-{yesterday.strftime("%Y-%m-%d")}.png', engine ='kaleido') 


    return fig

def build_graph_test_usd_bots():
    layout = go.Layout(
        autosize=False,
        width=1200,
        height=800,
        xaxis= go.layout.XAxis(linecolor = 'black',
                            linewidth = 1,
                            mirror = True),

        yaxis= go.layout.YAxis(linecolor = 'black',
                            linewidth = 1,
                            mirror = True),

        margin=go.layout.Margin(
            l=50,
            r=50,
            b=100,
            t=100,
            pad = 4
        )
    )
    fig = go.Figure(layout = layout)
    fig.update_layout(
    title="Test Bots (USD-Denominated) Daily Equity Curves",
    legend_title="BOT",
    font=dict(
        family="Courier New, monospace",
        size=18,
        color="Black")
    )
    prod_go = go.Scatter(x = prod_usd.index, y = prod_usd['equity'], mode = 'lines',  name = 'Total Equity')
    prod_go['line'] = dict(width=5,color='#006400')

    stv3_usd_ftx_go = go.Scatter(x = stv3_usd_ftx.index, y = stv3_usd_ftx['equity'], mode = 'lines',  name = 'STV3-USD-FTX', opacity = 0.5)
    ts2v3_usd_ftx_go = go.Scatter(x = ts2v3_usd_ftx.index, y = ts2v3_usd_ftx['equity'], mode = 'lines',  name = 'TS2V3-USD-FTX', opacity = 0.5)
    tt5v3_usd_ftx_go = go.Scatter(x = tt5v3_usd_ftx.index, y = tt5v3_usd_ftx['equity'], mode = 'lines',  name = 'TT5V3-USD-FTX',  opacity = 0.5)
    tt6v3_usd_ftx_go = go.Scatter(x = tt6v3_usd_ftx.index, y = tt6v3_usd_ftx['equity'], mode = 'lines',  name = 'TTV3-USD-FTX',  opacity = 0.5)
    flockls_usd_ftx_go = go.Scatter(x = flockls_usd_ftx.index, y =  flockls_usd_ftx['equity'], mode = 'lines',  name = 'FLOCKLS_FTX',  opacity = 0.5)

    fig.add_trace(stv3_usd_ftx_go)
    fig.add_trace(ts2v3_usd_ftx_go)
    fig.add_trace(tt5v3_usd_ftx_go)
    fig.add_trace(tt6v3_usd_ftx_go)
    fig.add_trace(prod_go)
    fig.add_trace(flockls_usd_ftx_go)
 
    
    if platform.system() == 'Linux':
        fig.write_image(file=f'email_data/total_usd_equity-{yesterday.strftime("%Y-%m-%d")}.png', engine ='kaleido')
    prod_usd_datatable_7 = prod_usd_datatable.tail(7).round(decimals = 6)
    figtable = go.Figure(data=[go.Table(
    #columnwidth = [75] + [50 for i in range(len(prod_usd_datatable.columns)-1)],
    header=dict(values=list(prod_usd_datatable_7.columns),
                align='left'),
    cells=dict(values = [prod_usd_datatable_7[col] for col in prod_usd_datatable_7.columns],
               align='left',
               font_size=14,
               height=32))])
    figtable.update_layout(width=2300, height=500,
    margin=go.layout.Margin(
        l=0, #left margin
        r=0, #right margin
        b=0
    ))
    if platform.system() == 'Linux':
        figtable.write_image(file=f'email_data/total_usd_equity-table-{yesterday.strftime("%Y-%m-%d")}.png', engine ='kaleido') 
    return fig

def build_graph_live_bots():
    layout = go.Layout(
        autosize=False,
        width=1200,
        height=800,
        xaxis= go.layout.XAxis(linecolor = 'black',
                            linewidth = 1,
                            mirror = True),

        yaxis= go.layout.YAxis(linecolor = 'black',
                            linewidth = 1,
                            mirror = True),

        margin=go.layout.Margin(
            l=50,
            r=50,
            b=100,
            t=100,
            pad = 4
        )
    )
    fig = go.Figure(layout = layout)
    fig.update_layout(
    title="Laetitude Bots Daily Equity Curves",
    legend_title="BOT",
    font=dict(
        family="Courier New, monospace",
        size=18,
        color="Black")
    )
    mm3_go = go.Scatter(x = mm3_s3.index, y = mm3_s3['equity'], mode = 'lines',  name = 'MM3',  opacity = 0.5)
    tt1_go = go.Scatter(x =tt1_s3.index, y = tt1_s3['equity'], mode = 'lines',  name = 'TT1',  opacity = 0.5)
    fn3_go = go.Scatter(x =fn3_s3.index, y = fn3_s3['equity'], mode = 'lines',  name = 'FN3',  opacity = 0.5)
    live_go = go.Scatter(x =live_s3.index, y = live_s3['equity'], mode = 'lines',  name = 'Total Equity')
    live_go['line'] = dict(width=5,color='#006400')


    fig.add_trace(mm3_go)
    fig.add_trace(tt1_go)
    fig.add_trace(fn3_go)
    fig.add_trace(live_go)
    if platform.system() == 'Linux':
        fig.write_image(file=f'email_data/Laetitudebots-{yesterday.strftime("%Y-%m-%d")}.png', engine ='kaleido')

    laetitude_bots_7 = laetitude_bots.round(decimals = 6)
    figtable = go.Figure(data=[go.Table(
    header=dict(values=list(laetitude_bots_7.columns),
                align='left'),
    cells=dict(values = [laetitude_bots_7[col] for col in laetitude_bots_7.columns],
               align='left',
               font_size=16,
               height=32))])
    figtable.update_layout(width=1300)
    if platform.system() == 'Linux':
        figtable.write_image(file=f'email_data/Laetitudebots-table-{yesterday.strftime("%Y-%m-%d")}.png', engine ='kaleido') 

    
    
    return fig

def build_graph_swapoo_bots():
    layout = go.Layout(
        autosize=False,
        width=1200,
        height=800,
        xaxis= go.layout.XAxis(linecolor = 'black',
                            linewidth = 1,
                            mirror = True),

        yaxis= go.layout.YAxis(linecolor = 'black',
                            linewidth = 1,
                            mirror = True),

        margin=go.layout.Margin(
            l=50,
            r=50,
            b=100,
            t=100,
            pad = 4
        )
    )
    fig = go.Figure(layout = layout)
    fig.update_layout(
    title="Swapoo.ai Bots Performance (since 05.28.2021)",
    legend_title="BOT",
    font=dict(
        family="Courier New, monospace",
        size=18,
        color="Black")
    )
    mm3_s = mm3_s3['2021-05-28':]
    tt1_s = tt1_s3['2021-05-28':]
    tt3_s = tt3_bmx['2021-05-28':]
    tt4_s = tt4_bmx['2021-05-28':]
    fn3_s = fn3_s3['2021-05-28':]
    ts2_s = ts2_bmx

    tt1_s_go = go.Scatter(x = tt1_s.index, y = tt1_s['equity']/tt1_s['equity'].iloc[0], mode = 'lines',  name = 'TT1')
    tt3_s_go = go.Scatter(x =tt1.index, y = tt3_s['equity']/tt3_s['equity'].iloc[0], mode = 'lines',  name = 'TT3')
    tt4_s_go = go.Scatter(x =tt4_s.index, y = tt4_s['equity']/tt4_s['equity'].iloc[0], mode = 'lines',  name = 'TT4')
    mm3_s_go = go.Scatter(x =mm3_s.index, y = mm3_s['equity']/mm3_s['equity'].iloc[0], mode = 'lines',  name = 'MM3')
    ####
    fn3_s_go = go.Scatter(x =fn3_s.index, y = fn3_s['equity']/fn3_s['equity'].iloc[0], mode = 'lines',  name = 'FN3')
    go.Scatter(x =fn3_s.index, y = fn3_s['equity']/fn3_s['equity'].iloc[0], mode = 'lines',  name = 'FN3')
    go.Scatter(x =fn3_s.index, y = fn3_s['equity']/fn3_s['equity'].iloc[0], mode = 'lines',  name = 'FN3')
    go.Scatter(x =fn3_s.index, y = fn3_s['equity']/fn3_s['equity'].iloc[0], mode = 'lines',  name = 'FN3')
    go.Scatter(x =fn3_s.index, y = fn3_s['equity']/fn3_s['equity'].iloc[0], mode = 'lines',  name = 'FN3')

    # FN3v2Rev
    fn3v2rev_bmx_go =  go.Scatter(x =fn3v2rev_bmx.index, y = fn3v2rev_bmx['equity']/fn3v2rev_bmx['equity'].iloc[0], mode = 'lines',  name = 'FN3v2Rev')
    # MM3v2
    mm3v2_bmx_go = go.Scatter(x =mm3v2_bmx.index, y = mm3v2_bmx['equity'], mode = 'lines',  name = 'MM3v2')
    # TT1v2
    tt1v2_bmx_go = go.Scatter(x =tt1v2_bmx.index, y = tt1v2_bmx['equity'], mode = 'lines',  name = 'TT1v2') 
    # TT4v2
    tt4v2_bmx_go = go.Scatter(x =tt4v2_bmx.index, y = tt4v2_bmx['equity'], mode = 'lines',  name = 'TT4v2')

    fig.add_trace(tt1_s_go)
    fig.add_trace(tt3_s_go)
    fig.add_trace(tt4_s_go)
    fig.add_trace(mm3_s_go)
    fig.add_trace(fn3_s_go)
    #######
    fig.add_trace(fn3v2rev_bmx_go)
    fig.add_trace(mm3v2_bmx_go)
    fig.add_trace(tt1v2_bmx_go)
    fig.add_trace(tt4v2_bmx_go)
    if platform.system() == 'Linux':
        fig.write_image(file=f'email_data/Swapoobots-{yesterday.strftime("%Y-%m-%d")}.png', engine ='kaleido') 

    return fig

#########################################################################################################
#HTML LAYOUT
#########################################################################################################

app.layout = html.Div([
    ## BTC Denominated bots
    html.Div([
        dcc.Graph(id='line_graph_equity', figure = build_graph_test_btc_bots())
    ],className='nine columns'
    ),
    html.Div([DataTable(
    id              = 'equity_table',
    data            =  main_datatable.tail(7).round(decimals = 6).to_dict('records'),
    style_table= {
        'overflowX': 'scroll',
        },
    style_data      = {
        'whiteSpace': 'normal',
        'height': 'auto',
        },
    style_header    = {
        'backgroundColor'   : 'pink',
        'fontWeight'        : 'bold',
        'color'             : 'black',
        },
    style_cell      = {
        'textAlign'         : 'center',
        },
    ),]),

    html.Hr(),
    ## USD Denominated bots
    html.Div([
        dcc.Graph(id='line_graph_usd_equity', figure = build_graph_test_usd_bots())
    ],className='nine columns'
    ),
    html.Div([DataTable(
    id              = 'usd_equity_table',
    data            =  prod_usd_datatable.tail(7).round(decimals = 6).to_dict('records'),
    style_table= {
        'overflowX': 'scroll',
        },
    style_data      = {
        'whiteSpace': 'normal',
        'height': 'auto',
        },
    style_header    = {
        'backgroundColor'   : 'pink',
        'fontWeight'        : 'bold',
        'color'             : 'black',
        },
    style_cell      = {
        'textAlign'         : 'center',
        },
    ),]),

    ##
    #live btc denominated bots

    html.Div([
        dcc.Graph(id='line_graph_equity', figure = build_graph_live_bots())
    ],className='nine columns'),

    html.Div([DataTable(
    id              = 'live_bots_table',
    data            =  laetitude_bots.round(decimals = 6).tail(7).to_dict('records'),
    style_data      = {
        'whiteSpace': 'normal',
        'height': 'auto',
        },
    style_header    = {
        'backgroundColor'   : 'pink',
        'fontWeight'        : 'bold',
        'color'             : 'black',
        },
    style_cell      = {
        'textAlign'         : 'center',
        'minWidth'          : 1,
        'maxWidth'          : 15,
        'width'             : 2
        },
    ),]),

    html.Hr(),
      html.Div([
        dcc.Graph(id='line_graph_live', figure = build_graph_swapoo_bots())
    ],className='nine columns'
    ),
    html.Hr(),

])
layout = app.layout

# @app.callback(
#     Output('line_graph','figure'),
#     [Input('input_1','value'),
#      Input('input_2','value'),
#      Input('input_3','value')]
# )
# def build_graph():
#     # dff=df[(df['CUISINE DESCRIPTION']==first_cuisine)|
#     #        (df['CUISINE DESCRIPTION']==second_cuisine)|
#     #        (df['CUISINE DESCRIPTION']==third_cuisine)]
#     fig = px.line(df, x="date", y="Value", color='BOT', height=600, color_discrete_sequence = px.colors.qualitative.Dark24)
#     fig.update_layout(yaxis={'title':'In Percent (%)'} ,
#                       title={'text':'Bot Monitoring Dashboard',
#                       'font':{'size':28},'x':0.5,'xanchor':'center'})
#     return fig

#---------------------------------------------------------------

