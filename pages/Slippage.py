from os import remove
import flask
import datetime as dt
import plotly.express as px
import plotly.graph_objects as go
import dash_bootstrap_components as dbc
import matplotlib.pyplot as plt
from dash.dash_table import DataTable
import ccxt
import pandas as pd
import numpy as np
import math
import dash
import math
from dash import Dash, Input, Output, State, dcc, html, callback
from pyparsing import delimited_list

dash.register_page(__name__)
app = dash.Dash(external_stylesheets = [dbc.themes.BOOTSTRAP])
current = dt.datetime.now()
current_td = dt.timedelta(
    hours=current.hour,
    minutes=current.minute,
    seconds=current.second,
    microseconds=current.microsecond,
)
today = current - current_td
today.strftime("%Y-%m-%d")
yesterday = today - dt.timedelta(days=1)

df = pd.read_excel('data/recent_opened_position_slippage.xlsx',sheet_name = None, header = [0], parse_dates=['pht'])

cols_ftx_set = set([])
cols_bmx_set = set([])

for key, dataf in df.items():

    dataf['nostrat'] = dataf['label'].str.replace(key+'-', '')
    dataf['exch'] = dataf['label'].str[:3]
    if key[:3] == 'FTX':
        cols_ftx_set.update(x for x in dataf['nostrat'].unique())
    elif key[:3] == 'BMX':
        cols_bmx_set.update(x for x in dataf['nostrat'].unique())


cols_ftx = list(cols_ftx_set)
cols_bmx = list(cols_bmx_set)
cols_exch = {'FTX': cols_ftx, 'BMX': cols_bmx }
strats = df.keys()

#bmex_strats = ['FN3','MM3','TT1','TT3','TT4','FN3v2','TT1v2','TT4v2','TS2v2','MM3v2','MM3v3'] #disabled DMIv2
initial_dataframe = [['TOTAL'], ['Average Long Position'], ['Average Short Position'],['']]
bmex_df = initial_dataframe.copy()
ftx_df = initial_dataframe.copy()

exchanges_list = {'BMX':bmex_df,'FTX':ftx_df}
exchanges = {}
exchanges_calc = {}
for key, exch in exchanges_list.items():
    print(key)
    for strat in strats:
        print(strat)
        if key == strat[:3]:
            cols = cols_exch[key]
            exch.append([strat] + 
                    [df[strat].loc[df[strat]['nostrat'] == basket]['size'].iat[-1] if not df[strat].loc[df[strat]['nostrat'] == basket].empty else math.nan for basket in cols] + 
                    [df[strat].loc[df[strat]['nostrat'] == basket]['average'].iat[-1] if not df[strat].loc[df[strat]['nostrat'] == basket].empty else math.nan for basket in cols] )
            exch_df = pd.DataFrame( 
            data=  exch,
            columns=  ['Strategy'] + cols + [col + '-s' for col in cols])

        
    exchanges[key] = exch_df






lot = {'BTCUSD': 100,'ETHUSD':1,'LTCUSD':1,'XRPUSD':1,'ADABTC':1000,'ETHBTC':1000}

#def apply_lot(x, val):
#    return x/val
#for col in cols:
#    datatable[col] = datatable[col].apply(lambda x: x/ lot.get(col,1))
        
#ser_long = [(datatable[datatable[col]>0][col] * datatable[datatable[col]>0][col+'-s']).sum()/datatable[datatable[col]>0][col].sum() for col in cols]
#ser_short = [(datatable[datatable[col]<0][col] * datatable[datatable[col]<0][col+'-s']).sum()/datatable[datatable[col]<0][col].sum() for col in cols]

ser_long = []
ser_short = []

for key, datatable in exchanges.items():
    cols = cols_exch[key]
    ser_long = []
    ser_short = []
    for col in cols:
        val_long = np.NaN
        val_short = np.NaN
        data = datatable[[col, col+'-s']]
        # data = datatable[col]
        data_long = data[data[col]>0]
        data_short = data[data[col]<0]
        val_long = (data_long[col].mul(data_long[col+'-s'])).sum()/data_long[col].sum()
        val_short = (data_short[col].mul(data_short[col+'-s'])).sum()/data_short[col].sum()
        ser_long.append(val_long)
        ser_short.append(val_short)

    # datatable = datatable[['Strategy']+ [col for col in cols] + [col+'-s' for col in cols]]
    datatable = datatable[['Strategy']+ [col for col in cols]] #+ [col+'-s' for col in cols]]
    datatable.loc[datatable['Strategy'] == 'TOTAL'] = ['TOTAL'] + [datatable[col].sum() for col in cols] + [math.nan for x in range(0,len(cols))]
    datatable.loc[datatable['Strategy'] == 'Average Long Position'] = ['Average Long Position'] +  [sr for sr in ser_long] + [math.nan for x in range(0,len(cols))]
    datatable.loc[datatable['Strategy'] == 'Average Short Position'] = ['Average Short Position'] +  [sr for sr in ser_short] + [math.nan for x in range(0,len(cols))]
    exchanges_calc[key] = datatable



# for col in cols:
#     val_long = np.NaN
#     val_short = np.NaN
#     data = datatable[[col, col+'-s']]
#     data_long = data[data[col]>0]
#     data_short = data[data[col]<0]
#     val_long = (data_long[col].mul(data_long[col+'-s'])).sum()/data_long[col].sum()
#     val_short = (data_short[col].mul(data_short[col+'-s'])).sum()/data_short[col].sum()
#     ser_long.append(val_long)
#     ser_short.append(val_short)

# datatable_final = datatable[['Strategy']+ [col for col in cols]]
# datatable_final.loc[datatable_final['Strategy'] == 'TOTAL'] = ['TOTAL'] + [datatable_final[col].sum() for col in cols]
# datatable_final.loc[datatable_final['Strategy'] == 'Average Long Position'] = ['Average Long Position'] +  [sr for sr in ser_long]
# datatable_final.loc[datatable_final['Strategy'] == 'Average Short Position'] = ['Average Short Position'] +  [sr for sr in ser_short]

#####################################
#Slippage
opens_slpg = pd.read_excel("data/recent_comparison_orders.xlsx",  sheet_name = None, dtype={'Unnamed: 0': dt.datetime})
opening_price_bmx = pd.read_excel("data/latest-bitmex.xlsx", index_col = None, sheet_name = None, parse_dates=['datetime'],)
opening_price_ftx = pd.read_excel("data/latest-ftx.xlsx", index_col = None, sheet_name = None, parse_dates=['datetime'],)

#open prices dataframe
prices_processed_bmx = pd.DataFrame()
prices_processed_ftx = pd.DataFrame()

for key, temp_df in opening_price_bmx.items():
    temp_df.set_index('datetime', inplace = True)
    temp_df = temp_df[['open']]
    temp_df.columns = [key]
    prices_processed_bmx = pd.merge(temp_df, prices_processed_bmx, left_index=True, right_index=True, how='outer')

for key, temp_df in opening_price_ftx.items():
    temp_df.set_index('datetime', inplace = True)
    temp_df = temp_df[['open']]
    temp_df.columns = [key.replace('/','').replace('USDT','USD')]
    prices_processed_ftx = pd.merge(temp_df, prices_processed_ftx, left_index=True, right_index=True, how='outer')


for i in list(opens_slpg.keys()):
    opens_slpg[i] = opens_slpg[i].drop(['backtester','Unnamed: 5'], axis = 1)
    opens_slpg[i].columns = ['utc','pht','size','average','checks','label']
    opens_slpg[i] = opens_slpg[i].drop(['checks'], axis = 1)
    opens_slpg[i] = opens_slpg[i].iloc[2:]
    opens_slpg[i] = opens_slpg[i][opens_slpg[i]['size'] != 0]

def rename_open_cols_slp(table):
    for i in list(table.keys()):
        table[i].columns    = ['UTC', 'PHT', "Size", 'Average', 'Label']
        table[i].PHT        = table[i].PHT.apply(lambda x: '\''+ str(x)[2:])
        table[i].UTC        = table[i].UTC.apply(lambda x: '\''+ str(x)[2:])
        #table[i].Size       = table[i].Size.apply(lambda x: "{:,}".format(x))
        #table[i].Average    = table[i].Average.apply(lambda x: "{:,}".format(x))

def add_slippage_calculation_slp(table):
    for key, temp_df in table.items():
        temp_df['Price'] = [0 for i in range(0,len(temp_df))]
        temp_df['Size'] = temp_df['Size'].fillna(0)
        temp_df['Average'] = temp_df['Average'].fillna(0)

        for index, row in temp_df.iterrows():
            if row['Label'][:3] == 'FTX':
                temp_df.loc[index, 'Price'] = prices_processed_ftx.loc[pd.to_datetime(row['UTC'].replace("'22",'2022'))][row['Label'].split('-')[-1]]
            if row['Label'][:3] == 'BMX':
                temp_df.loc[index, 'Price'] = prices_processed_bmx.loc[pd.to_datetime(row['UTC'].replace("'22",'2022'))][row['Label'].split('-')[-1]]
        temp_df['Price'] = pd.to_numeric(temp_df['Price'],errors = 'coerce')
        temp_df['Average'] = pd.to_numeric(temp_df['Average'],errors = 'coerce')
        temp_df['Slippage (%)'] = 100*(temp_df['Price']- temp_df['Average'])/temp_df['Price'] *np.sign(temp_df['Size'])*-1

def calculate_average_slippage(table, avg_table):
    for key, temp_df in table.items():
        new_df = pd.DataFrame(data = [], columns = ['TOKEN', 'SIZE','AVG SLIPPAGE (%)'])
        tokens_ser = temp_df['Label'].str.split("-")
        tokens = []
        for token in tokens_ser:
            tokens.append(token[-1])
        tokens = pd.Series(tokens).unique()

        for token in tokens:
            token_ser = temp_df.copy()
            splitted_ser = temp_df['Label'].str.split("-")
            filter_ser = []
            for iter in splitted_ser:
                filter_ser.append(iter[-1] == token)
            token_ser = token_ser[filter_ser]

            new_token = [token, np.nan, np.nan]
            if not token_ser.empty:

                new_token = [token,token_ser['Size'].abs().sum(), (token_ser['Size'].abs() * token_ser['Slippage (%)']).sum()/token_ser['Size'].abs().sum()]


            temp = pd.DataFrame([new_token], columns = new_df.columns)
            new_df = pd.concat([new_df, temp])
            new_df = new_df.fillna(0)
        avg_table[key]  = new_df

    
        


rename_open_cols_slp(opens_slpg)
add_slippage_calculation_slp(opens_slpg)
avg_slippage_dict = {}
calculate_average_slippage(opens_slpg, avg_slippage_dict)
opensList_slpg = list(opens_slpg.keys())[::-1]
print(opensList_slpg)
avg_slippage_dict_list = list(avg_slippage_dict.keys())
app.layout = html.Div([
    ##############
    #Dropdown


    ##############
    #Data table
    html.H1("Open Position"),
    html.H2("Select Exchange"),
    dcc.Dropdown(id         = 'avg_long_short_dropdown',
                 placeholder = "-Select a State-",
                 options    =  [{"label" : i, "value": i} for i in ['FTX','BMX']],
                 value      = 'FTX'),
    html.Hr(),
    html.Div([DataTable(
    id              = 'avg_long_short_table',
    data            = exchanges_calc['BMX'].round(decimals =3).to_dict('records'),
    style_table= {
        'overflowX': 'scroll',
        },
    style_data      = {
        'whiteSpace': 'normal',
        'height': 'auto',
        },
    style_header    = {
        'backgroundColor'   : 'pink',
        'fontWeight'        : 'bold',
        'color'             : 'black',
        },
    style_cell      = {
        'textAlign'         : 'center',
        },
    ),]),


    html.Hr(),
    html.H4("Slippage"),

    ##############
    #Dropdown
    html.Hr(),
    html.H2("Select Strategy"),
    dcc.Dropdown(id         = 'strat_slippage_dropdown',
                 placeholder = "-Select a State-",
                 options    =  [{"label" : i, "value": i} for i in opensList_slpg],
                 value      = opensList_slpg[0]),
    ##############
    html.Hr(),
    html.H4("Slippage"),
    DataTable(
        id              = 'slippage_table',
        data            = opens_slpg[opensList_slpg[0]].round(decimals = 3).to_dict('records'),
        columns         = [{"name": i, "id": i} for i in opens_slpg[opensList_slpg[0]]] ,
        style_header    = {
            'backgroundColor'   : 'pink',   
            'fontWeight'        : 'bold',
            'color'             : 'black',
            },
        style_cell      = {
            'textAlign'         : 'center',
            'minWidth'          : 1,
            'maxWidth'          : 15,
            'width'             : 2
            },
    ),
    html.Hr(),
        DataTable(
        id              = 'slippage_avg_table',
        data            = avg_slippage_dict[avg_slippage_dict_list[0]].round(decimals =3).to_dict('records'),
        columns         = [{"name": i, "id": i} for i in avg_slippage_dict[avg_slippage_dict_list[0]]] ,
        style_header    = {
            'backgroundColor'   : 'pink',   
            'fontWeight'        : 'bold',
            'color'             : 'black',
            },
        style_cell      = {
            'textAlign'         : 'center',
            'minWidth'          : 1,
            'maxWidth'          : 15,
            'width'             : 2
            },
    ),
    html.Br(),
    html.Br(),
    html.Hr(),
    #############
])
layout = app.layout



@callback(
    Output('avg_long_short_table', 'data'),
    [Input('avg_long_short_dropdown', 'value')])
def change_avg_long_short(value):
    return exchanges_calc[value].round(decimals =3).to_dict('records')

@callback(
    Output('slippage_table', 'data'),
    [Input('strat_slippage_dropdown', 'value')])
def open_slippage(value):
    return opens_slpg[value].round(decimals =3).to_dict('records')

@callback(
    Output('slippage_avg_table', 'data'),
    [Input('strat_slippage_dropdown', 'value')])
def open_avg_slippage(value):
    return avg_slippage_dict[value].round(decimals =3).to_dict('records')
