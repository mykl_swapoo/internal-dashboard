import flask
import datetime as dt
import plotly.express as px
import plotly.graph_objects as go
import dash_bootstrap_components as dbc
import matplotlib.pyplot as plt
from dash.dash_table import DataTable
from ccxt import binance, bitmex, ftx
import ccxt
import pandas as pd
import numpy as np
import dash
import platform
import calendar

from dash import Dash, Input, Output, State, dcc, html
dash.register_page(__name__)

# server      = flask.Flask(__name__) # define flask app.server
# app         = Dash(__name__, 
#                    server = server,
#                    url_base_pathname='/equity/',
#                    external_stylesheets = [dbc.themes.BOOTSTRAP])
#show = pd.to_datetime(df.index[-1]).dayofweek + 1

app = dash.Dash(external_stylesheets = [dbc.themes.BOOTSTRAP])

current = dt.datetime.now()
current_td = dt.timedelta(
    hours=current.hour,
    minutes=current.minute,
    seconds=current.second,
    microseconds=current.microsecond,
)
today = current - current_td
today.strftime("%Y-%m-%d")
yesterday = today - dt.timedelta(days=1)

day_of_week = dt.datetime.today().isoweekday()
nearest_sunday = dt.datetime.today() - dt.timedelta(days=day_of_week)
start_sunday = nearest_sunday - dt.timedelta(days = 7)

df = pd.read_csv('data/correct_bot_balance_'+yesterday.strftime("%Y-%m-%d")+'.csv')
error_pnl = pd.read_csv('data/error_pnl_'+yesterday.strftime("%Y-%m-%d")+'.csv')
# corrected_bot_balance['date'] = pd.to_datetime(corrected_bot_balance['date'])
# corrected_bot_balance['date'] = corrected_bot_balance['date'].dt.date 
# corrected_bot_balance.set_index("date", inplace=True)


df = df.drop('Unnamed: 0', 1)
df = pd.concat([df, error_pnl['error_pnl']], axis=1)


df.set_index("date", inplace=True)

initial = {
    "date" : ["2020-10-10"],
    "TTR-FTX" : [0.005], "TTR-BMEx": [0.005], "STM-BMEx" : [0.005], 
    "NMM-BMEx" : [0.005], "OCC-BMEx": [0.00477], "DMI-BMEx": [0.005]
    }
initial = pd.DataFrame(initial)
initial.set_index("date", inplace=True)

strats = [
    "TTR-FTX", "TTR-BMEx", "STM-BMEx", "NMM-BMEx", "OCC-BMEx",
    "DMI-BMEx", "OCC-FTX", "DMI-FTX", "TDC-FTX", "ICH-BMEx",
    "CHA-BMEx", "VOR-BMEx", "TDC-BMEx", "CHA-FTX", "VOR-V2-BMEx",
    "DMI-V2-BMEx", "TDC-V2-BMEx", "ICH-LO-BMEx", "DMI-LO-BMEx", 
    "TT3-BMEx", 'TT4-BMEx', 'FN1-BMEx', 'TS2-BMEx', 'TS2v3-BMEx',
    'MM3v3-BMEx', 'TS2v2-BMEx', 'FN3v2-BMEx', 'FN3v2rev-BMEx',
    'MM3v2-BMEx', 'TT1v2-BMEx', 'TT4v2-BMEx', 'DMIv2-BMEx', 'PAYDAY-BMEx',
    'TT7v3-FTX', 'TT8v3-FTX'
]
pd.set_option('display.max_columns', None)
########################################################################################################
#PROD
prod = df[strats]
prod = pd.concat([initial, prod], ignore_index=False)

prod['deposits'] = 0
prod['deposits'].loc['2020-10-10'] = prod.loc['2020-10-10'][0:6].sum()  # initial capital
prod['deposits'].loc['2020-10-27'] = 0.015                              # funded OCC-FTX, DMI-FTX, TDC-FTX
prod['deposits'].loc['2020-11-03'] = 0.005                              # funded ICH-BMEx
prod['deposits'].loc['2020-12-09'] = 0.015                              # funded CHA-BMEx, VOR-BMEx, TDC-BMEx
prod['deposits'].loc['2021-01-17'] = 0.005                              # funded CHA-FTX
prod['deposits'].loc['2021-03-03'] = -0.00630284 + -0.00370938          # stopped STM-BMEx and OCC-BMEx
prod['deposits'].loc['2021-04-29'] = 0.00630284                         # funded VOR-V2-BMEx using STM-BMEx fund
prod['deposits'].loc['2021-05-03'] = 0.00507377                         # funded DMI-V2-BMEx
prod['deposits'].loc['2021-05-05'] = 0.00370938                         # funded TDC-V2-BMEx using OCC-BMEx
prod['deposits'].loc['2021-05-17'] = 0.005                              # funded ICH-LO-BMEx
prod['deposits'].loc['2021-05-20'] = 0.005                              # funded DMI-LO-BMEx
prod['deposits'].loc['2021-05-27'] = 0.005 - 0.00882454 + 0.00882454    # stopped TTR-BMEx. funded TT4-BMEx using capital and TT3-BMEx using TTR-BMEx fund
prod['deposits'].loc['2021-06-10'] = 0.55262                            # additional funds
prod['deposits'].loc['2021-06-11'] = 0                                  # stopped ICH-BMEx, funds transferred to FN1-BMEx
prod['deposits'].loc['2021-07-02'] = 0                                  # stopped DMIv2, funds transferred to TS2-BMEx
prod['deposits'].loc['2021-10-12'] = 0.0185 - 0.0185                    # transferred balance from Chaos v1 to NMM-BMEx
prod['deposits'].loc['2021-11-11'] = 0.01410001                         # funded TS2v3
prod['deposits'].loc['2021-11-15'] = 0.572179                           # trasnferred from FN3
prod['deposits'].loc['2021-11-17'] = 0.00205605 - 0.00205605            # transfer from TDCv2 to MM3v3 
prod['deposits'].loc['2021-11-18'] = 0.02080546 - 0.02080546            # transfer from ICH-LO to FN3v2
prod['deposits'].loc['2021-11-24'] = 0.001                              # remaining balance on TS2v2 account "dust"
prod['deposits'].loc['2021-11-25'] = 0.0158 - 0.0158                    # transfer from TDCv2 to TS2v2 (funded 11.25.2021)  
prod['deposits'].loc['2021-12-01'] = 0.0167 - 0.0167                    # transfer from TDCv2 to TS2v2 and TS2v3 
prod['deposits'].loc['2021-12-07'] = -0.0236 - 0.0002 + 0.001           # TS2v2 balance transferred FN3 for transfer to a new account with parked balance
prod['deposits'].loc['2021-12-08'] = 0.0236                             # transfer from FN3 (Thomas) to TS2v2
prod['deposits'].loc['2022-01-31'] = 0.272029 - 0.272029                #transferred for testing from FN1 to TDCv2
# prod['deposits'].loc['2022-02-03'] = (0.669415979 + 0.019386706 + 0.013108333 + 0.036202333 + 0.018485529 + 0.000877    #REBALANCING
#                                       + 0.376203751 + 0.01051679 + 0.3966272 + 0.015923 + 0.017025 + 0.869685946
#                                       + 0.007685294 + 0.934501 + 0.888837065 + 0.035045353 - 0.024686 + 0.74562605)
prod['deposits'].loc['2022-02-03'] = (0.669415979 + 0.019386706 + 0.013108333 + 0.036202333 + 0.018485529 + 0.000877 + 0.376203751 - 0.74562605 
                                      - 0.01051679 + 0.3966272 + 0.015923 - 0.024686 + 0.017025 + 0.869685946 + 0.934501 + 0.03504530 + 0.88883700
                                      + 0.007685294)
prod['deposits'].loc['2022-02-14'] = 0.028068 + 0.071932                # transfer from Thomas to DMIv2 and TDCv2 (parked balance)
prod['deposits'].loc['2022-02-17'] = 0.0141373 - 0.0141373              # transfer from TDCv2 to TS2v3
prod['deposits'].loc['2022-03-30'] = 0.05911956 - 0.047795 - 0.01132456 # transfer from TDCv2 to OCC-FTX and PAYDAY
prod['deposits'].loc['2022-05-26'] = -0.0041 - 0.0135 + 0.001634 + 0.00041000   # transfer from OCC-FTX to TS2v3 and STv3, from Chaosdev to TT7v3, from TTR-FTXdev to TT8v3
prod['deposits'].loc['2022-05-30'] = -0.00929                           # transfer from OCC-FTX to STv3
prod['deposits'].loc['2022-06-01'] = -0.011                             # transfer from OCC-FTX to STv3 

prod.loc['2021-03-03':]['STM-BMEx'] = np.nan
prod.loc['2021-03-03':]['OCC-BMEx'] = np.nan
prod.loc['2021-05-27':]['TTR-BMEx'] = np.nan

prod.loc['2021-06-10']['ICH-BMEx'] = 0.30746904

prod.fillna(0, inplace=True)
prod['balance'] = (
    prod['TTR-FTX'] + prod['TTR-BMEx'] + prod['STM-BMEx'] + prod['NMM-BMEx'] + prod['OCC-BMEx'] + prod['DMI-BMEx'] + prod['OCC-FTX'] + prod['DMI-FTX'] + prod['TDC-FTX'] + 
    prod['ICH-BMEx'] + prod['CHA-BMEx'] + prod['VOR-BMEx'] + prod['TDC-BMEx'] + prod['CHA-FTX'] + prod['VOR-V2-BMEx'] + prod['DMI-V2-BMEx'] + prod['TDC-V2-BMEx'] + prod['ICH-LO-BMEx'] +
    prod['DMI-LO-BMEx'] + prod['TT3-BMEx'] + prod['TT4-BMEx'] + prod['FN1-BMEx'] + prod['TS2-BMEx'] + prod['TS2v3-BMEx'] + prod['MM3v3-BMEx'] + prod['FN3v2-BMEx'] + prod['TS2v2-BMEx'] +
    prod['FN3v2rev-BMEx'] + prod['MM3v2-BMEx'] + prod['TT1v2-BMEx'] + prod['TT4v2-BMEx'] + prod['DMIv2-BMEx'] + prod['PAYDAY-BMEx'] + prod['TT7v3-FTX'] + prod['TT8v3-FTX']
)
prod['% ch'] = (prod['balance']/(prod['deposits']+prod['balance'].shift(1)) - 1) * 100
prod.fillna(0, inplace=True)
prod['equity'] = (1 + prod['% ch']/100).cumprod() 
prod['% ch'] = round(prod['% ch'], 2)
prod.index = pd.to_datetime(prod.index)

weekly = prod["equity"].copy()
weekly = pd.DataFrame(weekly)
weekly["day"] = pd.to_datetime(weekly.index).dayofweek
weekly = weekly[weekly["day"].isin([6])]
weekly["% change"] = round(weekly["equity"].pct_change() * 100, 2)
# weekly["% change"].loc["2022-04-03"] = -0.02
weekly = pd.DataFrame(weekly[weekly["day"] == 6][["equity", "% change"]])
weekly.reset_index(inplace=True)
weekly.dropna(inplace=True)

####################################################################
#USD BASED
strats = [
    'STv3-USD-FTX', 'TS2v3-USD-FTX', 'TT5v3-USD-FTX', 'TT6v3-USD-FTX', 'FLOCKLS-USD-FTX'
]

prod_usd = df[strats]
prod_usd = pd.concat([initial, prod_usd], ignore_index=False)

prod_usd['deposits'] = 0
# prod_usd = prod_usd['2022-05-26':]
prod_usd['deposits'].loc['2022-05-26'] = 118.0984049 + 40.2 + 3.189589137 + 13.19747944 # transfer from OCC-FTX to TS2v3 and STv3, from TTR-FTX to TT5v3 and TT6v3
prod_usd['deposits'].loc['2022-05-30'] = 281.6151091
prod_usd['deposits'].loc['2022-06-01'] = 344.97694
prod_usd['deposits'].loc['2022-06-17'] = 100.18406843 #transferred from xxx to FLOCKLS-USD-FTX


prod_usd.fillna(0, inplace=True)
prod_usd['balance'] = (
    prod_usd['STv3-USD-FTX'] + prod_usd['TS2v3-USD-FTX'] + prod_usd['TT5v3-USD-FTX'] + prod_usd['TT6v3-USD-FTX'] + prod_usd['FLOCKLS-USD-FTX']
)
prod_usd['% ch'] = (prod_usd['balance']/(prod_usd['deposits']+prod_usd['balance'].shift(1)) - 1) * 100
prod_usd.fillna(0, inplace=True)
prod_usd['equity'] = (1 + prod_usd['% ch']/100).cumprod() 
prod_usd['% ch'] = round(prod_usd['% ch'], 2)
prod_usd.index = pd.to_datetime(prod_usd.index)

weekly_usd = prod_usd["equity"]['2022-05-22':].copy()
weekly_usd = pd.DataFrame(weekly_usd)
weekly_usd["day"] = pd.to_datetime(weekly_usd.index).dayofweek
weekly_usd = weekly_usd[weekly_usd["day"].isin([6])]
weekly_usd["% change"] = round(weekly_usd["equity"].pct_change() * 100, 2)
# weekly_usd["% change"].loc["2022-04-03"] = -0.02
weekly_usd = pd.DataFrame(weekly_usd[weekly_usd["day"] == 6][["equity", "% change"]])
weekly_usd.reset_index(inplace=True)
weekly_usd.dropna(inplace=True)

weekly_bots_usd = prod_usd['2022-05-22':].copy()
weekly_bots_usd.index = pd.to_datetime(weekly_bots_usd.index)
weekly_bots_usd = weekly_bots_usd[weekly_bots_usd.index.dayofweek == 6]
weekly_bots_usd.dropna(inplace=True)
weekly_bots_usd = weekly_bots_usd[-2:]

# weekly_bots_usd.loc['2022-05-22']['TS2v3-USD-FTX'] = 40.2
# weekly_bots_usd.loc['2022-05-22']['TT5v3-USD-FTX'] = 3.189589137
# weekly_bots_usd.loc['2022-05-22']['TT6v3-USD-FTX'] = 13.19747944
# weekly_bots_usd.loc['2022-05-22']['STv3-USD-FTX'] = 118.0984049

# weekly_bots_usd.loc['2022-05-22']['balance'] = weekly_bots_usd[['STv3-USD-FTX', 'TS2v3-USD-FTX', 'TT5v3-USD-FTX', 'TT6v3-USD-FTX']].loc['2022-05-22'].sum(axis=0) #174.685473
weekly_bots_usd = weekly_bots_usd[-2:]

weekly_bots_usd = weekly_bots_usd.loc[:, (weekly_bots_usd != 0).any(axis=0)]

weekly_bots_usd.drop(columns=['% ch', 'equity'], inplace=True)
weekly_bots_usd.rename(columns={'balance': 'TOTAL'}, inplace=True)
weekly_bots_usd = weekly_bots_usd.T
weekly_bots_usd.columns = weekly_bots_usd.columns.strftime('%Y-%m-%d')
last = weekly_bots_usd.columns[-1]
weekly_bots_usd['% ch'] = round(weekly_bots_usd.pct_change(axis=1)[last] * 100, 2)

#weekly_bots_usd.loc['STv3-USD-FTX']['% ch'] = round(100 * (weekly_bots_usd.loc['STv3-USD-FTX']['2022-06-05']/(weekly_bots_usd.loc['STv3-USD-FTX']['2022-05-29']+ 281.6151091 + 344.97694 ) - 1), 2)
#weekly_bots_usd.loc['TOTAL']['% ch'] = round(100 * (weekly_bots_usd.loc['TOTAL']['2022-06-05']/(weekly_bots_usd.loc['TOTAL']['2022-05-29']+ 281.6151091 + 344.97694 ) - 1),2)
weekly_bots_usd.reset_index(inplace = True)


weekly_bots_usd_total = weekly_bots_usd.tail(1).copy()


######################################################################
#LIVE
live = df[["NMM-BMEx-Live", "STM-BMEx-Live", "TTR-BMEx-Live", "ICH-BMEx-Live", "MM3-BMEx-Live", "TT1-BMEx-Live", "FN3-BMEx-Live"]]
live = live['2021-01-27':]
live.index = pd.to_datetime(live.index)

live['deposits'] = 0
live['deposits'].loc['2021-01-27'] = live.loc['2021-01-27'][0:3].sum()  #initial capital
live['deposits'].loc['2021-02-04'] = 1.5                                #additional capital to NMM-BMEx-Live, STM-BMEx-Live and TTR-BMEx-Live
live['deposits'].loc['2021-02-15'] = 0                                  #stopped STM-BMEx-Live and transferred capital to ICH-BMEx-Live
live['deposits'].loc['2021-05-27'] = 0                                  #stopped NMM-BMEx-Live & TTR-BMEx-Live, and transferred capital to MM3 and TT1
live['deposits'].loc['2021-06-10'] = -0.55873211                        #rebalanced capital from ICH-BMEx-Live to Prod Bots, stopped ICH-BMEx-Live, transferred cap to FN3
live['deposits'].loc['2021-11-15'] = -0.572179                          #transferred to TDCv2
live['deposits'].loc['2021-12-07'] = 0.0236                             #transferred to Thomas for transfer to TS2v2
live['deposits'].loc['2021-12-08'] = -0.0236                            #transferred to TS2v2
live['deposits'].loc['2022-02-03'] = (-0.83985639 - 0.905879 + 0.009632471) #REBALANCING

live.fillna(0, inplace=True)
live['balance'] = (live['NMM-BMEx-Live'] + live['STM-BMEx-Live'] + live['TTR-BMEx-Live'] + live['ICH-BMEx-Live'] + live['TT1-BMEx-Live'] + live['MM3-BMEx-Live']
                    + live['FN3-BMEx-Live'])
live['% ch'] = (live['balance']/(live['deposits']+live['balance'].shift(1)) - 1) * 100
live.fillna(0, inplace=True)
live['equity'] = (1 + live['% ch']/100).cumprod() 
live['% ch'] = round(live['% ch'], 2)
live.index = pd.to_datetime(live.index)

weekly_live = live["equity"].copy()
weekly_live = pd.DataFrame(weekly_live)
weekly_live["day"] = pd.to_datetime(weekly_live.index).dayofweek
weekly_live = weekly_live[weekly_live["day"].isin([6])]
weekly_live["% change"] = round(weekly_live["equity"].pct_change() * 100, 2)
weekly_live = pd.DataFrame(weekly_live[weekly_live["day"] == 6][["equity", "% change"]])
weekly_live.reset_index(inplace=True)
weekly_live.dropna(inplace=True)

############################################################################
# Creating the test bots table
weekly_bots = prod.copy()
weekly_bots.index = pd.to_datetime(weekly_bots.index)
weekly_bots = weekly_bots[weekly_bots.index.dayofweek == 6]
weekly_bots.dropna(inplace=True)
weekly_bots = weekly_bots[-2:]
weekly_bots = weekly_bots.loc[:, (weekly_bots != 0).any(axis=0)]
weekly_bots.drop(columns=['% ch', 'equity'], inplace=True)
weekly_bots.rename(columns={'balance': 'TOTAL'}, inplace=True)
weekly_bots = weekly_bots.T
weekly_bots.columns = weekly_bots.columns.strftime('%Y-%m-%d')
last = weekly_bots.columns[-1]
weekly_bots['% ch'] = round(weekly_bots.pct_change(axis=1)[last] * 100, 2)

weekly_bots = weekly_bots.round(decimals = 6)

####################################
#Adjustments
####################################
#march 27 - april 3
#weekly_bots.loc['OCC-FTX'][weekly_bots.columns[2]] = (weekly_bots.loc['OCC-FTX'][weekly_bots.columns[1]]/(weekly_bots.loc['OCC-FTX'][weekly_bots.columns[0]] +0.047795))-1
#weekly_bots.loc['PAYDAY-BMEx'][weekly_bots.columns[2]] = weekly_bots.loc['PAYDAY-BMEx'][weekly_bots.columns[1]]/(weekly_bots.loc['PAYDAY-BMEx'][weekly_bots.columns[0]] +0.01132456)-1

# weekly_bots.drop(index = 'TDC-V2-BMEx', inplace= True)
# weekly_bots.drop(index = 'TOTAL', inplace = True)
# new_sum = weekly_bots.sum()
# print(new_sum)
# weekly_bots.loc['TOTAL'] = [new_sum[weekly_bots.columns[0]], 
#                            new_sum[weekly_bots.columns[1]], 
#                            new_sum[weekly_bots.columns[1]]/(new_sum[weekly_bots.columns[0]] +0.05911956 - 0.047795 - 0.01132456) - 1]

# weekly_bots.loc['TOTAL'][weekly_bots.columns[2]] = weekly_bots.loc['TOTAL'][weekly_bots.columns[1]]/(weekly_bots.loc['TOTAL'][weekly_bots.columns[0]] +0.05911956 - 0.047795 - 0.01132456)-1
weekly_bots.reset_index(inplace=True)
weekly_bots['% ch'] = weekly_bots['% ch'].round(decimals = 2)
weekly_bots.rename(columns = {'index':''}, inplace = True)

###########
#image 
weekly_img = prod["equity"].copy()
weekly_img = pd.DataFrame(weekly_img)
weekly_img["day"] = pd.to_datetime(weekly_img.index).dayofweek
weekly_img = weekly_img[weekly_img["day"].isin([6])]
weekly_img["% change"] = round(weekly_img["equity"].pct_change() * 100, 2)
# weekly["% change"].loc["2022-04-03"] = -0.02
weekly_img = pd.DataFrame(weekly_img[weekly_img["day"] == 6][["equity", "% change"]])
weekly_img.reset_index(inplace=True)
weekly_img.dropna(inplace=True)
plt.figure(figsize=(16,8))
weekly_img["% change"].plot(kind="bar")
plt.title("Test Bots Weekly Performance")
plt.ylabel("% change")
plt.xlabel("Week")
xlocs, xlabs = plt.xticks()
for i, v in enumerate(weekly_img["% change"]):
    if v > 0:
        space = 0.8
    else:
        space = -0.8
    plt.text(xlocs[i], v + space, str(v), horizontalalignment="center")
#plt.savefig(f'email_data/weekly_bots-{yesterday.strftime("%Y-%m-%d")}.png')

############################################################################
#Table for live bots 
weekly_live_bots = live.copy()
weekly_live_bots.index = pd.to_datetime(weekly_live_bots.index)
weekly_live_bots = weekly_live_bots[weekly_live_bots.index.dayofweek == 6]
weekly_live_bots.dropna(inplace=True)
weekly_live_bots = weekly_live_bots[-2:]
weekly_live_bots = weekly_live_bots.loc[:, (weekly_live_bots != 0).any(axis=0)]
weekly_live_bots.drop(columns=['% ch', 'equity'], inplace=True)
weekly_live_bots.rename(columns={'balance': 'TOTAL'}, inplace=True)
weekly_live_bots = weekly_live_bots.T
weekly_live_bots.columns = weekly_live_bots.columns.strftime('%Y-%m-%d')
last = weekly_live_bots.columns[-1]
weekly_live_bots['% ch'] = round(weekly_live_bots.pct_change(axis=1)[last] * 100, 2)
weekly_live_bots.reset_index(inplace=True)
weekly_live_bots.rename(columns = {'index':''}, inplace = True)

############################################################################
#Table for total performance
start_sunday = str(start_sunday.date())
nearest_sunday = str(nearest_sunday.date())

total_df = pd.read_csv('data/weekly-performance.csv')
total_df.set_index('index',inplace=True)
total_sum = total_df.sum()
total_df.loc['TOTAL'] = [total_sum[0], total_sum[1]]
total_df['% ch'] =  ((total_df[total_df.columns[1]] - total_df[total_df.columns[0]])/total_df[total_df.columns[0]]) * 100
total_df.reset_index(inplace=True)
total_df.rename(columns = {'index':''}, inplace = True)
total_df['% ch'] = total_df['% ch'].round(decimals = 2)

figtable = go.Figure(data=[go.Table(
    header=dict(values=list(total_df.columns), align='left'),
    cells=dict(values = [total_df[col] for col in total_df.columns],
               align='left',
               font_size=16,
               height=32))])
figtable.update_layout(width=1300)

figtableusd = go.Figure(data=[go.Table(
    header=dict(values=list(weekly_bots_usd_total.columns), align='left'),
    cells=dict(values = [weekly_bots_usd_total[col] for col in weekly_bots_usd_total.columns],
               align='left',
               font_size=16,
               height=32))])


if platform.system() == 'Linux':
    figtableusd.write_image(file=f'email_data/total-usd-table-{yesterday.strftime("%Y-%m-%d")}.png', engine ='kaleido')
    figtable.write_image(file=f'email_data/total-table-{yesterday.strftime("%Y-%m-%d")}.png', engine ='kaleido') 
#########################################################################################


figtable1 = go.Figure(data=[go.Table(
    header=dict(values=list(weekly_live_bots.columns), align='left'),
    cells=dict(values = [weekly_live_bots[col] for col in weekly_live_bots.columns],
               align='left',
               font_size=16,
               height=32))])
figtable1.update_layout(width=1300)
if platform.system() == 'Linux':
    figtable1.write_image(file=f'email_data/weekly_live_bots-table-{yesterday.strftime("%Y-%m-%d")}.png', engine ='kaleido') 
weekly_bots


#############################################################################
#functions for creating the graphs


# graph creation for all btc test bots
def build_bar_graph_test_btc_bots():
    fig = px.bar(weekly, y='% change', x='date', text_auto='.2f',
            title="Test Bots Weekly Performance")
    fig.update_traces(textfont_size=15, textangle=0, textposition="outside", cliponaxis=False,marker_color = 'blue')
    
    if platform.system() == 'Linux':
        fig.write_image(file=f'email_data/TestbotsPerformance-{yesterday.strftime("%Y-%m-%d")}.png', engine ='kaleido') 
    
    figtable2 = go.Figure(data=[go.Table(
    header=dict(values=list(weekly_bots.columns), align='left'),
    cells=dict(values = [weekly_bots[col] for col in weekly_bots.columns],
               align='left',
               font_size=16,
               height=32))])
    figtable2.update_layout(width=1300, height=1000)
    if platform.system() == 'Linux':
        figtable2.write_image(file=f'email_data/weekly_bots-table-{yesterday.strftime("%Y-%m-%d")}.png', engine ='kaleido') 
    return fig

def build_bar_graph_test_usd_bots():
    fig = px.bar(weekly_usd, y='% change', x='date', text_auto='.2f',
            title="Test Bots (USD-denominated) Weekly Performance")
    fig.update_traces(textfont_size=15, textangle=0, textposition="outside", cliponaxis=False,marker_color = 'blue')
    
    if platform.system() == 'Linux':
        fig.write_image(file=f'email_data/TestUsdbotsPerformance-{yesterday.strftime("%Y-%m-%d")}.png', engine ='kaleido') 
    
    figtable3 = go.Figure(data=[go.Table(
    header=dict(values=list(weekly_bots_usd.columns), align='left'),
    cells=dict(values = [weekly_bots_usd[col] for col in weekly_bots_usd.columns],
               align='left',
               font_size=16,
               height=32))])
    figtable3.update_layout(width=1300, height=500)
    if platform.system() == 'Linux':
        figtable3.write_image(file=f'email_data/weekly_usd_bots-table-{yesterday.strftime("%Y-%m-%d")}.png', engine ='kaleido') 
    return fig

#graph for laetitude bots
def build_bar_graph_laetitude_bots():
    fig = px.bar(weekly_live, y='% change', x='date', text_auto='.2f',
            title="Laetitude Weekly Performance")
    fig.update_traces(hovertext=12,textfont_size=15, textangle=0, textposition="outside", cliponaxis=False,marker_color = 'blue')
    if platform.system() == 'Linux':
        fig.write_image(file=f'email_data/LaetitudebotsPerformance-{yesterday.strftime("%Y-%m-%d")}.png', engine ='kaleido')
    return fig


#########################################################################################################
#HTML LAYOUT
#########################################################################################################

app.layout = html.Div([
    html.Hr(),
    html.Div([
        dcc.Graph(id='bar_graph_test_btc_bots', figure = build_bar_graph_test_btc_bots())
    ],className='nine columns'
    ),
    html.H2('Weekly Performance of the Test Bots'),
    html.Div([DataTable(
    id              = 'equity_table',
    data            =  weekly_bots.round(decimals = 8).to_dict('records'),
    style_table= {
        'overflowX': 'scroll',
        },
    style_data      = {
        'whiteSpace': 'normal',
        'height': 'auto',
        },
    style_header    = {
        'backgroundColor'   : 'rgba(0,0,255, 0.4)',
        'fontWeight'        : 'bold',
        'color'             : 'black',
        },
    style_cell      = {
        'textAlign'         : 'center',
        },
    ),]),
    html.Hr(),
        html.Div([
        dcc.Graph(id='bar_graph_test_usd_bots', figure = build_bar_graph_test_usd_bots())
    ],className='nine columns'
    ),
    html.H2('Test Bots (USD - denomination ) Weekly Performance'),
    html.Div([DataTable(
    id              = 'equity_table',
    data            =  weekly_bots_usd.round(decimals = 8).to_dict('records'),
    style_table= {
        'overflowX': 'scroll',
        },
    style_data      = {
        'whiteSpace': 'normal',
        'height': 'auto',
        },
    style_header    = {
        'backgroundColor'   : 'rgba(0,0,255, 0.4)',
        'fontWeight'        : 'bold',
        'color'             : 'black',
        },
    style_cell      = {
        'textAlign'         : 'center',
        },
    ),]),
    html.Hr(),
    html.Div([
        dcc.Graph(id='bar_graph_test_bots', figure = build_bar_graph_laetitude_bots())
    ]),
    
    # table here
    html.Hr(),
     html.H2('Weekly Performance of Laetitude Bots'),
    html.Div([DataTable(
    id              = 'weeky_live',
    data            =  weekly_live_bots.round(decimals = 6).to_dict('records'),
    style_table= {
        'overflowX': 'scroll',
        },
    style_data      = {
        'whiteSpace': 'normal',
        'height': 'auto',
        },
    style_header    = {
        'backgroundColor'   : 'rgba(0,0,255, 0.4)',
        'fontWeight'        : 'bold',
        'color'             : 'black',
        },
    style_cell      = {
        'textAlign'         : 'center',
        },
    ),]),
    # another table here
    
    html.Hr(),
    html.H2('Total'),
    html.Div([DataTable(
    id              = 'total_df',
    data            =  total_df.to_dict('records'),
    style_table= {
        'overflowX': 'scroll',
        },
    style_data      = {
        'whiteSpace': 'normal',
        'height': 'auto',
        },
    style_header    = {
        'backgroundColor'   : 'rgba(0,0,255, 0.4)',
        'fontWeight'        : 'bold',
        'color'             : 'black',
        },
    style_cell      = {
        'textAlign'         : 'center',
        },
    ),]),
    
    
])
layout = app.layout

# @app.callback(
#     Output('line_graph','figure'),
#     [Input('input_1','value'),
#      Input('input_2','value'),
#      Input('input_3','value')]
# )
# def build_graph():
#     # dff=df[(df['CUISINE DESCRIPTION']==first_cuisine)|
#     #        (df['CUISINE DESCRIPTION']==second_cuisine)|
#     #        (df['CUISINE DESCRIPTION']==third_cuisine)]
#     fig = px.line(df, x="date", y="Value", color='BOT', height=600, color_discrete_sequence = px.colors.qualitative.Dark24)
#     fig.update_layout(yaxis={'title':'In Percent (%)'} ,
#                       title={'text':'Bot Monitoring Dashboard',
#                       'font':{'size':28},'x':0.5,'xanchor':'center'})
#     return fig

#---------------------------------------------------------------

