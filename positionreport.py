from __future__ import annotations

import boto3
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import datetime as dt
import time
import pandas as pd

current = dt.datetime.now()
current_td = dt.timedelta(
    hours=current.hour,
    minutes=current.minute,
    seconds=current.second,
    microseconds=current.microsecond,
)
today = current - current_td
today.strftime("%Y-%m-%d")
yesterday = today - dt.timedelta(days=1)

def run_report(html_template, Mismatch):
    ses_client = boto3.client(
    "ses",
    region_name="ap-southeast-1",
    aws_access_key_id="AKIA3E3HTSYVCW6TSKBS",
    aws_secret_access_key="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",
    )
    msg = MIMEMultipart()
    
    msg['From'] = 'trading.monitor@swapoolabs.com'
    #msg['To'] = 'matthew.ochoa@swapoolabs.com'
    # if else here mismatch none
    msg['To'] = 'matthew.ochoa@swapoolabs.com, anshul.dang@swapoolabs.com, mykhal.mangada@swapoolabs.com, lorenzo.ajoc@swapoolabs.com, glen.macadaeg@swapoolabs.com, emjc.santos@swapoolabs.com'
    # if there is mismatch
    msg['Subject'] = f'Positions Status Report - {yesterday.strftime("%Y-%m-%d")}'
    part = MIMEText(html_template, 'html')
    msg.attach(part)

    logo = f'Position_Status_Report.png'
    fp = open(logo, 'rb')
    img = MIMEImage(fp.read())
    img.add_header('Content-ID', 'headerlogo')
    fp.close()
    msg.attach(img)

    result = ses_client.send_raw_email(Source = msg['From'], 
        Destinations = msg['To'].split(', '),
        #ReplyToAddresses= [ "matthew.ochoa@swapoolabs.com" ], 
        RawMessage = {'Data': msg.as_string(),})

mismatch = pd.read_csv('data/recent_mismatch.csv', index_col= 'Unnamed: 0')
position_report = mismatch[mismatch['position mismatch'] != 'None']
#if not position_report.empty:
items_mismatch =''
for row in mismatch.iterrows():
    mismatch_text = ''
    if row[1]['position mismatch'] != 'None':
        mismatch_text = f'''<td style = 'color:red;'>{row[1]['position mismatch']}</th>'''
    else: 
        mismatch_text = f'''<td style>{row[1]['position mismatch']}</th>'''
    temp_element = f'''<tr>
        <th>{row[0]}</th>
        {mismatch_text}
    </tr>'''
    items_mismatch += temp_element
items_mismatch +='''</table></br>
<p> Check the mismatch on the <a href = 'http://192.168.1.221:5000/dashboard'> dashboard </a></p>'''
position_mismatch_html = '''
    <style type="text/css">
    .myTable { background-color:#eee;border-collapse:collapse; }
    .myTable td, .myTable th { padding:5px;border:1px solid #000; }
    </style>
        <div style="max-width:50%;float: left;height:300px; background:rgba(0,171,169)"> <img src="cid:headerlogo"/></div>
    <br>
    <table class="myTable">
    <tr>
    <th style = 'background-color:rgb(0,171,169);color:white;width:50%;' >Strategy-Basket</th>
    <th style = 'background-color:rgb(0,171,169);color:white;width:50%;' >Position Mismatch</th>
    </tr>
'''
position_mismatch_html += items_mismatch

run_report(position_mismatch_html, True)
    
