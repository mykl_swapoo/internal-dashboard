import trading.datasets as tds
import pandas as pd
import datetime as dt
import math


today = dt.datetime.today()
first_date = today - dt.timedelta(days= 60)
#tokens = ['BNBUSDT','XRPUSDT','SOLUSDT','ADAUSDT','AVAXUSDT','DOGEUSDT', 'MATICUSDT','TRXUSDT', 'LINKUSDT', 'BCHUSDT', 'ATOMUSDT']
tokens_bmx = ['BTCUSD','ETHUSD','LTCUSD','XRPUSD','ADAM22','ETHM22','BNBUSD']

tokens_ftx = ['MATIC-PERP','LINK-PERP','XRP-PERP',
            'DOGE-PERP','OMG-PERP','BTC-PERP','LTC-PERP',
            'ETH-PERP','RAY-PERP','ATLAS-PERP','AAVE-PERP',
            'POLIS-PERP','MNGO-PERP','BADGER-PERP','TRYB-PERP','MCB-PERP','CRO-PERP','FTM-PERP','ADA-PERP','SOL-PERP','GMT-PERP','APE-PERP','AVAX-PERP','TRX-PERP','NEAR-PERP']
cols_exch = {'FTX':tokens_ftx,'BMX':tokens_bmx}
tokens_bmx = cols_exch['BMX']
tokens_ftx = cols_exch['FTX']
dict_df_bmx = {}
dict_df_ftx = {}
for token in tokens_bmx:
    print(token)  
    df = pd.DataFrame(tds.OHLCV.from_exchange('BITMEX', token, '1h', start=first_date, end=today))
    df.reset_index(inplace=True)
    df['datetime'] = df['datetime'].dt.tz_localize(None)
    dict_df_bmx[token] = df

with pd.ExcelWriter('latest-bitmex.xlsx') as writer:
    for key, value in dict_df_bmx.items(): 
        value.to_excel(writer, sheet_name=key.replace('M22','BTC'))
    
for token in tokens_ftx:
    print(token)
    df = pd.DataFrame(tds.OHLCV.from_exchange('FTX', token, '15m', start=first_date, end=today))
    df.reset_index(inplace=True)
    df['datetime'] = df['datetime'].dt.tz_localize(None)
    dict_df_ftx[token] = df
    
with pd.ExcelWriter('latest-ftx.xlsx') as writer:
    for key, value in dict_df_ftx.items():
        value.to_excel(writer, sheet_name=key.replace('/','').replace('-PERP','USD'))