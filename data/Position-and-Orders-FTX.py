import flask
import plotly.express as px
import plotly.graph_objects as go
import dash_bootstrap_components as dbc
import dash

from dash.dash_table import DataTable
from ccxt import binance, bitmex, ftx
from pandas import DataFrame, MultiIndex, Timestamp, read_csv, read_excel, read_json
from dash import Dash, Input, Output, State, dcc, html, callback


dataPath = "data/"
mismatchFileName    = dataPath + "recent_mismatch-ftx.csv"
openPosFileName     = dataPath + "recent_opened_position-ftx.xlsx"
## no orders yet compOrdFileName     = dataPath + "recent_comparison_orders-ftx.xlsx"
compPosFileName     = dataPath + "recent_comparison_position-ftx.xlsx"


def ftx_curr_ftx_price():
    for i in ftx().fetch_markets():
        if i['symbol'] == "BTC/USD":
            return i['info']['price']

def ftx_curr_bmx_price():
    for i in bitmex().fetch_markets():
        if i['symbol'] == "BTC/USD":
            return i['info']['markPrice']

# def curr_bnb_price():
#     for i in binance().fetch_markets():
#         if i['symbol'] == "BTC/USDT":
#             display(i)
#             # return i['info']['price']

def ftx_rename_open_cols(table):
    for i in list(table.keys()):
        table[i].columns    = ['UTC', 'PHT', "Size", 'Average', 'Label']
        table[i].PHT        = table[i].PHT.apply(lambda x: '\''+ str(x)[2:])
        table[i].UTC        = table[i].UTC.apply(lambda x: '\''+ str(x)[2:])
        table[i].Size       = table[i].Size.apply(lambda x: "{:,}".format(x))
        table[i].Average    = table[i].Average.apply(lambda x: "{:,}".format(x))
        table[i].set_index('PHT', inplace = True, drop = False)
        
def ftx_rename_order_comp_cols(table):
    for i in list(table.keys()):
        table[i].columns = MultiIndex.from_tuples((
            ('Time', 'local'), 
            ("Time", 'adj'), 
            ('Live', 'S'),
            ('Live', 'A'),
            ('Backtest', 'S'),
            ('Backtest', 'A'),
            ('Checks', ''),
            ('Label', '')
            ))
        table[i]['Time','UTC']          = table[i].Time.local.apply(lambda x: '\''+ str(x)[2:])
        table[i]['Time','PHT']          = table[i].Time.adj.apply(lambda x: '\''+ str(x)[2:])
        table[i]['Live','Size']         = table[i].Live.S.apply(lambda x: "{:,}".format(x))
        table[i]['Live','Average']      = table[i].Live.A.apply(lambda x: "{:,}".format(x))
        table[i]['Backtest','Size']     = table[i].Backtest.S.apply(lambda x: "{:,}".format(x))
        table[i]['Backtest','Average']  = table[i].Backtest.A.apply(lambda x: "{:,}".format(x))
        tc          = table[i].columns
        table[i]    = table[i][list(tc[-6:]) + list(tc[6:8])]

def ftx_rename_pos_comp_cols(table):
    for i in list(table.keys()):
        table[i].columns = MultiIndex.from_tuples((
            ('Time', 'local'), 
            ("Time", 'adj'), 
            ('Live', 'Symbol'),
            ('Live', 'Qt'),
            ('Live', 'AE'),
            ('Live', 'IO'),
            ('Backtest', 'Sz'),
            ('Backtest', 'Av'),
            ('Checks', ''),
            ('Label', '')
            ))
        table[i]['Time','UTC'] = table[i].Time.local.apply(lambda x: '\''+ str(x)[2:])
        table[i]['Time','PHT'] = table[i].Time.adj.apply(lambda x: '\''+ str(x)[2:])
        tc          = table[i].columns
        table[i]    = table[i][list(tc[-2:]) + list(tc[2:-2])]
        table[i]['Live','Qty']          = table[i].Live.Qt.apply(lambda x: "{:,}".format(x))
        table[i]['Live','AvgEntry']     = table[i].Live.AE.apply(lambda x: "{:,}".format(x))
        table[i]['Live','Open']         = table[i].Live.IO.apply(lambda x: "{:,}".format(x))
        table[i]['Backtest','Size']     = table[i].Backtest.Sz.apply(lambda x: "{:,}".format(x))
        table[i]['Backtest','Average']  = table[i].Backtest.Av.apply(lambda x: "{:,}".format(x))
        tc          = table[i].columns
        table[i]    = table[i][list(tc[:3]) + list(tc[-5:]) + list(tc[-7:-5])]

def ftx_table_dash_cols(table):
    sheetList       = list(table.keys())
    comparisonID    = ["".join([col for col in multi_col if col]) for multi_col in list(table[sheetList[0]].columns)]
    comparisonCols  = [{"name": list(col), "id": id_} for col, id_ in zip(list(table[sheetList[0]].columns), comparisonID)]
    
    return comparisonCols

def ftx_table_dash_data(table, colName):
    return [{k: v for k, v in zip(["".join([col for col in multi_col if col]) for multi_col in list(table[colName].columns)]
                                         , row)} for row in table[colName].values]

mismatches  = read_csv(mismatchFileName)
opens       = read_excel(openPosFileName, index_col = None, sheet_name = None)
#no orders yet ordComp     = read_excel(compOrdFileName, index_col = None, sheet_name = None, header = [0,1])
posComp     = read_excel(compPosFileName, index_col = None, sheet_name = None, header = [0,1])

mismatches.set_index(mismatches.columns[0], inplace = True, drop = False)
mismatches.columns = ['Instrument', 'Position', 'Order']
#no orders yet opensList,ordCompList,posCompList   = list(opens.keys()), list(ordComp.keys()), list(posComp.keys())
opensList,posCompList   = list(opens.keys()), list(posComp.keys())

#no orders yet ftx_rename_open_cols(opens), ftx_rename_order_comp_cols(ordComp), ftx_rename_pos_comp_cols(posComp)
ftx_rename_open_cols(opens), ftx_rename_pos_comp_cols(posComp)

#no orders yet ordCompSlice = ftx_table_dash_data(ordComp, ordCompList[0])
posCompSlice = ftx_table_dash_data(posComp, posCompList[0])


# the style arguments for the sidebar.
SIDEBAR_STYLE = {
    'position'          : 'fixed',
    'top'               : 0,
    'left'              : 0,
    'bottom'            : 0,
    'width'             : '10%',
    'padding'           : '10px 10px',
    'background-color'  : '#f8f9fa'
}

# the style arguments for the main content page.
CONTENT_STYLE = {
    'margin-left'       : '20%',
    'margin-right'      : '5%',
    'padding'           : '20px 10p'
}

TEXT_STYLE = {
    'textAlign'         : 'center',
    'color'             : '#191970'
}

CARD_TEXT_STYLE = {
    'textAlign'         : 'center',
    'color'             : 'red'
}

sidebar = html.Div([
    html.H2('Quant', style = TEXT_STYLE),
    html.Hr(),
    ],
    style = SIDEBAR_STYLE,
    )

content_first_row = dbc.Row([
    html.Hr(),
    html.H2("BTC/USD Price"),
    dbc.Col(
        dbc.Card([
                dbc.CardBody([
                        html.P(id           = 'card_title_1', 
                               children     = ['BMX-BTC/USD'], 
                               className    ='card-title',
                                style       = CARD_TEXT_STYLE),
                        html.H4(id          = 'card_text_1', 
                                children    = ftx_curr_bmx_price(), 
                                style       = CARD_TEXT_STYLE),
        ]),]),
        md=4
    ),
    dbc.Col(
        dbc.Card([
                dbc.CardBody([
                        html.P('FTX-BTC/USD', 
                               className    = 'card-title', 
                               style        = CARD_TEXT_STYLE),
                        html.H4(ftx_curr_ftx_price(), 
                                style       = CARD_TEXT_STYLE),
        ]),]),
        md=4
    ),
    dbc.Col(
        dbc.Card([
                dbc.CardBody([
                        html.P('BNB-BTC/USD', 
                               className    = 'card-title', 
                               style        = CARD_TEXT_STYLE),
                        html.H4(ftx_curr_bmx_price(), 
                                style       = CARD_TEXT_STYLE),
        ]),]),
        md=4
    ),
])
opens_reports = dbc.Container([
    html.Hr(),
    html.H2("Select Strategy"),
    dcc.Dropdown(id         = 'ftx_tables_dropdown',
                 placeholder = "-Select a State-",
                 options    =  [{"label" : i, "value": i} for i in opensList],
                 value      = opensList[0]),
    html.H4("Open Positions"),
    DataTable(
        id              = 'ftx_opens_table',
        data            = opens[opensList[0]].to_dict('records'),
        columns         = [{"name": i, "id": i} for i in opens[opensList[0]]] ,
        style_header    = {
            'backgroundColor'   : 'pink',   
            'fontWeight'        : 'bold',
            'color'             : 'black',
            },
        style_cell      = {
            'textAlign'         : 'center',
            'minWidth'          : 1,
            'maxWidth'          : 15,
            'width'             : 2
            },
    ),
    html.Hr(),
    html.H4("Positions Comparison"),
    DataTable(
        id              = 'posComp_table',
        data            = ftx_table_dash_data(posComp, posCompList[0]),
        columns         = ftx_table_dash_cols(posComp),
        style_data      = {
            'whiteSpace': 'normal',
            'height': 'auto',
            },
        style_header    = {
            'backgroundColor'   : 'pink',
            'fontWeight'        : 'bold',
            'color'             : 'black',
            },
        style_cell      = {
            'textAlign'         : 'center',
            'minWidth'          : 1,
            'maxWidth'          : 15,
            'width'             : 2
            },
    ),
    html.Hr(),
    html.H4("Orders Comparison"),
    # DataTable(
    #     id              = 'orderComp_table',
    #     data            = ftx_table_dash_data(ordComp, ordCompList[0]),
    #     columns         = ftx_table_dash_cols(ordComp),
    #     page_size       = 11,
    #     style_data      = {
    #         'whiteSpace': 'normal',
    #         'height': 'auto',
    #         },
    #     style_header    = {
    #         'backgroundColor'   : 'pink',
    #         'fontWeight'        : 'bold',
    #         'color'             : 'black',
    #         },
    #     style_cell      = {
    #         'textAlign'         : 'center',
    #         'minWidth'          : 1,
    #         'maxWidth'          : 15,
    #         'width'             : 2
    #         },
    # ),

])

mismatch_reports =  dbc.Container([
            html.H4("Mismatch Report"),
            html.Hr(),
            DataTable(
                id          = 'tbl',
                data        = mismatches.to_dict('records'),
                columns     = [{"name": i, "id": i} for i in mismatches.columns],
                fixed_rows  = {'headers': True},
                style_table = {'height': '300px', 'overflowY': 'auto'},
                style_header={
                                'backgroundColor'   : 'pink',
                                'fontWeight'        : 'bold',
                                'color'             : 'black',
                                },
                style_cell  = {
                                'textAlign': 'center',
                                'minWidth': 5,
                                'maxWidth': 55,
                                'width': 20
                                },
                style_data  = {'whiteSpace': 'normal', 'height': 'auto', 'lineHeight': '15px'},
                style_cell_conditional=[
                    {'if': {'column_id': mismatches.columns[0]}, 'textAlign': 'left'},
                    {
                        'if': {
                            'filter_query': '{{Order}} = {}'.format('Mismatch'),
                            },
                        'backgroundColor': 'rgba(255,68,73,0.1)',
                        'color': 'red'
                    },
                    {
                        'if': {
                            'filter_query': '{{Position}} = {}'.format('Mismatch'),
                            },
                        'backgroundColor': 'rgba(255,68,73,0.1)',
                        'color': 'red'
                    },
                ],
                style_as_list_view  = True,
            ),
            ])

content = html.Div(
    [
        html.H1(id          = 'H1',
                children    = 'Position and Orders Dashboard',
                style       = {'textAlign'      :'center',
                                'marginTop'     : 40,
                                'marginBottom'  : 40}),
        mismatch_reports,
        content_first_row,
        opens_reports,
        html.Hr()
    ],
    style=CONTENT_STYLE
)

# server      = flask.Flask(__name__) # define flask app.server
# app         = Dash(__name__, 
#                    server = server,
#                    url_base_pathname='/dashboard/',
#                    external_stylesheets = [dbc.themes.BOOTSTRAP])

dash.register_page(__name__)
# server      = flask.Flask(__name__) # define flask app.server
# app         = Dash(__name__, 
#                    server = server,
#                    url_base_pathname='/equity/',
#                    external_stylesheets = [dbc.themes.BOOTSTRAP])
app = dash.Dash(external_stylesheets = [dbc.themes.BOOTSTRAP])


app.layout  = html.Div([
    #sidebar, 
    content,
    dcc.Interval(
        id          = 'interval-component',
        interval    = 1e3, # in milliseconds
        n_intervals = 0
    )
    ])
layout = app.layout

## open pos table
@callback(
    Output('ftx_opens_table', 'data'),
    [Input('ftx_tables_dropdown', 'value')])
def ftx_opens_col_name(value):
    return opens[value].to_dict('records')

## order comp table
# @callback(
#     Output('ftx_orderComp_table', 'data'),
#     [Input('ftx_tables_dropdown', 'value')])
# def ftx_orders_col_name(value):
#     return ftx_table_dash_data(ordComp, value)

## position comp table
@callback(
    Output('ftx_posComp_table', 'data'),
    [Input('ftx_tables_dropdown', 'value')])
def ftx_posn_col_name(value):
    return ftx_table_dash_data(posComp, value)

# if __name__ == '__main__':
#     app.run_server()
