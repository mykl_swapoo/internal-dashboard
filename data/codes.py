import ccxt
import decimal
import numpy as np
import pandas as pd


"""fetch token precision"""
markets = ccxt.ftx().fetch_markets()
tokens = [
    'BTC/USDT',
    'ETH/USDT',
    'SOL/USD',
    'FTT/USD',
    'FTM/USDT',
    ###
    'BNB/USDT',
    'MATIC/USDT',
    'LINK/USDT',
    'XRP/USDT',
    'DOGE/USDT',
    ###
    'LTC/USDT',
    'TRX/USDT',
    'YFI/USD',
    'OMG/USDT',
    'SXP/USD',
    ###
    'RUNE/USD',
    'BCH/USDT',
    'KNC/USD',
    'LRC/USDT',
    'ENJ/USDT',
    ###
    'EUR/USDT',
    'BAT/USDT',
    'MKR/USD',
    'COMP/USD',
    'SNX/USDT',
    'CHR/USDT'
]
# tokens = ['BTC', 'ETH', 'LUNA', 'SOL', 'MATIC', 'AVAX', 'NEAR', 'DOT', 'XRP', 'ADA', 'LTC']
# tokens = ['BTC', 'ETH', 'FTM', 'ATOM', 'ADA', 'BNB', 'FTT', 'DOGE', 'XRP', 'LTC']
# tokens = ['BTC', 'ETH', 'BNB', 'LTC', 'XRP', 'TRX', 'LINK', 'BAT', 'OMG', 'MATIC']
step = []
for token in tokens:
    step.extend([x['precision'].get('amount') for x in markets if x.get('id') == f'{token}-PERP'])

token_steps = dict(zip(tokens, step))

def calculate_size(exchange, symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    if exchange == 'ftx':
        pos_size = total_balance * def_alloc
        orig_size = pos_size * contract_rate
        
        if with_var == True:
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            
            size = min(orig_size, var_size)
            
        else:
            size = orig_size
            
        
        sym = symbol.split('/')[0]    
        
        try:
            precision = decimal.Decimal(str(token_steps.get(sym)))

            if precision == decimal.Decimal(1.0):
                precision = decimal.Decimal(0.0)

            size = round(size, abs(precision.as_tuple().exponent))
            
#             print(f'orig {orig_size}, var {var_size}, final {size}')
        except Exception as e:
            pass
#             print(f'Precision error: {symbol}. Use unedited size.')
        
        return size
    
    if exchange == 'binance':
        min_size = {
            'btcusdt': 100,
            'ethusdt': 1,
            'xrpusdt': 1,
            'ltcusdt': 1,
            'adabtc': 10,
            'ethbtc': 0.01
        }
        # orig position size
        pos_size = total_balance * def_alloc
        if symbol.lower() == 'eth/btc':
            orig_size = round(pos_size * contract_rate, 2)
        else:
            orig_size = np.floor(pos_size * contract_rate)

        if symbol.lower().startswith('btc'):
            if orig_size < 100: 
                if orig_size % 100 >= 90:
                    orig_size = round(orig_size, -2)
                else: 
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 100)
        elif symbol.lower() == 'ada/btc':
            if orig_size < 10:
                if round(orig_size % 10, 2) >= 9:
                    orig_size = round(orig_size, -1)
                else:
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 10)
        #var pos size
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            if symbol.lower().startswith('btc'):
                if var_size < 100: 
                    if var_size % 100 >= 90:
                        var_size = round(var_size, -2)
                    else: 
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 100)
            else:
                if symbol.lower().endswith('usdt'):
                    if var_size < 1:
                        if round(var_size % 1, 2) >= 0.9:
                            var_size = round(var_size, 0)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
                else:
                    if symbol.lower() == 'ada/btc':
                        if var_size < 10:
                            if round(var_size % 10, 2) >= 9:
                                var_size = round(var_size, -1)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 10)
                    elif symbol.lower() == 'eth/btc':
                        if var_size < 1:
                            if round(var_size % 0.01, 3) >= 0.009:
                                var_size = round(var_size, 2)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 1)
            size = min(orig_size, var_size)
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
        return size
    
    if exchange == 'bitmex':
        min_size = {
        'btcusd': 100,
        'ethusd': 1,
        'xrpusd': 1,
        'ltcusd': 1,
        'adabtc': 10,
        'ethbtc': 0.01
        }
        # orig position size
        pos_size = total_balance * def_alloc
        orig_size = np.floor(pos_size * contract_rate)
        if symbol.lower().startswith('btc'):
            if orig_size < 100: 
                if orig_size % 100 >= 90:
                    orig_size = round(orig_size, -2)
                else: 
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 100)
        elif symbol.lower() == 'ada/btc':
            if orig_size < 10:
                if round(orig_size % 10, 2) >= 9:
                    orig_size = round(orig_size, -1)
                else:
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 10)
        #var pos size
        if with_var:
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = (var_pos_size * contract_rate)
            if symbol.lower().startswith('btc'):
                if var_size < 100: 
                    if var_size % 100 >= 90:
                        var_size = round(var_size, -2)
                    else: 
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 100)
            else:
                if 'usd' in symbol.lower():
                    if var_size < 1:
                        if round(var_size % 1, 2) >= 0.9:
                            var_size = round(var_size, 0)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
                else:
                    if symbol.lower() == 'ada/btc':
                        if var_size < 10:
                            if round(var_size % 10, 2) >= 9:
                                var_size = round(var_size, -1)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 10)
                    elif symbol.lower() == 'eth/btc':
                        if var_size < 1:
                            if round(var_size % 0.01, 3) >= 0.009:
                                var_size = round(var_size, 2)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 1)
            size = min(orig_size, var_size)
        else:
            size = orig_size
            
        return size

def trail_comp(direction, percentage, price, current_trail):
    if direction > 0:
        trail_stop = price * (1-percentage)
        if price < trail_stop:
            trail_stop = current_trail
        elif (trail_stop < current_trail):
            trail_stop = current_trail
    elif direction < 0:
        trail_stop = price * (1+percentage)
        if price > trail_stop:
            trail_stop = current_trail
        elif (trail_stop > current_trail):
            trail_stop = current_trail
    return trail_stop


def trail_comp2(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    return trail_stop


def trail_comp3(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        elif last_close < prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
        
                
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        elif last_close > prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail 
        else:
            trail_stop = current_trail
    return trail_stop

def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
        asset.create_order(f'{strategy_name}_{i}', v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f'{strategy_name}_{i}'):
            asset.cancel_order(f'{strategy_name}_{i}')

def get_win_rate(df, pairs):
    
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    
    pairs_dict = {}
    for pair in pairs:
        
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        try:
            win_pct = win_trades / total
            lose_pct = losing_trades / total
        except Exception as e:
            print(e)
            win_pct = 0
            lose_pct = 0
        
        print(f'{pair} Won trades count: {win_trades}')
        print(f'{pair} Lost trades count: {losing_trades}')
        print(f'{pair} Total trades count: {total}')
        print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
        
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
        
    return pairs_dict
    
    
def proc_wr(old_dict, get_win_rate_dict):
    
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        
        
        old_dict[pair] = new2
    
    return old_dict

import matplotlib.pyplot as plt
import numpy as np
def plot_equity(equity):
    _, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [1, 3]},
                         sharex=True, figsize=(5.5*(16/9), 5.5))
    running_max = np.maximum.accumulate(equity)
    drawdown = -100 * ((running_max - equity) / running_max)
    ax[0].fill_between(equity.index, 0, drawdown, alpha=0.3, color='r')
    ax[0].set_title('Drawdown')
    ax[1].plot(equity)
    ax[1].set_title('Total Balance')
    return ax

import quantstats as qs

def proc_metrics(rets_df):
    
    returns = pd.DataFrame(rets_df).sum(axis=1)
    rets = returns['2019':]
    eq = (1+rets).cumprod()
    eq = eq[eq.index.hour == 0]
    rets = eq.pct_change().fillna(0)

    rfr = 0.04

    years = list(rets.index.year.drop_duplicates())

    sort = []
    draw = []
    equi = []
    news = []
    data = pd.DataFrame(columns={'year', 'Sortino', 'MDD', 'Equity', 'New Sharpe'})

    for year in years:
        year = str(year)
        year_rets = rets[year]

        #sortino
        sortino = qs.stats.sortino(year_rets, rf=0.04, periods=365, annualize=True)
        if sortino > 10:
            sortino = 10

        #drawdowns
        dd = qs.stats.max_drawdown(year_rets)

        year_rets = pd.DataFrame(year_rets, columns=['rets'])
        year_rets['eq'] = (1 + year_rets['rets']).cumprod()
        year_rets['rets'] = year_rets['eq'].pct_change().fillna(0)
        year_rets['eq'] = (1 + year_rets['rets']).cumprod()

        #equity
        year_eq = year_rets['eq'].iloc[-1]

        #linearity
        year_rets['log'] = np.log(year_rets['eq'])
        x = np.arange(year_rets.index.size)
        fit = np.polyfit(x, year_rets['log'], deg=1)
        fit_function = np.poly1d(fit)
        linearity_val = np.sqrt(((year_rets['log'] - fit_function(x))**2).mean())
        cagr_val = qs.stats.cagr(year_rets['eq'].pct_change().fillna(0), rf=rfr, periods=year_rets.shape[0], compounded=True)
        new_sharpe_val = cagr_val/linearity_val

        sort.append(sortino)
        draw.append(dd)
        equi.append(year_eq)
        news.append(new_sharpe_val)

        data = data.append({'New Sharpe': round(new_sharpe_val, 2), 'Equity': round(year_eq, 2),
                            'MDD': round(dd, 2), 'Sortino': round(sortino, 2), 'year': year}, ignore_index=True)

    sort_avg = round(sum(sort)/len(sort),2)
    draw_avg = round(sum(draw)/len(draw),2)
    equi_avg = round(sum(equi)/len(equi),2)
    final_eq = round(eq.iloc[-1], 2)
    new_sharpe_avg = round(sum(news)/len(news), 2)

    data = data.append({'year': 'AVG', 'Sortino': sort_avg, 'MDD': draw_avg, 'Equity': equi_avg, 'New Sharpe': new_sharpe_avg}, ignore_index=True)
    data.set_index('year', inplace=True)
    data = data[['Equity', 'MDD', 'Sortino', 'New Sharpe']]


    eq_test = eq
    test_eq = pd.DataFrame(eq_test.resample('M', convention='start').asfreq())[:-1]
    test_eq.loc['2022-03-21 00:00:00'] = eq_test.iloc[-1]
    test_eq = test_eq.pct_change()
    test_eq.columns = ['pct_change']
    test_eq.index = pd.to_datetime(test_eq.index)
    test_eq = test_eq.dropna()

    year_list = ['2020', '2021', '2022']


    dict_perf = {}

    for _year in year_list:
        _data = test_eq[_year]
        months = len(_data)
        pos_months = len(_data[_data > 0].dropna())
        neg_months = len(_data[_data < 0].dropna())
        cons_neg_months = 0
        max_neg_months = 0
        for _month in range(len(_data)):
            if _data.iloc[_month].values < 0:
                cons_neg_months += 1
            else: 
                cons_neg_months = 0
            max_neg_months = max(max_neg_months, cons_neg_months)


        dict_perf[_year] = {
            'Total Months': months,
            'Positive Months': pos_months,
            'Negative Months': neg_months,
            'Max. neg. months': max_neg_months,
            'Positive%': f'{round((pos_months / months)*100, 2)}%',
        }
        
    month_mets = pd.DataFrame(dict_perf)#.T
    month_mets['Total'] = month_mets.sum(axis=1)
    month_mets = month_mets.T
    month_mets['Max. neg. months'].iloc[-1] = max(month_mets['Max. neg. months'][:-1])
    month_mets['Positive%'].iloc[-1] = f"{round((month_mets['Positive Months'].iloc[-1] / month_mets['Total Months'].iloc[-1])*100, 2)}%"

    return data, month_mets
#     display(data)

