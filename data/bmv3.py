from __future__ import annotations

import os
import re
import ccxt
import numpy as np
import pandas as pd
import datetime as dt
from pandas.tseries.offsets import DateOffset

pd.options.mode.chained_assignment = None 

import warnings
warnings.filterwarnings("ignore")

from laetitudebots.util.talib_method import talib_method
from laetitudebots.util.custom_method import custom_method
from laetitudebots.backtest import Backtest

from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import boto3

from dateutil.relativedelta import relativedelta
import datetime as dt
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
import multiprocessing as mp
import os
from itertools import product
import pickle
import copy

import quantstats as qs
from tqdm.notebook import tqdm
import ccxt

from laetitudebots.util.talib_method import talib_method
from laetitudebots.util.custom_method import custom_method
from laetitudebots.backtest import Backtest
from laetitudebots.backtest.optimizer import Optimizer
########################################################
##FTX STV3 START


#FTX STV3 END 
########################################################
##FTX FLOCKLS START
# BITMEX

# alloc = 0
class flocklstest:
    def flockls_run(self):
        slippage = 0.0005
        taker_fee = 0.0007
        maker_fee = 0.0001

        config = {
            'context': {
                'is_shared_balance' : True,
                'total_pos' : 0,
                'balance': 100,  
                'assets': {},
                
                'parameters': {
                    'ma_len': 8,
                    'rebalancing_bars': 1, 
                    
                },
            },
            'slippage': slippage,
            'time_interval': 1,
            'time_unit': 'd',
            'start_date': '2022-05-01 00:00:00',
            'end_date': '2022-08-01 00:00:00',    
            'exchange': 'ftx',
            'window_length' : 2
            }


        top_bot_rank = 5
        token_count = 1 / (top_bot_rank * 2)

        # btc, eth, sol, gmt, ape, avax, tx, near, ftm, ada

        pairs = {
            'BTC-PERP':  ['ftx', token_count],
            'ETH-PERP':  ['ftx', token_count],
            'SOL-PERP':  ['ftx', token_count],
            'GMT-PERP':  ['ftx', token_count],
            'APE-PERP':  ['ftx', token_count],
            'AVAX-PERP':  ['ftx', token_count],
            'TRX-PERP':  ['ftx', token_count],
            'NEAR-PERP':  ['ftx', token_count],
            'FTM-PERP':  ['ftx', token_count],
            'ADA-PERP':  ['ftx', token_count],
            
        #     'BTC-USD':  ['ftx', token_count],
        #     'ETH-USD':  ['ftx', token_count],
        #     'SOL-USD':  ['ftx', token_count],
        #     'GMT-USD':  ['ftx', token_count],
        #     'APE-USD':  ['ftx', token_count],
        #     'AVAX-USD':  ['ftx', token_count],
        #     'TRX-USD':  ['ftx', token_count],
        #     'NEAR-USD':  ['ftx', token_count],
        #     'FTM-USD':  ['ftx', token_count],
        #     'ADA-USD':  ['ftx', token_count],
        }


        for i,v in pairs.items():
            config['context']['assets'][i] = {
                'exchange': v[0],
                'maker_fee': maker_fee,
                'taker_fee': taker_fee,
                'allocation' : v[1],
                
                'reb_count': 0,
                'top_bot_rank': 5,
                }

        from dateutil.relativedelta import relativedelta
        from laetitudebots.util.talib_method import talib_method

        import datetime as dt
        import pandas as pd
        import numpy as np
        import math
        import ccxt
        import itertools
        import warnings
        import decimal

        warnings.filterwarnings("ignore")


        def trail_comp(direction, percentage, price, current_trail):
            if direction > 0:
                trail_stop = price * (1-percentage)
                if price < trail_stop:
                    trail_stop = current_trail
                elif (trail_stop < current_trail):
                    trail_stop = current_trail
            elif direction < 0:
                trail_stop = price * (1+percentage)
                if price > trail_stop:
                    trail_stop = current_trail
                elif (trail_stop > current_trail):
                    trail_stop = current_trail
            return trail_stop


        def trail_comp2(direction, percentage, last_close, prev_close, current_trail):
            
            if direction > 0:
                if last_close > prev_close:
                    trail_stop = current_trail * (1+percentage)
                    if last_close < trail_stop:
                        trail_stop = current_trail
                else:
                    trail_stop = current_trail
            elif direction < 0:
                if last_close < prev_close:
                    trail_stop = current_trail * (1-percentage)
                    if last_close > trail_stop:
                        trail_stop = current_trail
                else:
                    trail_stop = current_trail
            return trail_stop


        def trail_comp3(direction, percentage, last_close, prev_close, current_trail):
            
            if direction > 0:
                if last_close > prev_close:
                    trail_stop = current_trail * (1+percentage)
                    if last_close < trail_stop:
                        trail_stop = current_trail
                elif last_close < prev_close:
                    trail_stop = current_trail * (1+percentage)
                    if last_close < trail_stop:
                        trail_stop = current_trail
                else:
                    trail_stop = current_trail
                
                        
            elif direction < 0:
                if last_close < prev_close:
                    trail_stop = current_trail * (1-percentage)
                    if last_close > trail_stop:
                        trail_stop = current_trail
                elif last_close > prev_close:
                    trail_stop = current_trail * (1-percentage)
                    if last_close > trail_stop:
                        trail_stop = current_trail 
                else:
                    trail_stop = current_trail
            return trail_stop

        def proc_order(ord_type, ord_size, side, price=None, trigger=None):
            order = {
                    'order_type' : ord_type,
                    'size' : ord_size,
                    'side' : side,
                    }
            if ord_type == 'stop':
                if trigger is not None:
                    order['trigger_price'] = trigger 
            
            if ord_type == 'limit':
                if price is not None:
                    order['price'] = price
            
            return order

        def execute_order(strategy_name, asset, **orders):
            for i,v in orders.items():
                asset.create_order(f'{strategy_name}_{i}', v)

        #         print(f'Execute order i: {i}')
        #         print(f'Execute order v: {v}')
                
        def cancel_order(strategy_name, asset, *orders):
            for i in orders:
                if asset.orders.get(f'{strategy_name}_{i}'):
        #             print(f'Cancel order i: {i}')
                    asset.cancel_order(f'{strategy_name}_{i}')


        """fetch token precision"""
        def get_ftx_precision(denom):
            markets = ccxt.ftx().fetch_markets()
            token_steps = {}
            for market in markets:
                if market.get('id').endswith(denom):
                    key = market.get('base')

                    token_steps[key] = market.get('precision').get('amount')
            return token_steps

        token_steps = get_ftx_precision('PERP')


        def calculate_size(exchange, symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
            if exchange == 'ftx':
                pos_size = total_balance * def_alloc
                orig_size = pos_size * contract_rate
                
                if with_var == True:
                    invalidation = abs((close-kwargs['stop'])/close)
                    var_pos_size = (total_balance * kwargs['risk']) / invalidation
                    var_size = var_pos_size * contract_rate
                    
                    size = min(orig_size, var_size)
                    
                else:
                    size = orig_size
                    
                
                sym = symbol.split('/')[0].split('-')[0]
                
                try:
                    precision = decimal.Decimal(str(token_steps.get(sym)))

                    if precision == decimal.Decimal(1.0):
                        precision = decimal.Decimal(0.0)

                    size = round(size, abs(precision.as_tuple().exponent))
                    
                except Exception as e:
                    pass
                
                return size
            
            if exchange == 'bitmex':
                min_size = {
                'btcusd': 100,
                'ethusd': 1,
                'xrpusd': 1,
                'ltcusd': 1,
                'adabtc': 10,
                'ethbtc': 0.01
                }
                # orig position size
                pos_size = total_balance * def_alloc
                orig_size = np.floor(pos_size * contract_rate)
                if symbol.lower().startswith('btc'):
                    if orig_size < 100: 
                        if orig_size % 100 >= 90:
                            orig_size = round(orig_size, -2)
                        else: 
                            orig_size = 0
                    else:
                        orig_size = orig_size - (orig_size % 100)
                elif symbol.lower() == 'ada/btc':
                    if orig_size < 10:
                        if round(orig_size % 10, 2) >= 9:
                            orig_size = round(orig_size, -1)
                        else:
                            orig_size = 0
                    else:
                        orig_size = orig_size - (orig_size % 10)
                #var pos size
                if with_var:
                    invalidation = abs((close-kwargs['stop'])/close)
                    var_pos_size = (total_balance * kwargs['risk']) / invalidation
                    var_size = (var_pos_size * contract_rate)
        #             print(orig_size, 'orig', var_size, 'vars')
                    if symbol.lower().startswith('btc'):
                        if var_size < 100: 
                            if var_size % 100 >= 90:
                                var_size = round(var_size, -2)
                            else: 
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 100)
                    else:
                        if 'usd' in symbol.lower():
                            if var_size < 1:
                                if round(var_size % 1, 2) >= 0.9:
                                    var_size = round(var_size, 0)
                                else:
                                    var_size = 0
                            else:
                                var_size = var_size - (var_size % 1)
                        else:
                            if symbol.lower() == 'ada/btc':
                                if var_size < 10:
                                    if round(var_size % 10, 2) >= 9:
                                        var_size = round(var_size, -1)
                                    else:
                                        var_size = 0
                                else:
                                    var_size = var_size - (var_size % 10)
                            elif symbol.lower() == 'eth/btc':
                                if var_size < 1:
                                    if round(var_size % 0.01, 3) >= 0.009:
                                        var_size = round(var_size, 2)
                                    else:
                                        var_size = 0
                                else:
                                    var_size = var_size - (var_size % 1)
                    size = min(orig_size, var_size)
                else:
                    size = orig_size
                return size




        def initialize_ftx(factors, config):
            ma_len = config['parameters']['ma_len']
            
            """
            Step 1: Obtain the EMA / SMA of the individual prices of instrument of lookback ‘Y’
            """
            factors['ma'] = talib_method(method='EMA', timeperiod=ma_len, args=[factors.close])
            
            """
            Step 2: At every point obtain the normalized price of the instrument by dividing the 
            instrument EMA/SMA price by the average of EMA/SMA prices of all instruments at that point
            """

            factors['avg_ma'] = talib_method(method='EMA', timeperiod=ma_len, args=[factors.ma])
            factors['norm1'] = factors['ma'] / factors['avg_ma']
            
            """
            Step 3: Find the difference between the normalized price of instrument against all the 
            other instruments normalized price and store it in array (example ‘differencearray’)
            """
            
            token_list = list(config['assets'].keys()) #list(pairs.keys())
            comb_list = []
            
            for subset in itertools.permutations(token_list, 2):
                comb_list.append(list(subset))
                    
            for i in comb_list:
                token1 = i[0].split('/')[0]
                token2 = i[1].split('/')[0]
                factors.ohlcv[('diffarray', f'{token1}-{token2}')] = factors.ohlcv[('norm1', i[0])] - \
                                                                                    factors.ohlcv[('norm1', i[1])]
            
            """
            Step 4: Normalize the ‘differencearray’ by dividing  each value of the ‘differencearray’ 
            by (differencearray’.max – differencearray’.min)
            """
            
            list_tokens = list(factors.ohlcv['diffarray'].columns)
            set_tokens = {x.split('-')[0] for x in list_tokens}
            
            for i in list_tokens:
                factors.ohlcv[('minmax', i)] = factors.ohlcv[('diffarray'), i].rolling(5).max() - \
                                    factors.ohlcv[('diffarray'), i].rolling(5).min()
            
            factors['normdiffarray'] = factors['diffarray'] / factors['minmax']
            
            """
            Step 5: Sum the normalized differencearray. Do this for all instruments and store it in array (example ‘vals’)
            """
            
            for i in set_tokens:
                curr_set = []
                for v in factors['diffarray']:
                    if i == v.split('-')[0]:
                        curr_set.append(v)
                        
                factors.ohlcv[('sumdiffarray', i+'-PERP')] = abs(factors['normdiffarray'][curr_set].sum(axis=1))
        #         factors.ohlcv[('sumdiffarray', i+'-USD')] = abs(factors['normdiffarray'][curr_set].sum(axis=1))
                
            """
            Step 6: Rank the array created above (‘vals’) and sell the top X (5 for example) positive ones and buy the bottom X (5 for example) negative (or least positive) ones
            """
            factors['rank'] = factors['sumdiffarray'].rank(axis=1, method='max')


        def process_ftx(window, assets, context):
            total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
            total_balance = total_unrealised_pnl + context['balance']
            
            for symbol, asset in assets.items():
        #         asset.reset_trackers()
                
                prev_bar = window.previous(symbol, 2)
                last_bar = window.latest(symbol)
                
                settings = context['assets'][symbol]
                
                position = asset.position
            
                """
                variable entry condition
                """
                
                sumdiff_cond = last_bar.sumdiffarray != 0
                
                token_count = len(context['assets'].keys())
                top_rank_count = int(token_count - settings['top_bot_rank'])
                bot_rank_count = int(token_count - (token_count - settings['top_bot_rank']))
                
                lg_sig = last_bar.rank > top_rank_count and sumdiff_cond
                st_sig = last_bar.rank <= bot_rank_count and sumdiff_cond
                
                pos_close = bot_rank_count < last_bar.rank <= top_rank_count
                
                stop = 0
                size = calculate_size('ftx', symbol, total_balance, last_bar.close, settings['allocation'], 
                                    asset.get_contract_rate(last_bar.close), 
                                    with_var=False, stop=stop, risk=0)

                print("WINDOW")
                print(window)
                print(f'Symbol: {symbol}')
                print(f"Window Latest - Timestamp: {last_bar.timestamp}")
                print(f"Window Latest - Open: {last_bar.open}")
                print(f"Window Latest - High: {last_bar.high}")
                print(f"Window Latest - Low: {last_bar.low}")
                print(f"Window Latest - Close: {last_bar.close}")
                print(f"Window Latest - rank: {last_bar.rank}")

                print(f"Long Entry: {lg_sig}")
                print(f"Short Entry: {st_sig}")

                print(f"Window Latest Size: {size}")
                print("######## \n")
                if last_bar.timestamp > pd.to_datetime('2022-06-19'):
                    if position.size != 0:
                        settings['reb_count'] += 1
                    if position.size == 0:
                        if lg_sig:
                            order = proc_order('market', size, 'buy')
                            execute_order('rider', asset, long=order)
                            
                            settings['reb_count'] = 0

            #                 asset.track_long_entry()
                            print('Order placed: Long.')
                            print(f'Order size: {size}')
                            print("Order: ", order)
                            print("Symbol: ", symbol)

                        elif st_sig:

                            order = proc_order('market', size, 'sell')
                            execute_order('rider', asset, short=order)

                            settings['reb_count'] = 0

            #                 asset.track_short_entry()
                            print('Order placed: Short.')
                            print(f'Order size: {size}')
                            print("Order: ", order)
                            print("Symbol: ", symbol)
                            
                    elif position.size > 0:
                        if st_sig:
                            order = proc_order('market', size + abs(position.size), 'sell')
                            execute_order('rider', asset, short=order)
                            
                            settings['reb_count'] = 0

            #                 asset.track_long_exit()
            #                 asset.track_short_entry()
                            print('Order placed: Reversal Long.')
                            print(f'Order size: {size}')
                            print("Order: ", order)
                            print("Symbol: ", symbol)
                            
                        elif pos_close:
                            order = proc_order('market', abs(position.size), 'sell')
                            execute_order('rider', asset, close_long=order)
                            
                            settings['reb_count'] = 0

            #                 asset.track_long_exit()
                            print('Order placed: Close Long.')
                            print("Order: ", order)
                            print("Symbol: ", symbol)

                    elif position.size < 0:
                        if lg_sig:
                            order = proc_order('market', size + abs(position.size), 'buy')
                            execute_order('rider', asset, long=order)
                            
                            settings['reb_count'] = 0

            #                 asset.track_short_exit()
            #                 asset.track_long_entry()

                            print('Order placed: Reversal Short.')
                            print(f'Order size: {size}')
                            print("Order: ", order)
                            print("Symbol: ", symbol)

                        elif pos_close:
                            order = proc_order('market', abs(position.size), 'buy')
                            execute_order('rider', asset, close_short=order)
                            
                            settings['reb_count'] = 0

            #                 asset.track_short_exit()

                            print('Order placed: Close Short.')
                            print("Order: ", order)
                            print("Symbol: ", symbol)
                    
                    bars = context['parameters']['rebalancing_bars']
                    
                    if (
                        (settings['reb_count'] % bars == 0) and 
                        (settings['reb_count'] != 0) and 
                        (position.size != 0)
                        ):
                        
                        curr_size = abs(position.size)
                        size_diff = abs(size - curr_size)
                        token_prec = symbol.split('/')[0].split('-')[0]
                        
                        if size_diff > token_steps.get(token_prec):
                            if position.size > 0:
                                ### if current size > calculated size, reduce position
                                if curr_size > size:
                                    order = proc_order('market', curr_size - size, 'sell')
                                    execute_order('rider', asset, reduce_long=order)

                                    print('Order placed: Reduce Long.')
                                    print("Current size: ", curr_size)
                                    print("Calculated size: ", size)
                                    print("Size diff: ", abs(curr_size - size))
                                    print("Order: ", order)
                                    print("Symbol: ", symbol)

                                ### if current size > calculated size, add position
                                else:
                                    order = proc_order('market', size - curr_size, 'buy')
                                    execute_order('rider', asset, add_long=order)

                                    print('Order placed: Add Long.')
                                    print("Current size: ", curr_size)
                                    print("Calculated size: ", size)
                                    print("Size diff: ", abs(curr_size - size))
                                    print("Order: ", order)
                                    print("Symbol: ", symbol)


                            if position.size < 0:
                                if curr_size > size:
                                    order = proc_order('market', curr_size - size, 'buy')
                                    execute_order('rider', asset, reduce_short=order)

                                    print('Order placed: Reduce Short.')
                                    print("Current size: ", curr_size)
                                    print("Calculated size: ", size)
                                    print("Size diff: ", abs(curr_size - size))
                                    print("Order: ", order)
                                    print("Symbol: ", symbol)

                                else:
                                    order = proc_order('market', size - curr_size, 'sell')
                                    execute_order('rider', asset, add_short=order)

                                    print('Order placed: Add Long.')
                                    print("Current size: ", curr_size)
                                    print("Calculated size: ", size)
                                    print("Size diff: ", abs(curr_size - size))
                                    print("Order: ", order)
                                    print("Symbol: ", symbol)


        bt = Backtest(config)
        bt.set_strategy(initialize=initialize_ftx, process=process_ftx)
        bt.run(progressbar=False)
        replacecol = bt.positions.loc['2021-05-28':].copy()
        replacecol.columns = pd.MultiIndex.from_tuples(((val[0],val[1].replace('-PERP','/USDT')) for val in replacecol.columns))
        replacecol.to_csv("FTX_FLOCKLS-positions.csv")

flocklsitem = flocklstest()
flocklsitem.flockls_run()

#FTX FLOCKLS END
####################################################################
#FTX TT6V3USD START
import warnings
warnings.filterwarnings("ignore")
class tt6v3usdtest:
    def tt6v3usd_run(self):
        strat_var = 1
        risk = 0.02
        trail_perc = 0.05
        rrr = 10
        pos_count_val = 5
        slippage = 0.0005

        slippage = 0.00025


        config = {
            'context': {
                'is_shared_balance' : True,
                'balance' : 1,
                'assets': {},
                'parameters': {
        #             'length': 185,
        #             'mult': 2,
                    # updated params 06.01.22
                    'length': 165,
                    'mult': 2,
                    'risk': risk,
                    
                    ###SL perc
                    'sl_perc': 0.1,
                    
                    ###TS
                    'trail_perc': trail_perc,
                    
                    ###TP 
                        ### Bars
                    'rrr': rrr, 
                    
                        ### RSI
                    'rsi_len': 14,
                    'rsi_ob': 70,
                    'rsi_os': 30,
                    
                },
            },
            'slippage': slippage,
            'time_interval': 1,
            'time_unit': 'h',   
            'start_date': '2019-01-01 00:00:00',    
        #     'end_date': '2021-12-31 23:59:59', 
            'end_date': dt.datetime.now(),
            'exchange': 'ftx',
            'window_length' : 2
            }

        n = 6
        pairs = {
            'MATIC/USDT': ['binance', 1/n],
            'LINK/USDT': ['binance', 1/n],
            'XRP/USDT': ['binance', 1/n],
            'DOGE/USDT': ['binance', 1/n],  
            'LTC/USDT': ['binance', 1/n],
            'OMG/USDT': ['binance', 1/n],
        }

        for i,v in pairs.items():
            if i.endswith('BTC'):
                maker_fee = -.0005
                taker_fee = 0.0025      
            else:
                maker_fee = .0002
                taker_fee = .0007
                
            config['context']['assets'][i] = {
                'exchange': v[0],
                'maker_fee': maker_fee,
                'taker_fee': taker_fee,
                'allocation' : v[1],

                ###Take profit
                'last_pos': None,
                
                ###Trailing Stop
                'trail': 0,
                
                # for var
                'distance': 0,
                'dist_date': '',
                'close': 0,
                'stop': 0,
                'cont_rate': 0
                }

        """
        Trailing stop

        """

        def trail_comp(direction, percentage, price, current_trail):
            if direction > 0:
                trail_stop = price * (1-percentage)
                if price < trail_stop:
                    trail_stop = current_trail
                elif (trail_stop < current_trail):
                    trail_stop = current_trail
            elif direction < 0:
                trail_stop = price * (1+percentage)
                if price > trail_stop:
                    trail_stop = current_trail
                elif (trail_stop > current_trail):
                    trail_stop = current_trail
            return trail_stop


        def trail_comp2(direction, percentage, last_close, prev_close, current_trail):
            
            if direction > 0:
                if last_close > prev_close:
                    trail_stop = current_trail * (1+percentage)
                    if last_close < trail_stop:
                        trail_stop = current_trail
                else:
                    trail_stop = current_trail
            elif direction < 0:
                if last_close < prev_close:
                    trail_stop = current_trail * (1-percentage)
                    if last_close > trail_stop:
                        trail_stop = current_trail
                else:
                    trail_stop = current_trail
            return trail_stop


        def trail_comp3(direction, percentage, last_close, prev_close, current_trail):
            
            if direction > 0:
                if last_close > prev_close:
                    trail_stop = current_trail * (1+percentage)
                    if last_close < trail_stop:
                        trail_stop = current_trail
                elif last_close < prev_close:
                    trail_stop = current_trail * (1+percentage)
                    if last_close < trail_stop:
                        trail_stop = current_trail
                else:
                    trail_stop = current_trail
                
                        
            elif direction < 0:
                if last_close < prev_close:
                    trail_stop = current_trail * (1-percentage)
                    if last_close > trail_stop:
                        trail_stop = current_trail
                elif last_close > prev_close:
                    trail_stop = current_trail * (1-percentage)
                    if last_close > trail_stop:
                        trail_stop = current_trail 
                else:
                    trail_stop = current_trail
            return trail_stop

        def proc_order(ord_type, ord_size, side, price=None, trigger=None):
            order = {
                    'order_type' : ord_type,
                    'size' : ord_size,
                    'side' : side,
                    }
            if ord_type is 'stop':
                if trigger is not None:
                    order['trigger_price'] = trigger 
        #             print('trigger price', order['trigger_price'])
            
            if ord_type is 'limit':
                if price is not None:
                    order['price'] = price
            
            return order

        def execute_order(strategy_name, asset, **orders):
            for i,v in orders.items():
        #         print('execute orders', i, v)
                asset.create_order(f'{strategy_name}_{i}', v)
                
        def cancel_order(strategy_name, asset, *orders):
            for i in orders:
                if asset.orders.get(f'{strategy_name}_{i}'):
        #             print('cancel orders', i)
                    asset.cancel_order(f'{strategy_name}_{i}')
            
        """fetch token precision"""
        markets = ccxt.ftx().fetch_markets()
        markets = [x for x in markets if x.get('id').endswith('-PERP')]
        token_steps = {}
        for market in markets:
            key = market.get('base')
            token_steps[key] = market.get('precision').get('amount')

        def calculate_size(exchange, symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
            if exchange == 'ftx':
                pos_size = total_balance * def_alloc
                orig_size = pos_size * contract_rate
                
                if with_var and (float(kwargs['risk']) > 0):
                    invalidation = abs((close-kwargs['stop'])/close)
                    var_pos_size = (total_balance * kwargs['risk']) / invalidation
                    var_size = var_pos_size * contract_rate
                    
                    size = min(orig_size, var_size)
                    
                elif (with_var == False) or (float(kwargs['risk']) == 0.0):
                    size = orig_size
                    
                
                sym = symbol.split('/')[0]    
                
                try:
                    precision = decimal.Decimal(str(token_steps.get(sym)))

                    if precision == decimal.Decimal(1.0):
                        precision = decimal.Decimal(0.0)

                    size = str(size).split('.')[0] + '.' + str(size).split('.')[1][:abs(precision.as_tuple().exponent)]
                    size = float(size)
                except Exception as e:
                    pass
        #             print(f'Precision error: {symbol}. Use unedited size.')
                
                return size
            
            if exchange == 'binance':
                min_size = {
                    'btcusdt': 100,
                    'ethusdt': 1,
                    'xrpusdt': 1,
                    'ltcusdt': 1,
                    'adabtc': 10,
                    'ethbtc': 0.01
                }
                # orig position size
                pos_size = total_balance * def_alloc
                if symbol.lower() == 'eth/btc':
                    orig_size = round(pos_size * contract_rate, 2)
                else:
                    orig_size = np.floor(pos_size * contract_rate)

                if symbol.lower().startswith('btc'):
                    if orig_size < 100: 
                        if orig_size % 100 >= 90:
                            orig_size = round(orig_size, -2)
                        else: 
                            orig_size = 0
                    else:
                        orig_size = orig_size - (orig_size % 100)
                elif symbol.lower() == 'ada/btc':
                    if orig_size < 10:
                        if round(orig_size % 10, 2) >= 9:
                            orig_size = round(orig_size, -1)
                        else:
                            orig_size = 0
                    else:
                        orig_size = orig_size - (orig_size % 10)
                #var pos size
                if with_var and (float(kwargs['risk']) > 0):
                    invalidation = abs((close-kwargs['stop'])/close)
                    var_pos_size = (total_balance * kwargs['risk']) / invalidation
                    var_size = var_pos_size * contract_rate
                    if symbol.lower().startswith('btc'):
                        if var_size < 100: 
                            if var_size % 100 >= 90:
                                var_size = round(var_size, -2)
                            else: 
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 100)
                    else:
                        if symbol.lower().endswith('usdt'):
                            if var_size < 1:
                                if round(var_size % 1, 2) >= 0.9:
                                    var_size = round(var_size, 0)
                                else:
                                    var_size = 0
                            else:
                                var_size = var_size - (var_size % 1)
                        else:
                            if symbol.lower() == 'ada/btc':
                                if var_size < 10:
                                    if round(var_size % 10, 2) >= 9:
                                        var_size = round(var_size, -1)
                                    else:
                                        var_size = 0
                                else:
                                    var_size = var_size - (var_size % 10)
                            elif symbol.lower() == 'eth/btc':
                                if var_size < 1:
                                    if round(var_size % 0.01, 3) >= 0.009:
                                        var_size = round(var_size, 2)
                                    else:
                                        var_size = 0
                                else:
                                    var_size = var_size - (var_size % 1)
                    size = min(orig_size, var_size)
                elif (with_var == False) or (float(kwargs['risk']) == 0.0):
                    size = orig_size
                return size

        def get_win_rate(df, pairs):
            
            for pair in pairs:
                df[(pair, 'pos')] = None
                df[(pair, 'wl')] = 0
                
                for i in range(len(df)):
                    if df.iloc[i][(pair, 'size')] > 0:
                        df[(pair, 'pos')].iloc[i] = 'long'
                    elif df.iloc[i][(pair, 'size')] < 0:
                        df[(pair, 'pos')].iloc[i] = 'short'
                    if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                        if df[(pair, 'pos')].iloc[i-1] == 'long':
                            if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                                df[(pair, 'wl')].iloc[i] = 1
                            else:
                                df[(pair, 'wl')].iloc[i] = -1
                        elif df[(pair, 'pos')].iloc[i-1] == 'short':
                            if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                                df[(pair, 'wl')].iloc[i] = -1
                            else:
                                df[(pair, 'wl')].iloc[i] = 1
            
            pairs_dict = {}
            for pair in pairs:
                
                win_trades = len(pos[pos[(pair, 'wl')] == 1])
                losing_trades = len(pos[pos[(pair, 'wl')] == -1])
                total = win_trades + losing_trades
                try:
                    win_pct = win_trades / total
                    lose_pct = losing_trades / total
                except Exception as e:
                    print(e)
                    win_pct = 0
                    lose_pct = 0
                
        #         print(f'{pair} Won trades count: {win_trades}')
        #         print(f'{pair} Lost trades count: {losing_trades}')
        #         print(f'{pair} Total trades count: {total}')
        #         print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
                
                pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
                
                
                
            return pairs_dict
            
            
        def proc_wr(old_dict, get_win_rate_dict):
            
            pair_keys = old_dict.keys()
            for pair in pair_keys:
                
                old = old_dict[pair]
                new = get_win_rate_dict[pair]
                
                new2 = [old[x] + new[x] for x in range(len(old))]
                new2[-1] = round(new2[0] / new2[2], 4)
                
                
                old_dict[pair] = new2
            
            return old_dict

        def metrics(eq):
            # risk free rate
            rfr = 0.04
            eq = eq[eq.index.hour == 0]
            rets = eq.pct_change().fillna(0)

            years = list(rets.index.year.drop_duplicates())
            years.remove(2022)

            sort = []
            draw = []
            equi = []
            news = []

            data = pd.DataFrame(columns={'year', 'Sortino', 'MDD', 'Equity', 'New Sharpe'})

            for year in years:
                year = str(year)
                year_rets = rets[year]

                #sortino
                sortino = qs.stats.sortino(year_rets, rf= rfr, periods=365, annualize=True)

                #dd
                dd = qs.stats.max_drawdown(year_rets)*100

                year_rets = pd.DataFrame(year_rets, columns=['rets'])
                year_rets['eq'] = (1 + year_rets['rets']).cumprod()

                #equity
                year_eq = qs.stats.comp(year_rets['rets']) + 1

                #linearity
                year_rets['log'] = np.log(year_rets['eq'])
                x = np.arange(year_rets.index.size)
                fit = np.polyfit(x, year_rets['log'], deg=1)
                fit_function = np.poly1d(fit)
                linearity_val = np.sqrt(((year_rets['log'] - fit_function(x))**2).mean())
                cagr_val = qs.stats.cagr(year_rets['eq'].pct_change().fillna(0), rf=rfr, periods=year_rets.shape[0], compounded=True)
                new_sharpe_val = cagr_val/linearity_val

                sort.append(sortino)
                draw.append(dd)
                equi.append(year_eq)
                news.append(new_sharpe_val)

                data = data.append({'New Sharpe': round(new_sharpe_val, 2),
                                    'Equity': round(year_eq, 2),
                                    'MDD': round(dd, 2), 
                                    'Sortino': round(sortino, 2),
                                    'year': year}, ignore_index=True)

            sort_avg = sum(sort)/len(sort)
            draw_avg = sum(draw)/len(draw)
            equi_avg = sum(equi)/len(equi)
            new_sharpe_avg = sum(news)/len(news)

            data = data.append({'year': 'AVG', 
                                'Sortino': sort_avg, 
                                'MDD': draw_avg, 
                                'Equity': equi_avg, 
                                'New Sharpe': new_sharpe_avg}, 
                            ignore_index=True)

            data.set_index('year', inplace=True)
            data = data[['Equity', 'MDD', 'Sortino', 'New Sharpe']]
            return data

        def plot_equity(equity):
            _, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [1, 3]},
                                sharex=True, figsize=(5.5*(16/9), 5.5))
            running_max = np.maximum.accumulate(equity)
            drawdown = -100 * ((running_max - equity) / running_max)
            ax[0].fill_between(equity.index, 0, drawdown, alpha=0.3, color='r')
            ax[0].set_title('Drawdown')
            ax[1].plot(equity)
            ax[1].set_title('Total Balance')
            return ax


        from laetitudebots.util.talib_method import talib_method
        from laetitudebots.util.custom_method import custom_method
        from laetitudebots.backtest import Backtest

        def initialize(factors, config):

            length = int(config['parameters']['length'])
            mult = float(config['parameters']['mult'])
        #     trail_perc = float(config['parameters']['trail_perc'])
        #     print(trail_perc)
            
            factors['high'] = factors.high #.fillna(0).ffill()
            factors['low'] = factors.low #.fillna(0).ffill()
            factors['close'] = factors.close #.fillna(0).ffill()

            factors['atr'] = talib_method(method='ATR', timeperiod=1, args=[factors.high, factors.low, factors.close])
            factors['avg_tr'] = talib_method(method='WMA', timeperiod=length, args=[factors.atr])
            factors['highest'] = factors.high.rolling(length).max()
            factors['lowest'] = factors.low.rolling(length).min()
            factors['hi_lim'] = (factors.highest.shift(1) - (factors.avg_tr.shift(1) * mult)).fillna(0)
            factors['lo_lim'] = (factors.lowest.shift(1) + (factors.avg_tr.shift(1) * mult)).fillna(0)
            factors['mid'] = ((factors.hi_lim + factors.lo_lim)/2).fillna(0)

        def process(window, assets, context):

            total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
            total_balance = total_unrealised_pnl + context['balance'] 

            ### VAR pos sizing
            risk = config['context']['parameters']['risk']
            
            for symbol, asset in assets.items():
                
                prev_bar = window.previous(symbol, 2)
                last_bar = window.latest(symbol)
                
                settings = context['assets'][symbol]
                
                ### Trailing Stop
                trail = settings['trail']
                
                position = asset.position
            
                #entry conditions
                lg_sig = (
                            ((max(prev_bar.hi_lim, prev_bar.lo_lim) > prev_bar.close) &
                            (max(last_bar.hi_lim, last_bar.lo_lim) < last_bar.close))
                            )

                st_sig = (
                            ((min(prev_bar.hi_lim, prev_bar.lo_lim) < prev_bar.close) &
                            (min(last_bar.hi_lim, last_bar.lo_lim) > last_bar.close))
                            )
                
                if lg_sig:
                    stop = (min(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
                elif st_sig:
                    stop = (max(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
                else:
                    stop = 0
                    
                size = calculate_size('ftx', symbol, total_balance, last_bar.close, settings['allocation'], 
                                    asset.get_contract_rate(last_bar.close), 
                                    with_var=True, stop=stop, risk=context['parameters']['risk'])
                
                date = pd.to_datetime(last_bar.timestamp)
                month = date.month
                year = date.year
                
                
        #         if lg_sig or st_sig:
        #             if (month==4) and (year==2022):
        #                 print('APRIL')
        #                 dist = (abs(last_bar.close-stop)/last_bar.close)*100
        #                 print('dist', dist)
        #                 if dist > config['context']['assets'][symbol]['distance']:
        #                     config['context']['assets'][symbol]['distance'] = dist
        #                     print(settings['distance'])
        #                     config['context']['assets'][symbol]['dist_date'] = last_bar.timestamp
        #                     print(settings['dist_date'])
        #                     print(last_bar.timestamp)
        #                     config['context']['assets'][symbol]['close'] = last_bar.close
        #                     config['context']['assets'][symbol]['stop'] = stop
        #                     config['context']['assets'][symbol]['cont_rate'] = asset.get_contract_rate(last_bar.close)
                        
        #             print('**********')
        #             print(symbol)
        #             print(last_bar.timestamp)
        #             print(f'close {last_bar.close}')
        #             print(f'stop {stop}')
        #             print(f'distance {(abs(last_bar.close-stop)/last_bar.close)*100}')
        #             print(f'contract rate {asset.get_contract_rate(last_bar.close)}')
        #             print('**********')

        # for var
        #         'distance': 0,
        #         'dist_date': '',
        #         'close': 0,
        #         'stop': 0,
        #         'cont_rate': 0
                
                
                trail_perc = context['parameters']['trail_perc']
                
                if position.size == 0:
                    cancel_order('rider', asset, 'long_stop', 'long_trail', 'short_stop', 'short_trail')
                    if lg_sig:                   
                        order = proc_order('market', size, 'buy')

                        ### long sl
        #                 order_sl = proc_order('stop', size, 'sell', trigger=stop)

                        ### Trail adjustment
                        settings['trail'] = stop

                        execute_order('rider', asset, long=order) #, long_stop=order_sl)
                                        
                    elif st_sig:
                        order = proc_order('market', size, 'sell')
        #                 order_sl = proc_order('stop', size, 'buy', trigger=stop)

                        settings['trail'] = stop

                        execute_order('rider', asset, short=order) #, short_stop=order_sl)
                
        #                 print(symbol, asset.get_contract_rate(last_bar.close))

                elif position.size > 0:
                    if st_sig:
        #                 cancel_order('rider', asset, 'long_stop', 'long_trail')
                        
                        order = proc_order('market', size + abs(position.size), 'sell')
        #                 order_sl = proc_order('stop', size, 'buy', trigger=stop)
                        
                        settings['trail'] = stop
                        
                        execute_order('rider', asset, short=order) #, short_stop=order_sl)

                    else:
                        if last_bar.close < settings['trail']:
                            order = proc_order('market', abs(position.size), 'sell')
                            execute_order('rider', asset, long_stop=order)
                        
                        else:
                            trail_stop = trail_comp(position.size, trail_perc, last_bar.low, settings['trail'])
                            if trail_stop > settings['trail']:
                                settings['trail'] = trail_stop
                
                elif position.size < 0:
                    if lg_sig:
                        
                        ### reverse to long
                        order = proc_order('market', size + abs(position.size), 'buy')
                        
                        settings['trail'] = stop
                        
                        execute_order('rider', asset, long=order)                

                    else:
                        if last_bar.close > settings['trail']:
                            order = proc_order('market', abs(position.size), 'buy')
                            execute_order('rider', asset, short_stop=order)
                        else:
                            trail_stop = trail_comp(position.size, trail_perc, last_bar.high, settings['trail'])
                            if trail_stop < settings['trail']:
                                settings['trail'] = trail_stop

        bt = Backtest(config)
        bt.set_strategy(initialize=initialize, process=process)
        bt.run(progressbar=False)



        bt = Backtest(config)
        bt.set_strategy(initialize=initialize, process=process)
        bt.run(progressbar=False)

        bt.positions.loc['2021-05-28':].to_csv("FTX_TT6V3USD-positions.csv")

bac = tt6v3usdtest()
bac.tt6v3usd_run()

####################
#TS2V3USD start ftx
import ccxt
def trail_comp(direction, percentage, price, current_trail):
    if direction > 0:
        trail_stop = price * (1-percentage)
        if price < trail_stop:
            trail_stop = current_trail
        elif (trail_stop < current_trail):
            trail_stop = current_trail
    elif direction < 0:
        trail_stop = price * (1+percentage)
        if price > trail_stop:
            trail_stop = current_trail
        elif (trail_stop > current_trail):
            trail_stop = current_trail
    return trail_stop
def trail_comp2(direction, percentage, last_close, prev_close, current_trail):
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    return trail_stop
def trail_comp3(direction, percentage, last_close, prev_close, current_trail):
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        elif last_close < prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        elif last_close > prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    return trail_stop
def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type == 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger
    if ord_type == 'limit':
        if price is not None:
            order['price'] = price
    return order
def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
        asset.create_order(f'{strategy_name}_{i}', v)
#         print(f'Execute order i: {i}')
#         print(f'Execute order v: {v}')
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f'{strategy_name}_{i}'):
#             print(f'Cancel order i: {i}')
            asset.cancel_order(f'{strategy_name}_{i}')
"""fetch token precision"""
markets = ccxt.ftx().fetch_markets()
tokens = ['BTC', 'ETH', 'LINK', 'RAY', 'ATLAS', 'AAVE', 'POLIS', 'MNGO', 'BADGER', 'TRYB', 'MCB', 'CRO', 'FTM']
tokens = [x+'-PERP' for x in tokens]
step = []
for token in tokens:
    step.extend([x['precision'].get('amount') for x in markets if x.get('id') == f'{token}'])
token_steps = dict(zip(tokens, step))
def calculate_size(exchange, symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    if exchange == 'ftx':
        pos_size = total_balance * def_alloc
        orig_size = pos_size * contract_rate
        if with_var == True:
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            size = min(orig_size, var_size)
        else:
            size = orig_size
        sym = symbol.split('/')[0]    
        try:
            precision = decimal.Decimal(str(token_steps.get(sym)))
            if precision == decimal.Decimal(1.0):
                precision = decimal.Decimal(0.0)
            size = round(size, abs(precision.as_tuple().exponent))
        except Exception as e:
            pass
        return size
    if exchange == 'bitmex':
        min_size = {
        'btcusd': 100,
        'ethusd': 1,
        'xrpusd': 1,
        'ltcusd': 1,
        'adabtc': 10,
        'ethbtc': 0.01
        }
        # orig position size
        pos_size = total_balance * def_alloc
        orig_size = np.floor(pos_size * contract_rate)
        if symbol.lower().startswith('btc'):
            if orig_size < 100: 
                if orig_size % 100 >= 90:
                    orig_size = round(orig_size, -2)
                else: 
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 100)
        elif symbol.lower() == 'ada/btc':
            if orig_size < 10:
                if round(orig_size % 10, 2) >= 9:
                    orig_size = round(orig_size, -1)
                else:
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 10)
        #var pos size
        if with_var:
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = (var_pos_size * contract_rate)
#             print(orig_size, 'orig', var_size, 'vars')
            if symbol.lower().startswith('btc'):
                if var_size < 100: 
                    if var_size % 100 >= 90:
                        var_size = round(var_size, -2)
                    else: 
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 100)
            else:
                if 'usd' in symbol.lower():
                    if var_size < 1:
                        if round(var_size % 1, 2) >= 0.9:
                            var_size = round(var_size, 0)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
                else:
                    if symbol.lower() == 'ada/btc':
                        if var_size < 10:
                            if round(var_size % 10, 2) >= 9:
                                var_size = round(var_size, -1)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 10)
                    elif symbol.lower() == 'eth/btc':
                        if var_size < 1:
                            if round(var_size % 0.01, 3) >= 0.009:
                                var_size = round(var_size, 2)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 1)
            size = min(orig_size, var_size)
        else:
            size = orig_size
        return size
import datetime as dt
slippage = 0.0005
taker_fee = 0.0007
maker_fee = 0.0007
config = {
    'context': {
        'is_shared_balance' : True,
        'balance' : 40,
        'assets': {},
        'parameters': {
            'basket1_len': 50,
            'basket2_len': 150,
            'risk': 0.03,
            ###SL perc
            'basket1_sl': 0.10,
            'basket2_sl': 0.09,
            ###TS
            'trail_perc': 0.01,
            ###TP 
                ### Bars
            'rrr': 2, 
                ### RSI
            'rsi_len': 14,
            'rsi_ob': 70,
            'rsi_os': 30,
        },
    },
    'slippage': slippage,
    'time_interval': '1',
    'time_unit': 'h',
    'start_date': '2022-05-10 00:00:00',    
    'end_date': dt.date.today(), # - dt.timedelta(days=1),
    'exchange': 'ftx',
    'window_length' : 2
    }
pairs1 = {}
pairs2 = {}
pairs_list1 = ['BTC', 'ETH', 'LINK']
for p in pairs_list1:
    pairs1[p+'-PERP'] = ['ftx', 0.153846153846153]
for i,v in pairs1.items():
    config['context']['assets'][i] = {
        'exchange': v[0],
        'maker_fee': maker_fee,
        'taker_fee': taker_fee,
        'allocation' : 0.153846153846153,
        ###Take profit
        'last_pos': None,
        ###Trailing Stop
        'trail': 0,
        }
pairs_list2 = [
    'RAY',
    'ATLAS',
    'AAVE',
    'POLIS',
    'MNGO',
    'BADGER',
    'TRYB',
    'MCB',
    'CRO',
    'FTM',
]
for p in pairs_list2:
    pairs2[p+'-PERP'] = ['ftx', 0.053846154]
for i,v in pairs2.items():
    config['context']['assets'][i] = {
        'exchange': v[0],
        'maker_fee': maker_fee,
        'taker_fee': taker_fee,
        'allocation' : 0.053846154,
        ###Take profit
        'last_pos': None,
        ###Trailing Stop
        'trail': 0,
        }
from laetitudebots.util.talib_method import talib_method
from laetitudebots.backtest import Backtest
import datetime as dt
import ccxt
import decimal
import numpy as np
import pandas as pd
def initialize_bitmex(factors, config):
    factors['ohlcv'] = factors['ohlcv'].fillna(0)
    basket1_len = config['parameters']['basket1_len']
    basket2_len = config['parameters']['basket2_len']
    rsi_len = config['parameters']['rsi_len']
    factors['rsi'] = talib_method(method='RSI', timeperiod=rsi_len, args=[factors.close])
    trail_perc = config['parameters']['trail_perc']
    factors['b1_mao'] = talib_method(method='SMA', timeperiod=basket1_len, args=[factors.open])
    factors['b1_mah'] = talib_method(method='SMA', timeperiod=basket1_len, args=[factors.high])
    factors['b1_mal'] = talib_method(method='SMA', timeperiod=basket1_len, args=[factors.low])
    factors['b1_mac'] = talib_method(method='SMA', timeperiod=basket1_len, args=[factors.close])
    factors['b1_wid'] = (factors['b1_mah'] - factors['b1_mal']) / factors['b1_mal']
    factors['b2_mao'] = talib_method(method='SMA', timeperiod=basket2_len, args=[factors.open])
    factors['b2_mah'] = talib_method(method='SMA', timeperiod=basket2_len, args=[factors.high])
    factors['b2_mal'] = talib_method(method='SMA', timeperiod=basket2_len, args=[factors.low])
    factors['b2_mac'] = talib_method(method='SMA', timeperiod=basket2_len, args=[factors.close])
    factors['b2_wid'] = (factors['b2_mah'] - factors['b2_mal']) / factors['b2_mal']
def process_bitmex(window, assets, context):
    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 
    ###parameters
    basket1_sl = context['parameters']['basket1_sl']
    basket2_sl = context['parameters']['basket2_sl']
    risk = context['parameters']['risk']
    trail_perc = context['parameters']['trail_perc']
    rrr = context['parameters']['rrr']
    basket1 = ['BTC-PERP', 'ETH-PERP', 'LINK-PERP', 'BTCUSD', 'ETHUSD', 'LINKUSD']
#     basket2 = ['RAYUSD', 'ATLASUSD', 'AAVEUSD', 'POLISUSD', 'MNGOUSD', 'BADGERUSD', 'TRYBUSD', 'MCBUSD', 'CROUSD', 'FTMUSD']
    basket2 = ['RAY-PERP', 'ATLAS-PERP', 'AAVE-PERP', 'POLIS-PERP', 'MNGO-PERP', 'BADGER-PERP', 'TRYB-PERP', 'MCB-PERP', 'CRO-PERP', 'FTM-PERP']
    for symbol, asset in assets.items():
        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)
        settings = context['assets'][symbol]
        last_pos = settings['last_pos']
        position = asset.position
        ### entry conditions
        if symbol in basket1:
            lg_sig = (last_bar.b1_wid < (10/100)) & (last_bar.b1_mac > last_bar.b1_mao) & (last_bar.close > last_bar.b1_mah)
            st_sig = (last_bar.b1_wid < (10/100)) & (last_bar.b1_mac < last_bar.b1_mao) & (last_bar.close < last_bar.b1_mal)
            if lg_sig:
                stop = last_bar.close * (1 - basket1_sl)
            elif st_sig:
                stop = last_bar.close * (1 + basket1_sl)
            else:
                stop = 0
        elif symbol in basket2:
            lg_sig = (last_bar.b2_wid < (10/100)) & (last_bar.b2_mac > last_bar.b2_mao) & (last_bar.close > last_bar.b2_mah)
            st_sig = (last_bar.b2_wid < (10/100)) & (last_bar.b2_mac < last_bar.b2_mao) & (last_bar.close < last_bar.b2_mal)
            if lg_sig:
                stop = last_bar.close * (1 - basket2_sl)
            elif st_sig:
                stop = last_bar.close * (1 + basket2_sl)
            else:
                stop = 0
        else:
            raise ValueError(f'{symbol} not in baskets.')
        ###sizing
        size = calculate_size(config['exchange'], symbol, total_balance, last_bar.close, settings['allocation'], 
                              asset.get_contract_rate(last_bar.close), 
                              with_var=True, stop=stop, risk=risk)
        if 'RAY' in symbol:
            print(asset.get_contract_rate(last_bar.close), size, last_bar.timestamp)
        rsi_ob = context['parameters']['rsi_ob']
        rsi_os = context['parameters']['rsi_os']
        rsi_xabove = prev_bar.rsi < rsi_ob and last_bar.rsi > rsi_ob
        rsi_xbelow = prev_bar.rsi > rsi_os and last_bar.rsi < rsi_os
        ### Trailing stop - kit
        trail_stop = settings['trail']
        if last_bar.timestamp > pd.to_datetime('2022-05-30 23'):
            if position.size == 0 and last_bar.close > 0:
                if asset.orders.get('rider_short_tp'):
                    asset.cancel_order('rider_short_tp')
                if asset.orders.get('rider_short_stop'):
                    asset.cancel_order('rider_short_stop')
                if asset.orders.get('rider_short_trail'):
                    asset.cancel_order('rider_short_trail')
                if asset.orders.get('rider_long_stop'):
                    asset.cancel_order('rider_long_stop')
                if asset.orders.get('rider_long_tp'):
                    asset.cancel_order('rider_long_tp')
                if asset.orders.get('rider_long_trail'):
                    asset.cancel_order('rider_long_trail')
                if lg_sig:
                    if (settings['last_pos'] is not 'Long') or (settings['last_pos'] is 'Long' and rsi_xabove):
                        settings['last_pos'] = 'Long'
                        ### long entry
                        order = proc_order('market', size, 'buy')
                        ### Trail adjustment
                        settings['trail'] = stop
                        ### TP
                        close_tp = abs((stop - last_bar.close) / last_bar.close) * rrr
                        close_tp2 = last_bar.close * (close_tp + 1)                    
                        order_tp = proc_order('limit', size, 'sell', price=close_tp2)
                        execute_order('rider', asset, long=order, long_tp=order_tp)
                elif st_sig:
                    if (settings['last_pos'] is not 'Short') or (settings['last_pos'] is 'Short' and rsi_xbelow):
                        settings['last_pos'] = 'Short'
                        ### long entry
                        order = proc_order('market', size, 'sell')
                        ### Trail adjustment
                        settings['trail'] = stop
                        ### TP
                        close_tp = abs((last_bar.close - stop) / last_bar.close) * rrr
                        close_max = last_bar.close * (1 - close_tp)
                        close_tp2 = max(last_bar.close*0.1, close_max)
                        order_tp = proc_order('limit', size, 'buy', price=close_tp2)
                        execute_order('rider', asset, short=order, short_tp=order_tp)
            elif position.size > 0:
                if st_sig:
                    ### reverse to short
                    if asset.orders.get('rider_long_stop'):
                        asset.cancel_order('rider_long_stop')
                    if asset.orders.get('rider_long_tp'):
                        asset.cancel_order('rider_long_tp')
                    if asset.orders.get('rider_long_trail'):
                        asset.cancel_order('rider_long_trail')
                    settings['last_pos'] = 'Short'
                    settings['trail'] = stop
                    order = proc_order('market', size + abs(position.size), 'sell')
                    close_tp = abs((last_bar.close - stop) / last_bar.close) * rrr
                    close_max = last_bar.close * (1 - close_tp)
                    close_tp2 = max(last_bar.close*0.1, close_max)
                    order_tp = proc_order('limit', size, 'buy', price=close_tp2)
                    execute_order('rider', asset, short=order, short_tp=order_tp)
                else:
                    settings['last_pos'] = 'Long'
                    ## Trail adjustment (kit)
                    trail_stop = trail_comp2(position.size, trail_perc, last_bar.close, prev_bar.close, settings['trail'])
                    settings['trail'] = trail_stop
                    if last_bar.close < trail_stop:
                        order = proc_order('market', abs(position.size), 'sell')
                        execute_order('rider', asset, long_trail=order)
                        if asset.orders.get('rider_long_tp'):
                            asset.cancel_order('rider_long_tp')
            elif position.size < 0:
                if lg_sig:
                    if asset.orders.get('rider_short_tp'):
                        asset.cancel_order('rider_short_tp')
                    settings['last_pos'] = 'Long'
                    settings['trail'] = stop
                    order = proc_order('market', size + abs(position.size), 'buy')
                    close_tp = abs((stop - last_bar.close) / last_bar.close) * rrr
                    close_tp2 = last_bar.close * (close_tp + 1)
                    order_tp = proc_order('limit', size, 'sell', price=close_tp2)                
                    execute_order('rider', asset, long=order, long_tp=order_tp)
                else:
                    trail_stop = trail_comp2(position.size, trail_perc, last_bar.close, prev_bar.close, settings['trail'])
                    settings['trail'] = trail_stop
                    if last_bar.close > trail_stop:
                        order = proc_order('market', abs(position.size), 'buy')
                        execute_order('rider', asset, short_trail=order)
                        if asset.orders.get('rider_short_tp'):
                            asset.cancel_order('rider_short_tp')
bt = Backtest(config)
bt.set_strategy(initialize=initialize_bitmex, process=process_bitmex)
bt.run(progressbar=False)
#TS2V3USD END

#bt.positions.loc['2021-05-28':].to_csv("FTX_TS2V3USD-positions.csv")

replacecol = bt.positions.loc['2021-05-28':].copy()
replacecol.columns = pd.MultiIndex.from_tuples(((val[0],val[1].replace('-PERP','/USDT')) for val in replacecol.columns))
replacecol.to_csv("FTX_TS2V3USD-positions.csv")
###################################################
#DMIv1 ftx start
def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
        asset.create_order(f'{strategy_name}_{i}', v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f'{strategy_name}_{i}'):
            asset.cancel_order(f'{strategy_name}_{i}')
    
"""fetch token precision"""
markets = ccxt.ftx().fetch_markets()

tokens = ['BTC',
         'ETH',
         'SOL',
         'FTT',
         'FTM',
         'BNB',
         'MATIC',
         'LINK',
         'XRP',
         'DOGE',
         'LTC',
         'TRX',
         'YFI',
         'OMG',
         'SXP',
         'RUNE',
         'BCH',
         'KNC',
         'LRC',
         'ENJ',
         'EUR',
         'BAT',
         'MKR',
         'COMP',
         'SNX',
         'CHR',
         'ETC',
         'ADA'
         ]

step = []
for token in tokens:
    step.extend([x['precision'].get('amount') for x in markets if x.get('id') == f'{token}-PERP'])

token_steps = dict(zip(tokens, step))

def calculate_size(exchange, symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    if exchange == 'ftx':
        pos_size = total_balance * def_alloc
        orig_size = pos_size * contract_rate
        
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            
            size = min(orig_size, var_size)
            
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
            
        
        sym = symbol.split('/')[0]    
        
        try:
            precision = decimal.Decimal(str(token_steps.get(sym)))

            if precision == decimal.Decimal(1.0):
                precision = decimal.Decimal(0.0)

            size = round(size, abs(precision.as_tuple().exponent))
        except Exception as e:
            pass
        
        return size
    
    if exchange == 'binance':
        min_size = {
            'btcusdt': 100,
            'ethusdt': 1,
            'xrpusdt': 1,
            'ltcusdt': 1,
            'adabtc': 10,
            'ethbtc': 0.01
        }
        # orig position size
        pos_size = total_balance * def_alloc
        if symbol.lower() == 'eth/btc':
            orig_size = round(pos_size * contract_rate, 2)
        else:
            orig_size = np.floor(pos_size * contract_rate)

        if symbol.lower().startswith('btc'):
            if orig_size < 100: 
                if orig_size % 100 >= 90:
                    orig_size = round(orig_size, -2)
                else: 
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 100)
        elif symbol.lower() == 'ada/btc':
            if orig_size < 10:
                if round(orig_size % 10, 2) >= 9:
                    orig_size = round(orig_size, -1)
                else:
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 10)
        #var pos size
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            if symbol.lower().startswith('btc'):
                if var_size < 100: 
                    if var_size % 100 >= 90:
                        var_size = round(var_size, -2)
                    else: 
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 100)
            else:
                if symbol.lower().endswith('usdt'):
                    if var_size < 1:
                        if round(var_size % 1, 2) >= 0.9:
                            var_size = round(var_size, 0)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
                else:
                    if symbol.lower() == 'ada/btc':
                        if var_size < 10:
                            if round(var_size % 10, 2) >= 9:
                                var_size = round(var_size, -1)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 10)
                    elif symbol.lower() == 'eth/btc':
                        if var_size < 1:
                            if round(var_size % 0.01, 3) >= 0.009:
                                var_size = round(var_size, 2)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 1)
            size = min(orig_size, var_size)
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
        return size

def get_win_rate(df, pairs):
    
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    
    pairs_dict = {}
    for pair in pairs:
        
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        try:
            win_pct = win_trades / total
            lose_pct = losing_trades / total
        except Exception as e:
            print(e)
            win_pct = 0
            lose_pct = 0
 
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
        
    return pairs_dict
    
    
def proc_wr(old_dict, get_win_rate_dict):
    
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        
        
        old_dict[pair] = new2
    
    return old_dict

def metrics(eq):
    # risk free rate
    rfr = 0.04
    eq = eq[eq.index.hour == 0]
    rets = eq.pct_change().fillna(0)

    years = list(rets.index.year.drop_duplicates())

    sort = []
    draw = []
    equi = []
    news = []

    data = pd.DataFrame(columns={'year', 'Sortino', 'MDD', 'Equity', 'New Sharpe'})

    for year in years:
        year = str(year)
        year_rets = rets[year]

        #sortino
        sortino = qs.stats.sortino(year_rets, rf= rfr, periods=365, annualize=True)

        #dd
        dd = qs.stats.max_drawdown(year_rets)*100

        year_rets = pd.DataFrame(year_rets, columns=['rets'])
        year_rets['eq'] = (1 + year_rets['rets']).cumprod()

        #equity
        year_eq = qs.stats.comp(year_rets['rets']) + 1

        #linearity
        year_rets['log'] = np.log(year_rets['eq'])
        x = np.arange(year_rets.index.size)
        fit = np.polyfit(x, year_rets['log'], deg=1)
        fit_function = np.poly1d(fit)
        linearity_val = np.sqrt(((year_rets['log'] - fit_function(x))**2).mean())
        cagr_val = qs.stats.cagr(year_rets['eq'].pct_change().fillna(0), rf=rfr, periods=year_rets.shape[0], compounded=True)
        new_sharpe_val = cagr_val/linearity_val

        sort.append(sortino)
        draw.append(dd)
        equi.append(year_eq)
        news.append(new_sharpe_val)

        data = data.append({'New Sharpe': round(new_sharpe_val, 2),
                            'Equity': round(year_eq, 2),
                            'MDD': round(dd, 2), 
                            'Sortino': round(sortino, 2),
                            'year': year}, ignore_index=True)


    sort_avg = sum(sort)/len(sort)
    draw_avg = sum(draw)/len(draw)
    equi_avg = sum(equi)/len(equi)
    new_sharpe_avg = sum(news)/len(news)

    data = data.append({'year': 'AVG', 
                        'Sortino': sort_avg, 
                        'MDD': draw_avg, 
                        'Equity': equi_avg, 
                        'New Sharpe': new_sharpe_avg}, 
                       ignore_index=True)

    data.set_index('year', inplace=True)
    data = data[['Equity', 'MDD', 'Sortino', 'New Sharpe']]
    return data

slippage = 0.0005


config = {
    'context': {
        'is_shared_balance' : True,
        'balance' : 1,
        'assets': {},
        'parameters': {
            'idx': 14,
            
        },
    },
    'slippage': slippage,
    'time_interval': 8,
    'time_unit': 'h',   
    'start_date': '2020-01-01 00:00:00',    
    'end_date': dt.datetime.now(),
    'exchange': 'ftx',
    'window_length' : 2
    }

pairs = {
    'ADA/USDT': ['binance', 1/3],
    'BTC/USDT': ['binance', 1/3],
    'ETH/USDT': ['binance', 1/3]

}

for i,v in pairs.items():
    if i.endswith('BTC'):
        maker_fee = -.0005
        taker_fee = 0.0025      
    else:
        maker_fee = -.00025
        taker_fee = 0.00075
        
    config['context']['assets'][i] = {
        'exchange': v[0],
        'maker_fee': maker_fee,
        'taker_fee': taker_fee,
        'allocation' : v[1],
        'collateral': 'BTC',

        ###Take profit
        'last_pos': None,

        }
    
def initialize(factors, config):
        
    idx = int(config['parameters']['idx'])


    factors['close'] = factors.close.ffill()
    factors['high'] = factors.high
    factors['slow'] = factors.low


    #dmi
    factors['dx'] = talib_method(method='DX', timeperiod = idx, args = [factors.high, factors.low, factors.close])
    factors['min_di'] = talib_method(method='MINUS_DI', timeperiod = idx, args =[factors.high, factors.low, factors.close])
    factors['plus_di'] = talib_method(method='PLUS_DI', timeperiod = idx, args =[factors.high, factors.low, factors.close])

def process(window, assets, context):
    
    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance']
    
    
    
    for symbol, asset in assets.items():
        position = asset.position
        last = window.latest(symbol)
        lastt = window.previous(symbol, 2)
        
        if not np.all([lastt.dx, lastt.plus_di, lastt.min_di]):
            continue
        settings = context['assets'][symbol]
    
        dx = last.dx
        pdi = last.plus_di
        mdi = last.min_di
          
        ### Entry Conditions
        lg_ent = ((dx > 25) and
                 (dx > lastt.dx) and
                 (pdi > mdi) and
                 (pdi > lastt.plus_di))
        lg_ext = (pdi < mdi) 
        
        st_ent = ((dx > 25) and
                 (dx > lastt.dx) and
                 (mdi > pdi) and
                 (mdi > lastt.min_di))
        
        st_ext = (pdi > mdi)
        stop = 0
        
        size = calculate_size('ftx', symbol, total_balance, last.close, settings['allocation'], 
                              asset.get_contract_rate(last.close, last.collateral), 
                              with_var=False, stop=0, risk=0)

        if position.size == 0: 
            if lg_ent:
                order = proc_order('market', size, 'buy')
                execute_order('dmi', asset, long=order)
            
            if st_ent:
                order = proc_order('market', size, 'sell')
                execute_order('dmi', asset, short=order)
                
        elif position.size > 0 and lg_ext:  
            order = proc_order('market', abs(position.size), 'sell')
            execute_order('dmi', asset, short=order)
            
            
        elif position.size < 0 and st_ext:  
            order = proc_order('market', abs(position.size), 'buy')
            execute_order('dmi', asset, long=order)
        
bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

bt.positions.loc['2021-05-28':].to_csv("FTX_DMIv1-positions.csv")

#DMIv1 ftx end
###################

########################
#PAYDAY
from codes import *

def get_win_rate(df, pairs):
    
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    
    pairs_dict = {}
    for pair in pairs:
        
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        win_pct = win_trades / total
        lose_pct = losing_trades / total
        
        print(f'{pair} Won trades count: {win_trades}')
        print(f'{pair} Lost trades count: {losing_trades}')
        print(f'{pair} Total trades count: {total}')
        print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
        
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
        
    return pairs_dict
    
    
def proc_wr(old_dict, get_win_rate_dict):
    
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        
        
        old_dict[pair] = new2
    
    return old_dict

from dateutil.relativedelta import relativedelta
import datetime as dt
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math

# BITMEX

# alloc = 0
slippage = 0.0005
taker_fee = 0.00075
maker_fee = -0.0001

config = {
    'context': {
        'is_shared_balance' : True,
        'balance' : 1,
        'assets': {},
        'parameters': {
            'ma_len': 50,
            'risk': 0.03,
            
            ###SL perc
            'sl_perc': 0.1,
            
            ###TS
            'trail_perc': 0.01,
            
            ###TP 
                ### Bars
            'rrr': 10, 
            
                ### RSI
            'rsi_len': 14,
            'rsi_ob': 70,
            'rsi_os': 30,
            
        },
    },
    'slippage': 0,
    'time_interval': '1',
    'time_unit': 'd',
    'start_date': '2017-08-17 00:00:00',    
    'end_date': dt.date.today(), # - dt.timedelta(days=1),
    'exchange': 'bitmex',
    'window_length' : 2
    }


pairs = {
    'BTC/USDT': ['binance', 0.33],
    'ETH/USDT': ['binance', 0.33],
    'BNB/USDT': ['binance', 0.33],
}

for i,v in pairs.items():
    config['context']['assets'][i] = {
        'exchange': v[0],
        'maker_fee': -.00025,
        'taker_fee': taker_fee,
        'allocation' : v[1],

        ###Take profit
        'last_pos': None,
        
        ###Trailing Stop
        'trail': 0,
        }

### SL with TP and Bars reentry


from laetitudebots.util.talib_method import talib_method
from laetitudebots.util.custom_method import custom_method
from laetitudebots.backtest import Backtest

def initialize(factors, config):
    pass
    
    ### Strategy indicators
def calculate_size(total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    # orig position size
    pos_size = total_balance * def_alloc
    orig_size = np.floor(pos_size * contract_rate)

    #var pos size
    if with_var == True:
        invalidation = abs((close-kwargs['stop'])/close)
        var_pos_size = (total_balance * kwargs['risk']) / invalidation
        var_size = np.floor((var_pos_size * contract_rate))
        size = min(orig_size, var_size)

    else:
        size = orig_size

    return size


def process(window, assets, context):
    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 
    
    for symbol, asset in assets.items():
        
        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)
        settings = context['assets'][symbol]
        position = asset.position
        
                              
        size = calculate_size(total_balance, last_bar.close, settings['allocation'], 
                              asset.get_contract_rate(last_bar.close), 
                              with_var=False)

        month_dict = {
                    30: [1, 3, 5, 7, 8, 10, 12],
                    29: [4, 6, 9, 11],
                    28: [2]
                    }
        
        time = pd.to_datetime(last_bar.timestamp)

        curr_month = time.month

        entry1 = 0
        entry2 = None
        
        for i in month_dict:
            if curr_month in month_dict.get(i):
                entry2 = i
        
        entry3 = 14
        
        exit1 = 1        
        exit2 = 2
        exit3 = 15

        lg_sig1 = time.is_month_end and time.dayofweek != 5
        lg_sig2 = time.day == entry2 and time.month != 6 
        lg_sig3 = time.day == entry3 and time.dayofweek != 0 and time.month != 11

        exit_sig1 = time.day == exit1 ###exit1 worse result
        if time.dayofweek == 5:
            exit_sig1 = False
        exit_sig2 = time.day == exit2
        exit_sig3 = time.day == exit3
        
        if time.month == 2:
            lg_sig3 = time.day == 13
            exit_sig3 = time.day == 14
            
        st_sig3 = False
        
        if time.month == 11 and time.day == entry3:
            if last_bar.close > last_bar.open:
                lg_sig3 = True
                st_sig3 = False
            elif last_bar.close < last_bar.open:
                lg_sig3 = False
                st_sig3 = True

        if position.size == 0 and last_bar.close > 0:
            if lg_sig1 or lg_sig2 or lg_sig3:
                order = proc_order('market', size, 'buy')
                execute_order('rider', asset, long=order)

                print('Order placed: Long.')
                print(f'Order size: {size}')
                print("Order: ", order)
                print("Asset: ", asset)
                print("Symbol: ", symbol)
                
            if st_sig3:
                order = proc_order('market', size, 'sell')
                execute_order('rider', asset, long=order)

                print('Order placed: Short.')
                print(f'Order size: {size}')
                print("Order: ", order)
                print("Asset: ", asset)
                print("Symbol: ", symbol)

        elif position.size != 0:
            if exit_sig1 or exit_sig2 or exit_sig3:
                if position.size > 0:
                    order_side = 'sell'
                elif position.size < 0:
                    order_side = 'buy'

                print('Order placed: Exit.')
                print(f'Order side: {order_side}')
                print("Asset: ", asset)
                print("Symbol: ", symbol)
                    
                order = proc_order('market', abs(position.size), order_side)
                execute_order('rider', asset, short=order)

"""
6 sunday
0 monday
"""
bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

bt.positions.loc['2021-05-28':].to_csv("BMX_PAYDAY-positions.csv")
#PAYDAY END
########################
# tdc
from dateutil.relativedelta import relativedelta
import datetime as dt
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math

def get_win_rate(df, pairs):
    
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    
    pairs_dict = {}
    for pair in pairs:
        
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        try:
            win_pct = win_trades / total
            lose_pct = losing_trades / total
        except Exception as e:
            print(e)
            win_pct = 0
            lose_pct = 0
        

        
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
        
        
        
    return pairs_dict
    
    
def proc_wr(old_dict, get_win_rate_dict):
    
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        
        
        old_dict[pair] = new2
    
    return old_dict

def metrics(eq):
    rfr = 0.04
    eq = eq[eq.index.hour == 0]
    rets = eq.pct_change().fillna(0)

    years = list(rets.index.year.drop_duplicates())

    sort = []
    draw = []
    equi = []
    news = []

    data = pd.DataFrame(columns={'year', 'Sortino', 'MDD', 'Equity', 'New Sharpe'})

    for year in years:
        year = str(year)
        year_rets = rets[year]

        #sortino
        sortino = qs.stats.sortino(year_rets, rf= rfr, periods=365, annualize=True)

        #dd
        dd = qs.stats.max_drawdown(year_rets)*100

        year_rets = pd.DataFrame(year_rets, columns=['rets'])
        year_rets['eq'] = (1 + year_rets['rets']).cumprod()

        #equity
        year_eq = qs.stats.comp(year_rets['rets']) + 1

        #linearity
        year_rets['log'] = np.log(year_rets['eq'])
        x = np.arange(year_rets.index.size)
        fit = np.polyfit(x, year_rets['log'], deg=1)
        fit_function = np.poly1d(fit)
        linearity_val = np.sqrt(((year_rets['log'] - fit_function(x))**2).mean())
        cagr_val = qs.stats.cagr(year_rets['eq'].pct_change().fillna(0), rf=rfr, periods=year_rets.shape[0], compounded=True)
        new_sharpe_val = cagr_val/linearity_val

        sort.append(sortino)
        draw.append(dd)
        equi.append(year_eq)
        news.append(new_sharpe_val)

        data = data.append({'New Sharpe': round(new_sharpe_val, 2),
                            'Equity': round(year_eq, 2),
                            'MDD': round(dd, 2), 
                            'Sortino': round(sortino, 2),
                            'year': year}, ignore_index=True)


    sort_avg = sum(sort)/len(sort)
    draw_avg = sum(draw)/len(draw)
    equi_avg = sum(equi)/len(equi)
    new_sharpe_avg = sum(news)/len(news)

    data = data.append({'year': 'AVG', 
                        'Sortino': sort_avg, 
                        'MDD': draw_avg, 
                        'Equity': equi_avg, 
                        'New Sharpe': new_sharpe_avg}, 
                       ignore_index=True)

    data.set_index('year', inplace=True)
    data = data[['Equity', 'MDD', 'Sortino', 'New Sharpe']]
    return data

# BITMEX
alloc = 0
slip = 0

config = {
    'context': {
        'is_shared_balance' : True,
        # 'balance' : 1,
        'balance': 1,  
        'assets': {
            'BTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075 + slip,
                'allocation': alloc
                # 'allocation':   0.60  #MJF
            },
            'ETH/BTC': {
                'exchange': 'binance',
                'maker_fee': -.0005,
                'taker_fee': .0025 + slip,
                'allocation' : alloc
                # 'allocation':   0.10  #MJF

            }
        },
        'parameters': {
            'band': 20,
            'keltner': 19, 
            'per': 18
        },

    },
    'slippage': 0,
    'time_interval': '1',
    'time_unit': 'd',
    'start_date': '2021-01-01 00:00:00',
    'end_date': dt.datetime.now(),
    'exchange': 'bitmex',
    'window_length' : 2
}

pairs_list = config['context']['assets'].keys()

to_trade = [
    'ETH/BTC',
    'BTC/USDT'

] 

dont_trade = []

if 'TRX/BTC' in to_trade:
    config['slippage'] = 0.005
else:
    config['slippage'] = 0.0005

for i in pairs_list:
    if i in to_trade:
        config['context']['assets'][i]['allocation'] = 1/(len(to_trade))
    else:
        config['context']['assets'][i]['allocation'] = 0
        if i != 'BTC/USDT':
            dont_trade.append(i)
    print(f"{i}: {config['context']['assets'][i]['allocation']}")

for d in dont_trade:
    config['context']['assets'].pop(d, None)

from laetitudebots.util.talib_method import talib_method
from laetitudebots.util import custom_method
from laetitudebots.backtest import Backtest
import numpy as np
import talib

def initialize(factors, config):
    band = int(config['parameters']['band'])
    keltner = int(config['parameters']['keltner'])
    per = int(config['parameters']['per'])
    
    factors['high'] = factors.high.ffill()
    factors['low'] = factors.low.ffill()
    factors['close'] = factors.close.ffill()
    
    factors['psar']= custom_method(talib.SAR, [factors.high, factors.low, 0.02, 0.2])
    factors['bb']= talib_method(method='SMA', timeperiod=band, args = [factors.close])

    factors['keltner'] = talib_method(method='EMA', timeperiod = keltner, args = [factors.close])    
    factors['ma1'] = talib_method(method='EMA', timeperiod = per, args = [factors.close])
    factors['ma2'] = talib_method(method='EMA', timeperiod = per, args = [factors.ma1])

def process(window, assets, context):
    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance']
    
    
    for symbol, asset in assets.items():
        position = asset.position
        last = window.latest(symbol)
        settings = context['assets'][symbol]  
            
        keltner = last.keltner
        bb = last.bb
        ma2 = last.ma2
        close = last.close
        psar = last.psar
            
        
        lg_ent = (keltner > bb) and (ma2 < close > psar)
        st_ent = (keltner < bb) and (ma2 > close < psar)
        
 
        if position.size <= 0 and lg_ent:

            pos_size = total_balance * settings['allocation']
            size = abs(position.size) + pos_size * asset.get_contract_rate(last.close)
            order = {
                    'order_type': 'market',
                    'size': size,
                    'side': 'buy'
                    }
            asset.create_order('tdc_long', order)
                
        elif position.size >= 0 and st_ent:

            pos_size = total_balance * settings['allocation']
            size = abs(position.size) + pos_size * asset.get_contract_rate(last.close)
            order = {
                    'order_type': 'market',
                    'size': size,
                    'side': 'sell'
                    }
            asset.create_order('tdc_short', order)

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)
bt.positions.loc['2021-05-28':].to_csv("BMX_TDC-positions.csv")

#TDC END
###################
##### dmiv2
###################
import matplotlib.pyplot as plt
import datetime as dt
import pandas as pd
import warnings
warnings.filterwarnings('ignore')

from laetitudebots.util.talib_method import talib_method
from laetitudebots.backtest import Backtest
import numpy as np

from dateutil.relativedelta import relativedelta
import datetime as dt

def get_win_rate(df, pairs):
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    pairs_dict = {}
    for pair in pairs:
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        win_pct = win_trades / total
        lose_pct = losing_trades / total
        print(f'{pair} Won trades count: {win_trades}')
        print(f'{pair} Lost trades count: {losing_trades}')
        print(f'{pair} Total trades count: {total}')
        print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
        
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
    return pairs_dict

def proc_wr(old_dict, get_win_rate_dict):
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        old_dict[pair] = new2
    return old_dict


"""
Trailing stop

"""

def trail_comp2(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    return trail_stop


def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
        asset.create_order(f'{strategy_name}_{i}', v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f'{strategy_name}_{i}'):
            asset.cancel_order(f'{strategy_name}_{i}')

def calculate_size(symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    min_size = {
        'btcusdt': 100,
        'ethusdt': 1,
        'xrpusdt': 1,
        'ltcusdt': 1,
        'adabtc': 10,
        'ethbtc': 0.01
    }
    # orig position size
    pos_size = total_balance * def_alloc
    orig_size = pos_size * contract_rate
    if symbol.lower().startswith('btc'):
        if orig_size < 100: 
            if orig_size % 100 >= 90:
                orig_size = round(orig_size, -2)
            else: 
                orig_size = 0
        else:
            orig_size = orig_size - (orig_size % 100)
    elif symbol.lower() == 'ada/btc':
        if orig_size < 10:
            if round(orig_size % 10, 2) >= 9:
                orig_size = round(orig_size, -1)
            else:
                orig_size = 0
        else:
            orig_size = orig_size - (orig_size % 10)
    #var pos size
    if with_var == True:
        invalidation = abs((close-kwargs['stop'])/close)
        var_pos_size = (total_balance * kwargs['risk']) / invalidation
        var_size = var_pos_size * contract_rate
        if symbol.lower().startswith('btc'):
            if var_size < 100: 
                if var_size % 100 >= 90:
                    var_size = round(var_size, -2)
                else: 
                    var_size = 0
            else:
                var_size = var_size - (var_size % 100)
        else:
            if 'usd' in symbol.lower():
                if var_size < 1:
                    if round(var_size % 1, 2) >= 0.9:
                        var_size = round(var_size, 0)
                    else:
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 1)
            else:
                if symbol.lower() == 'ada/btc':
                    if var_size < 10:
                        if round(var_size % 10, 2) >= 9:
                            var_size = round(var_size, -1)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 10)
                elif symbol.lower() == 'eth/btc':
                    if var_size < 1:
                        if round(var_size % 0.01, 3) >= 0.009:
                            var_size = round(var_size, 2)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
        size = min(orig_size, var_size)
    else:
        size = orig_size
    return size    


# BITMEX
config = {
    'context': {
        'is_shared_balance' : True,
        'balance': 1,  
        'assets': {
            'ETH/BTC': {
                'exchange': 'binance',
                'maker_fee': -.0005,
                'taker_fee': .0025,
                'allocation' : 0.05,
                'trail': 0
            },
            'BTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.35,
                'trail': 0
            },
            'ETH/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.35,
                'trail': 0
            },
            'XRP/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.25,
                'trail': 0
            }
        },
        'parameters': {
            'idx': 13,
            'ddx': 22,
            'risk': 0.02,
            
            #trail stop parameter
            'trail_perc': 0.01,
            
            #TP
            'rrr': 10
        },

    },
    'slippage': 0.0005,
    'time_interval': '12',
    'time_unit': 'h',
    'start_date': '2020-01-01 00:00:00',    
    'end_date': dt.date.today(),
    'exchange': 'bitmex',
    'window_length' : 2
}


def initialize(factors, config):
    
    idx = config['parameters']['idx']
    
    factors['close'] = factors.close.ffill()
    factors['high'] = factors.high
    factors['slow'] = factors.low
    
    #dmi
    factors['dx'] = talib_method(method='DX', timeperiod = idx, args = [factors.high, factors.low, factors.close])
    factors['min_di'] = talib_method(method='MINUS_DI', timeperiod = idx, args =[factors.high, factors.low, factors.close])
    factors['plus_di'] = talib_method(method='PLUS_DI', timeperiod = idx, args =[factors.high, factors.low, factors.close])

def process(window, assets, context):
    
    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance']
    
    ### VAR pos sizing
    risk = config['context']['parameters']['risk']  
    
    for symbol, asset in assets.items():
        position = asset.position
        last = window.latest(symbol)
        lastt = window.previous(symbol, 2)
        
        if not np.all([lastt.dx, lastt.plus_di, lastt.min_di]):
            continue
        settings = context['assets'][symbol]
             
        dx = last.dx
        pdi = last.plus_di
        mdi = last.min_di
        
        ### Dx config
        ddx = config['context']['parameters']['ddx']
        
        ### Trailing Stop
        trail_perc = config['context']['parameters']['trail_perc']
        
        ### Take profit
        rrr = config['context']['parameters']['rrr']
          
        ### Entry Conditions
        lg_ent = ((dx > ddx) and
                 (dx > lastt.dx) and
                 (last.close > lastt.close) and
                 (pdi > mdi) and
                 (pdi > lastt.plus_di))
        
        st_ent = ((dx > ddx) and
                 (dx > lastt.dx) and
                 (last.close < lastt.close) and
                 (mdi > pdi) and
                 (mdi > lastt.min_di))
        
        ## %-based SL
        if lg_ent:
            stop = lastt.close *0.97
        elif st_ent:
            stop = lastt.close *1.03
        else:
            stop = 0
        
        ### Trailing stop - kit
        trail_stop = settings['trail']

        size = calculate_size(symbol, total_balance, last.close, settings['allocation'], 
                              asset.get_contract_rate(last.close), 
                              with_var=True, stop=stop, risk=config['context']['parameters']['risk'])
        
        if position.size == 0:
            
            ### cancel orders
            cancel_order('dmi', asset, 'long_tp', 'short_tp')
            
            if lg_ent:
                order = proc_order('market', size, 'buy')

                ### Trail adjustment or SL
                settings['trail'] = stop
                order_sl = proc_order('stop', size, 'sell', trigger=stop)

                ### TP Computation
                close_tp  = abs((stop - last.close) / last.close) * rrr
                close_tp2 = last.close * (close_tp + 1)

                ### TP Orders
                order_tp  = proc_order('limit', size, 'sell', price=close_tp2)

                ### Execute
                execute_order('dmi', asset, long=order, long_stop=order_sl, long_tp=order_tp)
                
            
            elif st_ent:  
                order = proc_order('market', size, 'sell')
                settings['trail'] = stop
                
                order_sl  = proc_order('stop', size, 'buy', trigger=stop)

                close_tp  = abs((last.close - stop) / last.close) * rrr
                close_max = last.close * (1 - close_tp)
                
                close_tp2 = max(last.close*0.1, close_max)
                order_tp  = proc_order('limit', size, 'buy', price=close_tp2)

                execute_order('dmi', asset, short=order, short_stop=order_sl, short_tp=order_tp)

                
        elif position.size > 0:
            if st_ent:                   
                cancel_order('dmi', asset, 'long_stop', 'long_trail', 'long_tp')
                settings['trail'] = stop
                order     = proc_order('market', size + abs(position.size), 'sell')
                order_sl  = proc_order('stop', size, 'buy', trigger=stop)
                
                close_tp  = abs((last.close - stop) / last.close) * rrr
                close_max = last.close * (1 - close_tp)
                
                close_tp2 = max(last.close*0.1, close_max)
                order_tp  = proc_order('limit', size, 'buy', price=close_tp2)
                
                execute_order('dmi', asset, short=order, short_stop=order_sl, short_tp=order_tp)
                        
            
            else:
                trail_stop = trail_comp2(position.size, trail_perc, last.close, lastt.close, settings['trail'])
                settings['trail'] = trail_stop  

    
                if last.close < trail_stop:
                    order = proc_order('market', abs(position.size), 'sell')
                    execute_order('dmi', asset, long_trail=order)
                    cancel_order('dmi', asset, 'long_stop')
    
        elif position.size < 0:   
            if lg_ent:                
                cancel_order('dmi', asset, 'short_stop', 'short_trail', 'short_tp')
                settings['trail'] = stop
                order     = proc_order('market', size + abs(position.size), 'buy')
                order_sl  = proc_order('stop', size, 'sell', trigger=stop)
                
                close_tp  = abs((stop - last.close) / last.close) * rrr
                close_tp2 = last.close * (close_tp + 1)
                order_tp  = proc_order('limit', size, 'sell', price=close_tp2)
                
                execute_order('dmi', asset, long=order, long_stop=order_sl, long_tp=order_tp)
                
            else:                
                trail_stop = trail_comp2(position.size, trail_perc, last.close, lastt.close, settings['trail'])
                settings['trail'] = trail_stop

                if last.close > trail_stop:
                    order = proc_order('market', abs(position.size), 'buy')
                    execute_order('dmi', asset, long_trail=order)
                    cancel_order('dmi', asset, 'short_stop')

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar = False)

bt.positions.loc['2021-05-28':].to_csv("BMX_DMIv2-positions.csv")



#### 
####
#### TS2v3

# alloc = 0
slippage = 0.0005
taker_fee = 0.00075
maker_fee = -0.0001

config = {
    'context': {
        'is_shared_balance' : True,
        'balance' : 1,
        'assets': {},
        'parameters': {
            'ma_len': 39,
            'risk': 0.03,
            
            ###SL perc
            'sl_perc': 0.1,
            
            ###TS
            'trail_perc': 0.01,
            
            ###TP 
                ### Bars
            'rrr': 10, 
            
                ### RSI
            'rsi_len': 14,
            'rsi_ob': 70,
            'rsi_os': 30,
            
        },
    },
    'slippage': 0,
    'time_interval': '8',
    'time_unit': 'h',
    'start_date': '2022-02-16 16:00:00',    
    'end_date': dt.date.today(), # - dt.timedelta(days=1),
    'exchange': 'bitmex',
    'window_length' : 2
    }


pairs = {
    'BTC/USDT': ['binance', 0.375],
    'ETH/USDT': ['binance', 0.375],
    'XRP/USDT': ['binance', 0.25],
}

for i,v in pairs.items():
    config['context']['assets'][i] = {
        'exchange': v[0],
        'maker_fee': -.00025,
        'taker_fee': taker_fee,
        'allocation' : v[1],

        ###Take profit
        'last_pos': None,
        
        ###Trailing Stop
        'trail': 0,
        }

"""
Trailing stop

"""
def trail_comp(direction, percentage, price, current_trail):
    if direction > 0:
        trail_stop = price * (1-percentage)
        if price < trail_stop:
            trail_stop = current_trail
        elif (trail_stop < current_trail):
            trail_stop = current_trail
    elif direction < 0:
        trail_stop = price * (1+percentage)
        if price > trail_stop:
            trail_stop = current_trail
        elif (trail_stop > current_trail):
            trail_stop = current_trail
    return trail_stop


def trail_comp2(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    return trail_stop


def trail_comp3(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        elif last_close < prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
        
                
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        elif last_close > prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail 
        else:
            trail_stop = current_trail
    return trail_stop

def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
        asset.create_order(f'{strategy_name}_{i}', v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f'{strategy_name}_{i}'):
            asset.cancel_order(f'{strategy_name}_{i}')

def calculate_size(symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    min_size = {
        'btcusd': 100,
        'ethusd': 1,
        'xrpusd': 1,
        'ltcusd': 1,
        'adabtc': 10,
        'ethbtc': 0.01
    }
    # orig position size
    pos_size = total_balance * def_alloc
    orig_size = np.floor(pos_size * contract_rate)
    if symbol.lower().startswith('btc'):
        if orig_size < 100: 
            if orig_size % 100 >= 90:
                orig_size = round(orig_size, -2)
            else: 
                orig_size = 0
        else:
            orig_size = orig_size - (orig_size % 100)
    elif symbol.lower() == 'ada/btc':
        if orig_size < 10:
            if round(orig_size % 10, 2) >= 9:
                orig_size = round(orig_size, -1)
            else:
                orig_size = 0
        else:
            orig_size = orig_size - (orig_size % 10)
    #var pos size
    if with_var:
        invalidation = abs((close-kwargs['stop'])/close)
        var_pos_size = (total_balance * kwargs['risk']) / invalidation
        var_size = (var_pos_size * contract_rate)
        if symbol.lower().startswith('btc'):
            if var_size < 100: 
                if var_size % 100 >= 90:
                    var_size = round(var_size, -2)
                else: 
                    var_size = 0
            else:
                var_size = var_size - (var_size % 100)
        else:
            if 'usd' in symbol.lower():
                if var_size < 1:
                    if round(var_size % 1, 2) >= 0.9:
                        var_size = round(var_size, 0)
                    else:
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 1)
            else:
                if symbol.lower() == 'ada/btc':
                    if var_size < 10:
                        if round(var_size % 10, 2) >= 9:
                            var_size = round(var_size, -1)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 10)
                elif symbol.lower() == 'eth/btc':
                    if var_size < 1:
                        if round(var_size % 0.01, 3) >= 0.009:
                            var_size = round(var_size, 2)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
        size = min(orig_size, var_size)
    else:
        size = orig_size
    return size

def get_win_rate(df, pairs):
    
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    
    pairs_dict = {}
    for pair in pairs:
        
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        win_pct = win_trades / total
        lose_pct = losing_trades / total
        
        print(f'{pair} Won trades count: {win_trades}')
        print(f'{pair} Lost trades count: {losing_trades}')
        print(f'{pair} Total trades count: {total}')
        print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
        
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
        
    return pairs_dict
    
    
def proc_wr(old_dict, get_win_rate_dict):
    
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        
        
        old_dict[pair] = new2
    
    return old_dict

### SL with TP and Bars reentry
def initialize(factors, config):
    ma_len = config['parameters']['ma_len']
    
    ### TP reentry 
        ### RSI
    rsi_len = config['parameters']['rsi_len']
    factors['rsi'] = talib_method(method='RSI', timeperiod=rsi_len, args=[factors.close])
    
    ### Strategy Parameters
    trail_perc = config['parameters']['trail_perc']

    
    ### Strategy indicators
    factors['mao'] = talib_method(method='SMA', timeperiod=ma_len, args=[factors.open])
    factors['mah'] = talib_method(method='SMA', timeperiod=ma_len, args=[factors.high])
    factors['mal'] = talib_method(method='SMA', timeperiod=ma_len, args=[factors.low])
    factors['mac'] = talib_method(method='SMA', timeperiod=ma_len, args=[factors.close])
    factors['wid'] = (factors['mah'] - factors['mal']) / factors['mal']

def process(window, assets, context):
    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 

    ### SL perc
    sl_perc = context['parameters']['sl_perc']
    
    
    ### VAR pos sizing
    risk = context['parameters']['risk']
    
    ### Trailing Stop
    trail_perc = context['parameters']['trail_perc']
    
    ### Take profit
        ### Bars
    rrr = context['parameters']['rrr']
        ### RSI
    rsi_ob = context['parameters']['rsi_ob']
    rsi_os = context['parameters']['rsi_os']
    
    for symbol, asset in assets.items():
        
        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)
        
        settings = context['assets'][symbol]
        
        ### Take Profit
            ### Bars
        last_pos = settings['last_pos']
            ### Bars for reentry
        
            ### RSI
        rsi_xabove = prev_bar.rsi < rsi_ob and last_bar.rsi > rsi_ob
        rsi_xbelow = prev_bar.rsi > rsi_os and last_bar.rsi < rsi_os
        
        ### Trailing Stop
        trail = settings['trail']
        
        position = asset.position
    
        ### entry conditions
        lg_sig = (last_bar.wid < (10/100)) & (last_bar.mac > last_bar.mao) & (last_bar.close > last_bar.mah)
        st_sig = (last_bar.wid < (10/100)) & (last_bar.mac < last_bar.mao) & (last_bar.close < last_bar.mal)

        if lg_sig:
            stop = last_bar.close * (1 - sl_perc)
        elif st_sig:
            stop = last_bar.close * (1 + sl_perc)
        else:
            stop = 0
        
        size = calculate_size(symbol, total_balance, last_bar.close, settings['allocation'], 
                              asset.get_contract_rate(last_bar.close), 
                              with_var=True, stop=stop, risk=risk)
                              
        
        rsi_ob = context['parameters']['rsi_ob']
        rsi_os = context['parameters']['rsi_os']
        
        rsi_xabove = prev_bar.rsi < rsi_ob and last_bar.rsi > rsi_ob
        rsi_xbelow = prev_bar.rsi > rsi_os and last_bar.rsi < rsi_os
        
        ### Trailing stop - kit
            ### initial trail
        trail_stop = settings['trail']


        if position.size == 0:
            
            ### cancel orders
            cancel_order('rider', asset, 'long_stop', 'long_trail', 'long_tp', 'short_stop', 'short_trail', 'short_tp')

            if lg_sig:
                if (settings['last_pos'] != 'Long') or (settings['last_pos'] == 'Long' and rsi_xabove):
                    
                    settings['last_pos'] = 'Long'
                    
                    ### long entry
                    order = proc_order('market', size, 'buy')
                    
                    ### long sl
                    order_sl = proc_order('stop', size, 'sell', trigger=stop)
                    settings['trigger_price'] = stop
                    
                    ### Trail adjustment
                    settings['trail'] = stop
                    
                    """
                    stop = will be changed to SL of other strats
                    rrr = to be optimized (include in config)
                    """
                    
                    close_tp = abs((stop - last_bar.close) / last_bar.close) * rrr
                    close_tp2 = last_bar.close * (close_tp + 1)
                    ### TP
                    
                    order_tp = proc_order('limit', size, 'sell', price=close_tp2)
                    
                    execute_order('rider', asset, long=order, long_stop=order_sl, long_tp=order_tp)
                                
            elif st_sig:
                if (settings['last_pos'] != 'Short') or (settings['last_pos'] == 'Short' and rsi_xbelow):
                    
                    settings['last_pos'] = 'Short'

                    order = proc_order('market', size, 'sell')
                    order_sl = proc_order('stop', size, 'buy', trigger=stop)
                    settings['trigger_price'] = stop

                    close_tp = abs((last_bar.close - stop) / last_bar.close) * rrr
                    # close_tp2 = last_bar.close * (1 - close_tp)

                    close_max = last_bar.close * (1 - close_tp)
                    close_tp2 = max(last_bar.close*0.1, close_max)

                    order_tp = proc_order('limit', size, 'buy', price=close_tp2)

                    settings['trail'] = stop

                    execute_order('rider', asset, short=order, short_stop=order_sl, short_tp=order_tp)

        elif position.size > 0:
            if st_sig:
                
                settings['last_pos'] = 'Short'
                
                cancel_order('rider', asset, 'long_stop', 'long_trail', 'long_tp')
                
                ### reverse to short
                order = proc_order('market', size + abs(position.size), 'sell')
                order_sl = proc_order('stop', size, 'buy', trigger=stop)
                settings['trigger_price'] = stop
                
                close_tp = abs((last_bar.close - stop) / last_bar.close) * rrr
                # close_tp2 = last_bar.close * (1 - close_tp)

                close_max = last_bar.close * (1 - close_tp)
                close_tp2 = max(last_bar.close*0.1, close_max)
                
                order_tp = proc_order('limit', size, 'buy', price=close_tp2)
                
                settings['trail'] = stop
                
                execute_order('rider', asset, short=order, short_stop=order_sl, short_tp=order_tp)

            else:
                settings['last_pos'] = 'Long'
                
                ## Trail adjustment (kit)
                trail_stop = trail_comp2(position.size, trail_perc, last_bar.close, prev_bar.close, settings['trail'])
                settings['trail'] = trail_stop  

                
                if asset.orders.get('rider_long_stop'):
                    if settings['trigger_price'] < settings['trail']:
                        cancel_order('rider', asset, 'long_stop')
                        
                        ### trail long
                        order = proc_order('stop', abs(position.size), 'sell', trigger=settings['trail'])
                        execute_order('rider', asset, long_trail=order)
                      
                if asset.orders.get('rider_long_trail'):
                    if settings['trigger_price'] < settings['trail']:
                        cancel_order('rider', asset, 'long_trail')
                          
                        ### trail long
                        order = proc_order('stop', abs(position.size), 'sell', trigger=settings['trail'])
                        execute_order('rider', asset, long_trail=order)
         
        elif position.size < 0:
            if lg_sig:
                settings['last_pos'] = 'Long'
                cancel_order('rider', asset, 'short_stop', 'short_trail', 'short_tp')
                
                ### reverse to long
                order = proc_order('market', size + abs(position.size), 'buy')
                order_sl = proc_order('stop', size, 'sell', trigger=stop)
                settings['trigger_price'] = stop
                
                close_tp = abs((stop - last_bar.close) / last_bar.close) * rrr
                close_tp2 = last_bar.close * (close_tp + 1)
                order_tp = proc_order('limit', size, 'sell', price=close_tp2)
                
                settings['trail'] = stop
                
                execute_order('rider', asset, long=order, long_stop=order_sl, long_tp=order_tp)


            else:
                
                ## Trail adjustment (kit)
                trail_stop = trail_comp2(position.size, trail_perc, last_bar.close, prev_bar.close, settings['trail'])
                settings['trail'] = trail_stop

                if asset.orders.get('rider_short_stop'):
                    if settings['trigger_price'] > settings['trail']:
                        cancel_order('rider', asset, 'short_stop')
                        order = proc_order('stop', abs(position.size), 'buy', trigger=settings['trail'])
                        execute_order('rider', asset, short_trail=order)
                      
                if asset.orders.get('rider_short_trail'):
                    if settings['trigger_price'] > settings['trail']:
                        cancel_order('rider', asset, 'short_trail')
                        
                        order = proc_order('stop', abs(position.size), 'buy', trigger=settings['trail'])
                        execute_order('rider', asset, short_trail=order)

def process(window, assets, context):
    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 

    ### SL perc
    sl_perc = config['context']['parameters']['sl_perc']
    
    
    ### VAR pos sizing
    risk = config['context']['parameters']['risk']
    
    ### Trailing Stop
    trail_perc = config['context']['parameters']['trail_perc']
    
    ### Take profit
        ### Bars
    rrr = config['context']['parameters']['rrr']
        ### RSI
    rsi_ob = context['parameters']['rsi_ob']
    rsi_os = context['parameters']['rsi_os']
    
    for symbol, asset in assets.items():
        
        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)
        
        settings = context['assets'][symbol]
        
        ### Take Profit
            ### Bars
        last_pos = settings['last_pos']
            ### Bars for reentry
        
            ### RSI
        rsi_xabove = prev_bar.rsi < rsi_ob and last_bar.rsi > rsi_ob
        rsi_xbelow = prev_bar.rsi > rsi_os and last_bar.rsi < rsi_os
        
        ### Trailing Stop
        trail = settings['trail']
        
        position = asset.position
    
        ### entry conditions
        """
        Long = (last bar wid < 10%) & (last bar close MA > last bar open MA) & (last bar close > last bar high MA)
        Short = (last bar wid < 10%) & (last bar close MA < last bar open MA) & (last bar close < last bar low MA)
        """
        lg_sig = (last_bar.wid < (10/100)) & (last_bar.mac > last_bar.mao) & (last_bar.close > last_bar.mah)
        st_sig = (last_bar.wid < (10/100)) & (last_bar.mac < last_bar.mao) & (last_bar.close < last_bar.mal)
        
            
        if lg_sig:
            stop = last_bar.close * (1 - sl_perc)
        elif st_sig:
            stop = last_bar.close * (1 + sl_perc)
        else:
            stop = 0
        

        size = calculate_size(symbol, total_balance, last_bar.close, settings['allocation'], 
                              asset.get_contract_rate(last_bar.close), 
                              with_var=True, stop=stop, risk=risk)
        
        rsi_ob = context['parameters']['rsi_ob']
        rsi_os = context['parameters']['rsi_os']
        
        rsi_xabove = prev_bar.rsi < rsi_ob and last_bar.rsi > rsi_ob
        rsi_xbelow = prev_bar.rsi > rsi_os and last_bar.rsi < rsi_os
        
        ### Trailing stop - kit
            ### initial trail
        trail_stop = settings['trail']
        
        if position.size == 0:
            
            ### cancel orders
            cancel_order('rider', asset, 'long_stop', 'long_trail', 'long_tp', 'short_stop', 'short_trail', 'short_tp')

            if lg_sig:
                
                if (settings['last_pos'] != 'Long') or (settings['last_pos'] == 'Long' and rsi_xabove):
                    
                    settings['last_pos'] = 'Long'
                    
                    ### long entry
                    order = proc_order('market', size, 'buy')
                    
                    ### long sl
                    order_sl = proc_order('stop', size, 'sell', trigger=stop)
                    settings['trigger_price'] = stop
                
                    ### Trail adjustment
                    settings['trail'] = stop
                    
                    """
                    stop = will be changed to SL of other strats
                    rrr = to be optimized (include in config)
                    """
                    
                    close_tp = abs((stop - last_bar.close) / last_bar.close) * rrr
                    close_tp2 = last_bar.close * (close_tp + 1)
                    ### TP
                    
                    order_tp = proc_order('limit', size, 'sell', price=close_tp2)
                    
                    execute_order('rider', asset, long=order, long_stop=order_sl, long_tp=order_tp)
                                
            elif st_sig:
                if (settings['last_pos'] is not 'Short') or (settings['last_pos'] is 'Short' and rsi_xbelow):
                    
                    settings['last_pos'] = 'Short'

                    order = proc_order('market', size, 'sell')
                    order_sl = proc_order('stop', size, 'buy', trigger=stop)
                    settings['trigger_price'] = stop

                    close_tp = abs((last_bar.close - stop) / last_bar.close) * rrr
                    close_max = last_bar.close * (1 - close_tp)
                    close_tp2 = max(last_bar.close*0.1, close_max)

                    order_tp = proc_order('limit', size, 'buy', price=close_tp2)

                    settings['trail'] = stop

                    execute_order('rider', asset, short=order, short_stop=order_sl, short_tp=order_tp)

        elif position.size > 0:
            if st_sig:
                
                settings['last_pos'] = 'Short'
                
                cancel_order('rider', asset, 'long_stop', 'long_trail', 'long_tp')
                
                ### reverse to short
                order = proc_order('market', size + abs(position.size), 'sell')
                order_sl = proc_order('stop', size, 'buy', trigger=stop)
                
                close_tp = abs((last_bar.close - stop) / last_bar.close) * rrr
                close_max = last_bar.close * (1 - close_tp)
                close_tp2 = max(last_bar.close*0.1, close_max)
                order_tp = proc_order('limit', size, 'buy', price=close_tp2)
                
    
                settings['trail'] = stop
                settings['trigger_price'] = stop
                
                execute_order('rider', asset, short=order, short_stop=order_sl, short_tp=order_tp)
                

            else:
                ## Trail adjustment (kit)
                trail_stop = trail_comp2(position.size, trail_perc, last_bar.close, prev_bar.close, settings['trail'])
                settings['trail'] = trail_stop  
                
                
                
                if asset.orders.get('rider_long_stop'):
                    if settings['trigger_price'] < settings['trail']:
                        cancel_order('rider', asset, 'long_stop')
                        
                        ### trail long
                        order = proc_order('stop', abs(position.size), 'sell', trigger=settings['trail'])
                        execute_order('rider', asset, long_trail=order)
                      
                if asset.orders.get('rider_long_trail'):
                    if settings['trigger_price'] < settings['trail']:
                        cancel_order('rider', asset, 'long_trail')
                        
                        if symbol == 'ETH/USDT':
                            print('adjust long SL', settings['trigger_price'], settings['trail'], last_bar.timestamp)
                        ### trail long
                        order = proc_order('stop', abs(position.size), 'sell', trigger=settings['trail'])
                        execute_order('rider', asset, long_trail=order)
                              
         
        elif position.size < 0:
            if lg_sig:
                settings['last_pos'] = 'Long'
                cancel_order('rider', asset, 'short_stop', 'short_trail', 'short_tp')
                
                ### reverse to long
                order = proc_order('market', size + abs(position.size), 'buy')
                order_sl = proc_order('stop', size, 'sell', trigger=stop)
                
                close_tp = abs((stop - last_bar.close) / last_bar.close) * rrr
                close_tp2 = last_bar.close * (close_tp + 1)
                order_tp = proc_order('limit', size, 'sell', price=close_tp2)
                
                settings['trail'] = stop
                settings['trigger_price'] = stop
                
                execute_order('rider', asset, long=order, long_stop=order_sl, long_tp=order_tp)
                

            else:
                
                ## Trail adjustment (kit)
                trail_stop = trail_comp2(position.size, trail_perc, last_bar.close, prev_bar.close, settings['trail'])
                settings['trail'] = trail_stop
                
                
                
            
                if asset.orders.get('rider_short_stop'):
                    if settings['trigger_price'] > settings['trail']:
                        cancel_order('rider', asset, 'short_stop')
                        order = proc_order('stop', abs(position.size), 'buy', trigger=settings['trail'])
                        execute_order('rider', asset, short_trail=order)
                    
                      
                elif asset.orders.get('rider_short_trail'):
                    if settings['trigger_price'] > settings['trail']:
                        cancel_order('rider', asset, 'short_trail')
                        
                        order = proc_order('stop', abs(position.size), 'buy', trigger=settings['trail'])
                        execute_order('rider', asset, short_trail=order)
                        

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

# save_data
bt.positions.loc['2021-05-28':].to_csv("BMX_TS2v3-positions.csv")

#### TS2
# BITMEX

# alloc = 0
slippage = 0.0005
taker_fee = 0.00075
maker_fee = -0.0001

config = {
    'context': {
        'is_shared_balance' : True,
        # 'balance' : 1,
        'balance': 1,  
        'assets': {},
        'parameters': {
            'ma_len': 91,
            'mult': 3,
            'risk': 0.01,
            
            ###SL perc
            'sl_perc': 0.1,
            
            ###TS
            'trail_perc': 0.01,
            
            ###TP 
                ### Bars
            'pos_count': 5,
            'rrr': 5, 
            
                ### RSI
            'rsi_len': 14,
            'rsi_ob': 70,
            'rsi_os': 30,
            
        },
    },
    'slippage': 0,
    'time_interval': '8',
    'time_unit': 'h',
    'start_date': '2017-01-01 00:00:00',    
    'end_date': dt.date.today(), # - dt.timedelta(days=1),
    'exchange': 'bitmex',
    'window_length' : 2
    }


pairs = {
    'BTC/USDT': ['binance', 1],
}

for i,v in pairs.items():
    config['context']['assets'][i] = {
        'exchange': v[0],
        'maker_fee': -.00025,
        'taker_fee': taker_fee,
        'allocation' : v[1],

        ###Take profit
        'last_pos': None,
        'long_pos': 0,
        'short_pos': 0,
        'no_pos': 0,
        
        ###Trailing Stop
        'trail': 0,
        }


def initialize(factors, config):
    ma_len = config['parameters']['ma_len']
    
    factors['mao'] = talib_method(method='SMA', timeperiod=ma_len, args=[factors.open])
    factors['mah'] = talib_method(method='SMA', timeperiod=ma_len, args=[factors.high])
    factors['mal'] = talib_method(method='SMA', timeperiod=ma_len, args=[factors.low])
    factors['mac'] = talib_method(method='SMA', timeperiod=ma_len, args=[factors.close])
    factors['wid'] = (factors['mah'] - factors['mal']) / factors['mal']
    

def process(window, assets, context):

    total_unrealised_pnl = sum([asset.position.unrealised_pnl
                                for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance']
    

    for symbol, asset in assets.items():
        position = asset.position
        
        last_bar = window.latest(symbol)
            
        settings = context['assets'][symbol]
        

        lg_ent = (last_bar.wid < (10/100)) & (last_bar.mac > last_bar.mao) & (last_bar.close > last_bar.mah)
        st_ent = (last_bar.wid < (10/100)) & (last_bar.mac < last_bar.mao) & (last_bar.close < last_bar.mal)
        
        long_price = last_bar.close
        short_price = last_bar.close
        
        lg_close = (last_bar.close < last_bar.mal)# or (last_bar.close < (position.avg_price * (90/100)))
        st_close = (last_bar.close > last_bar.mah)# or (last_bar.close > (position.avg_price * (110/100)))
        
     
        if position.size == 0:
            if lg_ent:
                long_price = last_bar.close
                pos_size = total_balance * settings['allocation']

                size = pos_size * asset.get_contract_rate(long_price)
                
                order = {
                    'order_type': 'market',
                    'size': size,
                    'side': 'buy',
                }
                
                asset.create_order('ma_long', order)
            
            if st_ent:
                short_price = last_bar.close
                pos_size = total_balance * settings['allocation']
                
                size = pos_size * asset.get_contract_rate(short_price)
                order = {
                    'order_type': 'market',
                    'size': size,
                    'side': 'sell',
                }
                asset.create_order('ma_short', order)
                
        elif position.size < 0:
            if lg_ent:
                long_price = last_bar.close
                pos_size = total_balance * (1/len(assets))

                size = (abs(position.size)
                        + pos_size * asset.get_contract_rate(long_price)) 
                
                order = {
                    'order_type': 'market',
                    'size': size,
                    'side': 'buy',
                }
                
                asset.create_order('ma_long', order)
            elif st_close:
                size = abs(position.size)
                order = {
                    'order_type': 'market',
                    'size': size,
                    'side': 'buy',
                }
                asset.create_order('mashort_exit', order)
            
        elif position.size > 0:
            if st_ent:
                short_price = last_bar.close
                pos_size = total_balance * (1/len(assets))
                
                size = (abs(position.size)
                       + pos_size * asset.get_contract_rate(short_price))
                order = {
                    'order_type': 'market',
                    'size': size,
                    'side': 'sell',
                }
                asset.create_order('ma_short', order)
        
            elif lg_close: 
                size = abs(position.size)
                order = {
                    'order_type': 'market',
                    'size': size,
                    'side': 'sell',
                }
                asset.create_order('malong_exit', order)

    
bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)    

# save_data
bt.positions.loc['2021-05-28':].to_csv("BMX_TS2-positions.csv")



# %% [markdown]
# ### MM3

# %%
config = {
    'context': {
        'is_shared_balance' : True,
        # 'balance' : 1,
        'balance': 1,  
        'assets': {
            'BTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075 + 0.0005,
                'allocation': 0.33
            },
            'LTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075 + 0.0005,
                'allocation' : 0.33          
            },
            'XRP/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075 + 0.0005,
                'allocation' : 0.33
            }
        },
        'parameters': {
            # 'lb' : 78
            'lb' : 29
        },

    },
    'slippage': 0,
    'time_interval': '12',
    'time_unit': 'h',
    'start_date': '2017-01-01 00:00:00',
    'end_date': dt.date.today(), #dt.datetime.now(),
    'exchange': 'bitmex',
    'window_length' : 2
}

def nmm(values):
    nmm = 0.0
    sum = 0.0
    length = 0.0
    length = len(values)
    last_val = values[-1]
    x = 0
    for i in range(1, (length-1)):
        x =+ 1
        sum = sum + (last_val - values[i]) / np.sqrt(i*1.0)
    nmmv = sum/(length)
    return float(nmmv)

def initialize(factors, config):

    lb = config['parameters']['lb']

    factors['log'] = np.log(factors.close)
    factors['nmm'] = factors.log.rolling(lb).apply(nmm)

def process(window, assets, context):

    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 

    for symbol, asset in assets.items():

        position = asset.position

        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)

        settings = context['assets'][symbol] 

        lg_sig = (prev_bar.nmm < 0) & (last_bar.nmm > 0)
        st_sig = (prev_bar.nmm > 0) & (last_bar.nmm < 0)

        if position.size <= 0 and lg_sig:
            pos_size = total_balance * settings['allocation']
            size = abs(position.size) + pos_size * asset.get_contract_rate(last_bar.close)

            order = {
                'order_type' : 'market',
                'size' : size,
                'side' : 'buy'
            }

            asset.create_order('nmm_long', order)
        
        if position.size >= 0 and st_sig:
            pos_size = total_balance * settings['allocation']
            size = abs(position.size) + pos_size * asset.get_contract_rate(last_bar.close)

            order = {
                'order_type' : 'market',
                'size' : size,
                'side' : 'sell'
            }

            asset.create_order('nmm_short', order)

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

# save_data

bt.positions.loc['2021-05-28':].to_csv("BMX_MM3-positions.csv")

# %% [markdown]
# ### TT1

# %%
config = {
    'context': {
        'is_shared_balance' : True,
        'balance': 1,  
        'assets': {
            'BTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075 + 0.0005,
                'allocation': 1
            }
        },
        'parameters': {
            'length': 12,
            'mult': 5
            # 'length': 4,
            # 'mult': 4
        },

    },
    'slippage': 0,
    'time_interval': '4',
    'time_unit': 'h',
    'start_date': '2017-01-01 00:00:00',    
    'end_date': dt.date.today(),
    'exchange': 'bitmex',
    'window_length' : 2
}

def initialize(factors, config):

    length = config['parameters']['length']
    mult = config['parameters']['mult']

    factors['high'] = factors.high
    factors['low'] = factors.low
    factors['close'] = factors.close

    factors['atr'] = talib_method(method='ATR', timeperiod=1, args=[factors.high, factors.low, factors.close])
    factors['avg_tr'] = talib_method(method='WMA', timeperiod=length, args=[factors.atr])
    factors['highest'] = factors.high.rolling(length).max()
    factors['lowest'] = factors.low.rolling(length).min()
    factors['hi_lim'] = factors.highest.shift(1) - (factors.avg_tr.shift(1) * mult)
    factors['lo_lim'] = factors.lowest.shift(1) + (factors.avg_tr.shift(1) * mult) 

def process(window, assets, context):

    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 

    for symbol, asset in assets.items():

        position = asset.position

        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)

        if not np.all([prev_bar.atr, prev_bar.avg_tr, prev_bar.hi_lim, prev_bar.lo_lim]):
            continue

        settings = context['assets'][symbol] 

        lg_sig = last_bar.hi_lim < last_bar.close > last_bar.lo_lim
        st_sig = last_bar.hi_lim > last_bar.close < last_bar.lo_lim

        if position.size <= 0 and lg_sig:
            
            balance = total_balance
            pos_size = balance * settings['allocation']
            size = abs(position.size) + pos_size * asset.get_contract_rate(last_bar.close)

            order = {
                'order_type' : 'market',
                'size' : size,
                'side' : 'buy'
            }

            asset.create_order('rider_long', order)
        
        if position.size >= 0 and st_sig:

            balance = total_balance
            pos_size = balance * settings['allocation']
            size = abs(position.size) + pos_size * asset.get_contract_rate(last_bar.close)

            order = {
                'order_type' : 'market',
                'size' : size,
                'side' : 'sell'
            }

            asset.create_order('rider_short', order)
   
bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

# save_data

bt.positions.loc['2021-05-28':].to_csv("BMX_TT1-positions.csv")

# %% [markdown]
# ### TT3

# %%
config = {
    'context': {
        'is_shared_balance' : True,
        'balance': 1,  
        'assets': {
            'ADA/BTC': {
                'exchange': 'binance',
                'maker_fee': -.0005,
                'taker_fee': .0025 + 0.005,
                'allocation' : 0.14
            },
            'BTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075 + 0.0005,
                'allocation': (1-.14)/3
            },
            'LTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075 + 0.0005,
                'allocation' : (1-.14)/3
            },
            'XRP/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075 + 0.0005,
                'allocation' : (1-.14)/3
            }
        },
        'parameters': {
            # 'length': 30,
            # 'mult': 1
            'length': 5,
            'mult': 0.5
        },

    },
    'slippage': 0,
    'time_interval': '1',
    'time_unit': 'd',
    'start_date': '2017-01-01 00:00:00',    
    'end_date': dt.date.today(),
    'exchange': 'bitmex',
    'window_length' : 2
}

def initialize(factors, config):

    length = config['parameters']['length']
    mult = config['parameters']['mult']

    factors['high'] = factors.high
    factors['low'] = factors.low
    factors['close'] = factors.close

    factors['atr'] = talib_method(method='ATR', timeperiod=1, args=[factors.high, factors.low, factors.close])
    factors['avg_tr'] = talib_method(method='WMA', timeperiod=length, args=[factors.atr])
    factors['highest'] = factors.high.rolling(length).max()
    factors['lowest'] = factors.low.rolling(length).min()
    factors['hi_lim'] = factors.highest.shift(1) - (factors.avg_tr.shift(1) * mult)
    factors['lo_lim'] = factors.lowest.shift(1) + (factors.avg_tr.shift(1) * mult) 

def process(window, assets, context):

    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 

    for symbol, asset in assets.items():

        position = asset.position

        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)

        if not np.all([prev_bar.atr, prev_bar.avg_tr, prev_bar.hi_lim, prev_bar.lo_lim]):
            continue

        settings = context['assets'][symbol] 

        lg_sig = last_bar.hi_lim < last_bar.close > last_bar.lo_lim
        st_sig = last_bar.hi_lim > last_bar.close < last_bar.lo_lim

        if position.size <= 0 and lg_sig:
            
            balance = total_balance
            pos_size = balance * settings['allocation']
            size = abs(position.size) + pos_size * asset.get_contract_rate(last_bar.close)

            order = {
                'order_type' : 'market',
                'size' : size,
                'side' : 'buy'
            }

            asset.create_order('rider_long', order)
        
        if position.size >= 0 and st_sig:
            balance = total_balance
            pos_size = balance * settings['allocation']
            size = abs(position.size) + pos_size * asset.get_contract_rate(last_bar.close)

            order = {
                'order_type' : 'market',
                'size' : size,
                'side' : 'sell'
            }

            asset.create_order('rider_short', order)

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

# save_data

bt.positions.loc['2021-05-28':].to_csv("BMX_TT3-positions.csv")

# %% [markdown]
# ### FN3

# %%
time_int = '6'
time_uni = 'h'

config = {
    'context': {
        'is_shared_balance' : True,
        'balance': 1,  
        'assets': {
            'BTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075 + 0.0005,
                'allocation': 0.5
            },
            'ETH/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075 + 0.0005,
                'allocation' : 0.5          

            }
        },
        'parameters': {
            'tenkansenp': 14,
            'kijunsenp': 27           
            # 'tenkansenp': 8,
            # 'kijunsenp': 29           
        },

    },
    'slippage': 0,
    'time_interval': time_int,
    'time_unit': time_uni,
    'start_date': '2017-01-01 00:00:00',
    'end_date': dt.date.today(),
    'exchange': 'bitmex',
    'window_length' : 2
}

def initialize(factors, config):
    tenkansenp = config['parameters']['tenkansenp']
    kijunsenp = config['parameters']['kijunsenp']
    senkouspanbp = 52
    senkouspanlag = 26
    
    #tenkansen 
    factors['tkph'] = factors.high.rolling(tenkansenp).max()
    factors['tkpl'] = factors.low.rolling(tenkansenp).min()
    factors['tenkan_sen'] = (factors.tkph + factors.tkpl) / 2
    
    #kijunsen
    factors['kjph'] = factors.high.rolling(kijunsenp).max()
    factors['kjpl'] = factors.low.rolling(kijunsenp).min()
    factors['kijun_sen'] = (factors.kjph + factors.kjpl) / 2
    
    #senkouspan
    factors['senkou_span_a'] = ((factors.tenkan_sen + factors.kijun_sen) / 2).shift(senkouspanlag) #leadingspan_a
    factors['sksbph'] = factors.high.rolling(senkouspanbp).max()
    factors['sksbpl'] = factors.low.rolling(senkouspanbp).min()
    factors['senkou_span_b'] = ((factors.sksbph + factors.sksbpl) / 2).shift(senkouspanlag) #leadingspan_b
    
def process(window, assets, context):
    
    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance']
    
    for symbol, asset in assets.items():
        position = asset.position
        last = window.latest(symbol)
        lastlast = window.previous(symbol, 2)
        settings = context['assets'][symbol]  
          
        lg_ent = ((last.close > last.kijun_sen) and (last.senkou_span_a < last.close > last.senkou_span_b)) #and 
        lg_ext = (last.tenkan_sen < last.kijun_sen)   
        
        st_ent = ((last.close < last.kijun_sen) and (last.senkou_span_a > last.close < last.senkou_span_b))# and 
        st_ext = (last.tenkan_sen > last.kijun_sen)
        if position.size == 0: 
            
            if lg_ent:
                pos_size = total_balance * settings['allocation']
                size = pos_size * asset.get_contract_rate(last.close)

                order = {
                    'order_type' : 'market',
                    'size' : size,
                    'side' : 'buy'
                }
                asset.create_order('l_entry', order)

        if position.size > 0 and lg_ext:
            size = abs(position.size)

            order = {
                'order_type' : 'market',
                'size' : size,
                'side' : 'sell'
            }
            asset.create_order('l_exit', order)
            
        if position.size == 0: 
            if st_ent:
                pos_size = total_balance * settings['allocation']
                size = pos_size * asset.get_contract_rate(last.close)
                
                order = {
                    'order_type' : 'market',
                    'size' : size,
                    'side' : 'sell'
                }
                asset.create_order('s_entry', order)
                
        if position.size < 0 and st_ext:  
            size = abs(position.size) 
            
            order = {
                'order_type' : 'market',          
                'size' : size,
                'side' : 'buy'                    
            }
            asset.create_order('s_exit', order)
    
bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

bt.positions.loc['2021-05-28':].to_csv("BMX_FN3-positions.csv")


# %% [markdown]
# ### TT4

# %%
config = {
    'context': {
        'is_shared_balance' : True,
        'balance': 1,  
        'assets': {
            'ADA/BTC': {
                'exchange': 'binance',
                'maker_fee': -.0005,
                'taker_fee': .0025  + 0.005,
                'allocation' : 0.09
            },
            'BTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075 + 0.0005,
                'allocation' : 0.91
            }
        },
        'parameters': {
            # 'length': 34,
            # 'mult': 1.5,
            'length': 31,
            'mult': 1.5
        },

    },
    'slippage': 0,
    'time_interval': '1',
    'time_unit': 'd',
    'start_date': '2017-01-01 00:00:00',
    'end_date': dt.date.today(),
    'exchange': 'bitmex',
    'window_length' : 2
}

def initialize(factors, config):

    length = config['parameters']['length']
    mult = config['parameters']['mult']

    factors['high'] = factors.high
    factors['low'] = factors.low
    factors['close'] = factors.close

    factors['atr'] = talib_method(method='ATR', timeperiod=1, args=[factors.high, factors.low, factors.close])
    factors['avg_tr'] = talib_method(method='WMA', timeperiod=length, args=[factors.atr])
    factors['highest'] = factors.high.rolling(length).max()
    factors['lowest'] = factors.low.rolling(length).min()
    factors['hi_lim'] = factors.highest.shift(1) - (factors.avg_tr.shift(1) * mult)
    factors['lo_lim'] = factors.lowest.shift(1) + (factors.avg_tr.shift(1) * mult) 

def process(window, assets, context):

    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 

    for symbol, asset in assets.items():

        position = asset.position

        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)

        if not np.all([prev_bar.atr, prev_bar.avg_tr, prev_bar.hi_lim, prev_bar.lo_lim]):
            continue

        settings = context['assets'][symbol] 

        lg_sig = last_bar.hi_lim < last_bar.close > last_bar.lo_lim
        st_sig = last_bar.hi_lim > last_bar.close < last_bar.lo_lim

        if position.size <= 0 and lg_sig:
            
            balance = total_balance
            pos_size = balance * settings['allocation']
            size = abs(position.size) + pos_size * asset.get_contract_rate(last_bar.close)

            order = {
                'order_type' : 'market',
                'size' : size,
                'side' : 'buy'
            }

            asset.create_order('rider_long', order)
        
        if position.size >= 0 and st_sig:
            balance = total_balance
            pos_size = balance * settings['allocation']
            size = abs(position.size) + pos_size * asset.get_contract_rate(last_bar.close)

            order = {
                'order_type' : 'market',
                'size' : size,
                'side' : 'sell'
            }

            asset.create_order('rider_short', order)

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

# save_data

bt.positions.loc['2021-05-28':].to_csv("BMX_TT4-positions.csv")

# %% [markdown]
# ### MM3v3

# %%
### MM3v3

trail_val = 0.07
rrr_val = 5
var_val = 0.0225
pos_count_val = 5

config = {
    'context': {
        'is_shared_balance' : True,
        # 'balance' : 1,
        'balance': 1,  
        'assets': {
            'BTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.405,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            },
            'LTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.19,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            },
            'XRP/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.405,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            }
        },
        'parameters': {
            'lb': 28,
            # 'lb': 78,
            'risk': var_val,
            
            #trail stop parameter
            'trail_perc': trail_val,
            
            #tp parameters
            #bar count
            'pos_count':pos_count_val,
            'rrr': rrr_val,
            
            #RSI-based
            'rsi_len': 14,
            'rsi_ob': 60,
            'rsi_os': 40
            
        },

    },
    'slippage': 0.0005,
    'time_interval': '12',
    'time_unit': 'h',
    'start_date': '2017-01-01 00:00:00',    
    'end_date': dt.date.today(), # - dt.timedelta(days=1),
    'exchange': 'bitmex',
    'window_length' : 2
}

"""
Trailing stop

"""

def trail_comp(direction, percentage, price, current_trail):
    if direction > 0:
        trail_stop = price * (1-percentage)
        if price < trail_stop:
            trail_stop = current_trail
        elif (trail_stop < current_trail):
            trail_stop = current_trail
    elif direction < 0:
        trail_stop = price * (1+percentage)
        if price > trail_stop:
            trail_stop = current_trail
        elif (trail_stop > current_trail):
            trail_stop = current_trail
    return trail_stop


def trail_comp2(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    return trail_stop

def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
        asset.create_order(f"{strategy_name}_{i}", v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f"{strategy_name}_{i}"):
            asset.cancel_order(f"{strategy_name}_{i}")

def calculate_size(total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    # orig position size
    pos_size = total_balance * def_alloc
    orig_size = np.floor(pos_size * contract_rate)

    
    #var pos size
    if with_var == True:
        invalidation = abs((close-kwargs['stop'])/close)
        var_pos_size = (total_balance * kwargs['risk']) / invalidation
        var_size = np.floor((var_pos_size * contract_rate))
        size = min(orig_size, var_size)

    else:
        size = orig_size
        
    return size

def get_win_rate(df, pairs):
    
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    
    pairs_dict = {}
    for pair in pairs:
        
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        win_pct = win_trades / total
        lose_pct = losing_trades / total
        
        # print(f'{pair} Won trades count: {win_trades}')
        # print(f'{pair} Lost trades count: {losing_trades}')
        # print(f'{pair} Total trades count: {total}')
        # print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
        
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
        
        
        
    return pairs_dict
        
def proc_wr(old_dict, get_win_rate_dict):
    
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        
        
        old_dict[pair] = new2
    
    return old_dict

def nmm(values):
    nmm = 0.0
    sum = 0.0
    length = 0.0
    length = len(values)
    last_val = values[-1]
    x = 0
    for i in range(1, (length-1)):
        x =+ 1
        sum = sum + (last_val - values[i]) / np.sqrt(i*1.0)
    nmmv = sum/(length)
    return float(nmmv)

def initialize(factors, config):

    trail_perc = config['parameters']['trail_perc']

    lb = config['parameters']['lb']

    factors['log'] = np.log(factors.close)
    factors['nmm'] = factors.log.rolling(lb).apply(nmm)

def process(window, assets, context):

    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 

    ### VAR pos sizing
    risk = config['context']['parameters']['risk']
    
    # ### Trailing Stop
    # trail_perc = config_sl['context']['parameters']['trail_perc']
    
    for symbol, asset in assets.items():
        
        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)
        
        settings = context['assets'][symbol]
        
        ### Trailing Stop
        trail = settings['trail']
        
        position = asset.position
    
        # entry conditions
        # lg_sig = (last_bar.nmm > 0)
        # st_sig = (last_bar.nmm < 0)
        lg_sig = (prev_bar.nmm < 0) & (last_bar.nmm > 0)
        st_sig = (prev_bar.nmm > 0) & (last_bar.nmm < 0)
        
        if lg_sig:
            stop = last_bar.low * 0.99
        elif st_sig:
            stop = last_bar.high * 1.01
        else:
            stop = 0
               
        size = calculate_size(total_balance, last_bar.close, settings['allocation'], 
                              asset.get_contract_rate(last_bar.close), 
                              with_var=True, stop=stop, risk=config['context']['parameters']['risk'])
        
        trail_perc = context['parameters']['trail_perc']
        
        if position.size == 0:
            cancel_order('rider', asset, 'long_stop', 'long_trail', 'short_stop', 'short_trail')
            if lg_sig:                   
                order = proc_order('market', size, 'buy')

                ### long sl
                order_sl = proc_order('stop', size, 'sell', trigger=stop)

                ### Trail adjustment
                settings['trail'] = stop

                execute_order('rider', asset, long=order, long_stop=order_sl)
                                
            elif st_sig:
                order = proc_order('market', size, 'sell')
                order_sl = proc_order('stop', size, 'buy', trigger=stop)

                settings['trail'] = stop

                execute_order('rider', asset, short=order, short_stop=order_sl)

        elif position.size > 0:
            if st_sig:
                cancel_order('rider', asset, 'long_stop', 'long_trail')
                
                order = proc_order('market', size + abs(position.size), 'sell')
                order_sl = proc_order('stop', size, 'buy', trigger=stop)
                
                settings['trail'] = stop
                
                execute_order('rider', asset, short=order, short_stop=order_sl)

            else:
                trail_stop = trail_comp(position.size, trail_perc, last_bar.low, settings['trail'])
                settings['trail'] = trail_stop
                               
                if asset.orders.get('rider_long_stop'):
                    if asset.orders.get('rider_long_stop')['trigger_price'] < settings['trail']:
                        cancel_order('rider', asset, 'long_stop')
                        
                        ### trail long
                        order = proc_order('stop', abs(position.size), 'sell', trigger=settings['trail'])
                        execute_order('rider', asset, long_trail=order)
                      
                elif asset.orders.get('rider_long_trail'):
                    if asset.orders.get('rider_long_trail')['trigger_price'] < settings['trail']:
                        cancel_order('rider', asset, 'long_trail')
                          
                        ### trail long
                        order = proc_order('stop', abs(position.size), 'sell', trigger=settings['trail'])
                        execute_order('rider', asset, long_trail=order)
         
        elif position.size < 0:
            if lg_sig:
                cancel_order('rider', asset, 'short_stop', 'short_trail')
                
                ### reverse to long
                order = proc_order('market', size + abs(position.size), 'buy')
                order_sl = proc_order('stop', size, 'sell', trigger=stop)                
                settings['trail'] = stop
                
                execute_order('rider', asset, long=order, long_stop=order_sl)
                

            else:
                trail_stop = trail_comp(position.size, trail_perc, last_bar.high, settings['trail'])
                settings['trail'] = trail_stop
                
                if asset.orders.get('rider_short_stop'):
                    if asset.orders.get('rider_short_stop')['trigger_price'] > settings['trail']:
                        cancel_order('rider', asset, 'short_stop')
                        order = proc_order('stop', abs(position.size), 'buy', trigger=settings['trail'])
                        execute_order('rider', asset, short_trail=order)
                      
                if asset.orders.get('rider_short_trail'):
                    if asset.orders.get('rider_short_trail')['trigger_price'] > settings['trail']:
                        cancel_order('rider', asset, 'short_trail')
                        
                        order = proc_order('stop', abs(position.size), 'buy', trigger=settings['trail'])
                        execute_order('rider', asset, short_trail=order)
                        
bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

# save_data

bt.positions.loc['2021-05-28':].to_csv("BMX_MM3v3-positions.csv")

# %% [markdown]
# ### TS2v2

# %%
### TS2v2

ma_len_val = 38
risk_val = 0.03
sl_perc_val = 0.1
trail_perc_val = 0.01
rrr_val = 10

# BITMEX

# alloc = 0
slippage = 0.0005
taker_fee = 0.00075
maker_fee = -0.0001

config = {
    'context': {
        'is_shared_balance' : True,
        # 'balance' : 1,
        'balance': 1,  
        'assets': {},
        'parameters': {
            'ma_len': ma_len_val,
            'risk': risk_val,
            
            ###SL perc
            'sl_perc': sl_perc_val,
            
            ###TS
            'trail_perc': trail_perc_val,
            
            ###TP 
                ### Bars
            'rrr': rrr_val, 
            
                ### RSI
            'rsi_len': 14,
            'rsi_ob': 70,
            'rsi_os': 30,
            
        },
    },
    'slippage': 0,
    'time_interval': '8',
    'time_unit': 'h',
    'start_date': '2021-12-07 00:00:00',    
    'end_date': '2022-12-17 00:00:00',    
    'exchange': 'bitmex',
    'window_length' : 2
    }


pairs = {
    'BTC/USDT': ['binance', 0.375],
    'ETH/USDT': ['binance', 0.375],
    'XRP/USDT': ['binance', 0.25],
}

for i,v in pairs.items():
    config['context']['assets'][i] = {
        'exchange': v[0],
        'maker_fee': -.00025,
        'taker_fee': taker_fee,
        'allocation' : v[1],

        ###Take profit
        'last_pos': None,
        
        ###Trailing Stop
        'trail': 0,
        }
    
"""
Trailing stop

"""

def trail_comp(direction, percentage, price, current_trail):
    if direction > 0:
        trail_stop = price * (1-percentage)
        if price < trail_stop:
            trail_stop = current_trail
        elif (trail_stop < current_trail):
            trail_stop = current_trail
    elif direction < 0:
        trail_stop = price * (1+percentage)
        if price > trail_stop:
            trail_stop = current_trail
        elif (trail_stop > current_trail):
            trail_stop = current_trail
    return trail_stop


def trail_comp2(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    return trail_stop


def trail_comp3(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        elif last_close < prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
        
                
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        elif last_close > prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail 
        else:
            trail_stop = current_trail
    return trail_stop

def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
            # print('trigger price', order['trigger_price'])
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
        # print('execute orders', i, v)
        asset.create_order(f"{strategy_name}_{i}", v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f"{strategy_name}_{i}"):
            # print('cancel orders', i)
            asset.cancel_order(f"{strategy_name}_{i}")

def calculate_size(symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    min_size = {
        'btcusd': 100,
        'ethusd': 1,
        'xrpusd': 1,
        'ltcusd': 1,
        'adabtc': 10,
        'ethbtc': 0.01
    }
    # orig position size
    pos_size = total_balance * def_alloc
    orig_size = np.floor(pos_size * contract_rate)
    if symbol.lower().startswith('btc'):
        if orig_size < 100: 
            if orig_size % 100 >= 90:
                orig_size = round(orig_size, -2)
            else: 
                orig_size = 0
        else:
            orig_size = orig_size - (orig_size % 100)
    elif symbol.lower() == 'ada/btc':
        if orig_size < 10:
            if round(orig_size % 10, 2) >= 9:
                orig_size = round(orig_size, -1)
            else:
                orig_size = 0
        else:
            orig_size = orig_size - (orig_size % 10)
    #var pos size
    if with_var:
        invalidation = abs((close-kwargs['stop'])/close)
        var_pos_size = (total_balance * kwargs['risk']) / invalidation
        var_size = (var_pos_size * contract_rate)
        if symbol.lower().startswith('btc'):
            if var_size < 100: 
                if var_size % 100 >= 90:
                    var_size = round(var_size, -2)
                else: 
                    var_size = 0
            else:
                var_size = var_size - (var_size % 100)
        else:
            if 'usd' in symbol.lower():
                if var_size < 1:
                    if round(var_size % 1, 2) >= 0.9:
                        var_size = round(var_size, 0)
                    else:
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 1)
            else:
                if symbol.lower() == 'ada/btc':
                    if var_size < 10:
                        if round(var_size % 10, 2) >= 9:
                            var_size = round(var_size, -1)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 10)
                elif symbol.lower() == 'eth/btc':
                    if var_size < 1:
                        if round(var_size % 0.01, 3) >= 0.009:
                            var_size = round(var_size, 2)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
        size = min(orig_size, var_size)
    else:
        size = orig_size
    return size

def get_win_rate(df, pairs):
    
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    
    pairs_dict = {}
    for pair in pairs:
        
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        win_pct = win_trades / total
        lose_pct = losing_trades / total
        
        print(f'{pair} Won trades count: {win_trades}')
        print(f'{pair} Lost trades count: {losing_trades}')
        print(f'{pair} Total trades count: {total}')
        print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
        
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
        
        
        
    return pairs_dict
    
def proc_wr(old_dict, get_win_rate_dict):
    
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        
        
        old_dict[pair] = new2
    
    return old_dict

### SL with TP and Bars reentry

def initialize(factors, config):
    ma_len = config['parameters']['ma_len']
    
    ### TP reentry 
        ### RSI
    rsi_len = config['parameters']['rsi_len']
    factors['rsi'] = talib_method(method='RSI', timeperiod=rsi_len, args=[factors.close])
    
    ### Strategy Parameters
    trail_perc = config['parameters']['trail_perc']

    
    ### Strategy indicators
    factors['mao'] = talib_method(method='SMA', timeperiod=ma_len, args=[factors.open])
    factors['mah'] = talib_method(method='SMA', timeperiod=ma_len, args=[factors.high])
    factors['mal'] = talib_method(method='SMA', timeperiod=ma_len, args=[factors.low])
    factors['mac'] = talib_method(method='SMA', timeperiod=ma_len, args=[factors.close])
    factors['wid'] = (factors['mah'] - factors['mal']) / factors['mal']
    
def process(window, assets, context):
    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 

    ### SL perc
    sl_perc = config['context']['parameters']['sl_perc']
    
    
    ### VAR pos sizing
    risk = config['context']['parameters']['risk']
    
    ### Trailing Stop
    trail_perc = config['context']['parameters']['trail_perc']
    
    ### Take profit
        ### Bars
    rrr = config['context']['parameters']['rrr']
        ### RSI
    rsi_ob = context['parameters']['rsi_ob']
    rsi_os = context['parameters']['rsi_os']
    
    for symbol, asset in assets.items():
        
        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)
        
        settings = context['assets'][symbol]
        
        ### Take Profit
            ### Bars
        last_pos = settings['last_pos']
            ### Bars for reentry
        
            ### RSI
        rsi_xabove = prev_bar.rsi < rsi_ob and last_bar.rsi > rsi_ob
        rsi_xbelow = prev_bar.rsi > rsi_os and last_bar.rsi < rsi_os
        
        ### Trailing Stop
        trail = settings['trail']
        
        position = asset.position
    
        ### entry conditions
        lg_sig = (last_bar.wid < (10/100)) & (last_bar.mac > last_bar.mao) & (last_bar.close > last_bar.mah)
        st_sig = (last_bar.wid < (10/100)) & (last_bar.mac < last_bar.mao) & (last_bar.close < last_bar.mal)
        
            
        ### % based
        if lg_sig:
            stop = last_bar.close * (1 - sl_perc)
        elif st_sig:
            stop = last_bar.close * (1 + sl_perc)
        else:
            stop = 0
        

        size = calculate_size(symbol, total_balance, last_bar.close, settings['allocation'], 
                              asset.get_contract_rate(last_bar.close), 
                              with_var=True, stop=stop, risk=risk)
        
        rsi_ob = context['parameters']['rsi_ob']
        rsi_os = context['parameters']['rsi_os']
        
        rsi_xabove = prev_bar.rsi < rsi_ob and last_bar.rsi > rsi_ob
        rsi_xbelow = prev_bar.rsi > rsi_os and last_bar.rsi < rsi_os
        
        ### Trailing stop - kit
            ### initial trail
        trail_stop = settings['trail']
        
            
        if position.size == 0:
            
            ### cancel orders
            cancel_order('rider', asset, 'long_stop', 'long_trail', 'long_tp', 'short_stop', 'short_trail', 'short_tp')

            if lg_sig:
                if (settings['last_pos'] is not 'Long') or (settings['last_pos'] is 'Long' and rsi_xabove):
                    
                    settings['last_pos'] = 'Long'
                    
                    ### long entry
                    order = proc_order('market', size, 'buy')
                    
                    ### long sl
                    order_sl = proc_order('stop', size, 'sell', trigger=stop)
                    
                    ### Trail adjustment
                    settings['trail'] = stop
                    
                    """
                    stop = will be changed to SL of other strats
                    rrr = to be optimized (include in config)
                    """
                    
                    close_tp = abs((stop - last_bar.close) / last_bar.close) * rrr
                    close_tp2 = last_bar.close * (close_tp + 1)
                    ### TP
                    
                    order_tp = proc_order('limit', size, 'sell', price=close_tp2)
                    
                    execute_order('rider', asset, long=order, long_tp=order_tp)
                                
            elif st_sig:
                if (settings['last_pos'] is not 'Short') or (settings['last_pos'] is 'Short' and rsi_xbelow):
                    
                    settings['last_pos'] = 'Short'

                    order = proc_order('market', size, 'sell')
                    order_sl = proc_order('stop', size, 'buy', trigger=stop)

                    close_tp = abs((last_bar.close - stop) / last_bar.close) * rrr
                    close_tp2 = last_bar.close * (1 - close_tp)

                    order_tp = proc_order('limit', size, 'buy', price=close_tp2)

                    settings['trail'] = stop

                    execute_order('rider', asset, short=order, short_tp=order_tp)

        elif position.size > 0:
            if st_sig:
                
                settings['last_pos'] = 'Short'
                
                cancel_order('rider', asset, 'long_stop', 'long_trail', 'long_tp')
                
                ### reverse to short
                order = proc_order('market', size + abs(position.size), 'sell')
                order_sl = proc_order('stop', size, 'buy', trigger=stop)
                
                close_tp = abs((last_bar.close - stop) / last_bar.close) * rrr
                close_tp2 = last_bar.close * (1 - close_tp)
                order_tp = proc_order('limit', size, 'buy', price=close_tp2)
                
                settings['trail'] = stop
                
                execute_order('rider', asset, short=order, short_tp=order_tp)
                

            else:
                settings['last_pos'] = 'Long'
                
                ## Trail adjustment (kit)
                trail_stop = trail_comp2(position.size, trail_perc, last_bar.close, prev_bar.close, settings['trail'])
                settings['trail'] = trail_stop  

                if last_bar.close < trail_stop:
                    order = proc_order('market', abs(position.size), 'sell')
                    execute_order('rider', asset, long_trail=order)
                    cancel_order('rider', asset, 'long_stop', 'long_tp')
                
        elif position.size < 0:
            if lg_sig:
                settings['last_pos'] = 'Long'
                cancel_order('rider', asset, 'short_stop', 'short_trail', 'short_tp')
                
                ### reverse to long
                order = proc_order('market', size + abs(position.size), 'buy')
                order_sl = proc_order('stop', size, 'sell', trigger=stop)
                
                close_tp = abs((stop - last_bar.close) / last_bar.close) * rrr
                close_tp2 = last_bar.close * (close_tp + 1)
                order_tp = proc_order('limit', size, 'sell', price=close_tp2)
                
                settings['trail'] = stop
                
                execute_order('rider', asset, long=order, long_tp=order_tp)
                

            else:
                
                ## Trail adjustment (kit)
                trail_stop = trail_comp2(position.size, trail_perc, last_bar.close, prev_bar.close, settings['trail'])
                settings['trail'] = trail_stop
                
                if last_bar.close > trail_stop:
                    order = proc_order('market', abs(position.size), 'buy')
                    execute_order('rider', asset, short_trail=order)
                    cancel_order('rider', asset, 'short_stop', 'short_tp')

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

# save_data
bt.positions.loc['2021-05-28':].to_csv("BMX_TS2v2-positions.csv")

# %% [markdown]
# ### FN3v2

# %%
### FN3v2

trail_val = 0.01
var_val = 0.015

config = {
    'context': {
        'is_shared_balance' : True,
        # 'balance' : 1,
        'balance': 1,  
        'assets': {
            'ETH/BTC': {
                'exchange': 'binance',
                'maker_fee': -.0005,
                'taker_fee': .0025,
                'allocation' : 0.05,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            },
            'BTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.316,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            },
            'ETH/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.316,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            },
            'XRP/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.316,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            }
        },
        'parameters': {
            ### Strategy Params
            # 'tenkansenp': 13,
            # 'kijunsenp': 20,
            'tenkansenp': 5,
            'kijunsenp': 25,
            'risk': 0.015,
            #trail stop parameter
            'trail_perc': 0.01,
        },
    },
    'slippage': 0.0005,
    'time_interval': '8',
    'time_unit': 'h',
    'start_date': '2017-01-01 00:00:00',    
    'end_date': dt.date.today(),
    'exchange': 'bitmex',
    'window_length' : 2
}

"""
Trailing stop
"""
def trail_comp(direction, percentage, price, current_trail):
    if direction > 0:
        trail_stop = price * (1-percentage)
        if price < trail_stop:
            trail_stop = current_trail
        elif (trail_stop < current_trail):
            trail_stop = current_trail
    elif direction < 0:
        trail_stop = price * (1+percentage)
        if price > trail_stop:
            trail_stop = current_trail
        elif (trail_stop > current_trail):
            trail_stop = current_trail
    return trail_stop


def trail_comp2(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    return trail_stop

def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
        asset.create_order(f"{strategy_name}_{i}", v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f"{strategy_name}_{i}"):
            asset.cancel_order(f"{strategy_name}_{i}")

def calculate_size(symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    min_size = {
        'btcusdt': 100,
        'ethusdt': 1,
        'xrpusdt': 1,
        'ltcusdt': 1,
        'adabtc': 10,
        'ethbtc': 0.01
    }
    # orig position size
    pos_size = total_balance * def_alloc
    orig_size = pos_size * contract_rate
    if symbol.lower().startswith('btc'):
        if orig_size < 100: 
            if orig_size % 100 >= 90:
                orig_size = round(orig_size, -2)
            else: 
                orig_size = 0
        else:
            orig_size = orig_size - (orig_size % 100)
    elif symbol.lower() == 'ada/btc':
        if orig_size < 10:
            if round(orig_size % 10, 2) >= 9:
                orig_size = round(orig_size, -1)
            else:
                orig_size = 0
        else:
            orig_size = orig_size - (orig_size % 10)
    #var pos size
    if with_var == True:
        invalidation = abs((close-kwargs['stop'])/close)
        var_pos_size = (total_balance * kwargs['risk']) / invalidation
        var_size = var_pos_size * contract_rate
        if symbol.lower().startswith('btc'):
            if var_size < 100: 
                if var_size % 100 >= 90:
                    var_size = round(var_size, -2)
                else: 
                    var_size = 0
            else:
                var_size = var_size - (var_size % 100)
        else:
            if 'usd' in symbol.lower():
                if var_size < 1:
                    if round(var_size % 1, 2) >= 0.9:
                        var_size = round(var_size, 0)
                    else:
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 1)
            else:
                if symbol.lower() == 'ada/btc':
                    if var_size < 10:
                        if round(var_size % 10, 2) >= 9:
                            var_size = round(var_size, -1)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 10)
                elif symbol.lower() == 'eth/btc':
                    if var_size < 1:
                        if round(var_size % 0.01, 3) >= 0.009:
                            var_size = round(var_size, 2)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
        size = min(orig_size, var_size)
    else:
        size = orig_size
    return size

def get_win_rate(df, pairs):
    
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    
    pairs_dict = {}
    for pair in pairs:
        
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        win_pct = win_trades / total
        lose_pct = losing_trades / total
        
        # print(f'{pair} Won trades count: {win_trades}')
        # print(f'{pair} Lost trades count: {losing_trades}')
        # print(f'{pair} Total trades count: {total}')
        # print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
        
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
        
        
        
    return pairs_dict
    
def proc_wr(old_dict, get_win_rate_dict):
    
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        
        
        old_dict[pair] = new2
    
    return old_dict

def initialize(factors, config):
    ### Strategy Indicators    
    tenkansenp = config['parameters']['tenkansenp']
    kijunsenp = config['parameters']['kijunsenp']
    senkouspanbp = 52
    senkouspanlag = 26
    
    factors['close'] = factors.close.ffill()
    
    #tenkansen 
    factors['tkph'] = factors.high.rolling(tenkansenp).max()
    factors['tkpl'] = factors.low.rolling(tenkansenp).min()
    factors['tenkan_sen'] = (factors.tkph + factors.tkpl) / 2
    
    #kijunsen
    factors['kjph'] = factors.high.rolling(kijunsenp).max()
    factors['kjpl'] = factors.low.rolling(kijunsenp).min()
    factors['kijun_sen'] = (factors.kjph + factors.kjpl) / 2
    
    #senkouspan
    factors['senkou_span_a'] = ((factors.tenkan_sen + factors.kijun_sen) / 2).shift(senkouspanlag) #leadingspan_a
    factors['sksbph'] = factors.high.rolling(senkouspanbp).max()
    factors['sksbpl'] = factors.low.rolling(senkouspanbp).min()
    factors['senkou_span_b'] = ((factors.sksbph + factors.sksbpl) / 2).shift(senkouspanlag) #leadingspan_b

def process(window, assets, context):
    
    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance']
    
    ### VAR pos sizing
    risk = config['context']['parameters']['risk']    
    
    
    for symbol, asset in assets.items():
        position = asset.position
        last = window.latest(symbol)
        lastt = window.previous(symbol, 2)
        
        if not np.all([lastt.tenkan_sen, lastt.kijun_sen, last.senkou_span_a, lastt.senkou_span_b]):
            continue
            
        settings = context['assets'][symbol] 
        
        ### VAR pos sizing
       

        ### Trailing Stop
        trail_perc = context['parameters']['trail_perc']

        ### Entry Conditions
        lg_ent = ((last.close > last.kijun_sen) and 
                  (last.close > lastt.close) and 
                  (last.senkou_span_a < last.close > last.senkou_span_b)) #and  
        
        st_ent = ((last.close < last.kijun_sen) and 
                  (last.close < lastt.close) and 
                  (last.senkou_span_a > last.close < last.senkou_span_b))# and    

        ### FN3V2 SL
        if lg_ent:
            stop = lastt.close *0.97
        elif st_ent:
            stop = lastt.close *1.03
        else:
            stop = 0
        

        # Trailing stop - kit
        trail_stop = settings['trail']
            
        
        size = calculate_size(symbol, total_balance, last.close, settings['allocation'], 
                              asset.get_contract_rate(last.close), 
                              with_var=True, stop=stop, risk=config['context']['parameters']['risk'])
        

        if position.size == 0:
            cancel_order('fn', asset, 'long_stop', 'long_trail', 'short_stop', 'short_trail')
            if lg_ent:
                order = proc_order('market', size, 'buy')

                ### Trail adjustment or SL
                settings['trail'] = stop
                
                ### Execute
                execute_order('fn', asset, long=order)
           
            elif st_ent:  
                order = proc_order('market', size, 'sell')

                settings['trail'] = stop
                execute_order('fn', asset, short=order)
                
        elif position.size > 0:
            if st_ent:   
                cancel_order('fn', asset, 'long_stop','long_trail')
                
                order = proc_order('market', size + abs(position.size), 'sell')
            
                settings['trail'] = stop
                execute_order('fn', asset, short=order)
                        
            else:
                if last.close < settings['trail']:
                    order = proc_order('market', abs(position.size), 'sell')
                    execute_order('fn', asset, long_stop=order)

                else:
                    trail_stop = trail_comp2(position.size, trail_perc, last.close, lastt.close, settings['trail'])
                    settings['trail'] = trail_stop

                    if last.close < trail_stop:
                        order = proc_order('market', abs(position.size), 'sell')
                        execute_order('fn', asset, long_trail=order)
                        cancel_order('fn', asset, 'long_stop')

    
        elif position.size < 0:   
            if lg_ent:    
                cancel_order('fn', asset, 'short_stop', 'short_trail')
                order = proc_order('market', size + abs(position.size), 'buy')
               
                settings['trail'] = stop
                execute_order('fn', asset, long=order)

            
            else:
                if last.close > settings['trail']:
                    order = proc_order('market', abs(position.size), 'buy')
                    execute_order('fn', asset, short_stop=order)
                    
                else:
                    trail_stop = trail_comp2(position.size, trail_perc, last.close, lastt.close, settings['trail'])
                    settings['trail'] = trail_stop

            
                    if last.close > trail_stop:
                        order = proc_order('market', abs(position.size), 'buy')
                        execute_order('fn', asset, short_trail=order)
                        cancel_order('fn', asset, 'short_stop')

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

# save_data
bt.positions.loc['2021-05-28':].to_csv("BMX_FN3v2-positions.csv")

# %% [markdown]
# ### MM3v2

# %%
### MM3v2
trail_val = 0.07
rrr_val = 5
var_val = 0.02
pos_count_val = 5

config = {
    'context': {
        'is_shared_balance' : True,
        'balance' : 1,
        # 'balance': 0.01,  
        'assets': {
            'BTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.38,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            },
            'LTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.16,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            },
            'XRP/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.38,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            },
            'ADA/BTC': {
                'exchange': 'binance',
                'maker_fee': -.0005,
                'taker_fee': .0025,
                'allocation': 0.04,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0 
            },
            'ETH/BTC': {
                'exchange': 'binance',
                'maker_fee': -.0005,
                'taker_fee': .0025,
                'allocation': 0.04,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            }
        },
        'parameters': {
            # 'lb': 99,
            'lb': 41,
            'risk': var_val,
            
            #trail stop parameter
            'trail_perc': trail_val,
            
            #tp parameters
            #bar count
            'pos_count':pos_count_val,
            'rrr': rrr_val,
            
            #RSI-based
            'rsi_len': 14,
            'rsi_ob': 60,
            'rsi_os': 40
            
        },

    },
    'slippage': 0.0005,
    'time_interval': '8',
    'time_unit': 'h',
    'start_date': '2017-01-01 00:00:00',   
    'end_date': dt.datetime.now(),
    'exchange': 'bitmex',
    'window_length' : 2
}

"""
Trailing stop
"""
def trail_comp(direction, percentage, price, current_trail):
    if direction > 0:
        trail_stop = price * (1-percentage)
        if price < trail_stop:
            trail_stop = current_trail
        elif (trail_stop < current_trail):
            trail_stop = current_trail
    elif direction < 0:
        trail_stop = price * (1+percentage)
        if price > trail_stop:
            trail_stop = current_trail
        elif (trail_stop > current_trail):
            trail_stop = current_trail
    return trail_stop

def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
        asset.create_order(f"{strategy_name}_{i}", v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f"{strategy_name}_{i}"):
            asset.cancel_order(f"{strategy_name}_{i}")

def calculate_size(symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):

    min_size = {
        'btcusdt': 100,
        'ethusdt': 1,
        'xrpusdt': 1,
        'ltcusdt': 1,
        'adabtc': 10,
        'ethbtc': 0.01
    }
    # orig position size
    pos_size = total_balance * def_alloc
    
    if symbol.lower() == 'eth/btc':
        orig_size = round(pos_size * contract_rate, 2)
    else:
        orig_size = np.floor(pos_size * contract_rate)
    
    if symbol.lower().startswith('btc'):
        if orig_size < 100: 
            if orig_size % 100 >= 90:
                orig_size = round(orig_size, -2)
            else: 
                orig_size = 0
        else:
            orig_size = orig_size - (orig_size % 100)
    elif symbol.lower() == 'ada/btc':
        if orig_size < 10:
            if round(orig_size % 10, 2) >= 9:
                orig_size = round(orig_size, -1)
            else:
                orig_size = 0
        else:
            orig_size = orig_size - (orig_size % 10)
    
    #var pos size
    if with_var == True:
        invalidation = abs((close-kwargs['stop'])/close)
        var_pos_size = (total_balance * kwargs['risk']) / invalidation
        var_size = var_pos_size * contract_rate
        
        if symbol.lower().startswith('btc'):
            if var_size < 100: 
                if var_size % 100 >= 90:
                    var_size = round(var_size, -2)
                else: 
                    var_size = 0
            else:
                var_size = var_size - (var_size % 100)
        else:
            if symbol.lower().endswith('usdt'):
                if var_size < 1:
                    if round(var_size % 1, 2) >= 0.9:
                        var_size = round(var_size, 0)
                    else:
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 1)
            else:
                if symbol.lower() == 'ada/btc':
                    if var_size < 10:
                        if round(var_size % 10, 2) >= 9:
                            var_size = round(var_size, -1)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 10)
                elif symbol.lower() == 'eth/btc':
                    if var_size < 1:
                        if round(var_size % 0.01, 3) >= 0.009:
                            var_size = round(var_size, 2)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
        
        size = min(orig_size, var_size)

    else:
        size = orig_size
    
    return size

def nmm(values):
    nmm = 0.0
    sum = 0.0
    length = 0.0
    length = len(values)
    last_val = values[-1]
    x = 0
    for i in range(1, (length-1)):
        x =+ 1
        sum = sum + (last_val - values[i]) / np.sqrt(i*1.0)
    nmmv = sum/(length)
    return float(nmmv)

def initialize(factors, config):

    trail_perc = config['parameters']['trail_perc']

    lb = config['parameters']['lb']

    factors['log'] = np.log(factors.close)
    factors['nmm'] = factors.log.rolling(lb).apply(nmm)

def process(window, assets, context):

    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 

    ### VAR pos sizing
    risk = config['context']['parameters']['risk']
    
    for symbol, asset in assets.items():
        
        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)
        
        settings = context['assets'][symbol]
        
        ### Trailing Stop
        trail = settings['trail']
        
        position = asset.position
    
        #entry conditions
        lg_sig = (prev_bar.nmm < 0) & (last_bar.nmm > 0)
        st_sig = (prev_bar.nmm > 0) & (last_bar.nmm < 0)
        
        if lg_sig:
            stop = last_bar.low * 0.99
        elif st_sig:
            stop = last_bar.high * 1.01
        else:
            stop = 0
               
        size = calculate_size(symbol, total_balance, last_bar.close, settings['allocation'], 
                              asset.get_contract_rate(last_bar.close), 
                              with_var=True, stop=stop, risk=config['context']['parameters']['risk'])
        
        trail_perc = context['parameters']['trail_perc']
        
        if position.size == 0:
            cancel_order('rider', asset, 'long_stop', 'long_trail', 'short_stop', 'short_trail')
            if lg_sig:                   
                order = proc_order('market', size, 'buy')
                settings['trail'] = stop
                execute_order('rider', asset, long=order)
            elif st_sig:
                order = proc_order('market', size, 'sell')
                settings['trail'] = stop
                execute_order('rider', asset, short=order)

        elif position.size > 0:
            if st_sig:
                order = proc_order('market', size + abs(position.size), 'sell')
                settings['trail'] = stop
                execute_order('rider', asset, short=order)
            else:
                if last_bar.close < settings['trail']:
                    order = proc_order('market', abs(position.size), 'sell')
                    execute_order('rider', asset, long_stop=order)
                else:
                    trail_stop = trail_comp(position.size, trail_perc, last_bar.low, settings['trail'])
                    if trail_stop > settings['trail']:
                        settings['trail'] = trail_stop
        elif position.size < 0:
            if lg_sig:
                order = proc_order('market', size + abs(position.size), 'buy')
                settings['trail'] = stop
                execute_order('rider', asset, long=order)
            else:
                if last_bar.close > settings['trail']:
                    order = proc_order('market', abs(position.size), 'buy')
                    execute_order('rider', asset, short_stop=order)
                else:
                    trail_stop = trail_comp(position.size, trail_perc, last_bar.high, settings['trail'])
                    if trail_stop < settings['trail']:
                        settings['trail'] = trail_stop

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

# save_data
bt.positions.loc['2021-05-28':].to_csv("BMX_MM3v2-positions.csv")

# %% [markdown]
# ### TT1v2

# %%
### TT1v2
trail_val = 0.05
rrr_val = 5
var_val = 0.02
pos_count_val = 5
comb = 1


# BITMEX

config = {
    'context': {
        'is_shared_balance' : True,
        # 'balance' : 1,
        'balance': 1,  
        'assets': {
            'BTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.5,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            },
            'ETH/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.5,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            }
        },
        'parameters': {
            'length': 75,
            'mult': 4.5,
            # 'length': 96,
            # 'mult': 4.5,
            'risk': var_val,
            
            #trail stop parameter
            'trail_perc': trail_val,
            
            #tp parameters
            #bar count
            'pos_count':pos_count_val,
            'rrr': rrr_val,
            
            #RSI-based
            'rsi_len': 14,
            'rsi_ob': 60,
            'rsi_os': 40
            
        },

    },
    'slippage': 0.0005,
    'time_interval': '8',
    'time_unit': 'h',
    'start_date': '2017-01-01 00:00:00',    
    'end_date': dt.datetime.now(),
    'exchange': 'bitmex',
    'window_length' : 2
}

def trail_comp(direction, percentage, price, current_trail):
    if direction > 0:
        trail_stop = price * (1-percentage)
        if price < trail_stop:
            trail_stop = current_trail
        elif (trail_stop < current_trail):
            trail_stop = current_trail
    elif direction < 0:
        trail_stop = price * (1+percentage)
        if price > trail_stop:
            trail_stop = current_trail
        elif (trail_stop > current_trail):
            trail_stop = current_trail
    return trail_stop

def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
        asset.create_order(f"{strategy_name}_{i}", v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f"{strategy_name}_{i}"):
            asset.cancel_order(f"{strategy_name}_{i}")

def calculate_size(symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    min_size = {
        'btcusdt': 100,
        'ethusdt': 1,
        'xrpusdt': 1,
        'ltcusdt': 1,
        'adabtc': 10,
        'ethbtc': 0.01
    }
    pos_size = total_balance * def_alloc
    orig_size = np.floor(pos_size * contract_rate)
    
    if symbol.lower().startswith('btc'):
        if orig_size < 100: 
            if orig_size % 100 >= 90:
                orig_size = round(orig_size, -2)
            else: 
                orig_size = 0
        else:
            orig_size = orig_size - (orig_size % 100)
    elif symbol.lower() == 'ada/btc':
        if orig_size < 10:
            if round(orig_size % 10, 2) >= 9:
                orig_size = round(orig_size, -1)
            else:
                orig_size = 0
        else:
            orig_size = orig_size - (orig_size % 10)
    if with_var == True:
        invalidation = abs((close-kwargs['stop'])/close)
        var_pos_size = (total_balance * kwargs['risk']) / invalidation
        var_size = var_pos_size * contract_rate
        
        if symbol.lower().startswith('btc'):
            if var_size < 100: 
                if var_size % 100 >= 90:
                    var_size = round(var_size, -2)
                else: 
                    var_size = 0
            else:
                var_size = var_size - (var_size % 100)
        else:
            if symbol.lower().endswith('usdt'):
                if var_size < 1:
                    if round(var_size % 1, 2) >= 0.9:
                        var_size = round(var_size, 0)
                    else:
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 1)
            else:
                if symbol.lower() == 'ada/btc':
                    if var_size < 10:
                        if round(var_size % 10, 2) >= 9:
                            var_size = round(var_size, -1)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 10)
                elif symbol.lower() == 'eth/btc':
                    if var_size < 1:
                        if round(var_size % 0.01, 3) >= 0.009:
                            var_size = round(var_size, 2)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
        size = min(orig_size, var_size)
    else:
        size = orig_size
    return size

def initialize(factors, config):

    length = config['parameters']['length']
    mult = config['parameters']['mult']
    trail_perc = config['parameters']['trail_perc']
    
    factors['high'] = factors.high
    factors['low'] = factors.low
    factors['close'] = factors.close

    factors['atr'] = talib_method(method='ATR', timeperiod=1, args=[factors.high, factors.low, factors.close])
    factors['avg_tr'] = talib_method(method='WMA', timeperiod=length, args=[factors.atr])
    factors['highest'] = factors.high.rolling(length).max()
    factors['lowest'] = factors.low.rolling(length).min()
    factors['hi_lim'] = factors.highest.shift(1) - (factors.avg_tr.shift(1) * mult)
    factors['lo_lim'] = factors.lowest.shift(1) + (factors.avg_tr.shift(1) * mult) 
    factors['mid'] = (factors.hi_lim + factors.lo_lim)/2

def process(window, assets, context):

    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 

    ### VAR pos sizing
    risk = config['context']['parameters']['risk']
    
    for symbol, asset in assets.items():
        
        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)
        
        settings = context['assets'][symbol]
        
        ### Trailing Stop
        trail = settings['trail']
        
        position = asset.position
    
        lg_sig = (
                    ((max(prev_bar.hi_lim, prev_bar.lo_lim) > prev_bar.close) &
                     (max(last_bar.hi_lim, last_bar.lo_lim) < last_bar.close))
                    )
        
        st_sig = (
                    ((min(prev_bar.hi_lim, prev_bar.lo_lim) < prev_bar.close) &
                     (min(last_bar.hi_lim, last_bar.lo_lim) > last_bar.close))
                    )
        
        if lg_sig:
            stop = (min(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
        elif st_sig:
            stop = (max(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
        else:
            stop = 0
               
        size = calculate_size(symbol, total_balance, last_bar.close, settings['allocation'], 
                              asset.get_contract_rate(last_bar.close), 
                              with_var=True, stop=stop, risk=config['context']['parameters']['risk'])
                
        trail_perc = context['parameters']['trail_perc']
        
        if position.size == 0:
            cancel_order('rider', asset, 'long_stop', 'long_trail', 'short_stop', 'short_trail')
            if lg_sig:                   
                order = proc_order('market', size, 'buy')
                settings['trail'] = stop
                execute_order('rider', asset, long=order)
            elif st_sig:
                order = proc_order('market', size, 'sell')
                settings['trail'] = stop
                execute_order('rider', asset, short=order)
        elif position.size > 0:
            if st_sig:
                order = proc_order('market', size + abs(position.size), 'sell')
                settings['trail'] = stop
                execute_order('rider', asset, short=order)
            else:
                if last_bar.close < settings['trail']:
                    order = proc_order('market', abs(position.size), 'sell')
                    execute_order('rider', asset, long_stop=order)
                else:
                    trail_stop = trail_comp(position.size, trail_perc, last_bar.low, settings['trail'])
                    if trail_stop > settings['trail']:
                        settings['trail'] = trail_stop
        elif position.size < 0:
            if lg_sig:
                order = proc_order('market', size + abs(position.size), 'buy')
                settings['trail'] = stop
                execute_order('rider', asset, long=order)
            else:
                if last_bar.close > settings['trail']:
                    order = proc_order('market', abs(position.size), 'buy')
                    execute_order('rider', asset, short_stop=order)
                else:
                    trail_stop = trail_comp(position.size, trail_perc, last_bar.high, settings['trail'])
                    if trail_stop < settings['trail']:
                        settings['trail'] = trail_stop

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

# save_data
bt.positions.loc['2021-05-28':].to_csv("BMX_TT1v2-positions.csv")

# %% [markdown]
# ### TT4v2

# %%
### TT4v2

trail_val = 0.05
rrr_val = 5
var_val = 0.02
pos_count_val = 5
comb = 1

# BITMEX

config = {
    'context': {
        'is_shared_balance' : True,
        # 'balance' : 1,
        'balance': 1,  
        'assets': {
            'BTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.475,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            },
            'ETH/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.475,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            },
            'ETH/BTC': {
                'exchange': 'binance',
                'maker_fee': -.0005,
                'taker_fee': .0025,
                'allocation': 0.05,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            }
        },
        'parameters': {
            'length': 75,
            'mult': 4.5,
            # 'length': 10,
            # 'mult': 4.5,
            'risk': var_val,
            
            #trail stop parameter
            'trail_perc': trail_val,
            
            #tp parameters
            #bar count
            'pos_count':pos_count_val,
            'rrr': rrr_val,
            
            #RSI-based
            'rsi_len': 14,
            'rsi_ob': 60,
            'rsi_os': 40
            
        },

    },
    'slippage': 0.0005,
    'time_interval': '8',
    'time_unit': 'h',
    'start_date': '2017-01-01 00:00:00',    
    'end_date': dt.datetime.now(),
    'exchange': 'bitmex',
    'window_length' : 2
}

def trail_comp(direction, percentage, price, current_trail):
    if direction > 0:
        trail_stop = price * (1-percentage)
        if price < trail_stop:
            trail_stop = current_trail
        elif (trail_stop < current_trail):
            trail_stop = current_trail
    elif direction < 0:
        trail_stop = price * (1+percentage)
        if price > trail_stop:
            trail_stop = current_trail
        elif (trail_stop > current_trail):
            trail_stop = current_trail
    return trail_stop

def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
        asset.create_order(f"{strategy_name}_{i}", v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f"{strategy_name}_{i}"):
            asset.cancel_order(f"{strategy_name}_{i}")

def calculate_size(symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    min_size = {
        'btcusdt': 100,
        'ethusdt': 1,
        'xrpusdt': 1,
        'ltcusdt': 1,
        'adabtc': 10,
        'ethbtc': 0.01
    }
    pos_size = total_balance * def_alloc
    orig_size = np.floor(pos_size * contract_rate)
    
    if symbol.lower().startswith('btc'):
        if orig_size < 100: 
            if orig_size % 100 >= 90:
                orig_size = round(orig_size, -2)
            else: 
                orig_size = 0
        else:
            orig_size = orig_size - (orig_size % 100)
    elif symbol.lower() == 'ada/btc':
        if orig_size < 10:
            if round(orig_size % 10, 2) >= 9:
                orig_size = round(orig_size, -1)
            else:
                orig_size = 0
        else:
            orig_size = orig_size - (orig_size % 10)
    if with_var == True:
        invalidation = abs((close-kwargs['stop'])/close)
        var_pos_size = (total_balance * kwargs['risk']) / invalidation
        var_size = var_pos_size * contract_rate
        
        if symbol.lower().startswith('btc'):
            if var_size < 100: 
                if var_size % 100 >= 90:
                    var_size = round(var_size, -2)
                else: 
                    var_size = 0
            else:
                var_size = var_size - (var_size % 100)
        else:
            if symbol.lower().endswith('usdt'):
                if var_size < 1:
                    if round(var_size % 1, 2) >= 0.9:
                        var_size = round(var_size, 0)
                    else:
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 1)
            else:
                if symbol.lower() == 'ada/btc':
                    if var_size < 10:
                        if round(var_size % 10, 2) >= 9:
                            var_size = round(var_size, -1)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 10)
                elif symbol.lower() == 'eth/btc':
                    if var_size < 1:
                        if round(var_size % 0.01, 3) >= 0.009:
                            var_size = round(var_size, 2)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
        size = min(orig_size, var_size)
    else:
        size = orig_size
    return size

def initialize(factors, config):

    length = config['parameters']['length']
    mult = config['parameters']['mult']
    trail_perc = config['parameters']['trail_perc']
    
    factors['high'] = factors.high
    factors['low'] = factors.low
    factors['close'] = factors.close

    factors['atr'] = talib_method(method='ATR', timeperiod=1, args=[factors.high, factors.low, factors.close])
    factors['avg_tr'] = talib_method(method='WMA', timeperiod=length, args=[factors.atr])
    factors['highest'] = factors.high.rolling(length).max()
    factors['lowest'] = factors.low.rolling(length).min()
    factors['hi_lim'] = factors.highest.shift(1) - (factors.avg_tr.shift(1) * mult)
    factors['lo_lim'] = factors.lowest.shift(1) + (factors.avg_tr.shift(1) * mult) 
    factors['mid'] = (factors.hi_lim + factors.lo_lim)/2

def process(window, assets, context):

    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 

    ### VAR pos sizing
    risk = config['context']['parameters']['risk']
    
    for symbol, asset in assets.items():
        
        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)
        
        settings = context['assets'][symbol]
        
        ### Trailing Stop
        trail = settings['trail']
        
        position = asset.position
    
        lg_sig = (
                    ((max(prev_bar.hi_lim, prev_bar.lo_lim) > prev_bar.close) &
                     (max(last_bar.hi_lim, last_bar.lo_lim) < last_bar.close))
                    )
        
        st_sig = (
                    ((min(prev_bar.hi_lim, prev_bar.lo_lim) < prev_bar.close) &
                     (min(last_bar.hi_lim, last_bar.lo_lim) > last_bar.close))
                    )
        
        if lg_sig:
            stop = (min(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
        elif st_sig:
            stop = (max(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
        else:
            stop = 0
               
        size = calculate_size(symbol, total_balance, last_bar.close, settings['allocation'], 
                              asset.get_contract_rate(last_bar.close), 
                              with_var=True, stop=stop, risk=config['context']['parameters']['risk'])
                
        trail_perc = context['parameters']['trail_perc']
        
        if position.size == 0:
            cancel_order('rider', asset, 'long_stop', 'long_trail', 'short_stop', 'short_trail')
            if lg_sig:                   
                order = proc_order('market', size, 'buy')
                settings['trail'] = stop
                execute_order('rider', asset, long=order)
            elif st_sig:
                order = proc_order('market', size, 'sell')
                settings['trail'] = stop
                execute_order('rider', asset, short=order)
        elif position.size > 0:
            if st_sig:
                order = proc_order('market', size + abs(position.size), 'sell')
                settings['trail'] = stop
                execute_order('rider', asset, short=order)
            else:
                if last_bar.close < settings['trail']:
                    order = proc_order('market', abs(position.size), 'sell')
                    execute_order('rider', asset, long_stop=order)
                else:
                    trail_stop = trail_comp(position.size, trail_perc, last_bar.low, settings['trail'])
                    if trail_stop > settings['trail']:
                        settings['trail'] = trail_stop
        elif position.size < 0:
            if lg_sig:
                order = proc_order('market', size + abs(position.size), 'buy')
                settings['trail'] = stop
                execute_order('rider', asset, long=order)
            else:
                if last_bar.close > settings['trail']:
                    order = proc_order('market', abs(position.size), 'buy')
                    execute_order('rider', asset, short_stop=order)
                else:
                    trail_stop = trail_comp(position.size, trail_perc, last_bar.high, settings['trail'])
                    if trail_stop < settings['trail']:
                        settings['trail'] = trail_stop

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

# save_data
bt.positions.loc['2021-05-28':].to_csv("BMX_TT4v2-positions.csv")

# %% [markdown]
# ### DMIv2

# %%

config = {
    'context': {
        'is_shared_balance' : True,
        'balance' : 1,
        'assets': {},
        'parameters': {
            'idx': 11,
            'ddx': 22,
            'risk': 0.02,
            
            #trail stop parameter
            'trail_perc': 0.01,
            
            #TP
            'rrr': 10
        },
    },
    'slippage': 0.0005,
    'time_interval': '12',
    "time_unit" : "h",
    'start_date': '2017-01-01 00:00:00', 
    # 'end_date': '2022-02-15 12:00:00',
    'end_date': dt.datetime.now(),
    'exchange': 'bitmex',
    'window_length' : 2
    }

pairs = {
    'BTC/USDT': ['binance', 0.35],
    'ETH/USDT': ['binance', 0.35],
    'XRP/USD': ['bitstamp', 0.25],
    'ETH/BTC': ['binance', 0.05]
}

for i,v in pairs.items():
    if i.endswith('BTC'):
        maker_fee = -.0005
        taker_fee = 0.0025      
    else:
        maker_fee = -.00025
        taker_fee = 0.00075
        
    config['context']['assets'][i] = {
        'exchange': v[0],
        'maker_fee': maker_fee,
        'taker_fee': taker_fee,
        'allocation' : v[1],
        
        ###Trailing Stop
        'trail': 0,
        }

"""
Trailing stop
"""

def trail_comp(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    return trail_stop
    
def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
        asset.create_order(f"{strategy_name}_{i}", v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f"{strategy_name}_{i}"):
            asset.cancel_order(f"{strategy_name}_{i}")

def calculate_size(symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    min_size = {
        'btcusdt': 100,
        'ethusdt': 1,
        'xrpusdt': 1,
        'ltcusdt': 1,
        'adabtc': 10,
        'ethbtc': 0.01
    }
    pos_size = total_balance * def_alloc
    orig_size = np.floor(pos_size * contract_rate)
    
    if symbol.lower().startswith('btc'):
        if orig_size < 100: 
            if orig_size % 100 >= 90:
                orig_size = round(orig_size, -2)
            else: 
                orig_size = 0
        else:
            orig_size = orig_size - (orig_size % 100)
    elif symbol.lower() == 'ada/btc':
        if orig_size < 10:
            if round(orig_size % 10, 2) >= 9:
                orig_size = round(orig_size, -1)
            else:
                orig_size = 0
        else:
            orig_size = orig_size - (orig_size % 10)
    if with_var == True:
        invalidation = abs((close-kwargs['stop'])/close)
        var_pos_size = (total_balance * kwargs['risk']) / invalidation
        var_size = var_pos_size * contract_rate
        
        if symbol.lower().startswith('btc'):
            if var_size < 100: 
                if var_size % 100 >= 90:
                    var_size = round(var_size, -2)
                else: 
                    var_size = 0
            else:
                var_size = var_size - (var_size % 100)
        else:
            if symbol.lower().endswith('usdt'):
                if var_size < 1:
                    if round(var_size % 1, 2) >= 0.9:
                        var_size = round(var_size, 0)
                    else:
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 1)
            else:
                if symbol.lower() == 'ada/btc':
                    if var_size < 10:
                        if round(var_size % 10, 2) >= 9:
                            var_size = round(var_size, -1)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 10)
                elif symbol.lower() == 'eth/btc':
                    if var_size < 1:
                        if round(var_size % 0.01, 3) >= 0.009:
                            var_size = round(var_size, 2)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
        size = min(orig_size, var_size)
    else:
        size = orig_size
    return size


def initialize(factors, config):
    
    idx = config['parameters']['idx']
    
    factors['close'] = factors.close.ffill()
    factors['high'] = factors.high
    factors['slow'] = factors.low
    
    #dmi
    factors['dx'] = talib_method(method='DX', timeperiod = idx, args = [factors.high, factors.low, factors.close])
    factors['min_di'] = talib_method(method='MINUS_DI', timeperiod = idx, args =[factors.high, factors.low, factors.close])
    factors['plus_di'] = talib_method(method='PLUS_DI', timeperiod = idx, args =[factors.high, factors.low, factors.close])

def process(window, assets, context):
    
    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance']
    
    ### VAR pos sizing
    risk = config['context']['parameters']['risk']  
    
    for symbol, asset in assets.items():
        position = asset.position
        last = window.latest(symbol)
        lastt = window.previous(symbol, 2)
        
        if not np.all([lastt.dx, lastt.plus_di, lastt.min_di]):
            continue
        settings = context['assets'][symbol]
             
        dx = last.dx
        pdi = last.plus_di
        mdi = last.min_di
        
        ### Dx config
        ddx = config['context']['parameters']['ddx']
        
        ### Trailing Stop
        trail_perc = config['context']['parameters']['trail_perc']
        
        ### Take profit
        rrr = config['context']['parameters']['rrr']
          
        ### Entry Conditions
        lg_ent = ((dx > ddx) and
                 (dx > lastt.dx) and
                 (last.close > lastt.close) and
                 (pdi > mdi) and
                 (pdi > lastt.plus_di))
        
        st_ent = ((dx > ddx) and
                 (dx > lastt.dx) and
                 (last.close < lastt.close) and
                 (mdi > pdi) and
                 (mdi > lastt.min_di))
        
        ## %-based SL
        if lg_ent:
            stop = lastt.close *0.97
        elif st_ent:
            stop = lastt.close *1.03
        else:
            stop = 0
        
        ### Trailing stop - kit
        trail_stop = settings['trail']

        size = calculate_size(symbol, total_balance, last.close, settings['allocation'], 
                              asset.get_contract_rate(last.close), 
                              with_var=True, stop=stop, risk=config['context']['parameters']['risk'])
        
        if position.size == 0:
            
            ### cancel orders
            cancel_order('dmi', asset, 'long_tp', 'short_tp')
            
            if lg_ent:
                order = proc_order('market', size, 'buy')

                ### Trail adjustment or SL
                settings['trail'] = stop
                order_sl = proc_order('stop', size, 'sell', trigger=stop)

                ### TP Computation
                close_tp = abs((stop - last.close) / last.close) * rrr
                close_tp2 = last.close * (close_tp + 1)

                ### TP Orders
                order_tp = proc_order('limit', size, 'sell', price=close_tp2)

                ### Execute
                execute_order('dmi', asset, long=order, long_stop=order_sl, long_tp=order_tp)
                
            
            elif st_ent:  
                order = proc_order('market', size, 'sell')
                settings['trail'] = stop
                
                order_sl = proc_order('stop', size, 'buy', trigger=stop)

                close_tp = abs((last.close - stop) / last.close) * rrr
                close_max = last.close * (1 - close_tp)
                
                close_tp2 = max(last.close*0.1, close_max)
                order_tp = proc_order('limit', size, 'buy', price=close_tp2)

                execute_order('dmi', asset, short=order, short_stop=order_sl, short_tp=order_tp)

                
        elif position.size > 0:
            if st_ent:                   
                cancel_order('dmi', asset, 'long_stop', 'long_trail', 'long_tp')
                settings['trail'] = stop
                order = proc_order('market', size + abs(position.size), 'sell')
                order_sl = proc_order('stop', size, 'buy', trigger=stop)
                
                close_tp = abs((last.close - stop) / last.close) * rrr
                close_max = last.close * (1 - close_tp)
                
                close_tp2 = max(last.close*0.1, close_max)
                order_tp = proc_order('limit', size, 'buy', price=close_tp2)
                
                execute_order('dmi', asset, short=order, short_stop=order_sl, short_tp=order_tp)
                        
            
            else:
                trail_stop = trail_comp(position.size, trail_perc, last.close, lastt.close, settings['trail'])
                settings['trail'] = trail_stop  
                # print(trail_stop, last.timestamp)

    
                if last.close < trail_stop:
                    order = proc_order('market', abs(position.size), 'sell')
                    execute_order('dmi', asset, long_trail=order)
                    cancel_order('dmi', asset, 'long_stop')
    
        elif position.size < 0:   
            if lg_ent:                
                cancel_order('dmi', asset, 'short_stop', 'short_trail', 'short_tp')
        
                order = proc_order('market', size + abs(position.size), 'buy')
                settings['trail'] = stop
                order_sl = proc_order('stop', size, 'sell', trigger=stop)
                
                close_tp = abs((stop - last.close) / last.close) * rrr
                close_tp2 = last.close * (close_tp + 1)
                order_tp = proc_order('limit', size, 'sell', price=close_tp2)
                
                execute_order('dmi', asset, long=order, long_stop=order_sl, long_tp=order_tp)
                
            else:                
                trail_stop = trail_comp(position.size, trail_perc, last.close, lastt.close, settings['trail'])
                settings['trail'] = trail_stop
                # print(trail_stop, last.timestamp)

                if last.close > trail_stop:
                    order = proc_order('market', abs(position.size), 'buy')
                    execute_order('dmi', asset, long_trail=order)
                    cancel_order('dmi', asset, 'short_stop')


bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

# save_data
bt.positions.loc['2021-05-28':].to_csv("BMX_DMIv2-positions.csv")

#######################################################################
#DMIv1
def get_win_rate(df, pairs):
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    pairs_dict = {}
    for pair in pairs:
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        win_pct = win_trades / total
        lose_pct = losing_trades / total
        print(f'{pair} Won trades count: {win_trades}')
        print(f'{pair} Lost trades count: {losing_trades}')
        print(f'{pair} Total trades count: {total}')
        print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
        
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
    return pairs_dict

def proc_wr(old_dict, get_win_rate_dict):
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        old_dict[pair] = new2
    return old_dict

from dateutil.relativedelta import relativedelta
import datetime as dt

# BITMEX

config = {
    'context': {
        'is_shared_balance' : True,
        'balance': 1,  
        'assets': {
            'ADA/BTC': {
                'exchange': 'binance',
                'maker_fee': -.0005,
                'taker_fee': .0025,
                'allocation' : 0.33,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            },
#             },
            'BTC/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.33,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0
            },
            'ETH/USDT': {
                'exchange': 'binance',
                'maker_fee': -.00025,
                'taker_fee': .00075,
                'allocation': 0.33,
                'trail': 0,
                'last_pos': None,
                'long_pos': 0,
                'short_pos': 0,
                'no_pos': 0

            }
        },
        'parameters': {
            'idx': 15

            
        },

    },
    'slippage': 0.0005,
    'time_interval': '8',
    'time_unit': 'h',
    'start_date': '2017-01-01 00:00:00',    
    'end_date': dt.date.today(),
    'exchange': 'bitmex',
    'window_length' : 2
}

def initialize(factors, config):
        
    idx = config['parameters']['idx']


    factors['close'] = factors.close.ffill()
    factors['high'] = factors.high
    factors['slow'] = factors.low

    #dmi
    factors['dx'] = talib_method(method='DX', timeperiod = idx, args = [factors.high, factors.low, factors.close])
    factors['min_di'] = talib_method(method='MINUS_DI', timeperiod = idx, args =[factors.high, factors.low, factors.close])
    factors['plus_di'] = talib_method(method='PLUS_DI', timeperiod = idx, args =[factors.high, factors.low, factors.close])

def process(window, assets, context):
    
    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance']
    
    for symbol, asset in assets.items():
        position = asset.position
        last = window.latest(symbol)
        lastt = window.previous(symbol, 2)
        
        if not np.all([lastt.dx, lastt.plus_di, lastt.min_di]):
            continue
        settings = context['assets'][symbol]
        
        dx = last.dx
        pdi = last.plus_di
        mdi = last.min_di
          
        ### Entry Conditions
        lg_ent = ((dx > 25) and
                 (dx > lastt.dx) and
                 (pdi > mdi) and
                 (pdi > lastt.plus_di))
        lg_ext = (pdi < mdi) 
        
        st_ent = ((dx > 25) and
                 (dx > lastt.dx) and
                 (mdi > pdi) and
                 (mdi > lastt.min_di))
        
        st_ext = (pdi > mdi)
        
        if position.size == 0: 
            if lg_ent:
                pos_size = total_balance * settings['allocation']
                size =  pos_size * asset.get_contract_rate(last.close)  
                order = {
                    'order_type' : 'market',
                    'size' : size,
                    'side' : 'buy'
                }
                asset.create_order('l_entry', order)
                
        if position.size > 0 and lg_ext:  
            size = abs(position.size) 
            order = {
                'order_type' : 'market',          
                'size' : size,
                'side' : 'sell'                   
            }
            asset.create_order('l_exit', order)

        if position.size == 0: 
            if st_ent:
                pos_size = total_balance * settings['allocation']
                size = pos_size * asset.get_contract_rate(last.close) #+ abs(position.size) 
                order = {
                    'order_type' : 'market',
                    'size' : size,
                    'side' : 'sell'
                }
                asset.create_order('s_entry', order)
        if position.size < 0 and st_ext:  
            size = abs(position.size) 
            order = {
                'order_type' : 'market',          
                'size' : size,
                'side' : 'buy'                    
            }
            asset.create_order('s_exit', order)

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

# save_data
bt.positions.loc['2021-05-28':].to_csv("BMX_DMIv1-positions.csv")

#DMIv1 end
#####################################################################
#BMX VORTEX start
strat_var = 0
risk = 0.0225
trail_perc = 0.07
rrr = 5
pos_count_val = 5
slippage = 0
slippage = 0.0005

config = {
    'context': {
        'is_shared_balance' : True,
        'balance' : 1,
        'assets': {},
        'parameters': {
            # opt params 02.28.2022
            'v_period' : 29,
            't_period': 1,
            'risk': risk,
            
            ###SL perc
            'sl_perc': 0.1,
            
            ###TS
            'trail_perc': trail_perc,
            
            ###TP 
                ### Bars
            'rrr': rrr, 
            
                ### RSI
            'rsi_len': 14,
            'rsi_ob': 70,
            'rsi_os': 30,
            
        },
    },
    'slippage': slippage,
    'time_interval': 1,
    'time_unit': 'd',   
    'start_date': '2017-01-01 00:00:00',    
    'end_date': dt.datetime.now(),
    'exchange': 'bitmex',
    'window_length' : 2
    }


pairs = {
    'BTC/USDT': ['binance', 1/3],
#     'ETH/USDT': ['binance', 0],
#     'XRP/USDT': ['binance', 0.405],
#     'LTC/USDT': ['binance', 0.19],
#    'ETH/BTC': ['binance', 1/3],
    'ADA/BTC': ['binance', 1/3],
}

for i,v in pairs.items():
    if i.endswith('BTC'):
        maker_fee = -.0005
        taker_fee = 0.0025      
    else:
        maker_fee = -.00025
        taker_fee = 0.00075
        
    config['context']['assets'][i] = {
        'exchange': v[0],
        'maker_fee': maker_fee,
        'taker_fee': taker_fee,
        'allocation' : v[1],

        ###Take profit
        'last_pos': None,
        
        ###Trailing Stop
        'trail': 0,
        }

"""
Trailing stop

"""

def trail_comp(direction, percentage, price, current_trail):
    if direction > 0:
        trail_stop = price * (1-percentage)
        if price < trail_stop:
            trail_stop = current_trail
        elif (trail_stop < current_trail):
            trail_stop = current_trail
    elif direction < 0:
        trail_stop = price * (1+percentage)
        if price > trail_stop:
            trail_stop = current_trail
        elif (trail_stop > current_trail):
            trail_stop = current_trail
    return trail_stop


def trail_comp2(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    return trail_stop


def trail_comp3(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        elif last_close < prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
        
                
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        elif last_close > prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail 
        else:
            trail_stop = current_trail
    return trail_stop

def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
#             print('trigger price', order['trigger_price'])
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
#         print('execute orders', i, v)
        asset.create_order(f'{strategy_name}_{i}', v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f'{strategy_name}_{i}'):
#             print('cancel orders', i)
            asset.cancel_order(f'{strategy_name}_{i}')
    
"""fetch token precision"""
markets = ccxt.ftx().fetch_markets()
# tokens = ['ADA', 'BTC', 'ETC', 'ETH', 'LTC', 'XRP']
# tokens = ['BTC', 'ETH', 'LUNA', 'SOL', 'MATIC', 'AVAX', 'NEAR', 'DOT', 'XRP', 'ADA', 'LTC']
# tokens = ['BTC', 'ETH', 'FTM', 'ATOM', 'ADA', 'BNB', 'FTT', 'DOGE', 'XRP', 'LTC']
tokens = ['BTC',
         'ETH',
         'SOL',
         'FTT',
         'FTM',
         'BNB',
         'MATIC',
         'LINK',
         'XRP',
         'DOGE',
         'LTC',
         'TRX',
         'YFI',
         'OMG',
         'SXP',
         'RUNE',
         'BCH',
         'KNC',
         'LRC',
         'ENJ',
         'EUR',
         'BAT',
         'MKR',
         'COMP',
         'SNX',
         'CHR']

step = []
for token in tokens:
    step.extend([x['precision'].get('amount') for x in markets if x.get('id') == f'{token}-PERP'])

token_steps = dict(zip(tokens, step))

def calculate_size(exchange, symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    if exchange == 'ftx':
        pos_size = total_balance * def_alloc
        orig_size = pos_size * contract_rate
        
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            
            size = min(orig_size, var_size)
            
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
            
        
        sym = symbol.split('/')[0]    
        
        try:
            precision = decimal.Decimal(str(token_steps.get(sym)))

            if precision == decimal.Decimal(1.0):
                precision = decimal.Decimal(0.0)

            size = round(size, abs(precision.as_tuple().exponent))
        except Exception as e:
            pass
#             print(f'Precision error: {symbol}. Use unedited size.')
        
        return size
    
    if exchange == 'binance':
        min_size = {
            'btcusdt': 100,
            'ethusdt': 1,
            'xrpusdt': 1,
            'ltcusdt': 1,
            'adabtc': 10,
            'ethbtc': 0.01
        }
        # orig position size
        pos_size = total_balance * def_alloc
        if symbol.lower() == 'eth/btc':
            orig_size = round(pos_size * contract_rate, 2)
        else:
            orig_size = np.floor(pos_size * contract_rate)

        if symbol.lower().startswith('btc'):
            if orig_size < 100: 
                if orig_size % 100 >= 90:
                    orig_size = round(orig_size, -2)
                else: 
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 100)
        elif symbol.lower() == 'ada/btc':
            if orig_size < 10:
                if round(orig_size % 10, 2) >= 9:
                    orig_size = round(orig_size, -1)
                else:
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 10)
        #var pos size
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            if symbol.lower().startswith('btc'):
                if var_size < 100: 
                    if var_size % 100 >= 90:
                        var_size = round(var_size, -2)
                    else: 
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 100)
            else:
                if symbol.lower().endswith('usdt'):
                    if var_size < 1:
                        if round(var_size % 1, 2) >= 0.9:
                            var_size = round(var_size, 0)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
                else:
                    if symbol.lower() == 'ada/btc':
                        if var_size < 10:
                            if round(var_size % 10, 2) >= 9:
                                var_size = round(var_size, -1)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 10)
                    elif symbol.lower() == 'eth/btc':
                        if var_size < 1:
                            if round(var_size % 0.01, 3) >= 0.009:
                                var_size = round(var_size, 2)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 1)
            size = min(orig_size, var_size)
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
        return size

def get_win_rate(df, pairs):
    
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    
    pairs_dict = {}
    for pair in pairs:
        
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        try:
            win_pct = win_trades / total
            lose_pct = losing_trades / total
        except Exception as e:
            print(e)
            win_pct = 0
            lose_pct = 0
        
#         print(f'{pair} Won trades count: {win_trades}')
#         print(f'{pair} Lost trades count: {losing_trades}')
#         print(f'{pair} Total trades count: {total}')
#         print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
        
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
        
        
        
    return pairs_dict
    
    
def proc_wr(old_dict, get_win_rate_dict):
    
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        
        
        old_dict[pair] = new2
    
    return old_dict

def metrics(eq):
    # risk free rate
    rfr = 0.04
    eq = eq[eq.index.hour == 0]
    rets = eq.pct_change().fillna(0)

    years = list(rets.index.year.drop_duplicates())

    sort = []
    draw = []
    equi = []
    news = []

    data = pd.DataFrame(columns={'year', 'Sortino', 'MDD', 'Equity', 'New Sharpe'})

    for year in years:
        year = str(year)
        year_rets = rets[year]

        #sortino
        sortino = qs.stats.sortino(year_rets, rf= rfr, periods=365, annualize=True)

        #dd
        dd = qs.stats.max_drawdown(year_rets)*100

        year_rets = pd.DataFrame(year_rets, columns=['rets'])
        year_rets['eq'] = (1 + year_rets['rets']).cumprod()

        #equity
        year_eq = qs.stats.comp(year_rets['rets']) + 1

        #linearity
        year_rets['log'] = np.log(year_rets['eq'])
        x = np.arange(year_rets.index.size)
        fit = np.polyfit(x, year_rets['log'], deg=1)
        fit_function = np.poly1d(fit)
        linearity_val = np.sqrt(((year_rets['log'] - fit_function(x))**2).mean())
        cagr_val = qs.stats.cagr(year_rets['eq'].pct_change().fillna(0), rf=rfr, periods=year_rets.shape[0], compounded=True)
        new_sharpe_val = cagr_val/linearity_val

        sort.append(sortino)
        draw.append(dd)
        equi.append(year_eq)
        news.append(new_sharpe_val)

        data = data.append({'New Sharpe': round(new_sharpe_val, 2),
                            'Equity': round(year_eq, 2),
                            'MDD': round(dd, 2), 
                            'Sortino': round(sortino, 2),
                            'year': year}, ignore_index=True)

#     sort_avg = round(sum(sort)/len(sort),2)
#     draw_avg = round(sum(draw)/len(draw),2)
#     equi_avg = round(sum(equi)/len(equi),2)
#     new_sharpe_avg = round(sum(news)/len(news), 2)

    sort_avg = sum(sort)/len(sort)
    draw_avg = sum(draw)/len(draw)
    equi_avg = sum(equi)/len(equi)
    new_sharpe_avg = sum(news)/len(news)

    data = data.append({'year': 'AVG', 
                        'Sortino': sort_avg, 
                        'MDD': draw_avg, 
                        'Equity': equi_avg, 
                        'New Sharpe': new_sharpe_avg}, 
                       ignore_index=True)

    data.set_index('year', inplace=True)
    data = data[['Equity', 'MDD', 'Sortino', 'New Sharpe']]
    return data

from laetitudebots.util.talib_method import talib_method
from laetitudebots.backtest import Backtest

def initialize(factors, config):

    v_period = int(config['parameters']['v_period'])
    t_period = int(config['parameters']['t_period'])

    factors['vmp'] = abs(factors.high - factors.low.shift(1)).rolling(v_period).sum()
    factors['vmm'] = abs(factors.low - factors.high.shift(1)).rolling(v_period).sum()
    factors['str'] = talib_method(method='ATR', timeperiod=t_period, args=[factors.high, factors.low, factors.close]).rolling(v_period).sum()
    factors['vip'] = factors.vmp / factors.str
    factors['vim'] = factors.vmm / factors.str

def process(window, assets, context):
    
    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance']
    
    for symbol, asset in assets.items():
        position = asset.position
        
        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)
        
        settings = context['assets'][symbol]
        lg_sig = (prev_bar.vip < prev_bar.vim) & (last_bar.vip > last_bar.vim)
        st_sig = (prev_bar.vip > prev_bar.vim) & (last_bar.vip < last_bar.vim)
        stop = 0

        if position.size <= 0:
            if lg_sig:
                cancel_order('vortex', asset, 'short')
#                 if asset.orders.get('vortex_short'):
#                     asset.cancel_order('vortex_short')
                lg_ent = last_bar.high
#                 pos_size = total_balance * settings['allocation']
#                 size = abs(position.size) + pos_size * asset.get_contract_rate(lg_ent)
        
                size = calculate_size('binance', symbol, total_balance, lg_ent, settings['allocation'], 
                                      asset.get_contract_rate(lg_ent), 
                                      with_var=False, stop=stop, risk=context['parameters']['risk'])

                if lg_ent > last_bar.close:
                    
                    order = proc_order('stop', size + abs(position.size), 'buy', trigger=lg_ent)
                    
                    execute_order('vortex', asset, long=order)
      
        if position.size >= 0:
            if st_sig:
                cancel_order('vortex', asset, 'long')
#                 if asset.orders.get('vortex_long'):
#                     asset.cancel_order('vortex_long')
                st_ent = last_bar.low
#                 pos_size = total_balance * settings['allocation']
#                 size = abs(position.size) + pos_size * asset.get_contract_rate(st_ent)
        
                size = calculate_size('binance', symbol, total_balance, st_ent, settings['allocation'], 
                                      asset.get_contract_rate(st_ent), 
                                      with_var=False, stop=stop, risk=context['parameters']['risk'])

                if st_ent < last_bar.close:
                    
                    order = proc_order('stop', size + abs(position.size), 'sell', trigger=st_ent)
                    
                    execute_order('vortex', asset, short=order)

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

bt.positions.loc['2021-05-28':].to_csv("BMX_Vortex-positions.csv")


#BMX Vortex end
#####################################################################
#FTX TTR start

import warnings
warnings.filterwarnings("ignore")

strat_var = 0
risk = 0
trail_perc = 0
rrr = 0
pos_count_val = 0
slippage = 0

slippage = 0.0005

del config 

config = {
    'context': {
        'is_shared_balance' : True,
        'balance' : 1,
        'assets': {},
        'parameters': {
            #opt params 02.28.2022
            'length': 32,
            'mult': 2,
            'risk': risk,
            
            ###SL perc
            'sl_perc': 0.1,
            
            ###TS
            'trail_perc': trail_perc,
            
            ###TP 
                ### Bars
            'rrr': rrr, 
            
                ### RSI
            'rsi_len': 14,
            'rsi_ob': 70,
            'rsi_os': 30,
            
        },
    },
    'slippage': slippage,
    'time_interval': 8,
    'time_unit': 'h',   
    'start_date': '2017-01-01 00:00:00',    
    'end_date': dt.datetime.now(),
    'exchange': 'ftx',
    'window_length' : 2
    }


pairs = {
#     'ADA/USDT': ['binance', 1/6],
#     'BTC/USDT': ['binance', 1/6],
#     'ETC/USDT': ['binance', 1/6],
#     'ETH/USDT': ['binance', 1/6],
#     'XRP/USDT': ['binance', 1/6],
#     'LTC/USDT': ['binance', 1/6],
    
    'ADA-PERP': ['ftx', 1/6],
    'BTC-PERP': ['ftx', 1/6],
    'ETC-PERP': ['ftx', 1/6],
    'ETH-PERP': ['ftx', 1/6],
    'XRP-PERP': ['ftx', 1/6],
    'LTC-PERP': ['ftx', 1/6],
#     'ETH/BTC': ['binance', 0],
#     'ADA/BTC': ['binance', 0.09],
}

for i,v in pairs.items():
    if i.endswith('BTC'):
        maker_fee = -.0005
        taker_fee = 0.0025      
    else:
        maker_fee = -.00025
        taker_fee = 0.00075
        
    config['context']['assets'][i] = {
        'exchange': v[0],
        'maker_fee': maker_fee,
        'taker_fee': taker_fee,
        'allocation' : v[1],
        'collateral': 'BTC',

        ###Take profit
        'last_pos': None,
        
        ###Trailing Stop
        'trail': 0,
        }

"""
Trailing stop

"""

def trail_comp(direction, percentage, price, current_trail):
    if direction > 0:
        trail_stop = price * (1-percentage)
        if price < trail_stop:
            trail_stop = current_trail
        elif (trail_stop < current_trail):
            trail_stop = current_trail
    elif direction < 0:
        trail_stop = price * (1+percentage)
        if price > trail_stop:
            trail_stop = current_trail
        elif (trail_stop > current_trail):
            trail_stop = current_trail
    return trail_stop


def trail_comp2(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    return trail_stop


def trail_comp3(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        elif last_close < prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
        
                
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        elif last_close > prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail 
        else:
            trail_stop = current_trail
    return trail_stop

def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
#             print('trigger price', order['trigger_price'])
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
#         print('execute orders', i, v)
        asset.create_order(f'{strategy_name}_{i}', v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f'{strategy_name}_{i}'):
#             print('cancel orders', i)
            asset.cancel_order(f'{strategy_name}_{i}')
    
"""fetch token precision"""
markets = ccxt.ftx().fetch_markets()
# tokens = ['ADA', 'BTC', 'ETC', 'ETH', 'LTC', 'XRP']
# tokens = ['BTC', 'ETH', 'LUNA', 'SOL', 'MATIC', 'AVAX', 'NEAR', 'DOT', 'XRP', 'ADA', 'LTC']
# tokens = ['BTC', 'ETH', 'FTM', 'ATOM', 'ADA', 'BNB', 'FTT', 'DOGE', 'XRP', 'LTC']
tokens = ['BTC',
         'ETH',
         'SOL',
         'FTT',
         'FTM',
         'BNB',
         'MATIC',
         'LINK',
         'XRP',
         'DOGE',
         'LTC',
         'TRX',
         'YFI',
         'OMG',
         'SXP',
         'RUNE',
         'BCH',
         'KNC',
         'LRC',
         'ENJ',
         'EUR',
         'BAT',
         'MKR',
         'COMP',
         'SNX',
         'CHR',
         'ETC',
         'ADA'
         ]

step = []
for token in tokens:
    step.extend([x['precision'].get('amount') for x in markets if x.get('id') == f'{token}-PERP'])

token_steps = dict(zip(tokens, step))

def calculate_size(exchange, symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    if exchange == 'ftx':
        pos_size = total_balance * def_alloc
        orig_size = pos_size * contract_rate
        
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            
            size = min(orig_size, var_size)
            
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
            
        
        sym = symbol.split('/')[0]    
        
        try:
            precision = decimal.Decimal(str(token_steps.get(sym)))

            if precision == decimal.Decimal(1.0):
                precision = decimal.Decimal(0.0)

            size = round(size, abs(precision.as_tuple().exponent))
        except Exception as e:
            pass
#             print(f'Precision error: {symbol}. Use unedited size.')
        
        return size
    
    if exchange == 'binance':
        min_size = {
            'btcusdt': 100,
            'ethusdt': 1,
            'xrpusdt': 1,
            'ltcusdt': 1,
            'adabtc': 10,
            'ethbtc': 0.01
        }
        # orig position size
        pos_size = total_balance * def_alloc
        if symbol.lower() == 'eth/btc':
            orig_size = round(pos_size * contract_rate, 2)
        else:
            orig_size = np.floor(pos_size * contract_rate)

        if symbol.lower().startswith('btc'):
            if orig_size < 100: 
                if orig_size % 100 >= 90:
                    orig_size = round(orig_size, -2)
                else: 
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 100)
        elif symbol.lower() == 'ada/btc':
            if orig_size < 10:
                if round(orig_size % 10, 2) >= 9:
                    orig_size = round(orig_size, -1)
                else:
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 10)
        #var pos size
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            if symbol.lower().startswith('btc'):
                if var_size < 100: 
                    if var_size % 100 >= 90:
                        var_size = round(var_size, -2)
                    else: 
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 100)
            else:
                if symbol.lower().endswith('usdt'):
                    if var_size < 1:
                        if round(var_size % 1, 2) >= 0.9:
                            var_size = round(var_size, 0)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
                else:
                    if symbol.lower() == 'ada/btc':
                        if var_size < 10:
                            if round(var_size % 10, 2) >= 9:
                                var_size = round(var_size, -1)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 10)
                    elif symbol.lower() == 'eth/btc':
                        if var_size < 1:
                            if round(var_size % 0.01, 3) >= 0.009:
                                var_size = round(var_size, 2)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 1)
            size = min(orig_size, var_size)
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
        return size

def get_win_rate(df, pairs):
    
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    
    pairs_dict = {}
    for pair in pairs:
        
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        try:
            win_pct = win_trades / total
            lose_pct = losing_trades / total
        except Exception as e:
            print(e)
            win_pct = 0
            lose_pct = 0
        
#         print(f'{pair} Won trades count: {win_trades}')
#         print(f'{pair} Lost trades count: {losing_trades}')
#         print(f'{pair} Total trades count: {total}')
#         print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
        
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
        
        
        
    return pairs_dict
    
    
def proc_wr(old_dict, get_win_rate_dict):
    
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        
        
        old_dict[pair] = new2
    
    return old_dict

def metrics(eq):
    # risk free rate
    rfr = 0.04
    eq = eq[eq.index.hour == 0]
    rets = eq.pct_change().fillna(0)

    years = list(rets.index.year.drop_duplicates())

    sort = []
    draw = []
    equi = []
    news = []

    data = pd.DataFrame(columns={'year', 'Sortino', 'MDD', 'Equity', 'New Sharpe'})

    for year in years:
        year = str(year)
        year_rets = rets[year]

        #sortino
        sortino = qs.stats.sortino(year_rets, rf= rfr, periods=365, annualize=True)

        #dd
        dd = qs.stats.max_drawdown(year_rets)*100

        year_rets = pd.DataFrame(year_rets, columns=['rets'])
        year_rets['eq'] = (1 + year_rets['rets']).cumprod()

        #equity
        year_eq = qs.stats.comp(year_rets['rets']) + 1

        #linearity
        year_rets['log'] = np.log(year_rets['eq'])
        x = np.arange(year_rets.index.size)
        fit = np.polyfit(x, year_rets['log'], deg=1)
        fit_function = np.poly1d(fit)
        linearity_val = np.sqrt(((year_rets['log'] - fit_function(x))**2).mean())
        cagr_val = qs.stats.cagr(year_rets['eq'].pct_change().fillna(0), rf=rfr, periods=year_rets.shape[0], compounded=True)
        new_sharpe_val = cagr_val/linearity_val

        sort.append(sortino)
        draw.append(dd)
        equi.append(year_eq)
        news.append(new_sharpe_val)

        data = data.append({'New Sharpe': round(new_sharpe_val, 2),
                            'Equity': round(year_eq, 2),
                            'MDD': round(dd, 2), 
                            'Sortino': round(sortino, 2),
                            'year': year}, ignore_index=True)

#     sort_avg = round(sum(sort)/len(sort),2)
#     draw_avg = round(sum(draw)/len(draw),2)
#     equi_avg = round(sum(equi)/len(equi),2)
#     new_sharpe_avg = round(sum(news)/len(news), 2)

    sort_avg = sum(sort)/len(sort)
    draw_avg = sum(draw)/len(draw)
    equi_avg = sum(equi)/len(equi)
    new_sharpe_avg = sum(news)/len(news)

    data = data.append({'year': 'AVG', 
                        'Sortino': sort_avg, 
                        'MDD': draw_avg, 
                        'Equity': equi_avg, 
                        'New Sharpe': new_sharpe_avg}, 
                       ignore_index=True)

    data.set_index('year', inplace=True)
    data = data[['Equity', 'MDD', 'Sortino', 'New Sharpe']]
    return data

from laetitudebots.util.talib_method import talib_method
from laetitudebots.util.custom_method import custom_method
from laetitudebots.backtest import Backtest

def initialize(factors, config):

    length = int(config['parameters']['length'])
    mult = float(config['parameters']['mult'])
    trail_perc = float(config['parameters']['trail_perc'])
    
    factors['high'] = factors.high
    factors['low'] = factors.low
    factors['close'] = factors.close

    factors['atr'] = talib_method(method='ATR', timeperiod=1, args=[factors.high, factors.low, factors.close])
    factors['avg_tr'] = talib_method(method='WMA', timeperiod=length, args=[factors.atr])
    factors['highest'] = factors.high.rolling(length).max()
    factors['lowest'] = factors.low.rolling(length).min()
    factors['hi_lim'] = factors.highest.shift(1) - (factors.avg_tr.shift(1) * mult)
    factors['lo_lim'] = factors.lowest.shift(1) + (factors.avg_tr.shift(1) * mult) 
    factors['mid'] = (factors.hi_lim + factors.lo_lim)/2

def process(window, assets, context):

    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 

    ### VAR pos sizing
    risk = config['context']['parameters']['risk']
    
    for symbol, asset in assets.items():
        
        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)
        
        settings = context['assets'][symbol]
        
        ### Trailing Stop
        trail = settings['trail']
        
        position = asset.position
    
        #entry conditions
        lg_sig = (
                    ((max(prev_bar.hi_lim, prev_bar.lo_lim) > prev_bar.close) &
                     (max(last_bar.hi_lim, last_bar.lo_lim) < last_bar.close))
                    )

        st_sig = (
                    ((min(prev_bar.hi_lim, prev_bar.lo_lim) < prev_bar.close) &
                     (min(last_bar.hi_lim, last_bar.lo_lim) > last_bar.close))
                    )
        
        if lg_sig:
            stop = (min(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
        elif st_sig:
            stop = (max(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
        else:
            stop = 0
               
        size = calculate_size('ftx', symbol, total_balance, last_bar.close, settings['allocation'], 
                              asset.get_contract_rate(last_bar.close, last_bar.collateral), 
                              with_var=False, stop=stop, risk=context['parameters']['risk'])
        
        trail_perc = context['parameters']['trail_perc']
        
        if position.size <= 0:
            if lg_sig:
                order = proc_order('market', size + abs(position.size), 'buy')
                execute_order('rider', asset, long=order)
         
        if position.size >= 0:
            if st_sig:
                order = proc_order('market', size + abs(position.size), 'sell')
                execute_order('rider', asset, short=order)                

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

replacecol = bt.positions.loc['2021-05-28':].copy()
replacecol.columns = pd.MultiIndex.from_tuples(((val[0],val[1].replace('-PERP','/USDT')) for val in replacecol.columns))
replacecol.to_csv("FTX_TTR-positions.csv")
#FTX TTR end
#####################################################################
#BMX NMM start
strat_var = 0
risk = 0.0225
trail_perc = 0.07
rrr = 5
pos_count_val = 5
slippage = 0

slippage = 0.0005
# slippage = 0.00025


config = {
    'context': {
        'is_shared_balance' : True,
        'balance' : 1,
        'assets': {},
        'parameters': {
#             'lb': 29,
            #02.28.2022 opt params
            'lb': 43,
            'risk': risk,
            
            ###SL perc
            'sl_perc': 0.1,
            
            ###TS
            'trail_perc': trail_perc,
            
            ###TP 
                ### Bars
            'rrr': rrr, 
            
                ### RSI
            'rsi_len': 14,
            'rsi_ob': 70,
            'rsi_os': 30,
            
        },
    },
    'slippage': slippage,
    'time_interval': 8,
    'time_unit': 'h',   
    'start_date': '2017-01-01 00:00:00',    
    'end_date': dt.datetime.now(),
    'exchange': 'bitmex',
    'window_length' : 2
    }


pairs = {
#     'BTC/USDT': ['binance', 0.405],
    'ETH/USDT': ['binance', 1/6],
#     'XRP/USDT': ['binance', 0.405],
#    'LTC/BTC': ['binance',1/6],
    'LTC/USDT': ['binance', 1/6],
#    'ETH/BTC': ['binance', 1/6],
    'ADA/BTC': ['binance', 1/6],
#    'TRX/BTC': ['binance', 1/6]
}

for i,v in pairs.items():
    if i.endswith('BTC'):
        maker_fee = -.0005
        taker_fee = 0.0025      
    else:
        maker_fee = -.00025
        taker_fee = 0.00075
        
    config['context']['assets'][i] = {
        'exchange': v[0],
        'maker_fee': maker_fee,
        'taker_fee': taker_fee,
        'allocation' : v[1],

        ###Take profit
        'last_pos': None,
        
        ###Trailing Stop
        'trail': 0,
        }

"""
Trailing stop

"""

def trail_comp(direction, percentage, price, current_trail):
    if direction > 0:
        trail_stop = price * (1-percentage)
        if price < trail_stop:
            trail_stop = current_trail
        elif (trail_stop < current_trail):
            trail_stop = current_trail
    elif direction < 0:
        trail_stop = price * (1+percentage)
        if price > trail_stop:
            trail_stop = current_trail
        elif (trail_stop > current_trail):
            trail_stop = current_trail
    return trail_stop


def trail_comp2(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    return trail_stop


def trail_comp3(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        elif last_close < prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
        
                
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        elif last_close > prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail 
        else:
            trail_stop = current_trail
    return trail_stop

def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
#             print('trigger price', order['trigger_price'])
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
#         print('execute orders', i, v)
        asset.create_order(f'{strategy_name}_{i}', v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f'{strategy_name}_{i}'):
#             print('cancel orders', i)
            asset.cancel_order(f'{strategy_name}_{i}')
    
"""fetch token precision"""
markets = ccxt.ftx().fetch_markets()
# tokens = ['ADA', 'BTC', 'ETC', 'ETH', 'LTC', 'XRP']
# tokens = ['BTC', 'ETH', 'LUNA', 'SOL', 'MATIC', 'AVAX', 'NEAR', 'DOT', 'XRP', 'ADA', 'LTC']
# tokens = ['BTC', 'ETH', 'FTM', 'ATOM', 'ADA', 'BNB', 'FTT', 'DOGE', 'XRP', 'LTC']
tokens = ['BTC',
         'ETH',
         'SOL',
         'FTT',
         'FTM',
         'BNB',
         'MATIC',
         'LINK',
         'XRP',
         'DOGE',
         'LTC',
         'TRX',
         'YFI',
         'OMG',
         'SXP',
         'RUNE',
         'BCH',
         'KNC',
         'LRC',
         'ENJ',
         'EUR',
         'BAT',
         'MKR',
         'COMP',
         'SNX',
         'CHR']

step = []
for token in tokens:
    step.extend([x['precision'].get('amount') for x in markets if x.get('id') == f'{token}-PERP'])

token_steps = dict(zip(tokens, step))

def calculate_size(exchange, symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    if exchange == 'ftx':
        pos_size = total_balance * def_alloc
        orig_size = pos_size * contract_rate
        
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            
            size = min(orig_size, var_size)
            
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
            
        
        sym = symbol.split('/')[0]    
        
        try:
            precision = decimal.Decimal(str(token_steps.get(sym)))

            if precision == decimal.Decimal(1.0):
                precision = decimal.Decimal(0.0)

            size = round(size, abs(precision.as_tuple().exponent))
        except Exception as e:
            pass
#             print(f'Precision error: {symbol}. Use unedited size.')
        
        return size
    
    if exchange == 'binance':
        min_size = {
            'btcusdt': 100,
            'ethusdt': 1,
            'xrpusdt': 1,
            'ltcusdt': 1,
            'adabtc': 10,
            'ethbtc': 0.01
        }
        # orig position size
        pos_size = total_balance * def_alloc
        if symbol.lower() == 'eth/btc':
            orig_size = round(pos_size * contract_rate, 2)
        else:
            orig_size = np.floor(pos_size * contract_rate)

        if symbol.lower().startswith('btc'):
            if orig_size < 100: 
                if orig_size % 100 >= 90:
                    orig_size = round(orig_size, -2)
                else: 
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 100)
        elif symbol.lower() == 'ada/btc':
            if orig_size < 10:
                if round(orig_size % 10, 2) >= 9:
                    orig_size = round(orig_size, -1)
                else:
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 10)
        #var pos size
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            if symbol.lower().startswith('btc'):
                if var_size < 100: 
                    if var_size % 100 >= 90:
                        var_size = round(var_size, -2)
                    else: 
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 100)
            else:
                if symbol.lower().endswith('usdt'):
                    if var_size < 1:
                        if round(var_size % 1, 2) >= 0.9:
                            var_size = round(var_size, 0)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
                else:
                    if symbol.lower() == 'ada/btc':
                        if var_size < 10:
                            if round(var_size % 10, 2) >= 9:
                                var_size = round(var_size, -1)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 10)
                    elif symbol.lower() == 'eth/btc':
                        if var_size < 1:
                            if round(var_size % 0.01, 3) >= 0.009:
                                var_size = round(var_size, 2)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 1)
            size = min(orig_size, var_size)
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
        return size

def get_win_rate(df, pairs):
    
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    
    pairs_dict = {}
    for pair in pairs:
        
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        try:
            win_pct = win_trades / total
            lose_pct = losing_trades / total
        except Exception as e:
            print(e)
            win_pct = 0
            lose_pct = 0
        
#         print(f'{pair} Won trades count: {win_trades}')
#         print(f'{pair} Lost trades count: {losing_trades}')
#         print(f'{pair} Total trades count: {total}')
#         print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
        
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
        
        
        
    return pairs_dict
    
    
def proc_wr(old_dict, get_win_rate_dict):
    
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        
        
        old_dict[pair] = new2
    
    return old_dict

def metrics(eq):
    # risk free rate
    rfr = 0.04
    eq = eq[eq.index.hour == 0]
    rets = eq.pct_change().fillna(0)

    years = list(rets.index.year.drop_duplicates())

    sort = []
    draw = []
    equi = []
    news = []

    data = pd.DataFrame(columns={'year', 'Sortino', 'MDD', 'Equity', 'New Sharpe'})

    for year in years:
        year = str(year)
        year_rets = rets[year]

        #sortino
        sortino = qs.stats.sortino(year_rets, rf= rfr, periods=365, annualize=True)

        #dd
        dd = qs.stats.max_drawdown(year_rets)*100

        year_rets = pd.DataFrame(year_rets, columns=['rets'])
        year_rets['eq'] = (1 + year_rets['rets']).cumprod()

        #equity
        year_eq = qs.stats.comp(year_rets['rets']) + 1

        #linearity
        year_rets['log'] = np.log(year_rets['eq'])
        x = np.arange(year_rets.index.size)
        fit = np.polyfit(x, year_rets['log'], deg=1)
        fit_function = np.poly1d(fit)
        linearity_val = np.sqrt(((year_rets['log'] - fit_function(x))**2).mean())
        cagr_val = qs.stats.cagr(year_rets['eq'].pct_change().fillna(0), rf=rfr, periods=year_rets.shape[0], compounded=True)
        new_sharpe_val = cagr_val/linearity_val

        sort.append(sortino)
        draw.append(dd)
        equi.append(year_eq)
        news.append(new_sharpe_val)

        data = data.append({'New Sharpe': round(new_sharpe_val, 2),
                            'Equity': round(year_eq, 2),
                            'MDD': round(dd, 2), 
                            'Sortino': round(sortino, 2),
                            'year': year}, ignore_index=True)

#     sort_avg = round(sum(sort)/len(sort),2)
#     draw_avg = round(sum(draw)/len(draw),2)
#     equi_avg = round(sum(equi)/len(equi),2)
#     new_sharpe_avg = round(sum(news)/len(news), 2)

    sort_avg = sum(sort)/len(sort)
    draw_avg = sum(draw)/len(draw)
    equi_avg = sum(equi)/len(equi)
    new_sharpe_avg = sum(news)/len(news)

    data = data.append({'year': 'AVG', 
                        'Sortino': sort_avg, 
                        'MDD': draw_avg, 
                        'Equity': equi_avg, 
                        'New Sharpe': new_sharpe_avg}, 
                       ignore_index=True)

    data.set_index('year', inplace=True)
    data = data[['Equity', 'MDD', 'Sortino', 'New Sharpe']]
    return data

import numpy as np

def nmm(values):
    nmm = 0.0
    sum = 0.0
    length = 0.0
    length = len(values)
    last_val = values[-1]
    x = 0
    for i in range(1, (length-1)):
        x =+ 1
        sum = sum + (last_val - values[i]) / np.sqrt(i*1.0)
    nmmv = sum/(length)
    return float(nmmv)

from laetitudebots.util.talib_method import talib_method
from laetitudebots.util.custom_method import custom_method
from laetitudebots.backtest import Backtest

def initialize(factors, config):

    trail_perc = float(config['parameters']['trail_perc'])

    lb = int(config['parameters']['lb'])

    factors['log'] = np.log(factors.close)
    factors['nmm'] = factors.log.rolling(lb).apply(nmm)

def process(window, assets, context):

    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 

    ### VAR pos sizing
    risk = config['context']['parameters']['risk']
    
    for symbol, asset in assets.items():
        
        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)
        
        settings = context['assets'][symbol]
        
        ### Trailing Stop
        trail = settings['trail']
        
        position = asset.position
    
        #entry conditions 

        lg_sig = (prev_bar.nmm < 0) & (last_bar.nmm > 0)
        st_sig = (prev_bar.nmm > 0) & (last_bar.nmm < 0)
        
#         if lg_sig:
#             stop = (min(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
#         elif st_sig:
#             stop = (max(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
#         else:
        stop = 0
               
        size = calculate_size('binance', symbol, total_balance, last_bar.close, settings['allocation'], 
                              asset.get_contract_rate(last_bar.close), 
                              with_var=False, stop=stop, risk=context['parameters']['risk'])
        
        trail_perc = context['parameters']['trail_perc']
        
        if (position.size <= 0) and lg_sig:
#             if lg_sig:
                order = proc_order('market', size + abs(position.size), 'buy')
                execute_order('rider', asset, long=order)
         
        if (position.size >= 0) and st_sig:
#             if st_sig:
                order = proc_order('market', size + abs(position.size), 'sell')
                execute_order('rider', asset, short=order)                

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

bt.positions.loc['2021-05-28':].to_csv("BMX_NMM-positions.csv")
#BMX NMM end
####################################################################
#FTX TT5V3USD START
strat_var = 1
risk = 0.03
trail_perc = 0.05
rrr = 10
pos_count_val = 5
slippage = 0.0005

slippage = 0.00025


config = {
    'context': {
        'is_shared_balance' : True,
        'balance' : 1,
        'assets': {},
        'parameters': {
#             'length': 165,
#             'mult': 2.5,
            # updated params 06.01.22
            'length': 110,
            'mult': 4,
            'risk': risk,
            
            ###SL perc
            'sl_perc': 0.1,
            
            ###TS
            'trail_perc': trail_perc,
            
            ###TP 
                ### Bars
            'rrr': rrr, 
            
                ### RSI
            'rsi_len': 14,
            'rsi_ob': 70,
            'rsi_os': 30,
            
        },
    },
    'slippage': slippage,
    'time_interval': 1,
    'time_unit': 'h',   
    'start_date': '2019-01-01 00:00:00',    
#     'end_date': '2021-12-31 23:59:59', 
    'end_date': dt.datetime.now(),
    'exchange': 'ftx',
    'window_length' : 2
    }

n = 2
pairs = {
    'DOGE/USDT': ['binance', 1/n],
    'OMG/USDT': ['binance', 1/n],
}

for i,v in pairs.items():
    if i.endswith('BTC'):
        maker_fee = -.0005
        taker_fee = 0.0025      
    else:
        maker_fee = .0002
        taker_fee = .0007
        
    config['context']['assets'][i] = {
        'exchange': v[0],
        'maker_fee': maker_fee,
        'taker_fee': taker_fee,
        'allocation' : v[1],

        ###Take profit
        'last_pos': None,
        
        ###Trailing Stop
        'trail': 0,
        
        # for var
        'distance': 0,
        'dist_date': '',
        'close': 0,
        'stop': 0,
        'cont_rate': 0
        }

"""
Trailing stop

"""

def trail_comp(direction, percentage, price, current_trail):
    if direction > 0:
        trail_stop = price * (1-percentage)
        if price < trail_stop:
            trail_stop = current_trail
        elif (trail_stop < current_trail):
            trail_stop = current_trail
    elif direction < 0:
        trail_stop = price * (1+percentage)
        if price > trail_stop:
            trail_stop = current_trail
        elif (trail_stop > current_trail):
            trail_stop = current_trail
    return trail_stop


def trail_comp2(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    return trail_stop


def trail_comp3(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        elif last_close < prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
        
                
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        elif last_close > prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail 
        else:
            trail_stop = current_trail
    return trail_stop

def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
#             print('trigger price', order['trigger_price'])
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
#         print('execute orders', i, v)
        asset.create_order(f'{strategy_name}_{i}', v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f'{strategy_name}_{i}'):
#             print('cancel orders', i)
            asset.cancel_order(f'{strategy_name}_{i}')
    
"""fetch token precision"""
markets = ccxt.ftx().fetch_markets()
markets = [x for x in markets if x.get('id').endswith('-PERP')]
token_steps = {}
for market in markets:
    key = market.get('base')
    token_steps[key] = market.get('precision').get('amount')

def calculate_size(exchange, symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    if exchange == 'ftx':
        pos_size = total_balance * def_alloc
        orig_size = pos_size * contract_rate
        
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            
            size = min(orig_size, var_size)
            
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
            
        
        sym = symbol.split('/')[0]    
        
        try:
            precision = decimal.Decimal(str(token_steps.get(sym)))

            if precision == decimal.Decimal(1.0):
                precision = decimal.Decimal(0.0)

            size = str(size).split('.')[0] + '.' + str(size).split('.')[1][:abs(precision.as_tuple().exponent)]
            size = float(size)
        except Exception as e:
            pass
#             print(f'Precision error: {symbol}. Use unedited size.')
        
        return size
    
    if exchange == 'binance':
        min_size = {
            'btcusdt': 100,
            'ethusdt': 1,
            'xrpusdt': 1,
            'ltcusdt': 1,
            'adabtc': 10,
            'ethbtc': 0.01
        }
        # orig position size
        pos_size = total_balance * def_alloc
        if symbol.lower() == 'eth/btc':
            orig_size = round(pos_size * contract_rate, 2)
        else:
            orig_size = np.floor(pos_size * contract_rate)

        if symbol.lower().startswith('btc'):
            if orig_size < 100: 
                if orig_size % 100 >= 90:
                    orig_size = round(orig_size, -2)
                else: 
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 100)
        elif symbol.lower() == 'ada/btc':
            if orig_size < 10:
                if round(orig_size % 10, 2) >= 9:
                    orig_size = round(orig_size, -1)
                else:
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 10)
        #var pos size
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            if symbol.lower().startswith('btc'):
                if var_size < 100: 
                    if var_size % 100 >= 90:
                        var_size = round(var_size, -2)
                    else: 
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 100)
            else:
                if symbol.lower().endswith('usdt'):
                    if var_size < 1:
                        if round(var_size % 1, 2) >= 0.9:
                            var_size = round(var_size, 0)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
                else:
                    if symbol.lower() == 'ada/btc':
                        if var_size < 10:
                            if round(var_size % 10, 2) >= 9:
                                var_size = round(var_size, -1)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 10)
                    elif symbol.lower() == 'eth/btc':
                        if var_size < 1:
                            if round(var_size % 0.01, 3) >= 0.009:
                                var_size = round(var_size, 2)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 1)
            size = min(orig_size, var_size)
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
        return size

def get_win_rate(df, pairs):
    
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    
    pairs_dict = {}
    for pair in pairs:
        
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        try:
            win_pct = win_trades / total
            lose_pct = losing_trades / total
        except Exception as e:
            print(e)
            win_pct = 0
            lose_pct = 0
        
#         print(f'{pair} Won trades count: {win_trades}')
#         print(f'{pair} Lost trades count: {losing_trades}')
#         print(f'{pair} Total trades count: {total}')
#         print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
        
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
        
        
        
    return pairs_dict
    
    
def proc_wr(old_dict, get_win_rate_dict):
    
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        
        
        old_dict[pair] = new2
    
    return old_dict

def metrics(eq):
    # risk free rate
    rfr = 0.04
    eq = eq[eq.index.hour == 0]
    rets = eq.pct_change().fillna(0)

    years = list(rets.index.year.drop_duplicates())
    years.remove(2022)

    sort = []
    draw = []
    equi = []
    news = []

    data = pd.DataFrame(columns={'year', 'Sortino', 'MDD', 'Equity', 'New Sharpe'})

    for year in years:
        year = str(year)
        year_rets = rets[year]

        #sortino
        sortino = qs.stats.sortino(year_rets, rf= rfr, periods=365, annualize=True)

        #dd
        dd = qs.stats.max_drawdown(year_rets)*100

        year_rets = pd.DataFrame(year_rets, columns=['rets'])
        year_rets['eq'] = (1 + year_rets['rets']).cumprod()

        #equity
        year_eq = qs.stats.comp(year_rets['rets']) + 1

        #linearity
        year_rets['log'] = np.log(year_rets['eq'])
        x = np.arange(year_rets.index.size)
        fit = np.polyfit(x, year_rets['log'], deg=1)
        fit_function = np.poly1d(fit)
        linearity_val = np.sqrt(((year_rets['log'] - fit_function(x))**2).mean())
        cagr_val = qs.stats.cagr(year_rets['eq'].pct_change().fillna(0), rf=rfr, periods=year_rets.shape[0], compounded=True)
        new_sharpe_val = cagr_val/linearity_val

        sort.append(sortino)
        draw.append(dd)
        equi.append(year_eq)
        news.append(new_sharpe_val)

        data = data.append({'New Sharpe': round(new_sharpe_val, 2),
                            'Equity': round(year_eq, 2),
                            'MDD': round(dd, 2), 
                            'Sortino': round(sortino, 2),
                            'year': year}, ignore_index=True)

    sort_avg = sum(sort)/len(sort)
    draw_avg = sum(draw)/len(draw)
    equi_avg = sum(equi)/len(equi)
    new_sharpe_avg = sum(news)/len(news)

    data = data.append({'year': 'AVG', 
                        'Sortino': sort_avg, 
                        'MDD': draw_avg, 
                        'Equity': equi_avg, 
                        'New Sharpe': new_sharpe_avg}, 
                       ignore_index=True)

    data.set_index('year', inplace=True)
    data = data[['Equity', 'MDD', 'Sortino', 'New Sharpe']]
    return data

def plot_equity(equity):
    _, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [1, 3]},
                         sharex=True, figsize=(5.5*(16/9), 5.5))
    running_max = np.maximum.accumulate(equity)
    drawdown = -100 * ((running_max - equity) / running_max)
    ax[0].fill_between(equity.index, 0, drawdown, alpha=0.3, color='r')
    ax[0].set_title('Drawdown')
    ax[1].plot(equity)
    ax[1].set_title('Total Balance')
    return ax


from laetitudebots.util.talib_method import talib_method
from laetitudebots.util.custom_method import custom_method
from laetitudebots.backtest import Backtest

def initialize(factors, config):

    length = int(config['parameters']['length'])
    mult = float(config['parameters']['mult'])
#     trail_perc = float(config['parameters']['trail_perc'])
#     print(trail_perc)
    
    factors['high'] = factors.high #.fillna(0).ffill()
    factors['low'] = factors.low #.fillna(0).ffill()
    factors['close'] = factors.close #.fillna(0).ffill()

    factors['atr'] = talib_method(method='ATR', timeperiod=1, args=[factors.high, factors.low, factors.close])
    factors['avg_tr'] = talib_method(method='WMA', timeperiod=length, args=[factors.atr])
    factors['highest'] = factors.high.rolling(length).max()
    factors['lowest'] = factors.low.rolling(length).min()
    factors['hi_lim'] = (factors.highest.shift(1) - (factors.avg_tr.shift(1) * mult)).fillna(0)
    factors['lo_lim'] = (factors.lowest.shift(1) + (factors.avg_tr.shift(1) * mult)).fillna(0)
    factors['mid'] = ((factors.hi_lim + factors.lo_lim)/2).fillna(0)

def process(window, assets, context):

    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 

    ### VAR pos sizing
    risk = config['context']['parameters']['risk']
    
    for symbol, asset in assets.items():
        
        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)
        
        settings = context['assets'][symbol]
        
        ### Trailing Stop
        trail = settings['trail']
        
        position = asset.position
    
        #entry conditions
        lg_sig = (
                    ((max(prev_bar.hi_lim, prev_bar.lo_lim) > prev_bar.close) &
                     (max(last_bar.hi_lim, last_bar.lo_lim) < last_bar.close))
                    )

        st_sig = (
                    ((min(prev_bar.hi_lim, prev_bar.lo_lim) < prev_bar.close) &
                     (min(last_bar.hi_lim, last_bar.lo_lim) > last_bar.close))
                    )
        
        if lg_sig:
            stop = (min(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
        elif st_sig:
            stop = (max(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
        else:
            stop = 0
               
        size = calculate_size('ftx', symbol, total_balance, last_bar.close, settings['allocation'], 
                              asset.get_contract_rate(last_bar.close), 
                              with_var=True, stop=stop, risk=context['parameters']['risk'])
        
        date = pd.to_datetime(last_bar.timestamp)
        month = date.month
        year = date.year
        
        
#         if lg_sig or st_sig:
#             if (month==4) and (year==2022):
#                 print('APRIL')
#                 dist = (abs(last_bar.close-stop)/last_bar.close)*100
#                 print('dist', dist)
#                 if dist > config['context']['assets'][symbol]['distance']:
#                     config['context']['assets'][symbol]['distance'] = dist
#                     print(settings['distance'])
#                     config['context']['assets'][symbol]['dist_date'] = last_bar.timestamp
#                     print(settings['dist_date'])
#                     print(last_bar.timestamp)
#                     config['context']['assets'][symbol]['close'] = last_bar.close
#                     config['context']['assets'][symbol]['stop'] = stop
#                     config['context']['assets'][symbol]['cont_rate'] = asset.get_contract_rate(last_bar.close)
                
#             print('**********')
#             print(symbol)
#             print(last_bar.timestamp)
#             print(f'close {last_bar.close}')
#             print(f'stop {stop}')
#             print(f'distance {(abs(last_bar.close-stop)/last_bar.close)*100}')
#             print(f'contract rate {asset.get_contract_rate(last_bar.close)}')
#             print('**********')

# for var
#         'distance': 0,
#         'dist_date': '',
#         'close': 0,
#         'stop': 0,
#         'cont_rate': 0
        
        
        trail_perc = context['parameters']['trail_perc']
        
        if position.size == 0:
            cancel_order('rider', asset, 'long_stop', 'long_trail', 'short_stop', 'short_trail')
            if lg_sig:                   
                order = proc_order('market', size, 'buy')

                ### long sl
#                 order_sl = proc_order('stop', size, 'sell', trigger=stop)

                ### Trail adjustment
                settings['trail'] = stop

                execute_order('rider', asset, long=order) #, long_stop=order_sl)
                                
            elif st_sig:
                order = proc_order('market', size, 'sell')
#                 order_sl = proc_order('stop', size, 'buy', trigger=stop)

                settings['trail'] = stop

                execute_order('rider', asset, short=order) #, short_stop=order_sl)
        
#                 print(symbol, asset.get_contract_rate(last_bar.close))

        elif position.size > 0:
            if st_sig:
#                 cancel_order('rider', asset, 'long_stop', 'long_trail')
                
                order = proc_order('market', size + abs(position.size), 'sell')
#                 order_sl = proc_order('stop', size, 'buy', trigger=stop)
                
                settings['trail'] = stop
                
                execute_order('rider', asset, short=order) #, short_stop=order_sl)

            else:
                if last_bar.close < settings['trail']:
                    order = proc_order('market', abs(position.size), 'sell')
                    execute_order('rider', asset, long_stop=order)
                
                else:
                    trail_stop = trail_comp(position.size, trail_perc, last_bar.low, settings['trail'])
                    if trail_stop > settings['trail']:
                        settings['trail'] = trail_stop
         
        elif position.size < 0:
            if lg_sig:
                
                ### reverse to long
                order = proc_order('market', size + abs(position.size), 'buy')
                
                settings['trail'] = stop
                
                execute_order('rider', asset, long=order)                

            else:
                if last_bar.close > settings['trail']:
                    order = proc_order('market', abs(position.size), 'buy')
                    execute_order('rider', asset, short_stop=order)
                else:
                    trail_stop = trail_comp(position.size, trail_perc, last_bar.high, settings['trail'])
                    if trail_stop < settings['trail']:
                        settings['trail'] = trail_stop

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

bt.positions.loc['2021-05-28':].to_csv("FTX_TT5V3USD-positions.csv")
#FTX TT5V3USD END
####################################################################
# #FTX TT6V3USD START

# from dateutil.relativedelta import relativedelta
# import datetime as dt
# import pandas as pd
# import numpy as np
# import matplotlib.pyplot as plt
# import math
# import multiprocessing as mp
# import os
# from itertools import product
# import pickle
# import copy

# import quantstats as qs
# from tqdm.notebook import tqdm
# import ccxt

# from laetitudebots.util.talib_method import talib_method
# from laetitudebots.util.custom_method import custom_method
# from laetitudebots.backtest import Backtest
# from laetitudebots.backtest.optimizer import Optimizer

# import warnings
# warnings.filterwarnings("ignore")
# def tt6v3usd_run():
#     strat_var = 1
#     risk = 0.02
#     trail_perc = 0.05
#     rrr = 10
#     pos_count_val = 5
#     slippage = 0.0005

#     slippage = 0.00025


#     config = {
#         'context': {
#             'is_shared_balance' : True,
#             'balance' : 1,
#             'assets': {},
#             'parameters': {
#     #             'length': 185,
#     #             'mult': 2,
#                 # updated params 06.01.22
#                 'length': 165,
#                 'mult': 2,
#                 'risk': risk,
                
#                 ###SL perc
#                 'sl_perc': 0.1,
                
#                 ###TS
#                 'trail_perc': trail_perc,
                
#                 ###TP 
#                     ### Bars
#                 'rrr': rrr, 
                
#                     ### RSI
#                 'rsi_len': 14,
#                 'rsi_ob': 70,
#                 'rsi_os': 30,
                
#             },
#         },
#         'slippage': slippage,
#         'time_interval': 1,
#         'time_unit': 'h',   
#         'start_date': '2019-01-01 00:00:00',    
#     #     'end_date': '2021-12-31 23:59:59', 
#         'end_date': dt.datetime.now(),
#         'exchange': 'ftx',
#         'window_length' : 2
#         }

#     n = 6
#     pairs = {
#         'MATIC/USDT': ['binance', 1/n],
#         'LINK/USDT': ['binance', 1/n],
#         'XRP/USDT': ['binance', 1/n],
#         'DOGE/USDT': ['binance', 1/n],  
#         'LTC/USDT': ['binance', 1/n],
#         'OMG/USDT': ['binance', 1/n],
#     }

#     for i,v in pairs.items():
#         if i.endswith('BTC'):
#             maker_fee = -.0005
#             taker_fee = 0.0025      
#         else:
#             maker_fee = .0002
#             taker_fee = .0007
            
#         config['context']['assets'][i] = {
#             'exchange': v[0],
#             'maker_fee': maker_fee,
#             'taker_fee': taker_fee,
#             'allocation' : v[1],

#             ###Take profit
#             'last_pos': None,
            
#             ###Trailing Stop
#             'trail': 0,
            
#             # for var
#             'distance': 0,
#             'dist_date': '',
#             'close': 0,
#             'stop': 0,
#             'cont_rate': 0
#             }

#     """
#     Trailing stop

#     """

#     def trail_comp(direction, percentage, price, current_trail):
#         if direction > 0:
#             trail_stop = price * (1-percentage)
#             if price < trail_stop:
#                 trail_stop = current_trail
#             elif (trail_stop < current_trail):
#                 trail_stop = current_trail
#         elif direction < 0:
#             trail_stop = price * (1+percentage)
#             if price > trail_stop:
#                 trail_stop = current_trail
#             elif (trail_stop > current_trail):
#                 trail_stop = current_trail
#         return trail_stop


#     def trail_comp2(direction, percentage, last_close, prev_close, current_trail):
        
#         if direction > 0:
#             if last_close > prev_close:
#                 trail_stop = current_trail * (1+percentage)
#                 if last_close < trail_stop:
#                     trail_stop = current_trail
#             else:
#                 trail_stop = current_trail
#         elif direction < 0:
#             if last_close < prev_close:
#                 trail_stop = current_trail * (1-percentage)
#                 if last_close > trail_stop:
#                     trail_stop = current_trail
#             else:
#                 trail_stop = current_trail
#         return trail_stop


#     def trail_comp3(direction, percentage, last_close, prev_close, current_trail):
        
#         if direction > 0:
#             if last_close > prev_close:
#                 trail_stop = current_trail * (1+percentage)
#                 if last_close < trail_stop:
#                     trail_stop = current_trail
#             elif last_close < prev_close:
#                 trail_stop = current_trail * (1+percentage)
#                 if last_close < trail_stop:
#                     trail_stop = current_trail
#             else:
#                 trail_stop = current_trail
            
                    
#         elif direction < 0:
#             if last_close < prev_close:
#                 trail_stop = current_trail * (1-percentage)
#                 if last_close > trail_stop:
#                     trail_stop = current_trail
#             elif last_close > prev_close:
#                 trail_stop = current_trail * (1-percentage)
#                 if last_close > trail_stop:
#                     trail_stop = current_trail 
#             else:
#                 trail_stop = current_trail
#         return trail_stop

#     def proc_order(ord_type, ord_size, side, price=None, trigger=None):
#         order = {
#                 'order_type' : ord_type,
#                 'size' : ord_size,
#                 'side' : side,
#                 }
#         if ord_type is 'stop':
#             if trigger is not None:
#                 order['trigger_price'] = trigger 
#     #             print('trigger price', order['trigger_price'])
        
#         if ord_type is 'limit':
#             if price is not None:
#                 order['price'] = price
        
#         return order

#     def execute_order(strategy_name, asset, **orders):
#         for i,v in orders.items():
#     #         print('execute orders', i, v)
#             asset.create_order(f'{strategy_name}_{i}', v)
            
#     def cancel_order(strategy_name, asset, *orders):
#         for i in orders:
#             if asset.orders.get(f'{strategy_name}_{i}'):
#     #             print('cancel orders', i)
#                 asset.cancel_order(f'{strategy_name}_{i}')
        
#     """fetch token precision"""
#     markets = ccxt.ftx().fetch_markets()
#     markets = [x for x in markets if x.get('id').endswith('-PERP')]
#     token_steps = {}
#     for market in markets:
#         key = market.get('base')
#         token_steps[key] = market.get('precision').get('amount')

#     def calculate_size(exchange, symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
#         if exchange == 'ftx':
#             pos_size = total_balance * def_alloc
#             orig_size = pos_size * contract_rate
            
#             if with_var and (float(kwargs['risk']) > 0):
#                 invalidation = abs((close-kwargs['stop'])/close)
#                 var_pos_size = (total_balance * kwargs['risk']) / invalidation
#                 var_size = var_pos_size * contract_rate
                
#                 size = min(orig_size, var_size)
                
#             elif (with_var == False) or (float(kwargs['risk']) == 0.0):
#                 size = orig_size
                
            
#             sym = symbol.split('/')[0]    
            
#             try:
#                 precision = decimal.Decimal(str(token_steps.get(sym)))

#                 if precision == decimal.Decimal(1.0):
#                     precision = decimal.Decimal(0.0)

#                 size = str(size).split('.')[0] + '.' + str(size).split('.')[1][:abs(precision.as_tuple().exponent)]
#                 size = float(size)
#             except Exception as e:
#                 pass
#     #             print(f'Precision error: {symbol}. Use unedited size.')
            
#             return size
        
#         if exchange == 'binance':
#             min_size = {
#                 'btcusdt': 100,
#                 'ethusdt': 1,
#                 'xrpusdt': 1,
#                 'ltcusdt': 1,
#                 'adabtc': 10,
#                 'ethbtc': 0.01
#             }
#             # orig position size
#             pos_size = total_balance * def_alloc
#             if symbol.lower() == 'eth/btc':
#                 orig_size = round(pos_size * contract_rate, 2)
#             else:
#                 orig_size = np.floor(pos_size * contract_rate)

#             if symbol.lower().startswith('btc'):
#                 if orig_size < 100: 
#                     if orig_size % 100 >= 90:
#                         orig_size = round(orig_size, -2)
#                     else: 
#                         orig_size = 0
#                 else:
#                     orig_size = orig_size - (orig_size % 100)
#             elif symbol.lower() == 'ada/btc':
#                 if orig_size < 10:
#                     if round(orig_size % 10, 2) >= 9:
#                         orig_size = round(orig_size, -1)
#                     else:
#                         orig_size = 0
#                 else:
#                     orig_size = orig_size - (orig_size % 10)
#             #var pos size
#             if with_var and (float(kwargs['risk']) > 0):
#                 invalidation = abs((close-kwargs['stop'])/close)
#                 var_pos_size = (total_balance * kwargs['risk']) / invalidation
#                 var_size = var_pos_size * contract_rate
#                 if symbol.lower().startswith('btc'):
#                     if var_size < 100: 
#                         if var_size % 100 >= 90:
#                             var_size = round(var_size, -2)
#                         else: 
#                             var_size = 0
#                     else:
#                         var_size = var_size - (var_size % 100)
#                 else:
#                     if symbol.lower().endswith('usdt'):
#                         if var_size < 1:
#                             if round(var_size % 1, 2) >= 0.9:
#                                 var_size = round(var_size, 0)
#                             else:
#                                 var_size = 0
#                         else:
#                             var_size = var_size - (var_size % 1)
#                     else:
#                         if symbol.lower() == 'ada/btc':
#                             if var_size < 10:
#                                 if round(var_size % 10, 2) >= 9:
#                                     var_size = round(var_size, -1)
#                                 else:
#                                     var_size = 0
#                             else:
#                                 var_size = var_size - (var_size % 10)
#                         elif symbol.lower() == 'eth/btc':
#                             if var_size < 1:
#                                 if round(var_size % 0.01, 3) >= 0.009:
#                                     var_size = round(var_size, 2)
#                                 else:
#                                     var_size = 0
#                             else:
#                                 var_size = var_size - (var_size % 1)
#                 size = min(orig_size, var_size)
#             elif (with_var == False) or (float(kwargs['risk']) == 0.0):
#                 size = orig_size
#             return size

#     def get_win_rate(df, pairs):
        
#         for pair in pairs:
#             df[(pair, 'pos')] = None
#             df[(pair, 'wl')] = 0
            
#             for i in range(len(df)):
#                 if df.iloc[i][(pair, 'size')] > 0:
#                     df[(pair, 'pos')].iloc[i] = 'long'
#                 elif df.iloc[i][(pair, 'size')] < 0:
#                     df[(pair, 'pos')].iloc[i] = 'short'
#                 if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
#                     if df[(pair, 'pos')].iloc[i-1] == 'long':
#                         if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
#                             df[(pair, 'wl')].iloc[i] = 1
#                         else:
#                             df[(pair, 'wl')].iloc[i] = -1
#                     elif df[(pair, 'pos')].iloc[i-1] == 'short':
#                         if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
#                             df[(pair, 'wl')].iloc[i] = -1
#                         else:
#                             df[(pair, 'wl')].iloc[i] = 1
        
#         pairs_dict = {}
#         for pair in pairs:
            
#             win_trades = len(pos[pos[(pair, 'wl')] == 1])
#             losing_trades = len(pos[pos[(pair, 'wl')] == -1])
#             total = win_trades + losing_trades
#             try:
#                 win_pct = win_trades / total
#                 lose_pct = losing_trades / total
#             except Exception as e:
#                 print(e)
#                 win_pct = 0
#                 lose_pct = 0
            
#     #         print(f'{pair} Won trades count: {win_trades}')
#     #         print(f'{pair} Lost trades count: {losing_trades}')
#     #         print(f'{pair} Total trades count: {total}')
#     #         print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
            
#             pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
            
            
            
#         return pairs_dict
        
        
#     def proc_wr(old_dict, get_win_rate_dict):
        
#         pair_keys = old_dict.keys()
#         for pair in pair_keys:
            
#             old = old_dict[pair]
#             new = get_win_rate_dict[pair]
            
#             new2 = [old[x] + new[x] for x in range(len(old))]
#             new2[-1] = round(new2[0] / new2[2], 4)
            
            
#             old_dict[pair] = new2
        
#         return old_dict

#     def metrics(eq):
#         # risk free rate
#         rfr = 0.04
#         eq = eq[eq.index.hour == 0]
#         rets = eq.pct_change().fillna(0)

#         years = list(rets.index.year.drop_duplicates())
#         years.remove(2022)

#         sort = []
#         draw = []
#         equi = []
#         news = []

#         data = pd.DataFrame(columns={'year', 'Sortino', 'MDD', 'Equity', 'New Sharpe'})

#         for year in years:
#             year = str(year)
#             year_rets = rets[year]

#             #sortino
#             sortino = qs.stats.sortino(year_rets, rf= rfr, periods=365, annualize=True)

#             #dd
#             dd = qs.stats.max_drawdown(year_rets)*100

#             year_rets = pd.DataFrame(year_rets, columns=['rets'])
#             year_rets['eq'] = (1 + year_rets['rets']).cumprod()

#             #equity
#             year_eq = qs.stats.comp(year_rets['rets']) + 1

#             #linearity
#             year_rets['log'] = np.log(year_rets['eq'])
#             x = np.arange(year_rets.index.size)
#             fit = np.polyfit(x, year_rets['log'], deg=1)
#             fit_function = np.poly1d(fit)
#             linearity_val = np.sqrt(((year_rets['log'] - fit_function(x))**2).mean())
#             cagr_val = qs.stats.cagr(year_rets['eq'].pct_change().fillna(0), rf=rfr, periods=year_rets.shape[0], compounded=True)
#             new_sharpe_val = cagr_val/linearity_val

#             sort.append(sortino)
#             draw.append(dd)
#             equi.append(year_eq)
#             news.append(new_sharpe_val)

#             data = data.append({'New Sharpe': round(new_sharpe_val, 2),
#                                 'Equity': round(year_eq, 2),
#                                 'MDD': round(dd, 2), 
#                                 'Sortino': round(sortino, 2),
#                                 'year': year}, ignore_index=True)

#         sort_avg = sum(sort)/len(sort)
#         draw_avg = sum(draw)/len(draw)
#         equi_avg = sum(equi)/len(equi)
#         new_sharpe_avg = sum(news)/len(news)

#         data = data.append({'year': 'AVG', 
#                             'Sortino': sort_avg, 
#                             'MDD': draw_avg, 
#                             'Equity': equi_avg, 
#                             'New Sharpe': new_sharpe_avg}, 
#                         ignore_index=True)

#         data.set_index('year', inplace=True)
#         data = data[['Equity', 'MDD', 'Sortino', 'New Sharpe']]
#         return data

#     def plot_equity(equity):
#         _, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [1, 3]},
#                             sharex=True, figsize=(5.5*(16/9), 5.5))
#         running_max = np.maximum.accumulate(equity)
#         drawdown = -100 * ((running_max - equity) / running_max)
#         ax[0].fill_between(equity.index, 0, drawdown, alpha=0.3, color='r')
#         ax[0].set_title('Drawdown')
#         ax[1].plot(equity)
#         ax[1].set_title('Total Balance')
#         return ax


#     from laetitudebots.util.talib_method import talib_method
#     from laetitudebots.util.custom_method import custom_method
#     from laetitudebots.backtest import Backtest

#     def initialize(factors, config):

#         length = int(config['parameters']['length'])
#         mult = float(config['parameters']['mult'])
#     #     trail_perc = float(config['parameters']['trail_perc'])
#     #     print(trail_perc)
        
#         factors['high'] = factors.high #.fillna(0).ffill()
#         factors['low'] = factors.low #.fillna(0).ffill()
#         factors['close'] = factors.close #.fillna(0).ffill()

#         factors['atr'] = talib_method(method='ATR', timeperiod=1, args=[factors.high, factors.low, factors.close])
#         factors['avg_tr'] = talib_method(method='WMA', timeperiod=length, args=[factors.atr])
#         factors['highest'] = factors.high.rolling(length).max()
#         factors['lowest'] = factors.low.rolling(length).min()
#         factors['hi_lim'] = (factors.highest.shift(1) - (factors.avg_tr.shift(1) * mult)).fillna(0)
#         factors['lo_lim'] = (factors.lowest.shift(1) + (factors.avg_tr.shift(1) * mult)).fillna(0)
#         factors['mid'] = ((factors.hi_lim + factors.lo_lim)/2).fillna(0)

#     def process(window, assets, context):

#         total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
#         total_balance = total_unrealised_pnl + context['balance'] 

#         ### VAR pos sizing
#         risk = config['context']['parameters']['risk']
        
#         for symbol, asset in assets.items():
            
#             prev_bar = window.previous(symbol, 2)
#             last_bar = window.latest(symbol)
            
#             settings = context['assets'][symbol]
            
#             ### Trailing Stop
#             trail = settings['trail']
            
#             position = asset.position
        
#             #entry conditions
#             lg_sig = (
#                         ((max(prev_bar.hi_lim, prev_bar.lo_lim) > prev_bar.close) &
#                         (max(last_bar.hi_lim, last_bar.lo_lim) < last_bar.close))
#                         )

#             st_sig = (
#                         ((min(prev_bar.hi_lim, prev_bar.lo_lim) < prev_bar.close) &
#                         (min(last_bar.hi_lim, last_bar.lo_lim) > last_bar.close))
#                         )
            
#             if lg_sig:
#                 stop = (min(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
#             elif st_sig:
#                 stop = (max(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
#             else:
#                 stop = 0
                
#             size = calculate_size('ftx', symbol, total_balance, last_bar.close, settings['allocation'], 
#                                 asset.get_contract_rate(last_bar.close), 
#                                 with_var=True, stop=stop, risk=context['parameters']['risk'])
            
#             date = pd.to_datetime(last_bar.timestamp)
#             month = date.month
#             year = date.year
            
            
#     #         if lg_sig or st_sig:
#     #             if (month==4) and (year==2022):
#     #                 print('APRIL')
#     #                 dist = (abs(last_bar.close-stop)/last_bar.close)*100
#     #                 print('dist', dist)
#     #                 if dist > config['context']['assets'][symbol]['distance']:
#     #                     config['context']['assets'][symbol]['distance'] = dist
#     #                     print(settings['distance'])
#     #                     config['context']['assets'][symbol]['dist_date'] = last_bar.timestamp
#     #                     print(settings['dist_date'])
#     #                     print(last_bar.timestamp)
#     #                     config['context']['assets'][symbol]['close'] = last_bar.close
#     #                     config['context']['assets'][symbol]['stop'] = stop
#     #                     config['context']['assets'][symbol]['cont_rate'] = asset.get_contract_rate(last_bar.close)
                    
#     #             print('**********')
#     #             print(symbol)
#     #             print(last_bar.timestamp)
#     #             print(f'close {last_bar.close}')
#     #             print(f'stop {stop}')
#     #             print(f'distance {(abs(last_bar.close-stop)/last_bar.close)*100}')
#     #             print(f'contract rate {asset.get_contract_rate(last_bar.close)}')
#     #             print('**********')

#     # for var
#     #         'distance': 0,
#     #         'dist_date': '',
#     #         'close': 0,
#     #         'stop': 0,
#     #         'cont_rate': 0
            
            
#             trail_perc = context['parameters']['trail_perc']
            
#             if position.size == 0:
#                 cancel_order('rider', asset, 'long_stop', 'long_trail', 'short_stop', 'short_trail')
#                 if lg_sig:                   
#                     order = proc_order('market', size, 'buy')

#                     ### long sl
#     #                 order_sl = proc_order('stop', size, 'sell', trigger=stop)

#                     ### Trail adjustment
#                     settings['trail'] = stop

#                     execute_order('rider', asset, long=order) #, long_stop=order_sl)
                                    
#                 elif st_sig:
#                     order = proc_order('market', size, 'sell')
#     #                 order_sl = proc_order('stop', size, 'buy', trigger=stop)

#                     settings['trail'] = stop

#                     execute_order('rider', asset, short=order) #, short_stop=order_sl)
            
#     #                 print(symbol, asset.get_contract_rate(last_bar.close))

#             elif position.size > 0:
#                 if st_sig:
#     #                 cancel_order('rider', asset, 'long_stop', 'long_trail')
                    
#                     order = proc_order('market', size + abs(position.size), 'sell')
#     #                 order_sl = proc_order('stop', size, 'buy', trigger=stop)
                    
#                     settings['trail'] = stop
                    
#                     execute_order('rider', asset, short=order) #, short_stop=order_sl)

#                 else:
#                     if last_bar.close < settings['trail']:
#                         order = proc_order('market', abs(position.size), 'sell')
#                         execute_order('rider', asset, long_stop=order)
                    
#                     else:
#                         trail_stop = trail_comp(position.size, trail_perc, last_bar.low, settings['trail'])
#                         if trail_stop > settings['trail']:
#                             settings['trail'] = trail_stop
            
#             elif position.size < 0:
#                 if lg_sig:
                    
#                     ### reverse to long
#                     order = proc_order('market', size + abs(position.size), 'buy')
                    
#                     settings['trail'] = stop
                    
#                     execute_order('rider', asset, long=order)                

#                 else:
#                     if last_bar.close > settings['trail']:
#                         order = proc_order('market', abs(position.size), 'buy')
#                         execute_order('rider', asset, short_stop=order)
#                     else:
#                         trail_stop = trail_comp(position.size, trail_perc, last_bar.high, settings['trail'])
#                         if trail_stop < settings['trail']:
#                             settings['trail'] = trail_stop

#     bt = Backtest(config)
#     bt.set_strategy(initialize=initialize, process=process)
#     bt.run(progressbar=False)



#     bt = Backtest(config)
#     bt.set_strategy(initialize=initialize, process=process)
#     bt.run(progressbar=False)

#     bt.positions.loc['2021-05-28':].to_csv("FTX_TT6V3USD-positions.csv")

# tt6v3usd_run()
#FTX TT6V3USD END
####################################################################
#FTX TT7V3USD START

strat_var = 1
risk = 0.01
trail_perc = 0.05
rrr = 10
pos_count_val = 5
slippage = 0.0005

slippage = 0.00025


config = {
    'context': {
        'is_shared_balance' : True,
        'balance' : 1,
        'assets': {},
        'parameters': {
#             'length': 390,
#             'mult': 3.5,
            # updated params 06.01.2022
            'length': 390,
            'mult': 4,
            'risk': risk,
            
            ###SL perc
            'sl_perc': 0.1,
            
            ###TS
            'trail_perc': trail_perc,
            
            ###TP 
                ### Bars
            'rrr': rrr, 
            
                ### RSI
            'rsi_len': 14,
            'rsi_ob': 70,
            'rsi_os': 30,
            
        },
    },
    'slippage': slippage,
    'time_interval': 15,
    'time_unit': 'm',   
    'start_date': '2019-01-01 00:00:00',
    'end_date': dt.datetime.now(),
    'exchange': 'binance',
    'window_length' : 2
    }

n = 2
pairs = {
    'BTC/USDT': ['binance', 1/n],
    'LINK/USDT': ['binance', 1/n] 
}

for i,v in pairs.items():
    if i.endswith('BTC'):
        maker_fee = -.0005
        taker_fee = 0.0025      
    else:
        maker_fee = .0002
        taker_fee = .0007
        
    config['context']['assets'][i] = {
        'exchange': v[0],
        'maker_fee': maker_fee,
        'taker_fee': taker_fee,
        'allocation' : v[1],
        'collateral': 'BTC',

        ###Take profit
        'last_pos': None,
        
        ###Trailing Stop
        'trail': 0,
        }

"""
Trailing stop

"""

def trail_comp(direction, percentage, price, current_trail):
    if direction > 0:
        trail_stop = price * (1-percentage)
        if price < trail_stop:
            trail_stop = current_trail
        elif (trail_stop < current_trail):
            trail_stop = current_trail
    elif direction < 0:
        trail_stop = price * (1+percentage)
        if price > trail_stop:
            trail_stop = current_trail
        elif (trail_stop > current_trail):
            trail_stop = current_trail
    return trail_stop


def trail_comp2(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    return trail_stop


def trail_comp3(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        elif last_close < prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
        
                
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        elif last_close > prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail 
        else:
            trail_stop = current_trail
    return trail_stop

def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
#             print('trigger price', order['trigger_price'])
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
#         print('execute orders', i, v)
        asset.create_order(f'{strategy_name}_{i}', v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f'{strategy_name}_{i}'):
#             print('cancel orders', i)
            asset.cancel_order(f'{strategy_name}_{i}')
    
"""fetch token precision"""
markets = ccxt.ftx().fetch_markets()
markets = [x for x in markets if x.get('id').endswith('-PERP')]
token_steps = {}
for market in markets:
    key = market.get('base')
    token_steps[key] = market.get('precision').get('amount')

def calculate_size(exchange, symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    if exchange == 'ftx':
        pos_size = total_balance * def_alloc
        orig_size = pos_size * contract_rate
        
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            
            size = min(orig_size, var_size)
            
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
            
        
        sym = symbol.split('/')[0]    
        
        try:
            precision = decimal.Decimal(str(token_steps.get(sym)))

            if precision == decimal.Decimal(1.0):
                precision = decimal.Decimal(0.0)

            size = str(size).split('.')[0] + '.' + str(size).split('.')[1][:abs(precision.as_tuple().exponent)]
            size = float(size)
        except Exception as e:
            pass
#             print(f'Precision error: {symbol}. Use unedited size.')
        
        return size
    
    if exchange == 'binance':
        min_size = {
            'btcusdt': 100,
            'ethusdt': 1,
            'xrpusdt': 1,
            'ltcusdt': 1,
            'adabtc': 10,
            'ethbtc': 0.01
        }
        # orig position size
        pos_size = total_balance * def_alloc
        if symbol.lower() == 'eth/btc':
            orig_size = round(pos_size * contract_rate, 2)
        else:
            orig_size = np.floor(pos_size * contract_rate)

        if symbol.lower().startswith('btc'):
            if orig_size < 100: 
                if orig_size % 100 >= 90:
                    orig_size = round(orig_size, -2)
                else: 
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 100)
        elif symbol.lower() == 'ada/btc':
            if orig_size < 10:
                if round(orig_size % 10, 2) >= 9:
                    orig_size = round(orig_size, -1)
                else:
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 10)
        #var pos size
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            if symbol.lower().startswith('btc'):
                if var_size < 100: 
                    if var_size % 100 >= 90:
                        var_size = round(var_size, -2)
                    else: 
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 100)
            else:
                if symbol.lower().endswith('usdt'):
                    if var_size < 1:
                        if round(var_size % 1, 2) >= 0.9:
                            var_size = round(var_size, 0)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
                else:
                    if symbol.lower() == 'ada/btc':
                        if var_size < 10:
                            if round(var_size % 10, 2) >= 9:
                                var_size = round(var_size, -1)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 10)
                    elif symbol.lower() == 'eth/btc':
                        if var_size < 1:
                            if round(var_size % 0.01, 3) >= 0.009:
                                var_size = round(var_size, 2)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 1)
            size = min(orig_size, var_size)
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
        return size

def get_win_rate(df, pairs):
    
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    
    pairs_dict = {}
    for pair in pairs:
        
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        try:
            win_pct = win_trades / total
            lose_pct = losing_trades / total
        except Exception as e:
            print(e)
            win_pct = 0
            lose_pct = 0
        
#         print(f'{pair} Won trades count: {win_trades}')
#         print(f'{pair} Lost trades count: {losing_trades}')
#         print(f'{pair} Total trades count: {total}')
#         print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
        
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
        
        
        
    return pairs_dict
    
    
def proc_wr(old_dict, get_win_rate_dict):
    
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        
        
        old_dict[pair] = new2
    
    return old_dict

def metrics(eq):
    # risk free rate
    rfr = 0.04
    eq = eq[eq.index.hour == 0]
    rets = eq.pct_change().fillna(0)

    years = list(rets.index.year.drop_duplicates())
    years.remove(2022)

    sort = []
    draw = []
    equi = []
    news = []

    data = pd.DataFrame(columns={'year', 'Sortino', 'MDD', 'Equity', 'New Sharpe'})

    for year in years:
        year = str(year)
        year_rets = rets[year]

        #sortino
        sortino = qs.stats.sortino(year_rets, rf= rfr, periods=365, annualize=True)

        #dd
        dd = qs.stats.max_drawdown(year_rets)*100

        year_rets = pd.DataFrame(year_rets, columns=['rets'])
        year_rets['eq'] = (1 + year_rets['rets']).cumprod()

        #equity
        year_eq = qs.stats.comp(year_rets['rets']) + 1

        #linearity
        year_rets['log'] = np.log(year_rets['eq'])
        x = np.arange(year_rets.index.size)
        fit = np.polyfit(x, year_rets['log'], deg=1)
        fit_function = np.poly1d(fit)
        linearity_val = np.sqrt(((year_rets['log'] - fit_function(x))**2).mean())
        cagr_val = qs.stats.cagr(year_rets['eq'].pct_change().fillna(0), rf=rfr, periods=year_rets.shape[0], compounded=True)
        new_sharpe_val = cagr_val/linearity_val

        sort.append(sortino)
        draw.append(dd)
        equi.append(year_eq)
        news.append(new_sharpe_val)

        data = data.append({'New Sharpe': round(new_sharpe_val, 2),
                            'Equity': round(year_eq, 2),
                            'MDD': round(dd, 2), 
                            'Sortino': round(sortino, 2),
                            'year': year}, ignore_index=True)

    sort_avg = sum(sort)/len(sort)
    draw_avg = sum(draw)/len(draw)
    equi_avg = sum(equi)/len(equi)
    new_sharpe_avg = sum(news)/len(news)

    data = data.append({'year': 'AVG', 
                        'Sortino': sort_avg, 
                        'MDD': draw_avg, 
                        'Equity': equi_avg, 
                        'New Sharpe': new_sharpe_avg}, 
                       ignore_index=True)

    data.set_index('year', inplace=True)
    data = data[['Equity', 'MDD', 'Sortino', 'New Sharpe']]
    return data

def plot_equity(equity):
    _, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [1, 3]},
                         sharex=True, figsize=(5.5*(16/9), 5.5))
    running_max = np.maximum.accumulate(equity)
    drawdown = -100 * ((running_max - equity) / running_max)
    ax[0].fill_between(equity.index, 0, drawdown, alpha=0.3, color='r')
    ax[0].set_title('Drawdown')
    ax[1].plot(equity)
    ax[1].set_title('Total Balance')
    return ax


from laetitudebots.util.talib_method import talib_method
from laetitudebots.util.custom_method import custom_method
from laetitudebots.backtest import Backtest

def initialize(factors, config):

    length = int(config['parameters']['length'])
    mult = float(config['parameters']['mult'])
    
    factors['high'] = factors.high #.fillna(0).ffill()
    factors['low'] = factors.low #.fillna(0).ffill()
    factors['close'] = factors.close #.fillna(0).ffill()

    factors['atr'] = talib_method(method='ATR', timeperiod=1, args=[factors.high, factors.low, factors.close])
    factors['avg_tr'] = talib_method(method='WMA', timeperiod=length, args=[factors.atr])
    factors['highest'] = factors.high.rolling(length).max()
    factors['lowest'] = factors.low.rolling(length).min()
    factors['hi_lim'] = (factors.highest.shift(1) - (factors.avg_tr.shift(1) * mult)).fillna(0)
    factors['lo_lim'] = (factors.lowest.shift(1) + (factors.avg_tr.shift(1) * mult)).fillna(0)
    factors['mid'] = ((factors.hi_lim + factors.lo_lim)/2).fillna(0)

def process(window, assets, context):

    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 

    ### VAR pos sizing
    risk = config['context']['parameters']['risk']
    
    for symbol, asset in assets.items():
        
        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)
        
        settings = context['assets'][symbol]
        
        ### Trailing Stop
        trail = settings['trail']
        
        position = asset.position
    
        #entry conditions
        lg_sig = (
                    ((max(prev_bar.hi_lim, prev_bar.lo_lim) > prev_bar.close) &
                     (max(last_bar.hi_lim, last_bar.lo_lim) < last_bar.close))
                    )

        st_sig = (
                    ((min(prev_bar.hi_lim, prev_bar.lo_lim) < prev_bar.close) &
                     (min(last_bar.hi_lim, last_bar.lo_lim) > last_bar.close))
                    )
        
        if lg_sig:
            stop = (min(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
        elif st_sig:
            stop = (max(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
        else:
            stop = 0
                   
        size = calculate_size('ftx', symbol, total_balance, last_bar.close, settings['allocation'], 
                              asset.get_contract_rate(last_bar.close, last_bar.collateral), 
                              with_var=True, stop=stop, risk=context['parameters']['risk'])
        
        date = pd.to_datetime(last_bar.timestamp)
        month = date.month
        year = date.year
        
        
#         if lg_sig or st_sig:
#             if (month==4) and (year==2022):
#                 print('APRIL')
#                 dist = (abs(last_bar.close-stop)/last_bar.close)*100
#                 print('dist', dist)
#                 if dist > config['context']['assets'][symbol]['distance']:
#                     config['context']['assets'][symbol]['distance'] = dist
#                     print(settings['distance'])
#                     config['context']['assets'][symbol]['dist_date'] = last_bar.timestamp
#                     print(settings['dist_date'])
#                     print(last_bar.timestamp)
#                     config['context']['assets'][symbol]['close'] = last_bar.close
#                     config['context']['assets'][symbol]['stop'] = stop
#                     config['context']['assets'][symbol]['cont_rate'] = asset.get_contract_rate(last_bar.close, last_bar.collateral)
                
#         if lg_sig or st_sig:
#             if (month == 5) and (year == 2022):
#                 print('**********')
#                 print(symbol)
#                 print(last_bar.timestamp)
#                 print(f'close {last_bar.close}')
#                 print(f'stop {stop}')
#                 print(f'distance {(abs(last_bar.close-stop)/last_bar.close)*100}')
#                 print(f'contract rate {asset.get_contract_rate(last_bar.close)}')
#                 print('**********')

# for var
#         'distance': 0,
#         'dist_date': '',
#         'close': 0,
#         'stop': 0,
#         'cont_rate': 0
        
        
        trail_perc = context['parameters']['trail_perc']

        if position.size == 0:
            cancel_order('rider', asset, 'long_stop', 'long_trail', 'short_stop', 'short_trail')
            if lg_sig:                   
                order = proc_order('market', size, 'buy')

                ### long sl
#                 order_sl = proc_order('stop', size, 'sell', trigger=stop)

                ### Trail adjustment
                settings['trail'] = stop

                execute_order('rider', asset, long=order) #, long_stop=order_sl)

            elif st_sig:
                order = proc_order('market', size, 'sell')
#                 order_sl = proc_order('stop', size, 'buy', trigger=stop)

                settings['trail'] = stop

                execute_order('rider', asset, short=order) #, short_stop=order_sl)

#                 print(symbol, asset.get_contract_rate(last_bar.close))

        elif position.size > 0:
            if st_sig:
#                 cancel_order('rider', asset, 'long_stop', 'long_trail')

                order = proc_order('market', size + abs(position.size), 'sell')
#                 order_sl = proc_order('stop', size, 'buy', trigger=stop)

                settings['trail'] = stop

                execute_order('rider', asset, short=order) #, short_stop=order_sl)

            else:
                if last_bar.close < settings['trail']:
                    order = proc_order('market', abs(position.size), 'sell')
                    execute_order('rider', asset, long_stop=order)

                else:
                    trail_stop = trail_comp(position.size, trail_perc, last_bar.low, settings['trail'])
                    if trail_stop > settings['trail']:
                        settings['trail'] = trail_stop

        elif position.size < 0:
            if lg_sig:

                ### reverse to long
                order = proc_order('market', size + abs(position.size), 'buy')

                settings['trail'] = stop

                execute_order('rider', asset, long=order)                

            else:
                if last_bar.close > settings['trail']:
                    order = proc_order('market', abs(position.size), 'buy')
                    execute_order('rider', asset, short_stop=order)
                else:
                    trail_stop = trail_comp(position.size, trail_perc, last_bar.high, settings['trail'])
                    if trail_stop < settings['trail']:
                        settings['trail'] = trail_stop

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)


bt.positions.loc['2021-05-28':].to_csv("FTX_TT7V3USD-positions.csv")
#ftx TT7V3USD END
####################################################################
#FTX TT8V3USD START

strat_var = 1
risk = 0.02
trail_perc = 0.05
rrr = 10
pos_count_val = 5
slippage = 0.0005

slippage = 0.00025


slippage = 0.00025


config = {
    'context': {
        'is_shared_balance' : True,
        'balance' : 1,
        'assets': {},
        'parameters': {
#             'length': 170,
#             'mult': 2,
            #update params 06.01.22
            'length': 160,
            'mult': 2,
            'risk': risk,
            
            ###SL perc
            'sl_perc': 0.1,
            
            ###TS
            'trail_perc': trail_perc,
            
            ###TP 
                ### Bars
            'rrr': rrr, 
            
                ### RSI
            'rsi_len': 14,
            'rsi_ob': 70,
            'rsi_os': 30,
            
        },
    },
    'slippage': slippage,
    'time_interval': 1,
    'time_unit': 'h',   
    'start_date': '2019-01-01 00:00:00',    
    'end_date': dt.datetime.now(),
    'exchange': 'binance',
    'window_length' : 2
    }


n = 5
pairs = {
    'MATIC/USDT': ['binance', 1/n],
    'LINK/USDT': ['binance', 1/n],
    'XRP/USDT': ['binance', 1/n],
    'DOGE/USDT': ['binance', 1/n],
    'OMG/USDT': ['binance', 1/n],
}

for i,v in pairs.items():
    if i.endswith('BTC'):
        maker_fee = -.0005
        taker_fee = 0.0025      
    else:
        maker_fee = .0002
        taker_fee = .0007
        
    config['context']['assets'][i] = {
        'exchange': v[0],
        'maker_fee': maker_fee,
        'taker_fee': taker_fee,
        'allocation' : v[1],
        'collateral': 'BTC',

        ###Take profit
        'last_pos': None,
        
        ###Trailing Stop
        'trail': 0,
        }

"""
Trailing stop

"""

def trail_comp(direction, percentage, price, current_trail):
    if direction > 0:
        trail_stop = price * (1-percentage)
        if price < trail_stop:
            trail_stop = current_trail
        elif (trail_stop < current_trail):
            trail_stop = current_trail
    elif direction < 0:
        trail_stop = price * (1+percentage)
        if price > trail_stop:
            trail_stop = current_trail
        elif (trail_stop > current_trail):
            trail_stop = current_trail
    return trail_stop


def trail_comp2(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
    return trail_stop


def trail_comp3(direction, percentage, last_close, prev_close, current_trail):
    
    if direction > 0:
        if last_close > prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        elif last_close < prev_close:
            trail_stop = current_trail * (1+percentage)
            if last_close < trail_stop:
                trail_stop = current_trail
        else:
            trail_stop = current_trail
        
                
    elif direction < 0:
        if last_close < prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail
        elif last_close > prev_close:
            trail_stop = current_trail * (1-percentage)
            if last_close > trail_stop:
                trail_stop = current_trail 
        else:
            trail_stop = current_trail
    return trail_stop

def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
#             print('trigger price', order['trigger_price'])
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
#         print('execute orders', i, v)
        asset.create_order(f'{strategy_name}_{i}', v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f'{strategy_name}_{i}'):
#             print('cancel orders', i)
            asset.cancel_order(f'{strategy_name}_{i}')
    
"""fetch token precision"""
markets = ccxt.ftx().fetch_markets()
markets = [x for x in markets if x.get('id').endswith('-PERP')]
for x in markets:
    print(x.get('id'))

token_steps = {}
for market in markets:
    key = market.get('base')
    token_steps[key] = market.get('precision').get('amount')

def calculate_size(exchange, symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    if exchange == 'ftx':
        pos_size = total_balance * def_alloc
        orig_size = pos_size * contract_rate
        
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            
            size = min(orig_size, var_size)
            
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
            
        
        sym = symbol.split('/')[0]    
        
        try:
            precision = decimal.Decimal(str(token_steps.get(sym)))

            if precision == decimal.Decimal(1.0):
                precision = decimal.Decimal(0.0)

            size = str(size).split('.')[0] + '.' + str(size).split('.')[1][:abs(precision.as_tuple().exponent)]
            size = float(size)
        except Exception as e:
            pass
#             print(f'Precision error: {symbol}. Use unedited size.')
        
        return size
    
    if exchange == 'binance':
        min_size = {
            'btcusdt': 100,
            'ethusdt': 1,
            'xrpusdt': 1,
            'ltcusdt': 1,
            'adabtc': 10,
            'ethbtc': 0.01
        }
        # orig position size
        pos_size = total_balance * def_alloc
        if symbol.lower() == 'eth/btc':
            orig_size = round(pos_size * contract_rate, 2)
        else:
            orig_size = np.floor(pos_size * contract_rate)

        if symbol.lower().startswith('btc'):
            if orig_size < 100: 
                if orig_size % 100 >= 90:
                    orig_size = round(orig_size, -2)
                else: 
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 100)
        elif symbol.lower() == 'ada/btc':
            if orig_size < 10:
                if round(orig_size % 10, 2) >= 9:
                    orig_size = round(orig_size, -1)
                else:
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 10)
        #var pos size
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            if symbol.lower().startswith('btc'):
                if var_size < 100: 
                    if var_size % 100 >= 90:
                        var_size = round(var_size, -2)
                    else: 
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 100)
            else:
                if symbol.lower().endswith('usdt'):
                    if var_size < 1:
                        if round(var_size % 1, 2) >= 0.9:
                            var_size = round(var_size, 0)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
                else:
                    if symbol.lower() == 'ada/btc':
                        if var_size < 10:
                            if round(var_size % 10, 2) >= 9:
                                var_size = round(var_size, -1)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 10)
                    elif symbol.lower() == 'eth/btc':
                        if var_size < 1:
                            if round(var_size % 0.01, 3) >= 0.009:
                                var_size = round(var_size, 2)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 1)
            size = min(orig_size, var_size)
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
        return size

def get_win_rate(df, pairs):
    
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    
    pairs_dict = {}
    for pair in pairs:
        
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        try:
            win_pct = win_trades / total
            lose_pct = losing_trades / total
        except Exception as e:
            print(e)
            win_pct = 0
            lose_pct = 0
        
#         print(f'{pair} Won trades count: {win_trades}')
#         print(f'{pair} Lost trades count: {losing_trades}')
#         print(f'{pair} Total trades count: {total}')
#         print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
        
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
        
        
        
    return pairs_dict
    
    
def proc_wr(old_dict, get_win_rate_dict):
    
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        
        
        old_dict[pair] = new2
    
    return old_dict

def metrics(eq):
    # risk free rate
    rfr = 0.04
    eq = eq[eq.index.hour == 0]
    rets = eq.pct_change().fillna(0)

    years = list(rets.index.year.drop_duplicates())
    years.remove(2022)

    sort = []
    draw = []
    equi = []
    news = []

    data = pd.DataFrame(columns={'year', 'Sortino', 'MDD', 'Equity', 'New Sharpe'})

    for year in years:
        year = str(year)
        year_rets = rets[year]

        #sortino
        sortino = qs.stats.sortino(year_rets, rf= rfr, periods=365, annualize=True)

        #dd
        dd = qs.stats.max_drawdown(year_rets)*100

        year_rets = pd.DataFrame(year_rets, columns=['rets'])
        year_rets['eq'] = (1 + year_rets['rets']).cumprod()

        #equity
        year_eq = qs.stats.comp(year_rets['rets']) + 1

        #linearity
        year_rets['log'] = np.log(year_rets['eq'])
        x = np.arange(year_rets.index.size)
        fit = np.polyfit(x, year_rets['log'], deg=1)
        fit_function = np.poly1d(fit)
        linearity_val = np.sqrt(((year_rets['log'] - fit_function(x))**2).mean())
        cagr_val = qs.stats.cagr(year_rets['eq'].pct_change().fillna(0), rf=rfr, periods=year_rets.shape[0], compounded=True)
        new_sharpe_val = cagr_val/linearity_val

        sort.append(sortino)
        draw.append(dd)
        equi.append(year_eq)
        news.append(new_sharpe_val)

        data = data.append({'New Sharpe': round(new_sharpe_val, 2),
                            'Equity': round(year_eq, 2),
                            'MDD': round(dd, 2), 
                            'Sortino': round(sortino, 2),
                            'year': year}, ignore_index=True)

    sort_avg = sum(sort)/len(sort)
    draw_avg = sum(draw)/len(draw)
    equi_avg = sum(equi)/len(equi)
    new_sharpe_avg = sum(news)/len(news)

    data = data.append({'year': 'AVG', 
                        'Sortino': sort_avg, 
                        'MDD': draw_avg, 
                        'Equity': equi_avg, 
                        'New Sharpe': new_sharpe_avg}, 
                       ignore_index=True)

    data.set_index('year', inplace=True)
    data = data[['Equity', 'MDD', 'Sortino', 'New Sharpe']]
    return data

def plot_equity(equity):
    _, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [1, 3]},
                         sharex=True, figsize=(5.5*(16/9), 5.5))
    running_max = np.maximum.accumulate(equity)
    drawdown = -100 * ((running_max - equity) / running_max)
    ax[0].fill_between(equity.index, 0, drawdown, alpha=0.3, color='r')
    ax[0].set_title('Drawdown')
    ax[1].plot(equity)
    ax[1].set_title('Total Balance')
    return ax


from laetitudebots.util.talib_method import talib_method
from laetitudebots.util.custom_method import custom_method
from laetitudebots.backtest import Backtest

def initialize(factors, config):

    length = int(config['parameters']['length'])
    mult = float(config['parameters']['mult'])
#     trail_perc = float(config['parameters']['trail_perc'])
#     print(trail_perc)
    
    factors['high'] = factors.high #.fillna(0).ffill()
    factors['low'] = factors.low #.fillna(0).ffill()
    factors['close'] = factors.close #.fillna(0).ffill()

    factors['atr'] = talib_method(method='ATR', timeperiod=1, args=[factors.high, factors.low, factors.close])
    factors['avg_tr'] = talib_method(method='WMA', timeperiod=length, args=[factors.atr])
    factors['highest'] = factors.high.rolling(length).max()
    factors['lowest'] = factors.low.rolling(length).min()
    factors['hi_lim'] = (factors.highest.shift(1) - (factors.avg_tr.shift(1) * mult)).fillna(0)
    factors['lo_lim'] = (factors.lowest.shift(1) + (factors.avg_tr.shift(1) * mult)).fillna(0)
    factors['mid'] = ((factors.hi_lim + factors.lo_lim)/2).fillna(0)

def process(window, assets, context):

    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance'] 

    ### VAR pos sizing
    risk = config['context']['parameters']['risk']
    
    for symbol, asset in assets.items():
        
        prev_bar = window.previous(symbol, 2)
        last_bar = window.latest(symbol)
        
        settings = context['assets'][symbol]
        
        ### Trailing Stop
        trail = settings['trail']
        
        position = asset.position
    
        #entry conditions
        lg_sig = (
                    ((max(prev_bar.hi_lim, prev_bar.lo_lim) > prev_bar.close) &
                     (max(last_bar.hi_lim, last_bar.lo_lim) < last_bar.close))
                    )

        st_sig = (
                    ((min(prev_bar.hi_lim, prev_bar.lo_lim) < prev_bar.close) &
                     (min(last_bar.hi_lim, last_bar.lo_lim) > last_bar.close))
                    )
        
        if lg_sig:
            stop = (min(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
        elif st_sig:
            stop = (max(last_bar.hi_lim, last_bar.lo_lim) + last_bar.mid)/2
        else:
            stop = 0
               
        size = calculate_size('ftx', symbol, total_balance, last_bar.close, settings['allocation'], 
                              asset.get_contract_rate(last_bar.close, last_bar.collateral), 
                              with_var=True, stop=stop, risk=context['parameters']['risk'])
        
        date = pd.to_datetime(last_bar.timestamp)
        month = date.month
        year = date.year
        
        
#         if lg_sig or st_sig:
#             if (month==4) and (year==2022):
#                 print('APRIL')
#                 dist = (abs(last_bar.close-stop)/last_bar.close)*100
#                 print('dist', dist)
#                 if dist > config['context']['assets'][symbol]['distance']:
#                     config['context']['assets'][symbol]['distance'] = dist
#                     print(settings['distance'])
#                     config['context']['assets'][symbol]['dist_date'] = last_bar.timestamp
#                     print(settings['dist_date'])
#                     print(last_bar.timestamp)
#                     config['context']['assets'][symbol]['close'] = last_bar.close
#                     config['context']['assets'][symbol]['stop'] = stop
#                     config['context']['assets'][symbol]['cont_rate'] = asset.get_contract_rate(last_bar.close, last_bar.collateral)
                
#         if lg_sig or st_sig:
#             if (month == 5) and (year == 2022):
#                 print('**********')
#                 print(symbol)
#                 print(last_bar.timestamp)
#                 print(f'close {last_bar.close}')
#                 print(f'stop {stop}')
#                 print(f'distance {(abs(last_bar.close-stop)/last_bar.close)*100}')
#                 print(f'contract rate {asset.get_contract_rate(last_bar.close)}')
#                 print(f'total bal: {total_balance}')
#                 print(f'alloc: {settings["allocation"]}')
#                 print(f"risk: {context['parameters']['risk']}")
#                 print('**********')


# for var
#         'distance': 0,
#         'dist_date': '',
#         'close': 0,
#         'stop': 0,
#         'cont_rate': 0
        
        
        trail_perc = context['parameters']['trail_perc']
        
        if position.size == 0:
            cancel_order('rider', asset, 'long_stop', 'long_trail', 'short_stop', 'short_trail')
            if lg_sig:                   
                order = proc_order('market', size, 'buy')

                ### long sl
#                 order_sl = proc_order('stop', size, 'sell', trigger=stop)

                ### Trail adjustment
                settings['trail'] = stop

                execute_order('rider', asset, long=order) #, long_stop=order_sl)
                                
            elif st_sig:
                order = proc_order('market', size, 'sell')
#                 order_sl = proc_order('stop', size, 'buy', trigger=stop)

                settings['trail'] = stop

                execute_order('rider', asset, short=order) #, short_stop=order_sl)
        
#                 print(symbol, asset.get_contract_rate(last_bar.close))

        elif position.size > 0:
            if st_sig:
#                 cancel_order('rider', asset, 'long_stop', 'long_trail')
                
                order = proc_order('market', size + abs(position.size), 'sell')
#                 order_sl = proc_order('stop', size, 'buy', trigger=stop)
                
                settings['trail'] = stop
                
                execute_order('rider', asset, short=order) #, short_stop=order_sl)

            else:
                if last_bar.close < settings['trail']:
                    order = proc_order('market', abs(position.size), 'sell')
                    execute_order('rider', asset, long_stop=order)
                
                else:
                    trail_stop = trail_comp(position.size, trail_perc, last_bar.low, settings['trail'])
                    if trail_stop > settings['trail']:
                        settings['trail'] = trail_stop
         
        elif position.size < 0:
            if lg_sig:
                
                ### reverse to long
                order = proc_order('market', size + abs(position.size), 'buy')
                
                settings['trail'] = stop
                
                execute_order('rider', asset, long=order)                

            else:
                if last_bar.close > settings['trail']:
                    order = proc_order('market', abs(position.size), 'buy')
                    execute_order('rider', asset, short_stop=order)
                else:
                    trail_stop = trail_comp(position.size, trail_perc, last_bar.high, settings['trail'])
                    if trail_stop < settings['trail']:
                        settings['trail'] = trail_stop

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

bt.positions.loc['2021-05-28':].to_csv("FTX_TT8V3USD-positions.csv")
#ftx TT8V3USD END
####################################################################
#ftx TDC START

def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
#             print('trigger price', order['trigger_price'])
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
#         print('execute orders', i, v)
        asset.create_order(f'{strategy_name}_{i}', v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f'{strategy_name}_{i}'):
#             print('cancel orders', i)
            asset.cancel_order(f'{strategy_name}_{i}')
    
"""fetch token precision"""
markets = ccxt.ftx().fetch_markets()
# tokens = ['ADA', 'BTC', 'ETC', 'ETH', 'LTC', 'XRP']
# tokens = ['BTC', 'ETH', 'LUNA', 'SOL', 'MATIC', 'AVAX', 'NEAR', 'DOT', 'XRP', 'ADA', 'LTC']
# tokens = ['BTC', 'ETH', 'FTM', 'ATOM', 'ADA', 'BNB', 'FTT', 'DOGE', 'XRP', 'LTC']
tokens = ['BTC',
         'ETH',
         'SOL',
         'FTT',
         'FTM',
         'BNB',
         'MATIC',
         'LINK',
         'XRP',
         'DOGE',
         'LTC',
         'TRX',
         'YFI',
         'OMG',
         'SXP',
         'RUNE',
         'BCH',
         'KNC',
         'LRC',
         'ENJ',
         'EUR',
         'BAT',
         'MKR',
         'COMP',
         'SNX',
         'CHR',
         'ETC',
         'ADA'
         ]

step = []
for token in tokens:
    step.extend([x['precision'].get('amount') for x in markets if x.get('id') == f'{token}-PERP'])

token_steps = dict(zip(tokens, step))

def calculate_size(exchange, symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    if exchange == 'ftx':
        pos_size = total_balance * def_alloc
        orig_size = pos_size * contract_rate
        
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            
            size = min(orig_size, var_size)
            
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
            
        
        sym = symbol.split('/')[0]    
        
        try:
            precision = decimal.Decimal(str(token_steps.get(sym)))

            if precision == decimal.Decimal(1.0):
                precision = decimal.Decimal(0.0)

            size = round(size, abs(precision.as_tuple().exponent))
        except Exception as e:
            pass
#             print(f'Precision error: {symbol}. Use unedited size.')
        
        return size
    
    if exchange == 'binance':
        min_size = {
            'btcusdt': 100,
            'ethusdt': 1,
            'xrpusdt': 1,
            'ltcusdt': 1,
            'adabtc': 10,
            'ethbtc': 0.01
        }
        # orig position size
        pos_size = total_balance * def_alloc
        if symbol.lower() == 'eth/btc':
            orig_size = round(pos_size * contract_rate, 2)
        else:
            orig_size = np.floor(pos_size * contract_rate)

        if symbol.lower().startswith('btc'):
            if orig_size < 100: 
                if orig_size % 100 >= 90:
                    orig_size = round(orig_size, -2)
                else: 
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 100)
        elif symbol.lower() == 'ada/btc':
            if orig_size < 10:
                if round(orig_size % 10, 2) >= 9:
                    orig_size = round(orig_size, -1)
                else:
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 10)
        #var pos size
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            if symbol.lower().startswith('btc'):
                if var_size < 100: 
                    if var_size % 100 >= 90:
                        var_size = round(var_size, -2)
                    else: 
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 100)
            else:
                if symbol.lower().endswith('usdt'):
                    if var_size < 1:
                        if round(var_size % 1, 2) >= 0.9:
                            var_size = round(var_size, 0)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
                else:
                    if symbol.lower() == 'ada/btc':
                        if var_size < 10:
                            if round(var_size % 10, 2) >= 9:
                                var_size = round(var_size, -1)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 10)
                    elif symbol.lower() == 'eth/btc':
                        if var_size < 1:
                            if round(var_size % 0.01, 3) >= 0.009:
                                var_size = round(var_size, 2)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 1)
            size = min(orig_size, var_size)
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
        return size

def get_win_rate(df, pairs):
    
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    
    pairs_dict = {}
    for pair in pairs:
        
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        try:
            win_pct = win_trades / total
            lose_pct = losing_trades / total
        except Exception as e:
            print(e)
            win_pct = 0
            lose_pct = 0
        
#         print(f'{pair} Won trades count: {win_trades}')
#         print(f'{pair} Lost trades count: {losing_trades}')
#         print(f'{pair} Total trades count: {total}')
#         print(f'{pair} Winning trades pct: {round(win_pct * 100, 2)}%')
        
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
        
        
        
    return pairs_dict
    
    
def proc_wr(old_dict, get_win_rate_dict):
    
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        
        
        old_dict[pair] = new2
    
    return old_dict

def metrics(eq):
    # risk free rate
    rfr = 0.04
    eq = eq[eq.index.hour == 0]
    rets = eq.pct_change().fillna(0)

    years = list(rets.index.year.drop_duplicates())

    sort = []
    draw = []
    equi = []
    news = []

    data = pd.DataFrame(columns={'year', 'Sortino', 'MDD', 'Equity', 'New Sharpe'})

    for year in years:
        year = str(year)
        year_rets = rets[year]

        #sortino
        sortino = qs.stats.sortino(year_rets, rf= rfr, periods=365, annualize=True)

        #dd
        dd = qs.stats.max_drawdown(year_rets)*100

        year_rets = pd.DataFrame(year_rets, columns=['rets'])
        year_rets['eq'] = (1 + year_rets['rets']).cumprod()

        #equity
        year_eq = qs.stats.comp(year_rets['rets']) + 1

        #linearity
        year_rets['log'] = np.log(year_rets['eq'])
        x = np.arange(year_rets.index.size)
        fit = np.polyfit(x, year_rets['log'], deg=1)
        fit_function = np.poly1d(fit)
        linearity_val = np.sqrt(((year_rets['log'] - fit_function(x))**2).mean())
        cagr_val = qs.stats.cagr(year_rets['eq'].pct_change().fillna(0), rf=rfr, periods=year_rets.shape[0], compounded=True)
        new_sharpe_val = cagr_val/linearity_val

        sort.append(sortino)
        draw.append(dd)
        equi.append(year_eq)
        news.append(new_sharpe_val)

        data = data.append({'New Sharpe': round(new_sharpe_val, 2),
                            'Equity': round(year_eq, 2),
                            'MDD': round(dd, 2), 
                            'Sortino': round(sortino, 2),
                            'year': year}, ignore_index=True)

#     sort_avg = round(sum(sort)/len(sort),2)
#     draw_avg = round(sum(draw)/len(draw),2)
#     equi_avg = round(sum(equi)/len(equi),2)
#     new_sharpe_avg = round(sum(news)/len(news), 2)

    sort_avg = sum(sort)/len(sort)
    draw_avg = sum(draw)/len(draw)
    equi_avg = sum(equi)/len(equi)
    new_sharpe_avg = sum(news)/len(news)

    data = data.append({'year': 'AVG', 
                        'Sortino': sort_avg, 
                        'MDD': draw_avg, 
                        'Equity': equi_avg, 
                        'New Sharpe': new_sharpe_avg}, 
                       ignore_index=True)

    data.set_index('year', inplace=True)
    data = data[['Equity', 'MDD', 'Sortino', 'New Sharpe']]
    return data

slippage = 0.0005


config = {
    'context': {
        'is_shared_balance' : True,
        'balance' : 1,
        'assets': {},
        'parameters': {
            'band': 24,
            'keltner': 19, 
            'per': 33,
            'risk': 0.01,
            
        },
    },
    'slippage': slippage,
    'time_interval': 8,
    'time_unit': 'h',   
    'start_date': '2020-01-01 00:00:00',    
    'end_date': dt.datetime.now(),
    'exchange': 'ftx',
    'window_length' : 2
    }


pairs = {

    'BTC/USDT': ['binance', 1/2],
    'ETH/USDT': ['binance', 1/2]

}

for i,v in pairs.items():
    if i.endswith('BTC'):
        maker_fee = -.0005
        taker_fee = 0.0025      
    else:
        maker_fee = -.00025
        taker_fee = 0.00075
        
    config['context']['assets'][i] = {
        'exchange': v[0],
        'maker_fee': maker_fee,
        'taker_fee': taker_fee,
        'allocation' : v[1],
        'collateral': 'BTC',

        ###Take profit
        'last_pos': None,

        }
    
from laetitudebots.util.talib_method import talib_method
from laetitudebots.util import custom_method
from laetitudebots.backtest import Backtest
import numpy as np
import talib

def initialize(factors, config):
    band = int(config['parameters']['band'])
    keltner = int(config['parameters']['keltner'])
    per = int(config['parameters']['per'])
    
    factors['high'] = factors.high.ffill()
    factors['low'] = factors.low.ffill()
    factors['close'] = factors.close.ffill()
    
    factors['psar']= custom_method(talib.SAR, [factors.high, factors.low, 0.02, 0.2])
    factors['bb']= talib_method(method='SMA', timeperiod=band, args = [factors.close])

    factors['keltner'] = talib_method(method='EMA', timeperiod = keltner, args = [factors.close])    
    factors['ma1'] = talib_method(method='EMA', timeperiod = per, args = [factors.close])
    factors['ma2'] = talib_method(method='EMA', timeperiod = per, args = [factors.ma1])

def process(window, assets, context):
    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance']
    
    for symbol, asset in assets.items():
        position = asset.position
        last = window.latest(symbol)
        settings = context['assets'][symbol]  
            
        keltner = last.keltner
        bb = last.bb
        ma2 = last.ma2
        close = last.close
        psar = last.psar
        
        lg_ent = (keltner > bb) and (ma2 < close > psar)
        st_ent = (keltner < bb) and (ma2 > close < psar)
        
        stop = 0
        
        size = calculate_size('ftx', symbol, total_balance, last.close, settings['allocation'], 
                              asset.get_contract_rate(last.close, last.collateral), 
                              with_var=False, stop=stop, risk=context['parameters']['risk'])
        
        if position.size <= 0 and lg_ent: 
            
            order = proc_order('market', (size + abs(position.size)), 'buy')
            execute_order('tdc', asset, long=order)
                    
        elif position.size >= 0 and st_ent:
            order = proc_order('market', (size + abs(position.size)), 'sell')
            execute_order('tdc', asset, short=order)

bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

bt.positions.loc['2021-05-28':].to_csv("FTX_TDC-positions.csv")
#ftx TDC END
####################################################################

####################################################################
####################################################################
####################################################################

bot_keys = {
    'BMX_NMM': {
        'apiKey' : 'ZfDq4NtLthdwWDtBHIgqIVGd',
        'apiSecret' : 'AsffkKkmhOdtaERVOjWVUVyQ9ztOy1fccv2w2n5ipkLz7_8X',
        'interval' : 'H',
        "exchange": "BMX",
        'enableRateLimit': True,
    },
    'BMX_Vortex': {
        'apiKey' : 'i0pX7CvS9-wfWBAUD2kK_YC7',
        'apiSecret' : '1Avz-Qnr3DNb_SZcpB1p0mh3Ke8UlM6jJEPY1XVdXyXidIew',
        'interval' : 'H',
        "exchange": "BMX",
        'enableRateLimit': True,
    },
    'BMX_PAYDAY': {
        'apiKey' : '5gUmklkTPAgVtuuuPlkKhOhA',
        'apiSecret' : 'kIRWz_Rhz24LAOtlyW4W8emvFZdp6JNihTjCRLfsHqb91KBR',
        'interval' : 'H',
        "exchange": "BMX",
        'enableRateLimit': True,
    },
    'BMX_TDC': {
        'apiKey' : 'HBtRgw3HJ45LOWJg2TFpuXpI',
        'apiSecret' : 'TSeSvgKdtU1S8JlWX2S2cIf9SlbzlnMHS_NcBjFwpkmIGm9s',
        'interval' : 'H',
        "exchange": "BMX",
        'enableRateLimit': True,
    },
    'BMX_DMIv1': {
        'apiKey' : 'JkGBprSOD-zLkJcD1-Ln2WlD',
        'apiSecret' : 'JJ6q97HaWMyqanA7haiJ6CQOd0TYJCKacyO8Uz2xEPFW_ZsI',
        'interval' : 'H',
        "exchange": "BMX",
        'enableRateLimit': True,
        },
    'BMX_MM3': {
        "apiKey": "gBzf3wLQ-VMtV02DfhdgW7vi",
        "apiSecret": "cuXCXlwFEer4MUNtkFKYzAUEsTKvLE3uNHp2Prg7OPy5iJtC",
        'interval' : 'H',
        "exchange": "BMX",
        "enableRateLimit": True
    },
    'BMX_TT1': {
        "apiKey": "LFFOpH_abc8c9orol9XZlLiN",
        "apiSecret": "MtAZqS_fco475yySvFR9WzDFZSXYXfBKYvvC2v9YS-nWge3B",
        'interval' : 'H',
        "exchange": "BMX",
        "enableRateLimit": True
    },
    "BMX_TT3": {
        "apiKey": "13PDsSEncwiF7vet7u80jvLO",
        "apiSecret": "bybFuFJA3_bcaT-CSe4-rnUjN9B8tGId2hnwA4l7Lbor06EN",
        'interval' : 'H',
        "exchange": "BMX",
        "enableRateLimit": True
    },
    "BMX_TT4": {
        "apiKey": "rXXoixuTtvOX2gkWrKV0FPFe",
        "apiSecret": "Ft14Kire2wLk4y_ax4McFWsVr19V8MYYHyaUS1hxoHbbIptD",
        'interval' : 'H',
        "exchange": "BMX",
        "enableRateLimit": True
    },
    "BMX_FN3": {
        "apiKey": "824CQ9KsWfN_Wr24MVwEJ2UB",
        "apiSecret": "thfdovJIzHmrJXMOfpR4A60Yz0MrQtvUdqIuLEY-wonuh7KW",
        'interval' : 'H',
        "exchange": "BMX",
        "enableRateLimit": True
    },
    "BMX_TS2": {
        "apiKey": "8yeDAl_Chh5cVWNMrxZN4DB6",
        "apiSecret": "Ns9_jKl1Nji8O6HZq6S0HHvcf0AVg0aOUJUZpex1tbeqX9QK",
        'interval' : 'H',
        "exchange": "BMX",
        "enableRateLimit": True
    },
    "BMX_TS2v3": {
        "apiKey": "3ORhm2gzlDvwJM7Rbr0GOXjT",
        "apiSecret": "fM2mFNlX7ZxC3UbvwEny0c2VgsoklQpLr_vEIZwAMW1WA_bL",
        'interval' : 'H',
        "exchange": "BMX",
        "enableRateLimit": True
    },
    "BMX_MM3v3": {
        "apiKey": "dN43nXhwV8RJOV6SafFHs1nq",
        "apiSecret": "C1GYh0u9wgI1KDnrLLxXD2dKxkUaE_43s4tOj2rrU4X55M1X",
        'interval' : 'H',
        "exchange": "BMX",
        "enableRateLimit": True
    },
    "BMX_TS2v2": {
        "apiKey": "tcWenKl_X4RCuNvDKkIjPT7W",
        "apiSecret": "Cp8v3XErWqo9Y7oEffZ2sZF-2VjH9D1MgboZxgbnS3qOK-gh",
        'interval' : 'H',
        "exchange": "BMX",
        "enableRateLimit": True
    },
    "BMX_MM3v2": {
        "apiKey": "3TWjvTDI-0ao2rkkmIvIfi0E",
        "apiSecret": "izA3S3PFL27DDGQ5R2FAvISjHnC_2a-YAqAACO-a2N1X3TsP",
        'interval' : 'H',
        "exchange": "BMX",
        "enableRateLimit": True
    },
    "BMX_TT4v2": {
        "apiKey": "e9yi0aDMBwf5bfdheWJo7Hpo",
        "apiSecret": "Trr1Hd3c5PG-gZl_UTKdcYSPLwQvO3mOfmRhXTFfP_DTwnAV",
        'interval' : 'H',
        "exchange": "BMX",
        "enableRateLimit": True
    },
    "BMX_TT1v2": {
        "apiKey": "lYiBu_RK3pddBdrsiAXPbKnv",
        "apiSecret": "r3cZTZw3it3TbjaX3Qjk1urtrqp_InFt3URTfwitb3lDzF3c",
        'interval' : 'H',
        "exchange": "BMX",
        "enableRateLimit": True
    },
    "BMX_FN3v2": {
        "apiKey": "7aFpL1utkZUULCkirgmgKRgY",
        "apiSecret": "Sd6z4jfxkCss7JZ0d8m1cHYLdt_Dy_sJ3JfCiytCVIaUjof3",
        'interval' : 'H',
        "exchange": "BMX",
        "enableRateLimit": True
    },
    "BMX_DMIv2"             :{
        'apiKey'            : 'Ptf8UNAbXB4WSOpytrPdUpk6',
        'apiSecret'         : 'PVJR0RyYK-ojnmmBIKelb1S9r3WMQqskr3QpU62IfHloLBDS',
        'interval'          : 'H',
        "exchange"          : "BMX",
        'enableRateLimit'   : True
    },
    'FTX_DMIv1'             : {
        'apiKey'            : 'ScH71g4GK-zTVs8SNhV08oscxRCGZ5Ph59kYZTjy',
        'apiSecret'         : 'ajDbPHwR100rsT0_W9VFlJjWXHyZWHuNMh23aWsP',
        'interval'          : 'H',
        'enableRateLimit'   : True,
        "exchange"          : "FTX",
        "headers"           :{'FTX-SUBACCOUNT': 'DMI Prod'}
    },
    'FTX_TS2V3USD'          : {
        'apiKey'            : '5GK2wySpNH1OQF_6efpKSSfvsk5TlS5db9nhj_VB',
        'apiSecret'         : 'QgBtc0QiMxQUJFZuDdTt2ms_gzkYA4VaimpUTkyk',
        'interval'          : 'H',
        'enableRateLimit'   : True,
        "exchange"          : "FTX",
        "headers"           :{'FTX-SUBACCOUNT': 'TS2V3USD'}
    },
    'FTX_TTR': {
        'apiKey' : 'c0tID8vynAoXyovkwXrteNnSzScZ9Vqm6XQijU5m',
        'apiSecret' : 'yNpwOwwO7xICcGsxKOxSd5CQXeT2RpnRrs8mBjlB',
        'interval' : 'H',
        'enableRateLimit': True,
        "exchange": "FTX",
        "headers":{'FTX-SUBACCOUNT': 'TrendTraderStrategyProdTest'}
    },
    'FTX_TT5V3USD': {
        'apiKey' : 'G49Q6F0bN4tYVX1K8-l7eQ0yZdiIWyQ4lvUUZr1j',
        'apiSecret' : 'oJiH3D65I7h0pUq7D8afClmvopRESFu0lvawMxf6',
        'interval' : 'H',
        'enableRateLimit': True,
        "exchange": "FTX",
        "headers":{'FTX-SUBACCOUNT': 'TT5v3Live'}
    },
    'FTX_TT6V3USD': {
        'apiKey' : 'sk4k3pHmnUcLoZDr8NtPSB8q73gTAuBBSGzpYdRu',
        'apiSecret' : 'T5ze8g2uu0773G39rFBOOcp1eaZh3HF4tuUPK1zo',
        'interval' : 'H',
        'enableRateLimit': True,
        "exchange": "FTX",
        "headers":{'FTX-SUBACCOUNT': 'TT6v3Live'}
    },
    'FTX_TT7V3USD': {
        'apiKey' : 'sarUBmIUXmaIdprTDHmMTwOGck2sgFC0SFUTbsFS',
        'apiSecret' : 'IXcIVbeQIeiaWJtSHccnz9T1ZUCJG5QgqP_ewRnY',
        'interval' : '15min',
        'enableRateLimit': True,
        "exchange": "FTX",
        "headers":{'FTX-SUBACCOUNT': 'TT7v3Live'}
    },
    'FTX_TT8V3USD': {
        'apiKey' : '8x2adH88dtQxeoeoCSpphKK1lFiMArW_klI6_Wo3',
        'apiSecret' : 'F1ckxLR3etEiVDi-z_TRyFCJMCTnci4XwkyJsm4j',
        'interval' : 'H',
        'enableRateLimit': True,
        "exchange": "FTX",
        "headers":{'FTX-SUBACCOUNT': 'TT8v3Live'}
    },
    'FTX_TDC': {
        'apiKey' : 'ByzE7MA90Fx7xWVq-3M0Uc84lMPHXL8LALjg7JhH',
        'apiSecret' : 'Tr5vcE7Wc4nugQQhuwL7f6fsRvfTB3k8ozz0vI2z',
        'interval' : 'H',
        'enableRateLimit': True,
        "exchange": "FTX",
        "headers":{'FTX-SUBACCOUNT': 'TDC Strategy Dev Test'}
    },
    'FTX_FLOCKLS': {
        'apiKey' : 'mW_NINLlh7nuxxNvqMjSzzxjmx5J7KclpWq7HEOs',
        'apiSecret' : 'NKCtIjCdHCHh-VFzkp9aDq99NSk3-WQa1hNd3rcD',
        'interval' : 'H',
        'enableRateLimit': True,
        "exchange": "FTX",
        "headers":{'FTX-SUBACCOUNT': 'FlockLS'}
    },

}

keys = pd.DataFrame.from_dict(bot_keys, orient = 'index')


def unixTimeMillis(time):
    epoch = dt.datetime.utcfromtimestamp(0)
    return (time - epoch).total_seconds() * 1000.0


def downloadOHLCV(duration, time_unit, symbol, from_datetime, to_datetime = dt.datetime.now()):
    
    if to_datetime > dt.datetime.now():
        to_datetime = dt.datetime.now()
        
    dur_ = str(duration)
    tu_ = str(time_unit)
    tf = dur_ + tu_

    bitmex = ccxt.bitmex({"enableRateLimit": True})
    from_timestamp = unixTimeMillis(from_datetime)
    to_timestamp = unixTimeMillis(to_datetime)

    if tu_ == "d":
        m1_len = dt.timedelta(days=duration).total_seconds() * 1000

    if tu_ == "h":
        m1_len = dt.timedelta(hours=duration).total_seconds() * 1000

    if tu_ == "m":
        m1_len = dt.timedelta(minutes=duration).total_seconds() * 1000
    
    btc = []
    try:
        while from_timestamp < to_timestamp:
            btc_candles = bitmex.fetch_ohlcv(symbol, tf, from_timestamp, limit=1000)
            last = btc_candles[-1][0]
            from_timestamp = last + m1_len
            btc += btc_candles
      
    except:
        while from_timestamp < to_timestamp:
            btc_candles = bitmex.fetch_ohlcv('ADAZ21', tf, from_timestamp, limit=1000, params={'symbol':symbol})
            last = btc_candles[-1][0]
            from_timestamp = last + m1_len
            btc += btc_candles
        

    data = pd.DataFrame(btc)
    data.columns = ["timestamp", "open", "high", "low", "close", "volume"]
    data["timestamp"] = pd.to_datetime(data.timestamp, unit="ms")
    data.set_index("timestamp", inplace=True)
    data = data.loc[:to_datetime]
    return data

def botClient(bot, exchange, keys):
    if exchange == 'BMX':
        client = ccxt.bitmex({'apiKey':keys.loc[bot, 'apiKey'], 
                              'secret':keys.loc[bot, 'apiSecret'],
                              'enableRateLimit':keys.loc[bot, 'enableRateLimit']
                              })
    elif exchange == 'FTX':
        client = ccxt.ftx({'apiKey':keys.loc[bot, 'apiKey'], 
                           'secret':keys.loc[bot, 'apiSecret'],
                           'enableRateLimit':keys.loc[bot, 'enableRateLimit'],
                           'headers' : keys.loc[bot, 'headers']
                           })
    return client


def fetchBotOrders(bot, lookback, keys):
    exchange = str(keys.loc[bot]['exchange']).upper()
    
    client = botClient(bot, exchange, keys)
    print('start')
    last = today - dt.timedelta(days = lookback)
    since = int(unixTimeMillis(last))
    orders_ = pd.DataFrame()
    if exchange == 'BMX':
        orders_ = pd.DataFrame(client.fetch_orders(since = since))
        # orders_ =  orders_[orders_['status'] == 'closed']
        
    elif exchange == 'FTX':
        orders_ = pd.DataFrame()
        print('start')
        for i in list(set([i[1] for i in pd.read_csv(f'{bot}-positions.csv', header = [0,1], index_col = 0).drop('balance', axis = 1).columns])):
            #__x = i.replace(i.partition('/')[2], 'PERP').replace('/','-')
            print('start1')
            __x = i.replace('-PERP','PERP')
            __x = __x.replace('PERP','-PERP')
            __x = __x.replace('/USDT','-PERP')
            __x = __x.replace('USD','-PERP')
            o_ = pd.DataFrame(client.fetch_orders(__x, since = since))
            print(o_)
            print('start3')
            # o_ =  o_[o_['status'] == 'closed']
            orders_ = pd.concat([orders_,o_])
        if not orders_.empty:
            orders_.set_index('datetime', inplace = True)
            orders_.sort_index(ascending = True, inplace = True)
            orders_.symbol = orders_.symbol.apply(lambda x: x.replace('-PERP', 'USD'))
    else:
        raise Exception

    if orders_.empty:
        return orders_

    live_orders = pd.DataFrame(orders_)

    live_orders['datetime'] = pd.to_datetime(live_orders['timestamp'], unit = 'ms')
    live_orders['side']     = live_orders.side.replace(['buy', 'sell'], [1,-1])
    live_orders['size']     = live_orders['side'] * live_orders['amount']
    live_orders             = live_orders.set_index('datetime')
    
    if exchange == 'BMX':
        live_orders['symbol'] = live_orders['info'].apply(lambda x: x['symbol'])
        live_orders['symbol'] = live_orders.symbol.apply(lambda x: re.sub('[A-Z][0-9][0-9]','BTC', x)).replace('XBTUSD', 'BTCUSD')

    live_orders         = live_orders[['symbol','size', 'average']]
    live_orders         = live_orders.pivot(columns = 'symbol', values = ['size', 'average'])
    
    live_orders.index   = live_orders.index.floor(keys.loc[bot]['interval'])
    
    live_orders.columns = live_orders.columns.swaplevel(0, 1)
    return live_orders
    
    
def fetchBacktesterPositions(bot):
    backtester          = pd.read_csv(f'{bot}-positions.csv', header = [0,1], index_col = 0).drop('balance', axis=1)
    backtester.index    = pd.to_datetime(backtester.index)
    backtester.columns  = pd.MultiIndex.from_tuples([(re.sub('avg_price','average', backtester.columns[i][0]), re.sub('USDT', 'USD', backtester.columns[i][1])) for i in range(len(backtester.columns))])
    backtester.columns  = pd.MultiIndex.from_tuples([(backtester.columns[i][0], ''.join(backtester.columns[i][1].split('/'))) for i in range(len(backtester.columns))])
    backtester          = backtester[['size', 'average']]
    basket              = list(dict.fromkeys([backtester.columns[i][1] for i in range(len(backtester.columns))]))
    basket              = [''.join(x.split('/')) for x in basket]
    backtester.columns  = backtester.columns.swaplevel(0, 1)

    return backtester, basket
    
    
def fetchBotPositions(bot, keys):
    exchange    = str(keys.loc[bot]['exchange']).upper()
    client      = botClient(bot, exchange, keys)
    df          = pd.DataFrame(client.fetch_positions())
    
    if df.empty:
        return df
    
    __sy,__ot,__cq,__ae,__io,__fu,__ns,__ep,__os = 'symbol','openingTimestamp','currentQty','avgEntryPrice','isOpen','future','netSize','entryPrice','openSize' 
            
    if exchange == 'BMX':
        df[__ot] = pd.to_datetime(df[__ot]).apply(lambda x: x.tz_convert(None))
        df.loc[:, [__cq,__ae]] = df.loc[:, [__cq,__ae]].astype('float')
        df          = df[[__sy,__ot,__cq,__ae,__io]].set_index(__ot)
        df[__sy]    = df[__sy].apply(lambda x: re.sub('[A-Z][0-9][0-9]','BTC', x)).replace('XBTUSD', 'BTCUSD')
        
    elif exchange == 'FTX':    
        df[__ot] = pd.Timestamp.now().floor('h')
        df.set_index(__ot, inplace = True)
        df          = df[[__fu,__ns,__ep,__os ]]
        df.columns  = [__sy,__cq,__ae,__io]
        df.loc[:, [__cq,__ae]] = df.loc[:, [__cq,__ae]].astype('float')
        df[__sy]   = df[__sy].apply(lambda x: x.replace('-PERP', 'USD'))
        df[__io]   = df[__io].apply(lambda x: float(x) != 0)
        
    else:
        raise Exception(f'{exchange} not supported')
        
    return df

#########################################
#########################################
#########################################

bots = [
    'FTX_FLOCKLS',
    'FTX_TDC',
    'FTX_TT8V3USD',
    'FTX_TT7V3USD',
    'FTX_TT6V3USD',
    'FTX_TT5V3USD',
    'BMX_NMM',
    'FTX_TS2V3USD',
    'FTX_TTR',
    'BMX_Vortex',
    'FTX_DMIv1',
    'BMX_DMIv1',
    'BMX_DMIv2',
    'BMX_FN3',
    'BMX_FN3v2', 
    'BMX_MM3',
    'BMX_MM3v2', 
    'BMX_MM3v3', 
    'BMX_PAYDAY',
    'BMX_TDC',
    'BMX_TS2',
    'BMX_TS2v3',
    'BMX_TS2v2',
    'BMX_TT1',
    'BMX_TT1v2', 
    'BMX_TT3',
    'BMX_TT4',
    'BMX_TT4v2',
    ]

epoch = dt.datetime.utcfromtimestamp(0)

current = dt.datetime.now()
today = pd.to_datetime(current.date())

current_hour = today + dt.timedelta(hours = current.hour) 

print('------------------------ positions report ---------------------')
lookback        = 30
positions_data  = {}
anomalies       = []
status_reports  = []
for bot in bots:
    errors              = 0
    exchange            = str(keys.loc[bot]['exchange']).upper()
    backtester, basket  = fetchBacktesterPositions(bot)
    last                = today - pd.DateOffset(lookback)
    curr_positions      = fetchBotPositions(bot, keys)
    live_bot            = fetchBotOrders(bot, lookback, keys)
    
    __lb,__bt,__sy,__cq,__ae,__io,__si,__dt,__av = 'live_bot','backtester','symbol','currentQty','avgEntryPrice','isOpen','size','datetime','average'
    for pair in basket:
        try:
            if curr_positions[curr_positions.symbol == pair].empty:
                raise Exception
            else:
                # COMPARE WITH BACKTESTER DATA
                position_comparison = pd.concat([curr_positions[curr_positions[__sy] == pair], backtester[pair].tail(1)], axis = 1).ffill()

        except (KeyError, AttributeError, Exception) as e:
            no_positions = pd.DataFrame(np.nan, index = [current_hour], columns = [__sy,__cq,__ae,__io])
            position_comparison = pd.concat([no_positions, backtester[pair].tail(1)]).sort_index().ffill()

        arrays = [list(np.concatenate([[__lb] * 4, [__bt] * 2])), list(np.concatenate([[__sy,__cq,__ae,__io], list(backtester[pair].columns.values)]))]
        
        tuples = list(zip(*arrays))
        position_comparison.columns = pd.MultiIndex.from_tuples(tuples)
        position_comparison = position_comparison.fillna(0)
        
        ### checks if backtest and live positions match
        for i in range(len(position_comparison)):
            if ((position_comparison[__lb][__cq].iloc[-1] > 0) & (position_comparison[__bt][__si].iloc[-1] > 0)):
                check_position = 'buy'
            elif ((position_comparison[__lb][__cq].iloc[-1] < 0) & (position_comparison[__bt][__si].iloc[-1] < 0)):
                check_position = 'sell'
            elif ((position_comparison[__lb][__cq].iloc[-1] == 0) & (position_comparison[__bt][__si].iloc[-1] == 0)):
                check_position = ' '
            else:
                check_position = 'mismatch'
        position_comparison['checks'] = np.nan
        position_comparison.loc[position_comparison.index[-1], 'checks'] = check_position

        print('Datetime opened:')
        if position_comparison[__lb].iloc[-1,3] == True:
            # if open
            date_opened = pd.DataFrame(columns = [__dt,__si,__av])
            try:
                date_opened = fetchBotOrders(bot, 60, keys)[pair].dropna()
            except KeyError:
                if date_opened.dropna().empty:
                    date_opened.loc['0', __dt] =  pd.to_datetime(dt.datetime.now().replace(tzinfo = None).date()) + dt.timedelta(hours = 0)
                    date_opened.set_index(__dt, inplace = True)
                    
        else:
            date_opened = pd.DataFrame(columns = [__dt,__si,__av])
            date_opened.loc['0', __dt] =  pd.to_datetime(dt.datetime.now().replace(tzinfo = None).date()) + dt.timedelta(hours = 0)
            date_opened.set_index(__dt, inplace = True)
        
        status_reports.append('-----------------------------------------------------------------------------------------')
        
        if position_comparison[position_comparison.checks == 'mismatch'].empty:
            status_reports.append(f'0 mismatch in {bot}-{pair}')
            conclusion = f'None'
            
        else:
            status_reports.append(f'Mismatch in {bot}-{pair} positions')
            conclusion = f'Mismatch'
            status_reports.append(position_comparison.to_string())
            anomalies.append(f'positionAnomaly-{bot}-{pair}.csv')
            errors +=1
            status_reports.append('Datetime opened:')
        
        positions_data[f'{bot}-{pair}'] = {'comparison': position_comparison.to_dict(), 'opened': date_opened.tail(1).to_dict(), 'position mismatch': conclusion}
        

print('------------------------ orders report ---------------------')
lookback        = 3 
orders_data     = {}       
anomalies       = []
status_reports  = []

for bot in bots:
    live_bot            = fetchBotOrders(bot, lookback, keys)
    last                = today - pd.DateOffset(lookback)
    backtester, basket  = fetchBacktesterPositions(bot)
    curr_positions      = fetchBotPositions(bot, keys)
    errors              = 0
    
    for pair in basket:
        try:
            order_comparison        = pd.concat([live_bot[pair].dropna(), backtester[pair][__si].diff(), backtester[pair][__av]], axis = 1)
        
        except KeyError:
            no_trades               = pd.DataFrame(np.nan, index = pd.date_range(last, today), columns = [__si,__av])
            order_comparison        = pd.concat([no_trades, backtester[pair][__si].diff(), backtester[pair][__av]], axis = 1)


        order_comparison            = order_comparison.reset_index().rename(columns = {'index':f'{bot}-{pair}'}).set_index(f'{bot}-{pair}')
        order_comparison.columns    = pd.MultiIndex.from_product([[__lb,__bt], order_comparison.columns[0:2]])
        order_comparison            = order_comparison.fillna(0).loc[last:(today + dt.timedelta(hours = 9)).strftime('%Y-%m-%d %H:%M:%S')]

        order_checks = []
        for i in range(len(order_comparison)):
            
            if (order_comparison[__lb][__si].iloc[i] > 0) & (order_comparison[__bt][__si].iloc[i] > 0):
                check_order = 'buy'
            elif (order_comparison[__lb][__si].iloc[i] < 0) & (order_comparison[__bt][__si].iloc[i] < 0):
                check_order = 'sell'
            elif (order_comparison[__lb][__si].iloc[i] == 0) & (order_comparison[__bt][__si].iloc[i] == 0):
                check_order = ' '
            else:
                check_order = 'mismatch'
                
            order_checks.append(check_order)

        order_comparison['checks'] = order_checks

        if order_comparison[order_comparison.checks == 'mismatch'].empty:
            status_reports.append(f'All orders match for {bot}-{pair}')
            conclusion = 'None'
        else:
            status_reports.append(f'Anomaly found in {bot}-{pair} orders')
            status_reports.append(order_comparison.to_string())
            anomalies.append(f'OrderAnomaly-{bot}-{pair}.csv')
            conclusion = 'Mismatch'
            errors += 1
    
        orders_data[f'{bot}-{pair}'] = {'comparison': order_comparison.to_dict(), 'order mismatch': conclusion}
    
    status_reports.append('-----------------------------------------------------------------------------')

__var = pd.DataFrame(positions_data)
for i in __var.index[:-1]:
    kkk = pd.DataFrame()
    with pd.ExcelWriter(f"recent_{i}_position.xlsx") as writer:         
        for j in __var.columns:
            try:
                if __bot != str(j).partition("-")[0]:
                    kkk = pd.DataFrame()
            except:
                print(j)
            __bot = str(j).partition("-")[0]
            try:
                print(__bot)
                xxx = pd.DataFrame(__var.loc[i][j])
                xxx["label"] = j
                xxx["pht"]  = xxx.index
                xxx.pht  = xxx.pht.apply(lambda x: x.tz_localize("UTC").tz_convert("Asia/Manila").tz_localize(None))
                ccc = list(xxx.columns)
                ccc = [ccc[-1]] + ccc[:-1]
                xxx = xxx[ccc]
                kkk = kkk.append(xxx)
                kkk.to_excel(writer, sheet_name = f"{__bot}")
            except:
                print(f"skipping positions {i} {j}")

conc = pd.DataFrame(__var.loc["position mismatch"])

__var = pd.DataFrame(orders_data)
for i in __var.index[:-1]:
    kkk = pd.DataFrame()
    with pd.ExcelWriter(f"recent_{i}_orders.xlsx") as writer:         
        for j in __var.columns:
            try:
                if __bot != str(j).partition("-")[0]:
                    kkk = pd.DataFrame()
            except:
                print(j)                
            __bot = str(j).partition("-")[0]
            try:
                print(__bot)
                xxx = pd.DataFrame(__var.loc[i][j])
                xxx["label"] = j
                xxx["pht"]  = xxx.index
                xxx.pht  = xxx.pht.apply(lambda x: x.tz_localize("UTC").tz_convert("Asia/Manila").tz_localize(None))
                ccc = list(xxx.columns)
                ccc = [ccc[-1]] + ccc[:-1]
                xxx = xxx[ccc]
                kkk = kkk.append(xxx)
                kkk.to_excel(writer, sheet_name = f"{__bot}")
            except:
                print(f"skipping orders {i} {j}")

positionOrders = pd.concat([conc,pd.DataFrame(__var.loc["order mismatch"])], axis = 1)
positionOrders.to_csv("recent_mismatch.csv")


print('------------------------ slippage ---------------------')
slpglkbk    = 30 
slpgordrdat = {}       
slpganmly   = []
slpgstatrep = []
for bot in bots:
    slperr              = 0
    slplvbt             = fetchBotOrders(bot, slpglkbk, keys)
    slplst              = today - pd.DateOffset(slpglkbk)
    slpbtstr, slpbskt   = fetchBacktesterPositions(bot)
    slpcurrpos          = fetchBotPositions(bot, keys)
    
    for pair in slpbskt:
        try:
            remove_dup = slplvbt[pair].dropna().copy()
            remove_dup.reset_index(inplace = True)
            remove_dup.drop_duplicates(subset='datetime', inplace = True)
            remove_dup.set_index('datetime',inplace = True)
            slpordrcmpr        = pd.concat([remove_dup, slpbtstr[pair][__si].diff(), slpbtstr[pair][__av]], axis = 1)
            # slpordrcmpr        = pd.concat([slplvbt[pair].dropna(), slpbtstr[pair][__si].diff(), slpbtstr[pair][__av]], axis = 1)
        
        except KeyError:
            slpnotrd           = pd.DataFrame(np.nan, index = pd.date_range(slplst, today), columns = [__si,__av])
            slpordrcmpr        = pd.concat([slpnotrd, slpbtstr[pair][__si].diff(), slpbtstr[pair][__av]], axis = 1)


        slpordrcmpr            = slpordrcmpr.reset_index().rename(columns = {'index':f'{bot}-{pair}'}).set_index(f'{bot}-{pair}')
        slpordrcmpr.columns    = pd.MultiIndex.from_product([[__lb,__bt], slpordrcmpr.columns[0:2]])
        slpordrcmpr            = slpordrcmpr.fillna(0).loc[slplst:(today + dt.timedelta(hours = 9)).strftime('%Y-%m-%d %H:%M:%S')]

        slpordrchk = []
        for i in range(len(slpordrcmpr)):
            
            if (slpordrcmpr[__lb][__si].iloc[i] > 0) & (slpordrcmpr[__bt][__si].iloc[i] > 0):
                check_order = 'buy'
            elif (slpordrcmpr[__lb][__si].iloc[i] < 0) & (slpordrcmpr[__bt][__si].iloc[i] < 0):
                check_order = 'sell'
            elif (slpordrcmpr[__lb][__si].iloc[i] == 0) & (slpordrcmpr[__bt][__si].iloc[i] == 0):
                check_order = ' '
            else:
                check_order = 'mismatch'
                
            slpordrchk.append(check_order)

        slpordrcmpr['checks'] = slpordrchk

        if slpordrcmpr[slpordrcmpr.checks == 'mismatch'].empty:
            slpgstatrep.append(f'All orders match for {bot}-{pair}')
            conclusion = 'None'
        else:
            slpgstatrep.append(f'Anomaly found in {bot}-{pair} orders')
            slpgstatrep.append(slpordrcmpr.to_string())
            slpganmly.append(f'OrderAnomaly-{bot}-{pair}.csv')
            conclusion = 'Mismatch'
            slperr += 1
    
        slpgordrdat[f'{bot}-{pair}'] = {'comparison': slpordrcmpr.to_dict(), 'order mismatch': conclusion}
    
    slpgstatrep.append('-----------------------------------------------------------------------------')


__var = pd.DataFrame(slpgordrdat)
for i in __var.index[:-1]:
    kkk = pd.DataFrame()
    with pd.ExcelWriter(f"recent_{i}_orders_slippage.xlsx") as writer:         
        for j in __var.columns:
            try:
                if __bot != str(j).partition("-")[0]:
                    kkk = pd.DataFrame()
            except:
                print(j)                
            __bot = str(j).partition("-")[0]
            try:
                print(__bot)
                xxx = pd.DataFrame(__var.loc[i][j])
                xxx["label"] = j
                xxx["pht"]  = xxx.index
                xxx.pht  = xxx.pht.apply(lambda x: x.tz_localize("UTC").tz_convert("Asia/Manila").tz_localize(None))
                ccc = list(xxx.columns)
                ccc = [ccc[-1]] + ccc[:-1]
                xxx = xxx[ccc]
                kkk = kkk.append(xxx)
                kkk.to_excel(writer, sheet_name = f"{__bot}")
            except:
                print(f"skipping orders {i} {j}")
