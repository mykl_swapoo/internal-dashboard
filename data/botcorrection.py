import pandas as pd
import numpy as np
import ccxt
import datetime as dt
import logging 

error_file = {'strategyName':['TT1-BMEx-Live', 'FN3-BMEx-Live', 'FN3-BMEx-Live', 'MM3-BMEx-Live'],
              'symbol':['BTC/USD', 'BTC/USD', 'ETH/USD', 'BTC/USD'],
              'currentStatus':[False, False, False, False],
              'startDate':['2021-10-01', '2021-10-01', '2021-10-01', '2021-10-01'],
              'endDate':['2021-10-05', '2021-10-05', '2021-10-05', '2021-11-17'],
              'side':['long', 'long', 'long', 'long'],
              'entryPrice':[47494.5, 47617.29, 3276.24, 47485.5],
              'size':[51500, 11700, 75, 11800]
             }


epoch = dt.datetime.utcfromtimestamp(0)


def unix_time_millis(time):
    return (time - epoch).total_seconds() * 1000.0


def download_data(duration, time_unit, symbol, from_datetime):
    dur_ = str(duration)
    tu_ = str(time_unit)
    tf = dur_ + tu_

    bitmex = ccxt.bitmex({"enableRateLimit": True, 'rateLimit' : 1000})
    from_timestamp = unix_time_millis(from_datetime)

    if tu_ == "d":
        m1_len = dt.timedelta(days=duration).total_seconds() * 1000

    if tu_ == "h":
        m1_len = dt.timedelta(hours=duration).total_seconds() * 1000

    if tu_ == "m":
        m1_len = dt.timedelta(minutes=duration).total_seconds() * 1000

    btc = []
    now = bitmex.milliseconds()
    while from_timestamp < now:
        btc_candles = bitmex.fetch_ohlcv(
            symbol, tf, from_timestamp, limit=1000
        )
        last = btc_candles[-1][0]
        from_timestamp = last + m1_len
        btc += btc_candles

    data = pd.DataFrame(btc)
    data.columns = ["timestamp", "open", "high", "low", "close", "volume"]
    data["timestamp"] = pd.to_datetime(data.timestamp, unit="ms")
    data.set_index("timestamp", inplace=True)
    return data


def pnl(symbol, side, entry, exit, size):
    multipliers = {
        "ETH/USD": 0.001e-3,
        "ETHUSD": 0.001e-3,
        "XRP/USD": 0.0002,
        "LTC/USD": 0.002e-3,
    }

    if symbol.endswith("BTC"):
        pnl_ = (exit - entry) * size
    elif symbol.startswith("BTC") or symbol.startswith("XBT"):
        pnl_ = (1 / entry - 1 / exit) * size
    else:
        pnl_ = (exit - entry) * size * multipliers[symbol]

    if side == "long":
        return pnl_
    elif side == "short":
        return -pnl_


def calculate_corrections(theory_balances, 
                          symbol, 
                          startDate, 
                          side, 
                          entryPrice, 
                          size, 
                          currentStatus,  
                          endDate):
    
    df = download_data(1, "d", symbol, startDate)[["close"]].loc[
        : theory_balances.index[-1]
    ]
    calculation = download_data(1, "d", symbol, startDate)[["close"]]
    calculation.columns = [symbol]
    calculation["btc_pnl"] = pnl(symbol, 
                                 side, 
                                 entryPrice, 
                                 calculation[symbol], 
                                 size)

    corrections = pd.Series(index=theory_balances.index).fillna(0)
    corrections[startDate:] = calculation["btc_pnl"].iloc[:-1]

    if currentStatus == True:
        pass
    else:
        corrections.loc[(endDate + dt.timedelta(days=1)).strftime("%Y-%m-%d"):] = corrections.loc[endDate.strftime("%Y-%m-%d")]

    return corrections


def get_corrected_balances_df(downloaded_balances, final=True):
    error_df = pd.DataFrame.from_dict(error_file)
    error_df["startDate"] = pd.to_datetime(error_df["startDate"])
    error_df["endDate"] = pd.to_datetime(error_df["endDate"])

    current = dt.datetime.now()
    current_td = dt.timedelta(
        hours=current.hour,
        minutes=current.minute,
        seconds=current.second,
        microseconds=current.microsecond,
    )
    today = current - current_td
    today.strftime("%Y-%m-%d")
    yesterday = today - dt.timedelta(days=1)

    actual_balances = pd.read_csv(
        downloaded_balances, sep=",", index_col=1, parse_dates=True
    )
    theory_balances = pd.DataFrame(
        index=pd.date_range(
            start=actual_balances.index[0], end=yesterday.strftime("%Y-%m-%d")
        ),
        columns=actual_balances.columns, dtype=float
    )
    for col in theory_balances.columns:
        theory_balances[col].values[:] = 0

    for i in range(len(error_df)):
        theory_balances[error_df["strategyName"].iloc[i]] += calculate_corrections(theory_balances,
                                                                                   error_df["symbol"].iloc[i],
                                                                                   error_df["startDate"].iloc[i],
                                                                                   error_df["side"].iloc[i],
                                                                                   error_df["entryPrice"].iloc[i],
                                                                                   error_df["size"].iloc[i],
                                                                                   error_df["currentStatus"].iloc[i],
                                                                                   error_df["endDate"].iloc[i]
                                                                                  )

    correction_balances = actual_balances + theory_balances
    # if final==True:
    #     return correction_balances
    # else:
    return theory_balances, correction_balances

current = dt.datetime.now()
current_td = dt.timedelta(
    hours=current.hour,
    minutes=current.minute,
    seconds=current.second,
    microseconds=current.microsecond,
)
today = current - current_td
today.strftime("%Y-%m-%d")
yesterday = today - dt.timedelta(days=1)

logging.basicConfig(filename=f'botcorrection-log-{today.strftime("%Y-%m-%d")}.log', 
                    level = logging.DEBUG,
                    format = '%(asctime)s - %(message)s',
                    datefmt = '%d-%b-%y %H:%M:%S')

##error_pnl = get_corrected_balances_df('botmonitoring'+yesterday.strftime("%Y-%m-%d")+'.csv', False)
logging.debug('Initialization Completed.')
try :
    logging.debug('Starting Main Processes')
    error_pnl , corrected_bot_balance = get_corrected_balances_df('botmonitoring'+yesterday.strftime("%Y-%m-%d")+'.csv', False)
    error_pnl = error_pnl[(error_pnl.sum()>0).index[(error_pnl.sum()>0)]]
    error_pnl['error_pnl'] = error_pnl.sum(axis=1)
    logging.debug('Main Processes Done')
    logging.debug('Error PNL saving start...')
    error_pnl.to_csv('error_pnl_'+yesterday.strftime("%Y-%m-%d")+'.csv')
    logging.debug('Saving Error PNL Done')
    logging.debug('correct balances saving start...')
    #corrected_bot_balance = get_corrected_balances_df('botmonitoring'+yesterday.strftime("%Y-%m-%d")+'.csv')
    corrected_bot_balance.to_csv('correct_bot_balance_'+yesterday.strftime("%Y-%m-%d")+'.csv')
    logging.debug('Saving Correct Balance Done')
    logging.debug('Success!')
except Exception as e:
    logging.debug('Error !')
    logging.debug('Error !')
    logging.debug(f'Error {e}')
    logging.debug('Error !')
    logging.debug('Error !')
