from trading.datasets import OHLCV
from pandas import DataFrame, concat, Timedelta

pairs = ["BTC/USD", "ETH/USD", "ETHBTC"]

results = DataFrame()

for pair in pairs:
    x = OHLCV.from_exchange("ftx", pair, "8h")
    x[pair] = x.open.pct_change() * 100
    results = concat([results, x[pair]], axis = 1)

    
results.sort_index(ascending = False, inplace = True)
results = results.head()

results["datetime"] = results.index
results["datetime"] = results["datetime"].apply(lambda x: str(x + Timedelta(hours = 8)).split("+")[0] + " PHT")
results.index.rename("datetime", inplace = True)
results.set_index("datetime", drop = True, inplace = True)
results.to_csv("session-change.csv")