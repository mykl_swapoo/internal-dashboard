from __future__ import annotations

import os
import re
from symtable import Symbol
import ccxt
import numpy as np
import pandas as pd
import datetime as dt
from pandas.tseries.offsets import DateOffset
from flatten_json import flatten

pd.options.mode.chained_assignment = None 

import warnings
warnings.filterwarnings("ignore")

from laetitudebots.util.talib_method import talib_method
from laetitudebots.util.custom_method import custom_method
from laetitudebots.backtest import Backtest

from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import boto3


# # # # # # # tdc
# # # # # # # # ---
# # # # # # # # ---
# # # # # # # # ---
# # # # # # # # ---

from dateutil.relativedelta import relativedelta
import datetime as dt
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
import pickle
import ccxt
import quantstats as qs

import warnings
warnings.filterwarnings("ignore")

from laetitudebots.util.talib_method import talib_method
from laetitudebots.util import custom_method
from laetitudebots.backtest import Backtest
import numpy as np
import talib

####################
#DMIv1 ftx start
def proc_order(ord_type, ord_size, side, price=None, trigger=None):
    order = {
            'order_type' : ord_type,
            'size' : ord_size,
            'side' : side,
            }
    if ord_type is 'stop':
        if trigger is not None:
            order['trigger_price'] = trigger 
#             print('trigger price', order['trigger_price'])
    
    if ord_type is 'limit':
        if price is not None:
            order['price'] = price
    
    return order

def execute_order(strategy_name, asset, **orders):
    for i,v in orders.items():
#         print('execute orders', i, v)
        asset.create_order(f'{strategy_name}_{i}', v)
        
def cancel_order(strategy_name, asset, *orders):
    for i in orders:
        if asset.orders.get(f'{strategy_name}_{i}'):
#             print('cancel orders', i)
            asset.cancel_order(f'{strategy_name}_{i}')
    
"""fetch token precision"""
markets = ccxt.ftx().fetch_markets()

tokens = ['BTC',
         'ETH',
         'SOL',
         'FTT',
         'FTM',
         'BNB',
         'MATIC',
         'LINK',
         'XRP',
         'DOGE',
         'LTC',
         'TRX',
         'YFI',
         'OMG',
         'SXP',
         'RUNE',
         'BCH',
         'KNC',
         'LRC',
         'ENJ',
         'EUR',
         'BAT',
         'MKR',
         'COMP',
         'SNX',
         'CHR',
         'ETC',
         'ADA'
         ]

step = []
for token in tokens:
    step.extend([x['precision'].get('amount') for x in markets if x.get('id') == f'{token}-PERP'])

token_steps = dict(zip(tokens, step))

def calculate_size(exchange, symbol, total_balance, close, def_alloc, contract_rate, with_var=False, **kwargs):
    if exchange == 'ftx':
        pos_size = total_balance * def_alloc
        orig_size = pos_size * contract_rate
        
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            
            size = min(orig_size, var_size)
            
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
            
        
        sym = symbol.split('/')[0]    
        
        try:
            precision = decimal.Decimal(str(token_steps.get(sym)))

            if precision == decimal.Decimal(1.0):
                precision = decimal.Decimal(0.0)

            size = round(size, abs(precision.as_tuple().exponent))
        except Exception as e:
            pass
#             print(f'Precision error: {symbol}. Use unedited size.')
        
        return size
    
    if exchange == 'binance':
        min_size = {
            'btcusdt': 100,
            'ethusdt': 1,
            'xrpusdt': 1,
            'ltcusdt': 1,
            'adabtc': 10,
            'ethbtc': 0.01
        }
        # orig position size
        pos_size = total_balance * def_alloc
        if symbol.lower() == 'eth/btc':
            orig_size = round(pos_size * contract_rate, 2)
        else:
            orig_size = np.floor(pos_size * contract_rate)

        if symbol.lower().startswith('btc'):
            if orig_size < 100: 
                if orig_size % 100 >= 90:
                    orig_size = round(orig_size, -2)
                else: 
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 100)
        elif symbol.lower() == 'ada/btc':
            if orig_size < 10:
                if round(orig_size % 10, 2) >= 9:
                    orig_size = round(orig_size, -1)
                else:
                    orig_size = 0
            else:
                orig_size = orig_size - (orig_size % 10)
        #var pos size
        if with_var and (float(kwargs['risk']) > 0):
            invalidation = abs((close-kwargs['stop'])/close)
            var_pos_size = (total_balance * kwargs['risk']) / invalidation
            var_size = var_pos_size * contract_rate
            if symbol.lower().startswith('btc'):
                if var_size < 100: 
                    if var_size % 100 >= 90:
                        var_size = round(var_size, -2)
                    else: 
                        var_size = 0
                else:
                    var_size = var_size - (var_size % 100)
            else:
                if symbol.lower().endswith('usdt'):
                    if var_size < 1:
                        if round(var_size % 1, 2) >= 0.9:
                            var_size = round(var_size, 0)
                        else:
                            var_size = 0
                    else:
                        var_size = var_size - (var_size % 1)
                else:
                    if symbol.lower() == 'ada/btc':
                        if var_size < 10:
                            if round(var_size % 10, 2) >= 9:
                                var_size = round(var_size, -1)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 10)
                    elif symbol.lower() == 'eth/btc':
                        if var_size < 1:
                            if round(var_size % 0.01, 3) >= 0.009:
                                var_size = round(var_size, 2)
                            else:
                                var_size = 0
                        else:
                            var_size = var_size - (var_size % 1)
            size = min(orig_size, var_size)
        elif (with_var == False) or (float(kwargs['risk']) == 0.0):
            size = orig_size
        return size

def get_win_rate(df, pairs):
    
    for pair in pairs:
        df[(pair, 'pos')] = None
        df[(pair, 'wl')] = 0
        
        for i in range(len(df)):
            if df.iloc[i][(pair, 'size')] > 0:
                df[(pair, 'pos')].iloc[i] = 'long'
            elif df.iloc[i][(pair, 'size')] < 0:
                df[(pair, 'pos')].iloc[i] = 'short'
            if df[(pair, 'pos')].iloc[i-1] != df[(pair, 'pos')].iloc[i]:
                if df[(pair, 'pos')].iloc[i-1] == 'long':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = 1
                    else:
                        df[(pair, 'wl')].iloc[i] = -1
                elif df[(pair, 'pos')].iloc[i-1] == 'short':
                    if df[(pair, 'avg_price')].iloc[i-1] < df[(pair, 'avg_price')].iloc[i]:
                        df[(pair, 'wl')].iloc[i] = -1
                    else:
                        df[(pair, 'wl')].iloc[i] = 1
    
    pairs_dict = {}
    for pair in pairs:
        
        win_trades = len(pos[pos[(pair, 'wl')] == 1])
        losing_trades = len(pos[pos[(pair, 'wl')] == -1])
        total = win_trades + losing_trades
        try:
            win_pct = win_trades / total
            lose_pct = losing_trades / total
        except Exception as e:
            print(e)
            win_pct = 0
            lose_pct = 0
 
        pairs_dict[pair] = [win_trades, losing_trades, total, round(win_pct, 2)]
        
        
        
    return pairs_dict
    
    
def proc_wr(old_dict, get_win_rate_dict):
    
    pair_keys = old_dict.keys()
    for pair in pair_keys:
        
        old = old_dict[pair]
        new = get_win_rate_dict[pair]
        
        new2 = [old[x] + new[x] for x in range(len(old))]
        new2[-1] = round(new2[0] / new2[2], 4)
        
        
        old_dict[pair] = new2
    
    return old_dict

def metrics(eq):
    # risk free rate
    rfr = 0.04
    eq = eq[eq.index.hour == 0]
    rets = eq.pct_change().fillna(0)

    years = list(rets.index.year.drop_duplicates())

    sort = []
    draw = []
    equi = []
    news = []

    data = pd.DataFrame(columns={'year', 'Sortino', 'MDD', 'Equity', 'New Sharpe'})

    for year in years:
        year = str(year)
        year_rets = rets[year]

        #sortino
        sortino = qs.stats.sortino(year_rets, rf= rfr, periods=365, annualize=True)

        #dd
        dd = qs.stats.max_drawdown(year_rets)*100

        year_rets = pd.DataFrame(year_rets, columns=['rets'])
        year_rets['eq'] = (1 + year_rets['rets']).cumprod()

        #equity
        year_eq = qs.stats.comp(year_rets['rets']) + 1

        #linearity
        year_rets['log'] = np.log(year_rets['eq'])
        x = np.arange(year_rets.index.size)
        fit = np.polyfit(x, year_rets['log'], deg=1)
        fit_function = np.poly1d(fit)
        linearity_val = np.sqrt(((year_rets['log'] - fit_function(x))**2).mean())
        cagr_val = qs.stats.cagr(year_rets['eq'].pct_change().fillna(0), rf=rfr, periods=year_rets.shape[0], compounded=True)
        new_sharpe_val = cagr_val/linearity_val

        sort.append(sortino)
        draw.append(dd)
        equi.append(year_eq)
        news.append(new_sharpe_val)

        data = data.append({'New Sharpe': round(new_sharpe_val, 2),
                            'Equity': round(year_eq, 2),
                            'MDD': round(dd, 2), 
                            'Sortino': round(sortino, 2),
                            'year': year}, ignore_index=True)

#     sort_avg = round(sum(sort)/len(sort),2)
#     draw_avg = round(sum(draw)/len(draw),2)
#     equi_avg = round(sum(equi)/len(equi),2)
#     new_sharpe_avg = round(sum(news)/len(news), 2)

    sort_avg = sum(sort)/len(sort)
    draw_avg = sum(draw)/len(draw)
    equi_avg = sum(equi)/len(equi)
    new_sharpe_avg = sum(news)/len(news)

    data = data.append({'year': 'AVG', 
                        'Sortino': sort_avg, 
                        'MDD': draw_avg, 
                        'Equity': equi_avg, 
                        'New Sharpe': new_sharpe_avg}, 
                       ignore_index=True)

    data.set_index('year', inplace=True)
    data = data[['Equity', 'MDD', 'Sortino', 'New Sharpe']]
    return data

slippage = 0.0005


config = {
    'context': {
        'is_shared_balance' : True,
        'balance' : 1,
        'assets': {},
        'parameters': {
            'idx': 14,
#             'risk': 0.01,
            
        },
    },
    'slippage': slippage,
    'time_interval': 8,
    'time_unit': 'h',   
    'start_date': '2020-01-01 00:00:00',    
    'end_date': dt.datetime.now(),
    'exchange': 'ftx',
    'window_length' : 2
    }


pairs = {
    'ADA/USDT': ['binance', 1/3],
    'BTC/USDT': ['binance', 1/3],
    'ETH/USDT': ['binance', 1/3]

}

for i,v in pairs.items():
    if i.endswith('BTC'):
        maker_fee = -.0005
        taker_fee = 0.0025      
    else:
        maker_fee = -.00025
        taker_fee = 0.00075
        
    config['context']['assets'][i] = {
        'exchange': v[0],
        'maker_fee': maker_fee,
        'taker_fee': taker_fee,
        'allocation' : v[1],
        'collateral': 'BTC',

        ###Take profit
        'last_pos': None,

        }
    
def initialize(factors, config):
        
    idx = int(config['parameters']['idx'])


    factors['close'] = factors.close.ffill()
    factors['high'] = factors.high
    factors['slow'] = factors.low


    #dmi
    factors['dx'] = talib_method(method='DX', timeperiod = idx, args = [factors.high, factors.low, factors.close])
    factors['min_di'] = talib_method(method='MINUS_DI', timeperiod = idx, args =[factors.high, factors.low, factors.close])
    factors['plus_di'] = talib_method(method='PLUS_DI', timeperiod = idx, args =[factors.high, factors.low, factors.close])

def process(window, assets, context):
    
    total_unrealised_pnl = sum([asset.position.unrealised_pnl for _, asset in assets.items()])
    total_balance = total_unrealised_pnl + context['balance']
    
    
    
    for symbol, asset in assets.items():
        position = asset.position
        last = window.latest(symbol)
        lastt = window.previous(symbol, 2)
        
        if not np.all([lastt.dx, lastt.plus_di, lastt.min_di]):
            continue
        settings = context['assets'][symbol]
    
        dx = last.dx
        pdi = last.plus_di
        mdi = last.min_di
          
        ### Entry Conditions
        lg_ent = ((dx > 25) and
                 (dx > lastt.dx) and
#                  (last.close > lastt.close) and
                 (pdi > mdi) and
                 (pdi > lastt.plus_di))
        lg_ext = (pdi < mdi) 
        
        st_ent = ((dx > 25) and
                 (dx > lastt.dx) and
#                  (last.close < lastt.close) and
                 (mdi > pdi) and
                 (mdi > lastt.min_di))
        
        st_ext = (pdi > mdi)
        stop = 0
        
        size = calculate_size('ftx', symbol, total_balance, last.close, settings['allocation'], 
                              asset.get_contract_rate(last.close, last.collateral), 
                              with_var=False, stop=0, risk=0)

        if position.size == 0: 
            if lg_ent:
                order = proc_order('market', size, 'buy')
                execute_order('dmi', asset, long=order)
            
            if st_ent:
                order = proc_order('market', size, 'sell')
                execute_order('dmi', asset, short=order)
                
        elif position.size > 0 and lg_ext:  
            order = proc_order('market', abs(position.size), 'sell')
            execute_order('dmi', asset, short=order)
            
            
        elif position.size < 0 and st_ext:  
            order = proc_order('market', abs(position.size), 'buy')
            execute_order('dmi', asset, long=order)
        
bt = Backtest(config)
bt.set_strategy(initialize=initialize, process=process)
bt.run(progressbar=False)

bt.positions.loc['2021-05-28':].to_csv("DMIv1-positions.csv")

#DMIv1 ftx end
###################

bot_keys = {
    'DMIv1': {
        'apiKey' : 'ScH71g4GK-zTVs8SNhV08oscxRCGZ5Ph59kYZTjy',
        'secret' : 'ajDbPHwR100rsT0_W9VFlJjWXHyZWHuNMh23aWsP',
        'enableRateLimit': True,
        "exchange": "FTX",
        "headers":{'FTX-SUBACCOUNT': 'DMI Prod'}
    },
}
keys = pd.DataFrame.from_dict(bot_keys, orient='index')

def unixTimeMillis(time):
    epoch = dt.datetime.utcfromtimestamp(0)
    return (time - epoch).total_seconds() * 1000.0


def downloadOHLCV(duration, time_unit, symbol, from_datetime, to_datetime = dt.datetime.now()):
    
    if to_datetime > dt.datetime.now():
        to_datetime = dt.datetime.now()
        
    dur_ = str(duration)
    tu_ = str(time_unit)
    tf = dur_ + tu_

    bitmex = ccxt.bitmex({"enableRateLimit": True})
    from_timestamp = unixTimeMillis(from_datetime)
    to_timestamp = unixTimeMillis(to_datetime)

    if tu_ == "d":
        m1_len = dt.timedelta(days=duration).total_seconds() * 1000

    if tu_ == "h":
        m1_len = dt.timedelta(hours=duration).total_seconds() * 1000

    if tu_ == "m":
        m1_len = dt.timedelta(minutes=duration).total_seconds() * 1000
    
    btc = []
    try:
        while from_timestamp < to_timestamp:
            btc_candles = bitmex.fetch_ohlcv(symbol, tf, from_timestamp, limit=1000)
            last = btc_candles[-1][0]
            from_timestamp = last + m1_len
            btc += btc_candles
      
    except:
        while from_timestamp < to_timestamp:
            btc_candles = bitmex.fetch_ohlcv('ADAZ21', tf, from_timestamp, limit=1000, params={'symbol':symbol})
            last = btc_candles[-1][0]
            from_timestamp = last + m1_len
            btc += btc_candles
        

    data = pd.DataFrame(btc)
    data.columns = ["timestamp", "open", "high", "low", "close", "volume"]
    data["timestamp"] = pd.to_datetime(data.timestamp, unit="ms")
    data.set_index("timestamp", inplace=True)
    data = data.loc[:to_datetime]
    return data

def botClient(bot, keys=keys, exchange = 'bitmex'):
    #changes1
    if exchange == 'bitmex':
        client = ccxt.bitmex({'apiKey':keys.loc[bot, 'apiKey'], 
                            'secret':keys.loc[bot, 'apiSecret'],
                            'enableRateLimit':keys.loc[bot, 'enableRateLimit']
                            })
        return client
    elif exchange == 'ftx':
        client = ccxt.ftx({'apiKey':keys.loc[bot, 'apiKey'], 
                    'secret':keys.loc[bot, 'secret'],
                    'enableRateLimit':keys.loc[bot, 'enableRateLimit'],
                    'headers' : keys.loc[bot, 'headers']
                })
        return client

def fetchBotOrders(bot, lookback, cleaned=True, exchange = 'ftx'):
    client = botClient(bot, exchange = exchange)
    last = today - dt.timedelta(days=lookback)
    since = int(unixTimeMillis(last))
    x = client.fetchOrders(since=since, limit=500)
    orders_ = pd.DataFrame(x)
    if orders_.empty:
        return orders_
    live_orders = pd.DataFrame(orders_)
    live_orders['datetime'] = pd.to_datetime(live_orders['timestamp'], unit='ms')
    live_orders = live_orders.set_index('datetime')
    live_orders['side'] = live_orders.side.replace(['buy', 'sell'], [1,-1])
    live_orders['size'] = live_orders['side']*live_orders['amount']
    live_orders['symbol'] = live_orders.symbol.apply(lambda x: re.sub(':USD',
                                                                      '', x)).replace('BTC/USD', 'BTCUSD')

    if cleaned == False:
        return live_orders
    else:
        live_orders = live_orders[['symbol','size', 'average']]
        live_orders = live_orders.pivot(columns='symbol', values=['size', 'average'])
        live_orders.columns = live_orders.columns.swaplevel(0, 1)
        live_orders.index = live_orders.index.floor('h')
        return live_orders

def fetchBotOrders_pair(bot, lookback, pair, cleaned=True, exchange = 'ftx'):
    client = botClient(bot, exchange = exchange)
    last = today - dt.timedelta(days=lookback)
    since = int(unixTimeMillis(last))
    x = client.fetchOrders(since=since, symbol = re.sub('USD','/USD',pair) + ':USD', limit=500)
    orders_ = pd.DataFrame(x)
    if orders_.empty:
        return orders_
    live_orders = pd.DataFrame(orders_)

    live_orders['datetime'] = pd.to_datetime(live_orders['timestamp'], unit='ms')
    live_orders = live_orders.set_index('datetime')
    live_orders['side'] = live_orders.side.replace(['buy', 'sell'], [1,-1])
    live_orders['size'] = live_orders['side']*live_orders['amount']
    #live_orders['symbol'] = live_orders['info'].apply(lambda x: x['symbol'])
    live_orders['symbol'] = live_orders.symbol.apply(lambda x: re.sub(':USD',
                                                                      '', x)).replace('BTC/USD', 'BTCUSD')
    #live_orders['currency'] = live_orders['info'].apply(lambda x: x['currency'])

    if cleaned == False:
        return live_orders
    else:
        live_orders = live_orders[['symbol','size', 'average']]
        live_orders = live_orders.pivot(columns='symbol', values=['size', 'average'])
        live_orders.columns = live_orders.columns.swaplevel(0, 1)
        live_orders.index = live_orders.index.floor('h')
        print('---------------------------')
        #print('with pair')
        #print(live_orders)
        print('---------------------------')
        return live_orders

def fetchBacktesterPositions(bot, reverse=True):
    from_path = f'{bot}-positions.csv'
    backtester = pd.read_csv(from_path, header=[0,1], index_col=0).drop('balance', axis=1)
    backtester.index = pd.to_datetime(backtester.index)
    backtester.columns = pd.MultiIndex.from_tuples([(
        re.sub('avg_price',
               'average', 
               backtester.columns[i][0]),
        re.sub('USDT', 
               'USD', 
               backtester.columns[i][1]))
        for i in range(len(backtester.columns))]
    )
    backtester.columns = pd.MultiIndex.from_tuples([(backtester.columns[i][0],
                                                     ''.join(backtester.columns[i][1].split('/')),
                                                     )
                                                    for i in range(len(backtester.columns))])
    backtester = backtester[['size', 'average']]
    basket = list(dict.fromkeys([backtester.columns[i][1] for i in range(len(backtester.columns))]))
    basket = [''.join(x.split('/')) for x in basket]
    if reverse == False:
        return backtester, basket
    else:
        backtester.columns = backtester.columns.swaplevel(0, 1)
        return backtester, basket
    
def fetchBotPositions(bot, cleaned=True, exchange = 'ftx'):
    client = botClient(bot, exchange = exchange)
    df = pd.DataFrame()
    for val in client.fetch_positions():
        data = flatten(val)
        x = [data[key] for key in data.keys()]
        temp_df = pd.DataFrame(data = [x], columns = data.keys())
        df = pd.concat([df, temp_df])
    if df.empty:
        return df
    
    if cleaned == False:
        return df
    else:
        df = df[['info_future', 'info_netSize','entryPrice']]
        df.columns = ['symbol','currentQty', 'avgEntryPrice']
        df.loc[:, ['currentQty', 'avgEntryPrice']] = df.loc[:, ['currentQty', 'avgEntryPrice']].astype('float')
        #df['openingTimestamp'] = np.nan
        df['isOpen'] = np.nan
        df['symbol'] = df.symbol.apply(lambda x: re.sub('-PERP','USD', x)).replace('-PERP', 'USD')
        return df    
        

bots = [
    'DMIv1'
]

epoch = dt.datetime.utcfromtimestamp(0)

current = dt.datetime.now()
today = pd.to_datetime(current.date())

current_hour = today + dt.timedelta(hours=current.hour) 


lookback = 30
anomalies = []
status_reports = []
positions_data = {}
for bot in bots:
    
    live_bot = fetchBotOrders(bot, lookback)
    backtester, basket = fetchBacktesterPositions(bot)
    curr_positions = fetchBotPositions(bot)
    last = today - DateOffset(lookback)
    print(f'Current bot: {bot}-{basket}')
    errors = 0
    for pair in basket:
        print(f'Current bot-pair: {bot}-{pair}')
        try:
            if curr_positions[curr_positions.symbol==pair].empty:
                raise Exception
            else:
                curr_token_pos = curr_positions[curr_positions['symbol'] == pair].reset_index()
                recent_pos = backtester[pair].tail(1).reset_index()
                position_comparison = pd.concat([curr_token_pos, recent_pos], axis = 1).ffill() 
                position_comparison = position_comparison.drop('index', axis = 1)
                position_comparison = position_comparison.set_index('timestamp')


        except (KeyError, AttributeError, Exception) as e:
            print(f'throw {pair}')
            no_positions = pd.DataFrame(np.nan, index=[current_hour], columns=['symbol',
                                                                                   'currentQty',
                                                                                   'avgEntryPrice',
                                                                                   'isOpen'
                                                                                  ])
            position_comparison = pd.concat([no_positions, 
                                             backtester[pair].tail(1)]).sort_index().ffill()

        arrays = [list(np.concatenate([['live_bot']*4, ['backtester']*2])),
                  list(np.concatenate([['symbol',
                                        'currentQty',
                                        'avgEntryPrice',
                                        'isOpen'
                                       ], list(backtester[pair].columns.values)]))]
        tuples = list(zip(*arrays))
        print(tuples)
        print(position_comparison.columns)
        position_comparison.columns = pd.MultiIndex.from_tuples(tuples)
        position_comparison = position_comparison.fillna(0)
        
        for i in range(len(position_comparison)):
            if ((position_comparison['live_bot']['currentQty'].iloc[-1] > 0) & 
                (position_comparison['backtester']['size'].iloc[-1] > 0)):
                check_position = 'buy'
            elif ((position_comparison['live_bot']['currentQty'].iloc[-1] < 0) & 
                  (position_comparison['backtester']['size'].iloc[-1] < 0)):
                check_position = 'sell'
            elif ((position_comparison['live_bot']['currentQty'].iloc[-1] == 0) & 
                  (position_comparison['backtester']['size'].iloc[-1] == 0)):
                check_position = ' '
            else:
                check_position = 'mismatch'
        position_comparison['checks'] = np.nan
        position_comparison.loc[position_comparison.index[-1], 'checks'] = check_position


        print('Datetime opened:')
        if position_comparison['live_bot'].iloc[-1,3] == True:
            date_opened = fetchBotOrders(bot, lookback=60)[pair].dropna()
            if date_opened.dropna().empty:
                date_opened = pd.DataFrame(columns = ['datetime','size', 'average'])
                date_opened.loc['0', 'datetime'] =  pd.to_datetime(dt.datetime.now().replace(tzinfo = None).date()) + dt.timedelta(hours = 0)
                date_opened.set_index('datetime', inplace = True)
            else:
                print(date_opened.tail(1))
                
        else:
            date_opened = pd.DataFrame(columns = ['datetime','size', 'average'])
            date_opened.loc['0', 'datetime'] =  pd.to_datetime(dt.datetime.now().replace(tzinfo = None).date()) + dt.timedelta(hours = 0)
            date_opened.set_index('datetime', inplace = True)
        
        status_reports.append('-----------------------------------------------------------------------------------------')
        if position_comparison[position_comparison.checks == 'mismatch'].empty:
            status_reports.append(f'0 mismatch in {bot}-{pair}')
            conclusion = f'None'
        else:
            status_reports.append(f'Mismatch in {bot}-{pair} positions')
            conclusion = f'Mismatch'
            status_reports.append(position_comparison.to_string())
            anomalies.append(f'positionAnomaly-{bot}-{pair}.csv')
            errors +=1
            status_reports.append('Datetime opened:')
        
        positions_data[f'{bot}-{pair}'] = {'comparison': position_comparison.to_dict(), 
                                           'opened': date_opened.tail(1).to_dict(), 
                                           'position mismatch': conclusion}


lookback = 30
anomalies = []
status_reports = []
orders_data = {}
for bot in bots:
    #live_bot = fetchBotOrders(bot, lookback)
    backtester, basket = fetchBacktesterPositions(bot)
    #curr_positions = fetchBotPositions(bot)
    last = today - DateOffset(lookback)
    errors = 0
    for pair in basket:
        live_bot = fetchBotOrders_pair(bot, lookback,pair = pair)
        try:
            # print(live_bot[pair].dropna().index.has_duplicates)
            # print(len(live_bot[pair].dropna()))
            # print(backtester[pair]['size'].diff())
            # print(len(backtester[pair]['size'].diff()))
            # print(len(backtester[pair]['average']))
            # print(backtester[pair]['average'])

            # live_bot[pair].dropna().to_csv('duplicatedmv1.csv')
            # print(backtester[pair]['size'].diff().index.has_duplicates)
            # print(backtester[pair]['average'].index.has_duplicates)
            # order_comparison = pd.concat([live_bot[pair].dropna(),
            #                               backtester[pair]['size'].diff(),
            #                               backtester[pair]['average'] 
            #                              ], axis=1)
            #order_comparison = live_bot[pair].dropna().merge(backtester[pair]['size'].diff())
            print('-------------')
            print(pair)
            print('-------------')
            #print(live_bot[re.sub('USD','-PERP', pair)].dropna().index)
            #print(backtester[pair]['size'].diff().index)
            #order_comparison = live_bot[pair].dropna().join(backtester[pair]['size'].diff(), how = 'left')
            #order_comparison = order_comparison.merge(backtester[pair]['average'])
            merged = pd.concat([backtester[pair]['size'].diff(),
                                backtester[pair]['average'] 
                                     ], axis=1)
            order_comparison = live_bot[pair].dropna().join(merged, lsuffix='_left', rsuffix='_right' )
        except KeyError:
            no_trades = pd.DataFrame(np.nan, index=pd.date_range(last, today), columns=['size', 'average'])
            order_comparison = pd.concat([no_trades, backtester[pair]['size'].diff(), backtester[pair]['average']], axis=1)
        #print(order_comparison)
        order_comparison = order_comparison.reset_index().rename(columns={'index':f'{bot}-{pair}'})
        print(order_comparison)
        order_comparison = order_comparison.set_index(f'{bot}-{pair}')
        order_comparison.columns = pd.MultiIndex.from_product([['live_bot', 'backtester'], ['size','average']])
        order_comparison = order_comparison.fillna(0).loc[last:(today + dt.timedelta(hours=9)).strftime('%Y-%m-%d %H:%M:%S')]
        order_checks = []
        for i in range(len(order_comparison)):
            if (order_comparison['live_bot']['size'].iloc[i] > 0) & (order_comparison['backtester']['size'].iloc[i] > 0):
                check_order = 'buy'
            elif (order_comparison['live_bot']['size'].iloc[i] < 0) & (order_comparison['backtester']['size'].iloc[i] < 0):
                check_order = 'sell'
            elif (order_comparison['live_bot']['size'].iloc[i] == 0) & (order_comparison['backtester']['size'].iloc[i] == 0):
                check_order = ' '
            else:
                check_order = 'mismatch'
            order_checks.append(check_order)

        order_comparison['checks'] = order_checks

        if order_comparison[order_comparison.checks == 'mismatch'].empty:
            status_reports.append(f'All orders match for {bot}-{pair}')
            conclusion = 'None'
        else:
            status_reports.append(f'Anomaly found in {bot}-{pair} orders')
            status_reports.append(order_comparison.to_string())
            anomalies.append(f'OrderAnomaly-{bot}-{pair}.csv')
            conclusion = 'Mismatch'
            errors += 1
    
        orders_data[f'{bot}-{pair}'] = {'comparison': order_comparison.to_dict(),  
                                           'order mismatch': conclusion}
    
    status_reports.append('-----------------------------------------------------------------------------')



__d8 = str(today.date())
__var = pd.DataFrame(positions_data)
print('recent opened comparison')
print(__var)
__var.to_csv('var.csv')
print('recent opened comparison')
print(__var.index[:-1])
for i in __var.index[:-1]:
    kkk = pd.DataFrame()
    with pd.ExcelWriter(f"recent_{i}_position-ftx.xlsx") as writer:   
        for j in __var.columns:
            try:
                if __bot != str(j).partition("-")[0]:
                    kkk = pd.DataFrame()
            except:
                print(j)
            __bot = str(j).partition("-")[0]
            try:
                print(__bot)
                xxx = pd.DataFrame(__var.loc[i][j])
                xxx["label"] = j
                xxx["pht"]  = xxx.index
                xxx.pht  = xxx.pht.apply(lambda x: x.tz_localize("UTC").tz_convert("Asia/Manila").tz_localize(None))
                ccc = list(xxx.columns)
                ccc = [ccc[-1]] + ccc[:-1]
                xxx = xxx[ccc]
                kkk = kkk.append(xxx)
                kkk.to_excel(writer, sheet_name = f"{__bot}")
            except:
                print(f"skipping positions {i} {j}")

conc = pd.DataFrame(__var.loc["position mismatch"])
conc['order mismatch'] = 'Work in Progress'
# conc.to_csv("recent_mismatch-ftx.csv")

__var = pd.DataFrame(orders_data)
for i in __var.index[:-1]:
    kkk = pd.DataFrame()
    with pd.ExcelWriter(f"recent_{i}_orders-ftx.xlsx") as writer:         
        for j in __var.columns:
            try:
                if __bot != str(j).partition("-")[0]:
                    kkk = pd.DataFrame()
            except:
                print(j)                
            __bot = str(j).partition("-")[0]
            try:
                print(__bot)
                xxx = pd.DataFrame(__var.loc[i][j])
                xxx["label"] = j
                xxx["pht"]  = xxx.index
                xxx.pht  = xxx.pht.apply(lambda x: x.tz_localize("UTC").tz_convert("Asia/Manila").tz_localize(None))
                ccc = list(xxx.columns)
                ccc = [ccc[-1]] + ccc[:-1]
                xxx = xxx[ccc]
                kkk = kkk.append(xxx)
                kkk.to_excel(writer, sheet_name = f"{__bot}")
            except:
                print(f"skipping orders {i} {j}")

positionOrders = pd.concat([conc,pd.DataFrame(__var.loc["order mismatch"])], axis = 1)
positionOrders.to_csv("recent_mismatch.csv")

positionOrders.to_csv("recent_mismatch-ftx.csv")