
import pandas as pd
import datetime as dt
import numpy as np

import ccxt

import os

logs = open('logs.txt', 'a+')
today = str(dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
logs.write('+ + + + + + + + + '+today+' + + + + + + + + \n')
logs.write('Accessing keys... \n')

# READ-ONLY API KEYS
# trend_bmx = ccxt.bitmex({
#     'apiKey' : 'emcQ4_I3FEPcMEQkkwODciQ8',
#     'secret' : 'WpDI02dletOAAZRLoJzdcwDqK51h-bCxp29pYYZXR6q0Eapu',    #disabled 05.27.2021
#     'enableRateLimit': True
# })

trend_ftx = ccxt.ftx({
    'apiKey' : 'c0tID8vynAoXyovkwXrteNnSzScZ9Vqm6XQijU5m',
    'secret' : 'yNpwOwwO7xICcGsxKOxSd5CQXeT2RpnRrs8mBjlB',
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'TrendTraderStrategyProdTest'}
})

# stoch_bmx = ccxt.bitmex({
#     'apiKey' : 'p5jdfKuxpSY7fZRbKRj7cr7o',
#     'secret' : 'pJ9w2QiNQgsJqejWYCqx2yvVvYrfGedUmx6B99Xr2XOGveuf'
# })

nmm_bmx = ccxt.bitmex({
    'apiKey' : 'ZfDq4NtLthdwWDtBHIgqIVGd',
    'secret' : 'AsffkKkmhOdtaERVOjWVUVyQ9ztOy1fccv2w2n5ipkLz7_8X',
    'enableRateLimit': True
})

occ_bmx = ccxt.bitmex({
    'apiKey' : 'c389cvq0YlkPEQqTch8tluei',
    'secret' : '1sT--fc2u8V6wDB2-GL4lhSgTYDLdYlyz9WhcpwzM3-rEH8k',
    'enableRateLimit': True
})

dmi_bmx = ccxt.bitmex({
    'apiKey' : 'JkGBprSOD-zLkJcD1-Ln2WlD',
    'secret' : 'JJ6q97HaWMyqanA7haiJ6CQOd0TYJCKacyO8Uz2xEPFW_ZsI',
    'enableRateLimit': True
})

# added 10/28/2020
occ_ftx = ccxt.ftx({
    'apiKey' : 'BZX0w81jcgBw7FULtYB8VINBxYBew_BpqRdOq5oP',
    'secret' : '0TrEhIzEgvpWASTP9xPUlLwnuSxg_ljVyL5nFUV3',
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'OCC Prod'}
})

dmi_ftx = ccxt.ftx({
    'apiKey' : 'ScH71g4GK-zTVs8SNhV08oscxRCGZ5Ph59kYZTjy',
    'secret' : 'ajDbPHwR100rsT0_W9VFlJjWXHyZWHuNMh23aWsP',
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'DMI Prod'}
})

tdc_ftx = ccxt.ftx({
    'apiKey' : 'DqFBl0b9G89y-XGuOgMmnn8NGiTRXQ-5NlK_SHo0',
    'secret' : 'HI7LW3tlwgKT0GFX1XsPbRxCkd0BHeuTbykrp1FZ',
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'TDC Prod'}
})

# added 11/04/2020
# ich_bmx = ccxt.bitmex({
#     'apiKey' : 'M72VN9ZKrjElez6nSMmlO7LF',
#     'secret' : 'hH08GlwfhDBxEZeUOWITD0nbEbUDvJMF4a8zzcXcbNoPbg2r', #diabled 06.09.2021
#     'enableRateLimit': True
# })

# added 12/09/2020
# cha_bmx = ccxt.bitmex({
#     'apiKey' : 'YQwwyZqAk_BeL6sWGkQ00ULK',
#     'secret' : 'zLgks0k2fvQdvDaZE80eEaUQo7UR-VHEV7IDmjGYB2yX2blu', #removed 12.07.2021
#     'enableRateLimit': True
# })

vor_bmx = ccxt.bitmex({
    'apiKey' : 'i0pX7CvS9-wfWBAUD2kK_YC7',
    'secret' : '1Avz-Qnr3DNb_SZcpB1p0mh3Ke8UlM6jJEPY1XVdXyXidIew',
    'enableRateLimit': True
})

tdc_bmx = ccxt.bitmex({
    'apiKey' : 'HBtRgw3HJ45LOWJg2TFpuXpI',
    'secret' : 'TSeSvgKdtU1S8JlWX2S2cIf9SlbzlnMHS_NcBjFwpkmIGm9s',
    'enableRateLimit': True
})

# added 01/18/2021
cha_ftx = ccxt.ftx({
    'apiKey' : 'yQO_4iSdTtmAZ5LYeZZSmMQi0bzTAZVCFdPI1Qpt',
    'secret' : 'YtIAF_HgRdQSKJqSGI1aXvF0DRv9isn-e_4V04KW',
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'ChaosStrategyProdTest'}
})

# added 01/27/2021
# stoch_bmx_live = ccxt.bitmex({
#     'apiKey' : 'm2NUhkUEdu9iZqmFFeeNDtZD',
#     'secret' : 'HqgNYu0pPV17UYsPcKcAJ72NOXrR_2Scqliv1EV6WqK6Uaj7'
# })

# trend_bmx_live = ccxt.bitmex({                                        #disabled 05.27.2021
#     'apiKey' : 'qNZw1bB9vOiLv1TqYWbiaHqf',
#     'secret' : 'PcnbBOALFYhfI7OfvVoruTeQvxgtYbZLZ09pGd5b7WI87TvN',
#     'enableRateLimit': True
# })

# nmm_bmx_live = ccxt.bitmex({
#     'apiKey' : 'lxSgy3Bydsj9PKHm8b8jy79Z',
#     'secret' : '13PUvTeQhmcARhYHghY5eOMKP9xXjrXJYn6aCdf51Hwj_Vn3',    #disabled 05.27.2021
#     'enableRateLimit': True
# })

# added 02/15/2021
# ich_bmx_live = ccxt.bitmex({
#     'apiKey' : 'wUdO5XNf6lS--qgig9NTxU2X',
#     'secret' : 'jc3h_YbNw5_hBXzIMHvqZS5n2L0B0u73eJ1TGw1LoMS9TX-8', # disabled 06.09.2021
#     'enableRateLimit': True
# })

# added 04/29/2021
# vorv2_bmx = ccxt.bitmex({
#     'apiKey' : '4aUpwhAo3WN8Uiy8I6spIFHr',
#     'secret' : '-n6tlSV_25laIIfwlQZncAkaABp8mTub59gcbglviE2PwfaT', #disabled 11/17/2021
#     'enableRateLimit': True
# })

# added 05/03/2021
# dmiv2_bmx = ccxt.bitmex({
#     'apiKey' : 'iS_Lb4fFZIF4_sg1Flii50ma',
#     'secret' : 'Rfnf4PBw4yXskP1neGZC7rocoYlvYzzVXNeosp2XbF3H_vbQ',    # disabled 07.02.2021
#     'enableRateLimit': True
# })

# added 05/05/2021

# tdcv2_bmx = ccxt.bitmex({
#     'apiKey' : '5gUmklkTPAgVtuuuPlkKhOhA',
#     'secret' : 'kIRWz_Rhz24LAOtlyW4W8emvFZdp6JNihTjCRLfsHqb91KBR',    # disabled 03.30.2022 changed to payday strat
#     'enableRateLimit': True
# })

# added 05/17/2021

# ichlo_bmx = ccxt.bitmex({
#     'apiKey' : '7aFpL1utkZUULCkirgmgKRgY',
#     'secret' : 'Sd6z4jfxkCss7JZ0d8m1cHYLdt_Dy_sJ3JfCiytCVIaUjof3', #changed to FN3v2
#     'enableRateLimit': True
# })

# added 05/20/2021

# dmilo_bmx = ccxt.bitmex({
#     'apiKey' : 'pyD5b64FQzsnW6_HASaPFHZV',
#     'secret' : 'c7rzDLJI-MA_1xZjSSfes8t9hqu4WfP1u-S6jQCBul8XGIbS', #disabled 12/7/12
#     'enableRateLimit': True
# })

# added 5/27/2021

mm3_bmx_live = ccxt.bitmex({
    'apiKey' : 'gBzf3wLQ-VMtV02DfhdgW7vi',
    'secret' : 'cuXCXlwFEer4MUNtkFKYzAUEsTKvLE3uNHp2Prg7OPy5iJtC',
    'enableRateLimit': True
})

tt1_bmx_live = ccxt.bitmex({
    'apiKey' : 'LFFOpH_abc8c9orol9XZlLiN',
    'secret' : 'MtAZqS_fco475yySvFR9WzDFZSXYXfBKYvvC2v9YS-nWge3B',
    'enableRateLimit': True
})

tt3_bmx = ccxt.bitmex({
    'apiKey' : '13PDsSEncwiF7vet7u80jvLO',
    'secret' : 'bybFuFJA3_bcaT-CSe4-rnUjN9B8tGId2hnwA4l7Lbor06EN',
    'enableRateLimit': True
})

tt4_bmx = ccxt.bitmex({
    'apiKey' : 'rXXoixuTtvOX2gkWrKV0FPFe',
    'secret' : 'Ft14Kire2wLk4y_ax4McFWsVr19V8MYYHyaUS1hxoHbbIptD',
    'enableRateLimit': True
})

# added 06.10.2021

# fn1_bmx = ccxt.bitmex({
#     'apiKey' : 'Ptf8UNAbXB4WSOpytrPdUpk6',
#     'secret' : 'PVJR0RyYK-ojnmmBIKelb1S9r3WMQqskr3QpU62IfHloLBDS',    #transferred API to DMIv2
#     'enableRateLimit': True
# })

fn3_bmx = ccxt.bitmex({
    'apiKey' : '824CQ9KsWfN_Wr24MVwEJ2UB',
    'secret' : 'thfdovJIzHmrJXMOfpR4A60Yz0MrQtvUdqIuLEY-wonuh7KW',
    'enableRateLimit': True
})

# added 07.02.2021 -- TS
ts2_bmx = ccxt.bitmex({
    'apiKey' : '8yeDAl_Chh5cVWNMrxZN4DB6',
    'secret' : 'Ns9_jKl1Nji8O6HZq6S0HHvcf0AVg0aOUJUZpex1tbeqX9QK',
    'enableRateLimit': True
})

# added 11.12.2021 -- TS2v2 - Market Based
ts2v3_bmx = ccxt.bitmex({
    'apiKey' : '3ORhm2gzlDvwJM7Rbr0GOXjT',
    'secret' : 'fM2mFNlX7ZxC3UbvwEny0c2VgsoklQpLr_vEIZwAMW1WA_bL',
    'enableRateLimit': True
})

# added 11.17.2021 -- MM3v3
mm3v3_bmx = ccxt.bitmex({
    'apiKey' : 'dN43nXhwV8RJOV6SafFHs1nq',
    'secret' : 'C1GYh0u9wgI1KDnrLLxXD2dKxkUaE_43s4tOj2rrU4X55M1X',
    'enableRateLimit': True
})

# added 11/18/2021

# fn3v2_bmx = ccxt.bitmex({
#     'apiKey' : '7aFpL1utkZUULCkirgmgKRgY',
#     'secret' : 'Sd6z4jfxkCss7JZ0d8m1cHYLdt_Dy_sJ3JfCiytCVIaUjof3', # changed to time based
#     'enableRateLimit': True
# })

#added 11/25/2021 - Time Based

ts2v2_bmx = ccxt.bitmex({
    # 'apiKey' : 'mLNgGi3ESxiiC7sd6k8aJl6W',
    # 'secret' : 'TGgWyFoPc4rtnETRJgXGOXpayWvY2dTWjE1SMxhLu_1gROAr', #changed 12/7/2021 due to change in account
    'apiKey' : 'tcWenKl_X4RCuNvDKkIjPT7W',
    'secret' : 'Cp8v3XErWqo9Y7oEffZ2sZF-2VjH9D1MgboZxgbnS3qOK-gh',
    'enableRateLimit': True
})

#added 12/7/2021 - Time Based

mm3v2_bmx = ccxt.bitmex({
    'apiKey' : '3TWjvTDI-0ao2rkkmIvIfi0E',
    'secret' : 'izA3S3PFL27DDGQ5R2FAvISjHnC_2a-YAqAACO-a2N1X3TsP',
    'enableRateLimit': True
})

#added 12/7/12 - Time Based

tt4v2_bmx = ccxt.bitmex({
    'apiKey' : 'e9yi0aDMBwf5bfdheWJo7Hpo',
    'secret' : 'Trr1Hd3c5PG-gZl_UTKdcYSPLwQvO3mOfmRhXTFfP_DTwnAV',
    'enableRateLimit': True
})

#added 12/7/12 - Time Based

tt1v2_bmx = ccxt.bitmex({
    'apiKey' : 'lYiBu_RK3pddBdrsiAXPbKnv',
    'secret' : 'r3cZTZw3it3TbjaX3Qjk1urtrqp_InFt3URTfwitb3lDzF3c',
    'enableRateLimit': True
})

# added 12/7/2021 - Time Based

fn3v2r_bmx = ccxt.bitmex({
    'apiKey' : '7aFpL1utkZUULCkirgmgKRgY',
    'secret' : 'Sd6z4jfxkCss7JZ0d8m1cHYLdt_Dy_sJ3JfCiytCVIaUjof3', #from FN3v2
    'enableRateLimit': True
})

# added 02/14/2022 - Mixed Execution

dmi2_bmx = ccxt.bitmex({
    'apiKey' : 'Ptf8UNAbXB4WSOpytrPdUpk6',
    'secret' : 'PVJR0RyYK-ojnmmBIKelb1S9r3WMQqskr3QpU62IfHloLBDS',
    'enableRateLimit': True
})

# added 03/30/2022

payday_bmx = ccxt.bitmex({
    'apiKey' : '5gUmklkTPAgVtuuuPlkKhOhA',
    'secret' : 'kIRWz_Rhz24LAOtlyW4W8emvFZdp6JNihTjCRLfsHqb91KBR',
    'enableRateLimit': True
})

# DEV BOTS

tt_dev_ftx = ccxt.ftx({
    'apiKey' : "W0IDwo_R9BsKRi367VWuYpnn3pT_-tj3hDb3puZn",
    'secret' : "RAEOZ0IqGhFgZOb6hi4jXPS0e8N23R6OCwcf_0R9",
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'TrendTraderStrategy'},
    'rateLimit' : 1000
})

ch_dev_ftx = ccxt.ftx({
    'apiKey' : "vWFAuXyfMgefYcq6CGCPVPcpkDCSq7h8DGSmmIif",
    'secret' : "xqVy-PZdxeAOfpr9fdzmYAQd51P7k6miuREHSp5x",
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'ChaosStrategy'},
    'rateLimit' : 1000
})

oc_dev_ftx = ccxt.ftx({
    'apiKey' : "SEsEV6hNnJJI0VRu24ZTcfv2AbDCWVsEXV_umBx5",
    'secret' : "HMdeXLsU9N4mm5PT0brmtv_Z-TbBRj30VQmqpNQ5",
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'OCC Strategy Dev Test'},
    'rateLimit' : 1000
})

dm_dev_ftx = ccxt.ftx({
    'apiKey' : "Izo3lhaIPxU-hnWw1ujcmoMBUwZpVe91udrfMP-I",
    'secret' : "vJyWw5aHsZQFvFcWh1i2LZn7_wjcgQQWQJw4dQBB",
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'DMI Strategy Dev Test'},
    'rateLimit' : 1000
})

td_dev_ftx = ccxt.ftx({
    'apiKey' : "ByzE7MA90Fx7xWVq-3M0Uc84lMPHXL8LALjg7JhH",
    'secret' : "Tr5vcE7Wc4nugQQhuwL7f6fsRvfTB3k8ozz0vI2z",
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'TDC Strategy Dev Test'},
    'rateLimit' : 1000
})

# added 05/26/2022
tt5v3_ftx = ccxt.ftx({
    'apiKey' : "G49Q6F0bN4tYVX1K8-l7eQ0yZdiIWyQ4lvUUZr1j",
    'secret' : "oJiH3D65I7h0pUq7D8afClmvopRESFu0lvawMxf6",
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'TT5v3Live'},
    'rateLimit' : 1000
})

# added 05/26/2022
tt6v3_ftx = ccxt.ftx({
    'apiKey' : "sk4k3pHmnUcLoZDr8NtPSB8q73gTAuBBSGzpYdRu",
    'secret' : "T5ze8g2uu0773G39rFBOOcp1eaZh3HF4tuUPK1zo",
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'TT6v3Live'},
    'rateLimit' : 1000
})

# added 05/26/2022
tt7v3_ftx = ccxt.ftx({
    'apiKey' : "sarUBmIUXmaIdprTDHmMTwOGck2sgFC0SFUTbsFS",
    'secret' : "IXcIVbeQIeiaWJtSHccnz9T1ZUCJG5QgqP_ewRnY",
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'TT7v3Live'},
    'rateLimit' : 1000
})

# added 05/26/2022
tt8v3_ftx = ccxt.ftx({
    'apiKey' : "8x2adH88dtQxeoeoCSpphKK1lFiMArW_klI6_Wo3",
    'secret' : "F1ckxLR3etEiVDi-z_TRyFCJMCTnci4XwkyJsm4j",
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'TT8v3Live'},
    'rateLimit' : 1000
})

# added 05/26/2022
stv3_ftx = ccxt.ftx({
    'apiKey' : "7WFN7WHJI9Oqc1Kwm_dHOxCXwTGs7oV5FWdbyvQz",
    'secret' : "sY2ZGH-IcG2IoAVfAxTXoan7snNN8fgJQOqDxtOX",
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'Session Trading Strategy Prod'},
    'rateLimit' : 1000
})

# added 05/26/2022
ts2v3_ftx = ccxt.ftx({
    'apiKey' : "5GK2wySpNH1OQF_6efpKSSfvsk5TlS5db9nhj_VB",
    'secret' : "QgBtc0QiMxQUJFZuDdTt2ms_gzkYA4VaimpUTkyk",
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'TS2V3USD'},
    'rateLimit' : 1000
})

# added 06/17/2022
flock_ftx = ccxt.ftx({
    'apiKey' : "mW_NINLlh7nuxxNvqMjSzzxjmx5J7KclpWq7HEOs",
    'secret' : "NKCtIjCdHCHh-VFzkp9aDq99NSk3-WQa1hNd3rcD",
    'enableRateLimit': True,
    'headers' : {'FTX-SUBACCOUNT': 'FlockLS'},
    'rateLimit' : 1000
})




other_day = str(dt.date.today() - dt.timedelta(days=2))
monitoring = pd.read_csv('botmonitoring'+other_day+'.csv')
monitoring_df = pd.DataFrame(monitoring)
monitoring_df = monitoring_df.drop('Unnamed: 0', 1)

yesterday = str(dt.date.today() - dt.timedelta(days=1))

if (
    (yesterday != monitoring_df.date.iloc[-1]) and 
    (os.path.exists(os.path.join('botmonitoring'+yesterday+'.csv')) == False)
):

    # FTX
    logs.write('Accessing FTX... \n')
    binance = ccxt.binance()
    btc_ticker = binance.fetch_ticker('BTC/USDT')
    btc_close = float(btc_ticker['close'])
    trend_ftx_yest = trend_ftx.fetch_balance()
    occ_ftx_yest = occ_ftx.fetch_balance()
    dmi_ftx_yest = dmi_ftx.fetch_balance()
    tdc_ftx_yest = tdc_ftx.fetch_balance()
    cha_ftx_yest = cha_ftx.fetch_balance()
    tt5v3_ftx_yest = tt5v3_ftx.fetch_balance()
    tt6v3_ftx_yest = tt6v3_ftx.fetch_balance()
    tt7v3_ftx_yest = tt7v3_ftx.fetch_balance()
    tt8v3_ftx_yest = tt8v3_ftx.fetch_balance()
    stv3_ftx_yest = stv3_ftx.fetch_balance()
    ts2v3_ftx_yest = ts2v3_ftx.fetch_balance()
    flock_ftx_yest = flock_ftx.fetch_balance()
    
    # DEV BOTS
    tt_dev_ftx_yest = tt_dev_ftx.fetch_balance()
    ch_dev_ftx_yest = ch_dev_ftx.fetch_balance()
    oc_dev_ftx_yest = oc_dev_ftx.fetch_balance()
    dm_dev_ftx_yest = dm_dev_ftx.fetch_balance()
    td_dev_ftx_yest = td_dev_ftx.fetch_balance()

    logs.write('Computing BTC Equivalent of FTX USD balance value... \n')
    
    trend_ftx_yest_btc = (
            (float(trend_ftx_yest['total']['USD'] / btc_close)) + 
            float(trend_ftx_yest['total']['BTC']))
    occ_ftx_yest_btc = (
            (float(occ_ftx_yest['total']['USD'] / btc_close)) + 
            float(occ_ftx_yest['total']['BTC']))
    try:
        dmi_ftx_yest_btc = (
            (float(dmi_ftx_yest['total']['USD'] / btc_close)) + 
            float(dmi_ftx_yest['total']['BTC']))
    except:
        dmi_ftx_yest_btc = float(dmi_ftx_yest['total']['USD']) / btc_close
    
    tdc_ftx_yest_btc = (
            (float(tdc_ftx_yest['total']['USD'] / btc_close)) + 
            float(tdc_ftx_yest['total']['BTC']))
    try:
        cha_ftx_yest_btc = (
            (float(cha_ftx_yest['total']['USD'] / btc_close)) + 
            float(cha_ftx_yest['total']['BTC']))
    except:
        cha_ftx_yest_btc = float(cha_ftx_yest['total']['USD']) / btc_close
    
    if (('USD' in tt5v3_ftx_yest['total'].keys()) and
        ('BTC' in tt5v3_ftx_yest['total'].keys())):
        tt5v3_ftx_yest_btc = (
                (float(tt5v3_ftx_yest['total']['USD'])) + 
                (float(tt5v3_ftx_yest['total']['BTC'] * btc_close)))
    elif 'USD' not in tt5v3_ftx_yest['total'].keys():
        tt5v3_ftx_yest_btc = float(tt5v3_ftx_yest['total']['BTC'] * btc_close)
    elif 'BTC' not in tt5v3_ftx_yest['total'].keys():
        tt5v3_ftx_yest_btc = float(tt5v3_ftx_yest['total']['USD'])
    
    if (('USD' in tt6v3_ftx_yest['total'].keys()) and
        ('BTC' in tt6v3_ftx_yest['total'].keys())):
        tt6v3_ftx_yest_btc = (
                (float(tt6v3_ftx_yest['total']['USD'])) + 
                (float(tt6v3_ftx_yest['total']['BTC'] * btc_close)))
    elif 'USD' not in tt6v3_ftx_yest['total'].keys():
        tt6v3_ftx_yest_btc = float(tt6v3_ftx_yest['total']['BTC'] * btc_close)
    elif 'BTC' not in tt6v3_ftx_yest['total'].keys():
        tt6v3_ftx_yest_btc = float(tt6v3_ftx_yest['total']['USD'])
    
    tt7v3_ftx_yest_btc = (
            (float(tt7v3_ftx_yest['total']['USD'] / btc_close)) + 
            float(tt7v3_ftx_yest['total']['BTC']))
    
    if (('USD' in tt8v3_ftx_yest['total'].keys()) and
        ('BTC' in tt8v3_ftx_yest['total'].keys())):
        tt8v3_ftx_yest_btc = (
                (float(tt8v3_ftx_yest['total']['USD'] / btc_close)) + 
                float(tt8v3_ftx_yest['total']['BTC']))
    elif 'USD' not in tt8v3_ftx_yest['total'].keys():
        tt8v3_ftx_yest_btc = float(tt8v3_ftx_yest['total']['BTC'])
    elif 'BTC' not in tt8v3_ftx_yest['total'].keys():
        tt8v3_ftx_yest_btc = float(tt8v3_ftx_yest['total']['USD']) / btc_close
    
    if (('USD' in stv3_ftx_yest['total'].keys()) and
        ('BTC' in stv3_ftx_yest['total'].keys())):
        stv3_ftx_yest_btc = (
                (float(stv3_ftx_yest['total']['USD'])) + 
                (float(stv3_ftx_yest['total']['BTC'] * btc_close)))
    elif (('USD' not in stv3_ftx_yest['total'].keys()) and
        ('BTC' in stv3_ftx_yest['total'].keys())):
        stv3_ftx_yest_btc = float(stv3_ftx_yest['total']['BTC'] * btc_close)
    elif (('BTC' not in stv3_ftx_yest['total'].keys()) and 
        ('USD' in stv3_ftx_yest['total'].keys())):
        stv3_ftx_yest_btc = float(stv3_ftx_yest['total']['USD'])
    else:
        stv3_ftx_yest_btc = 0
    
    if (('USD' in ts2v3_ftx_yest['total'].keys()) and
        ('BTC' in ts2v3_ftx_yest['total'].keys())):
        ts2v3_ftx_yest_btc = (
                (float(ts2v3_ftx_yest['total']['USD'])) + 
                (float(ts2v3_ftx_yest['total']['BTC'] * btc_close)))
    elif (('USD' not in ts2v3_ftx_yest['total'].keys()) and
        ('BTC' in ts2v3_ftx_yest['total'].keys())):
        ts2v3_ftx_yest_btc = float(ts2v3_ftx_yest['total']['BTC'] * btc_close)
    elif (('BTC' not in ts2v3_ftx_yest['total'].keys()) and 
        ('USD' in ts2v3_ftx_yest['total'].keys())):
        ts2v3_ftx_yest_btc = float(ts2v3_ftx_yest['total']['USD'])
    else:
        ts2v3_ftx_yest_btc = 0

    #DEV BOTS
    tt_dev_ftx_yest_btc = (
            (float(tt_dev_ftx_yest['total']['USD'] / btc_close)) + 
            float(tt_dev_ftx_yest['total']['BTC']))
    ch_dev_ftx_yest_btc = (
            (float(ch_dev_ftx_yest['total']['USD'] / btc_close)) + 
            float(ch_dev_ftx_yest['total']['BTC']))
    oc_dev_ftx_yest_btc = (
            (float(oc_dev_ftx_yest['total']['USD'] / btc_close)) + 
            float(oc_dev_ftx_yest['total']['BTC']))
    dm_dev_ftx_yest_btc = (
            (float(dm_dev_ftx_yest['total']['USD'] / btc_close)) + 
            float(dm_dev_ftx_yest['total']['BTC']))
    td_dev_ftx_yest_btc = (
            (float(td_dev_ftx_yest['total']['USD'] / btc_close)) + 
            float(td_dev_ftx_yest['total']['BTC'])) 
    
    # BITMEX
    logs.write('Accessing BitMEx... \n')
    # trend_bmx_yest = trend_bmx.fetch_balance()['total']['BTC'] #disabled 05.27.2021
    # stoch_bmx_yest = stoch_bmx.fetch_balance()['total']['BTC']
    nmm_bmx_yest   = nmm_bmx.fetch_balance()['total']['BTC']
    occ_bmx_yest   = occ_bmx.fetch_balance()['total']['BTC']
    dmi_bmx_yest   = dmi_bmx.fetch_balance()['total']['BTC']
    # ich_bmx_yest   = ich_bmx.fetch_balance()['total']['BTC'] #disabled 06.10.2021
    # cha_bmx_yest   = cha_bmx.fetch_balance()['total']['BTC'] #removed 12.07.2021
    vor_bmx_yest   = vor_bmx.fetch_balance()['total']['BTC']
    tdc_bmx_yest   = tdc_bmx.fetch_balance()['total']['BTC']
    # trend_bmx_live_yest = trend_bmx_live.fetch_balance()['total']['BTC'] disabled 05.27.2021
    # stoch_bmx_live_yest = stoch_bmx_live.fetch_balance()['total']['BTC']
    # nmm_bmx_live_yest   = nmm_bmx_live.fetch_balance()['total']['BTC'] disabled 05.27.2021
    # ich_bmx_live_yest = ich_bmx_live.fetch_balance()['total']['BTC'] #disabled 06.10.2021
    # vorv2_bmx_yest = vorv2_bmx.fetch_balance()['total']['BTC'] #disabled 11.17.2021
    # dmiv2_bmx_yest = dmiv2_bmx.fetch_balance()['total']['BTC']    #disabled 07.02.2021
    # tdcv2_bmx_yest = tdcv2_bmx.fetch_balance()['total']['BTC'] # disabled 03.30.2022
    # ichlo_bmx_yest = ichlo_bmx.fetch_balance()['total']['BTC'] #changed to FN3v2
    # dmilo_bmx_yest = dmilo_bmx.fetch_balance()['total']['BTC'] disabled 12.07.2021
    mm3_bmx_live_yest = mm3_bmx_live.fetch_balance()['total']['BTC']
    tt1_bmx_live_yest = tt1_bmx_live.fetch_balance()['total']['BTC']
    tt3_bmx_yest = tt3_bmx.fetch_balance()['total']['BTC']
    tt4_bmx_yest = tt4_bmx.fetch_balance()['total']['BTC']
    # fn1_bmx_yest = fn1_bmx.fetch_balance()['total']['BTC'] changed to DMI2 02.14.2022
    fn3_bmx_yest = fn3_bmx.fetch_balance()['total']['BTC']
    ts2_bmx_yest = ts2_bmx.fetch_balance()['total']['BTC']
    ts2v2_bmx_yest = ts2v2_bmx.fetch_balance()['total']['BTC']
    mm3v3_bmx_yest = mm3v3_bmx.fetch_balance()['total']['BTC']
    # fn3v2_bmx_yest = fn3v2_bmx.fetch_balance()['total']['BTC'] changed to FN3v2rev
    ts2v3_bmx_yest = ts2v3_bmx.fetch_balance()['total']['BTC']
    mm3v2_bmx_yest = mm3v2_bmx.fetch_balance()['total']['BTC']
    tt4v2_bmx_yest = tt4v2_bmx.fetch_balance()['total']['BTC']
    tt1v2_bmx_yest = tt1v2_bmx.fetch_balance()['total']['BTC']
    fn3v2r_bmx_yest = fn3v2r_bmx.fetch_balance()['total']['BTC']
    dmi2_bmx_yest = dmi2_bmx.fetch_balance()['total']['BTC']
    payday_bmx_yest = payday_bmx.fetch_balance()['total']['BTC']

    logs.write('Preparing Dataframe \n')

    monitoring_df = monitoring_df.append({'date' : yesterday, 
                    'TTR-FTX': trend_ftx_yest_btc, 'TTR-BMEx': np.nan,
                    'STM-BMEx': np.nan, 'NMM-BMEx': nmm_bmx_yest,
                    'OCC-BMEx' : np.nan, 'DMI-BMEx' : dmi_bmx_yest,
                    'OCC-FTX' : occ_ftx_yest_btc, 'DMI-FTX' : dmi_ftx_yest_btc,
                    'TDC-FTX' : tdc_ftx_yest_btc, 'ICH-BMEx': np.nan,
                    'CHA-BMEx' : np.nan, 'VOR-BMEx': vor_bmx_yest,
                    'TDC-BMEx' : tdc_bmx_yest, 'CHA-FTX': cha_ftx_yest_btc,
                    'VOR-V2-BMEx' : np.nan, 'DMI-V2-BMEx' : np.nan,
                    'TDC-V2-BMEx' : np.nan, 'ICH-LO-BMEx' : np.nan,
                    'DMI-LO-BMEx' : np.nan, 'TT3-BMEx' : tt3_bmx_yest,
                    'TT4-BMEx' : tt4_bmx_yest, 'FN1-BMEx' : np.nan,
                    'TS2-BMEx': ts2_bmx_yest, 'TS2v3-BMEx': ts2v3_bmx_yest,
                    'MM3v3-BMEx': mm3v3_bmx_yest, 'FN3v2-BMEx': np.nan,
                    'TS2v2-BMEx': ts2v2_bmx_yest, 'MM3v2-BMEx': mm3v2_bmx_yest,
                    'TT1v2-BMEx': tt1v2_bmx_yest, 'TT4v2-BMEx': tt4v2_bmx_yest,
                    'FN3v2rev-BMEx': fn3v2r_bmx_yest, 'DMIv2-BMEx': dmi2_bmx_yest,
                    'PAYDAY-BMEx': payday_bmx_yest, 
                    'TT5v3-USD-FTX': tt5v3_ftx_yest_btc, 'TT6v3-USD-FTX': tt6v3_ftx_yest_btc,
                    'TT7v3-FTX': tt7v3_ftx_yest_btc, 'TT8v3-FTX': tt8v3_ftx_yest_btc,
                    'STv3-USD-FTX': stv3_ftx_yest_btc, 'TS2v3-USD-FTX': ts2v3_ftx_yest_btc,
                    'TTR-BMEx-Live' : np.nan, 'STM-BMEx-Live' : np.nan,
                    'NMM-BMEx-Live' : np.nan, 'ICH-BMEx-Live' : np.nan,
                    'TT1-BMEx-Live' : tt1_bmx_live_yest, 'MM3-BMEx-Live' : mm3_bmx_live_yest,
                    'FN3-BMEx-Live' : fn3_bmx_yest,

                    'Chaos_FTX_DEV': ch_dev_ftx_yest_btc,
                    'TTR_FTX_DEV': tt_dev_ftx_yest_btc,
                    'TDC_FTX_DEV': td_dev_ftx_yest_btc,
                    'OCC_FTX_DEV': oc_dev_ftx_yest_btc,
                    'DMI_FTX_DEV': dm_dev_ftx_yest_btc 
                    }, ignore_index=True)
    logs.write('Downloading to CSV.... \n')
    monitoring_df.to_csv('botmonitoring'+yesterday+'.csv')
    logs.write('CSV Downloaded! \n')

else:
    logs.write('botmonitoring'+yesterday+'.csv already exists! >_< \n')
    logs.write(' ')

logs.close()
